<?php

namespace app\commands;

use yii;
use app\models\CronReports;

class CronController extends AbstractController
{
    protected $reporter;
    
    public function beforeAction($action)
    {
//  Create reporter
        $this->reporter = new CronReports();
        
        return parent::beforeAction($action);
    }
    
    public function afterAction($action, $result)
    {
//  Complete report
        if (!empty($this->reporter->id) && empty($this->reporter->end_timestamp)) {
            $this->reporter->completeReport($this->stat);
        }
        
        return parent::afterAction($action, $result);
    }
}