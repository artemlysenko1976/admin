<?php

namespace app\commands;

use yii;
use yii\console\Controller;
use app\models\Languages;
use yii\helpers\Console;

class AbstractController extends Controller
{
    protected $displayed_lines = [];
    
    protected $collect_displayed_lines = false;
    
    protected $start_time = 0;
    
    protected $stat = [];
    
    const DISPLAY_NORMAL = 'normal';
    const DISPLAY_ERROR = 'error';
    const DISPLAY_SUCCESS = 'success';
    const DISPLAY_WARNING = 'warning';
    const DISPLAY_SEPARATOR = 'separator';
    const DISPLAY_START = 'start';
    const DISPLAY_END = 'end';
    
    public function beforeAction($action)
    {
        $this->start_time = microtime(true);
        
//  Get languages
        Yii::$app->params['langs'] = Languages::find()
            ->where(['deleted' => 0])
            ->orderBy("basic DESC, name ASC")
            ->all();
        
//  Set current language
        Yii::$app->params['lang'] = Yii::$app->params['langs'][0];
        Yii::$app->language = Yii::$app->params['lang']->code;
        
        $this->start_time = microtime(true);
        
        return parent::beforeAction($action);
    }
    
    public function afterAction($action, $result)
    {
        $end_time = microtime(true);
        $time = round($end_time - $this->start_time, 5);
        
        if (!empty($this->stat)) {
            $this->displayLine('', self::DISPLAY_SEPARATOR);
            foreach ($this->stat as $k => $v) {
                $this->displayLine($k . ': ' . round($v));
            }
        }
        
        $this->displayLine('----------------------------------------------------', 'end');
        $this->displayLine($time, 'end');
        $this->displayLine('END', 'end');
        $this->displayLine('----------------------------------------------------', 'end');
        $this->displayLine();
        
        return parent::afterAction($action, $result);
    }
    
    /**
     * Displays line with new line delimiter and collects all displayed lines
     * @param string $string
     * @param string $type output type: 'normal', 'error', 'success', 'warning', 'start', 'end'
     */
    public function displayLine($string='', $type=self::DISPLAY_NORMAL)
    {
        switch ($type) {
            case 'error':
                $mode = Console::FG_RED;
                break;
            case 'success':
                $mode = Console::FG_CYAN;
                break;
            case 'warning':
                $mode = Console::FG_YELLOW;
                break;
            case 'start':
            case 'end':
                $mode = Console::FG_GREY;
                break;
            default:
                $mode = null;
        }
        
        if ($type == 'start') {
            $this->stdout('' . "\n");
            $this->stdout('====================================================' . "\n", $mode);
            $this->stdout($string . "\n", $mode);
            $this->stdout('====================================================' . "\n", $mode);
        } elseif ($type == 'separator') { 
            $this->stdout('----------------------------------------------------' . "\n");
        } else {
            $this->stdout($string . "\n", $mode);
        }
        
        $this->displayed_lines[] = $string;
    }
    
    public function setCollectDisplayedLines($value=true)
    {
        $this->collect_displayed_lines = boolval($value);
    }
    
    /**
     * Returns collected displayed lines
     * @param boolean $as_string if TRUE, implodes all lines to string
     * @return boolean|string|array
     */
    public function getDisplayedLines($as_string=true)
    {
        if ($this->collect_displayed_lines == false) {
            return false;
        }
        
        if ($as_string === true) {
            if (!empty($this->displayed_lines)) {
                return implode("\n", $this->displayed_lines);
            } else {
                return '';
            }
        } else {
            if (!empty($this->displayed_lines)) {
                return $this->displayed_lines;
            } else {
                return [];
            }
        }
    }
}