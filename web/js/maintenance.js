function rotate(element, degree, direction)
{
    element.css('transform', 'rotate(' + degree + 'deg)');  
    element.css('WebkitTransform', 'rotate(' + degree + 'deg)');  
    element.css('-moz-transform', 'rotate(' + degree + 'deg)');  
    setTimeout(function() {
            if (direction == 'cw') {
                degree++;
            } else if (direction == 'ccw') {
                degree--;
            }
            rotate(element, degree, direction);
        },
        10
    );
}

$(function() {
    rotate($(".cog_box_1"), 0, 'cw');
    rotate($(".cog_box_2"), 0, 'ccw');
    rotate($(".cog_box_3"), 0, 'cw');
});