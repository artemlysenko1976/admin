/**
 * Server
 * @returns {Client}
 */
function Chat(manager_id)
{
    this.manager_id = manager_id;
    
    this.getList(true);
}

Chat.prototype = {
    constructor: Chat,
    
    manager_id: null,
    active_chat_id: null,
    
    getList: function(load_active_chat) {
        var chat = this;
        
        $.ajax({
            url: '/chat/list',
            async: true,
            method: 'get',
            dataType: 'json',
            success: function(data){
                $('.chat_list_loading').hide();
                if (data.length == 0) {
//  Empty list
                    $('.chat_list').hide();
                    $('.chat_list_empty').show();
                } else {
//  Display list
                    for (var i in data) {
                        var id = 'list_chat_' + data[i]['id'];
                        var element = $('.chat_list').find('#' + id);
                        
                        if (element.length === 0) {
//  Create new element
                            var class_name = (data[i]['active'] == true ? 'active' : '');
                            
                            var html = '<li id="' + id + '" class="list-group-item chat_list_item ' + class_name + '" onclick="chat.openChat(' + data[i]['investor_id'] + ')">';
                                html += '<img src="' + data[i]['photo'] + '" alt="">';
                                html += data[i]['last_name'] + ' ' + data[i]['first_name'];
                                html += '<span class="badge" style="' + (data[i]['new_messages'] == 0 ? 'display:none;' : '') + '">' + data[i]['new_messages'] + '</span>';
                            html += '</li>';

                            $('.chat_list').append(html);
                        } else {
//  Update existing element
                            element.find('.badge').html(data[i]['new_messages']);
                            if (data[i]['new_messages'] > 0) {
                                element.find('.badge').show();
                            } else {
                                element.find('.badge').hide();
                            }
                        }
                        
//  Load content of active chat
                        if (load_active_chat == true && data[i]['active'] == true) {
                            chat.openChat(data[i]['investor_id']);
                        }
                    }
                    
                    $('.chat_list_empty').hide();
                    $('.chat_list').show();
                }
            }
        });
    },
    
    openChat: function(investor_id) {
        var chat = this;
        
        $.ajax({
            url: '/chat/open/' + investor_id,
            async: true,
            method: 'get',
            dataType: 'json',
            success: function(data){
                if (data['chat']) {
                    $('#investor_search').val('');
                    
//  List item
                    var list_id = 'list_chat_' + data['chat']['id'];
                    var list_element = $('.chat_list').find('#' + list_id);
                    if (list_element.length == 0) {
                        var list_html = '<li id="' + list_id + '" class="list-group-item chat_list_item active" onclick="chat.openChat(' + data['chat']['investor_id'] + ')">';
                            list_html += '<img src="' + data['investor']['photo'] + '" alt="">';
                            list_html += data['investor']['last_name'] + ' ' + data['investor']['first_name'];
                            list_html += '<span class="badge" style="display:none;">0</span>';
                        list_html += '</li>';
                        
                        $('.chat_list').prepend(list_html);
                    }
                    
                    $('.chat_list').find('.chat_list_item').removeClass('active');
                    $('.chat_list').find('#' + list_id).addClass('active');
                    
                    $('.chat_list_empty').hide();
                    $('.chat_list').show();
                    
//  Chat details
                    var html = '<h3>';
                        html += '<img src="' + data['investor']['photo'] + '" alt="">';
                        html += '<a href="/investors/view/' + data['chat']['investor_id'] + '" target="_blank">';
                            html += data['investor']['last_name'] + ' ' + data['investor']['first_name'];
                        html += '</a>';
                        html += '<hr>';
                    html += '</h3>';
                    
                    $('.chat_header').html(html);
                    
//  Messages
                    $('.chat_messages').html('');
                    
                    if (data['messages'].length > 0) {
                        for (var i in data['messages']) {
                            $('.chat_messages').append(chat.getMessageHtml(data['messages'][i]));
                        }
                    }
                    
//  Scroll messages to end
                    $('.chat_messages').scrollTop($('.chat_messages')[0].scrollHeight);
                    
//  Chat form fields
                    $('#chat_message_form').find('#chat_id').val(data['chat']['id']);
                    $('.chat_form_box').show();
                    
//  Unmark new messages
                    chat.unmarkNewMessages();
                }
            }
        });
    },
    
    unmarkNewMessages: function() {
        setTimeout(
            function(){
                $('.chat_message_new').remove();
            },
            10000
        );
    },
    
//  Message CRUD ===============================================================
    sendMessage: function() {
        var form = $('#chat_message_form');
        if (form.length === 0) {
            return false;
        }
        
        var allow_sending = false;
        if (form.find('#chat_message').val() != '') {
            allow_sending = true;
        }
        if ($('#chat_files')[0].files.length > 0) {
            allow_sending = true;
        }
        
        if (allow_sending === false) {
            return false;
        }
        
        var request_data = new FormData(form[0]);
        
        $.ajax({
            url: '/chat/message',
            async: true,
            data: request_data,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            dataType: 'json',
            success: function(data){
                if (Object.keys(data['errors']).length > 0) {
                    displayErrors(data['errors'], form);
                } else {
                    form.find('#chat_message').val('');
                    form.find('#chat_files').val('');
                    
                    $('.chat_messages').append(chat.getMessageHtml(data['message']));
                    $('.chat_messages').scrollTop($('.chat_messages')[0].scrollHeight);
                }
            }
        });
    },
    
    editMessage: function() {
        var form = $('#edit_message_form');
        if (form.length === 0) {
            return false;
        }
        
        var allow_sending = false;
        if (form.find('#edit_message').val() != '') {
            allow_sending = true;
        }
        if (form.find('.edit_message_existed_files').length > 0) {
            allow_sending = true;
        }
        if (form.find('#edit_files')[0].files.length > 0) {
            allow_sending = true;
        }
        
        if (allow_sending === false) {
            return false;
        }
        
        var request_data = new FormData(form[0]);
        
        $.ajax({
            url: '/chat/editmessage',
            async: true,
            data: request_data,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            dataType: 'json',
            success: function(data){
                if (Object.keys(data['errors']).length > 0) {
                    displayErrors(data['errors'], form);
                } else {
                    var message_id = form.find('#edit_message_id').val();
                    
                    form.find('#edit_message_id').val('');
                    form.find('#edit_message').val('');
                    form.find('#edit_files').val('');
                    
                    var message_container = $('#chat_message_' + message_id);
                    message_container.find('.chat_message_content').html(data['message']['content']);

                    if (Object.keys(data['message']['files']).length > 0) {
                        var files_html = '';
                        for (var i in data['message']['files']) {
                            files_html += '<a href="' + data['message']['files'][i] + '" target="_blank">';
                                files_html += '<span class="fa fa-file"></span>';
                                files_html += i;
                            files_html += '</a>';
                        }
                        
                        message_container.find('.chat_message_files').html(files_html);
                        message_container.find('.chat_message_container').show();
                    } else {
                        message_container.find('.chat_message_files').html('');
                        message_container.find('.chat_message_container').hide();
                    }
                    
                    $('#edit_message_form_box').hide();
                    $('#edit_message_modal').find('.modal-footer').hide();
                    $('#edit_message_error').hide();
                    $('#edit_message_success').show();
                    
                    setTimeout(
                        function(){
                            $('#edit_message_modal').find('.close').trigger('click');
                        },
                        2000
                    );
                }
            }
        });
    },
    
    loadDeleteModal: function(message_id) {
        $('#delete_message_id').val(message_id);
        
        $('#delete_message_error').hide();
        $('#delete_message_success').hide();
        $('#delete_message_confirm').show();
        
        $('#delete_message_modal').find('.modal-footer').show();
    },
    
    deleteMessage: function(message_id) {
        $.ajax({
            url: '/chat/deletemessage/' + message_id,
            async: true,
            method: 'get',
            dataType: 'html',
            success: function(data){
                $('#delete_message_confirm').hide();
                if (data == 'ok') {
                    $('#chat_message_' + message_id).remove();
                    
                    $('#delete_message_modal').find('.modal-footer').hide();
                    $('#delete_message_error').hide();
                    $('#delete_message_success').show();
                    
                    setTimeout(
                        function(){
                            $('#delete_message_modal').find('.close').trigger('click');
                        },
                        2000
                    );
                } else {
                    $('#delete_message_success').hide();
                    $('#delete_message_error').show();
                }
            }
        });
    },
//  END Message CRUD ===========================================================
    
    loadMessage: function(message_id) {
        $('#edit_message_files').html('');
        
        $('#edit_message_error').hide();
        $('#edit_message_success').hide();
        $('#edit_message_form_box').hide();
        $('#edit_message_loading').show();
        
        var form = $('#edit_message_form');
        form.find('#edit_message_id').val(message_id);
        form.find('#edit_files').val('');
        form.find('#edit_message').val('');
        
        $.ajax({
            url: '/chat/loadmessage/' + message_id,
            async: true,
            method: 'get',
            dataType: 'json',
            success: function(data){
                if (data) {
                    form.find('#edit_message').val(data['content']);
                    
                    if (data['files']) {
                        var files_html = '';
                        var files_index = 0;
                        for (var file_name in data['files']) {
                            files_html += '<div id="edit_message_file_' + files_index + '">';
                                files_html += '<input type="hidden" name="files[' + file_name + ']" value="' + data['files'][file_name] + '" class="edit_message_existed_files">';
                                files_html += '<span class="fa fa-file"></span>';
                                files_html += '<span class="edit_message_file_name">' + file_name + '</span>';
                                files_html += '<a class="fa fa-remove" onclick="$(\'#edit_message_file_' + files_index + '\').remove();"></a>';
                            files_html += '</div>';
                            
                            files_index++;
                        }
                        
                        $('#edit_message_files').html(files_html);
                    }
                    
                    $('#edit_message_loading').hide();
                    $('#edit_message_form_box').show();
                    $('#edit_message_modal').find('.modal-footer').show();
                }
            }
        });
    },
    
    getMessageHtml: function(message) {
        var html = '';
        html += '<div id="chat_message_' + message['id'] + '" class="row chat_message_box ' + message['author'] + '_message">';
        
        if (message['author'] == 'investor') {
//  investor
            html += '<div class="col-md-9">';
                html += '<div class="x_panel">';
                    html += '<div class="row">';
                        html += '<div class="col-md-1 chat_avatar_box">';
                            html += '<img src="' + message['investor_photo'] + '" alt="" title="' + message['investor_last_name'] + ' ' + message['investor_first_name'] + '">';
                        html += '</div>';
                        html += '<div class="col-md-11 chat_message">';
                            html += '<p class="chat_message_time">';
                                html += message['time'];
                                if (message['new'] == true) {
                                    html += '<span class="glyphicon glyphicon-asterisk chat_message_new"></span>';
                                }
                            html += '</p>';
                            html += '<div class="chat_message_content">' + message['content'] + '</div>';
                            var files_display = (Object.keys(message['files']).length > 0 ? 'block' : 'none');
                            html += '<div class="chat_message_container" style="display:' + files_display + '">';
                                html += '<hr>';
                                html += '<div class="chat_message_files">';
                                if (Object.keys(message['files']).length > 0) {
                                    for (var i in message['files']) {
                                        html += '<a href="' + message['files'][i] + '" target="_blank">';
                                            html += '<span class="fa fa-file"></span>';
                                            html += i;
                                        html += '</a>';
                                    }
                                }
                                html += '</div>';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
            html += '<div class="col-md-3"></div>';
        } else if (message['author'] == 'manager') {
//  manager
            html += '<div class="col-md-3"></div>';
            html += '<div class="col-md-9">';
                html += '<div class="x_panel">';
                    html += '<div class="row">';
                        html += '<div class="col-md-11 chat_message">';
                            html += '<p class="chat_message_time">';
                                html += message['time'];
                                html += '<a class="glyphicon glyphicon-edit edit_icon" data-toggle="modal" data-target="#edit_message_modal" onclick="chat.loadMessage(' + message['id'] + ');"></a>';
                                html += '<a class="glyphicon glyphicon-remove edit_icon" data-toggle="modal" data-target="#delete_message_modal" onclick="chat.loadDeleteModal(' + message['id'] + ');"></a>';
                            html += '</p>';
                            html += '<div class="chat_message_content">' + message['content'] + '</div>';
                            var files_display = (Object.keys(message['files']).length > 0 ? 'block' : 'none');
                            html += '<div class="chat_message_container" style="display:' + files_display + '">';
                                html += '<hr>';
                                html += '<div class="chat_message_files">';
                                if (Object.keys(message['files']).length > 0) {
                                    for (var i in message['files']) {
                                        html += '<a href="' + message['files'][i] + '" target="_blank">';
                                            html += '<span class="fa fa-file"></span>';
                                            html += i;
                                        html += '</a>';
                                    }
                                }
                                html += '</div>';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="col-md-1 chat_avatar_box">';
                            html += '<img src="' + message['manager_photo'] + '" alt="" title="' + message['manager_last_name'] + ' ' + message['manager_first_name'] + '">';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        } else if (message['author'] == 'system') {
//  system
            html += '<div class="col-md-3"></div>';
            html += '<div class="col-md-9">';
                html += '<div class="x_panel">';
                    html += '<div class="row">';
                        html += '<div class="col-md-11 chat_message">';
                            html += '<p class="chat_message_time">';
                                html += message['time'];
                            html += '</p>';
                            html += '<div class="chat_message_content">' + message['content'] + '</div>';
                        html += '</div>';
                        html += '<div class="col-md-1 chat_avatar_box">';
                            html += '<img src="/images/logo_dark_bg.png" alt="" title="Системное сообщение">';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        }
        
        html += '</div>';
        
        return html;
    },
    
    getActiveChatNewMessages: function() {
        var chat = this;
        
        var chat_header = $('.chat_header').html();
        
        if (chat_header == '' || chat_header === undefined) {
            return;
        }
        
        var last_message_id = 0;
        
        var messages = $('.chat_message_box');
        
        if (messages.length > 0) {
            var last_message = $(messages[messages.length - 1]);
            last_message_id = last_message.attr('id');
            last_message_id = last_message_id.replace('chat_message_', '');
        }
        
        var request_data = {
            last_message_id: last_message_id,
        };
        
        $.ajax({
            url: '/chat/getnewmessages',
            async: true,
            data: request_data,
            method: 'get',
            dataType: 'json',
            success: function(data){
                if (data) {
                    if (data.length > 0) {
                        for (var i in data) {
                            $('.chat_messages').append(chat.getMessageHtml(data[i]));
                        }
                        
                        $('.chat_messages').scrollTop($('.chat_messages')[0].scrollHeight);
                        
                        chat.unmarkNewMessages();
                    }
                }
            }
        });
    },
    
    getActiveChatUpdatedMessages: function() {
        var chat = this;
        
        var chat_header = $('.chat_header').html();
        
        if (chat_header == '' || chat_header === undefined) {
            return;
        }
        
        $.ajax({
            url: '/chat/getupdatedmessages',
            async: true,
            method: 'get',
            dataType: 'json',
            success: function(data){
                if (data) {
                    if (data.length > 0) {
                        for (var i in data) {
                            var message_box_id = 'chat_message_' + data[i]['id'];
                            var container = $('#' + message_box_id);
                            if (container.length > 0) {
                                container.replaceWith(chat.getMessageHtml(data[i]));
                            }
                        }
                    }
                }
            }
        });
    },
}