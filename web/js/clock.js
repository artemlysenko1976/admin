/**
 * http://www.alessioatzeni.com/blog/css3-digital-clock-with-jquery
 */

$(document).ready(function() {
    setInterval( function() {
            var seconds = parseInt($("#sec").html());
            seconds++;
            if (seconds == 60) {
                seconds = 0;
                
                var minutes = parseInt($("#min").html());
                minutes++;
                if (minutes == 60) {
                    minutes = 0;
                    
                    var hours = parseInt($("#hours").html());
                    hours++;
                    if (hours == 24) {
                        hours = 0;
                    }
                    
                    $("#hours").html(( hours < 10 ? "0" : "" ) + hours);
                }
                
                $("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
            }
            $("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
        },
        1000
    );
});