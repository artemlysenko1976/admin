//  Validation =================================================================
var validation_urls = {
    users_add: 'users/validate',
    groups_add: 'groups/validate',
    auth_password: 'auth/validatepassword',
    email_add: 'email/validate',
    local_langsadd: 'local/langsvalidate',
    local_dictadd: 'local/dictvalidate',
    settings_add: 'settings/validate',
    ip_add: 'ip/validate',
    clients_add: 'clients/validate',
    countries_add: 'countries/validate',
    goods_goodsadd: 'goods/goodsvalidate',
    goods_catsadd: 'goods/catsvalidate',
    goods_paramsadd: 'goods/paramsvalidate',
    goods_brandsadd: 'goods/brandsvalidate',
    payments_paymentsadd: 'payments/paymentsvalidate',
    payments_methodsadd: 'payments/methodsvalidate',
    payments_currenciesadd: 'payments/currenciesvalidate',
    settings_varsadd: 'settings/varsvalidate',
};

var validation_callbacks = {
    
};

function validate(controller, action)
{
    var form = $('#record_form');
    if (form.length === 0) {
        alert('Form not found');
        
        return false;
    }
    
    form.find('#submit_button').attr('disabled', 'disabled');
    
    var validation_key = controller + '_' + action;
    var data = $('#record_form').serializeArray();
    var errors = [];
    
    if (validation_callbacks[validation_key] !== undefined) {
        errors = validation_callbacks[validation_key](data);
    } else if (validation_urls[validation_key] !== undefined) {
        errors = makeValidationRequest(data, validation_urls[validation_key]);
    }
    
    if (errors !== undefined && Object.keys(errors).length == 0) {
        form.on("submit", function(){});
        form.submit();
        
        return false;
    }
    
    displayErrors(errors, form);
    form.find('#submit_button').attr('disabled', false);
    
    return false;
}

function saveEntity(controller, action, return_action)
{
    var form = $('#record_form');
    if (form.length === 0) {
        alert('Form not found');
        
        return false;
    }
    
    form.find('#submit_button').attr('disabled', 'disabled');
    
    var validation_key = controller + '_' + action;
    var data = new FormData(form[0]);
    var errors = [];
    var response;
    
    if (validation_callbacks[validation_key] !== undefined) {
        response = validation_callbacks[validation_key](data);
    } else if (validation_urls[validation_key] !== undefined) {
        response = makeAjaxRequest(data, validation_urls[validation_key]);
    }
    errors = response['errors'];
    
    if (Object.keys(errors).length > 0) {
        displayErrors(response['errors'], form);
    } else if (response['error'] != '') {
        new PNotify({
            title: $('#dictionary_entries').find('#server_error').html(),
            text: response['error'],
            type: 'error',
            styling: 'bootstrap3'
        });
    } else {
        new PNotify({
            title: $('#dictionary_entries').find('#success').html(),
            text: $('#dictionary_entries').find('#success_operation').html(),
            type: 'success',
            styling: 'bootstrap3'
        });
        setTimeout(
            function(){
                window.location.href = '/' + controller + '/' + return_action;
            }, 
            3000
        );
    }
    
    form.find('#submit_button').attr('disabled', false);
    
    return false;
}

function displayErrors(errors, form)
{
    $('.form_error').remove();
    $('.general_alert').remove();
    form.find('.has-error').removeClass('has-error');
    
    if (errors === undefined) {
        return;
    }
    
    if (Object.keys(errors).length > 0) {
        var error_offsets = [];
        var min_offset;
        
        var general_alert = '<div class="alert alert-danger general_alert">Some errors are occurred</div>';
        form.find('#submit_button').parent().prepend(general_alert);
        
        for (var i in errors) {
            var closest = form.find('#' + i).closest('.form-group');
            
            if (closest.length > 0) {
                var html = '<div class="form_error">' + errors[i] + '</div>';
                closest.append(html);
                closest.addClass('has-error');
                error_offsets[i] = closest.offset().top;
            }
        }
        
        if (Object.keys(error_offsets).length > 0) {
            var min_offset;
            for (var i in error_offsets) {
                if (min_offset === undefined) {
                    min_offset = error_offsets[i];
                } else {
                    if (error_offsets[i] < min_offset) {
                        min_offset = error_offsets[i];
                    }
                }
            }
        }

        if (min_offset !== undefined) {
            var window_start = $(window).scrollTop();
            var window_end = window_start + $(window).height();

            if (min_offset < window_start || min_offset > window_end) {
                $('html, body').animate({
                    scrollTop: (min_offset - 10)
                });
            }
        }
    }
}

function displayErrorsInTabs(errors, tabs)
{
    $('#tabs').find('li').removeClass('error');
    
    if (Object.keys(errors).length > 0) {
        var jump_to_tab = true;
        
        for (var error_key in errors) {
            for (var tab_name in tabs) {
                for (var i in tabs[tab_name]) {
                    if (tabs[tab_name][i] == error_key) {
                        $('#tab_' + tab_name + '_label').addClass('error');
                        if (jump_to_tab === true) {
                            $('#tab_' + tab_name + '_label').find('a').trigger('click');
                            jump_to_tab = false;
                        }
                    }
                }
            }
        }
    }
}

function makeValidationRequest(data, url)
{   
    var result; 
    
    $.ajax({
        url: '/' + url,
        data: data,
        async: false,
        method: 'post',
        dataType: 'json',
        success: function(data){
            result = data;
        }
    });
    
    return result;
};

function makeAjaxRequest(data, url)
{   
    var result; 
    
    $.ajax({
        url: '/' + url,
        data: data,
        async: false,
        method: 'post',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        enctype: 'multipart/form-data',
        success: function(data){
            result = data;
        }
    });
    
    return result;
};
//   END Validation ============================================

function addFromSelectToTable(select, table, post_name)
{
    var option = $(select).find('option:selected');
    
    var html = '<tr>';
        html += '<td>';
            html += option.html();
        html += '</td>';
    html += '</tr>';
    
    $(table).append(html);
}

function setMenuItemUrl(url_selector, url)
{
    $(url_selector).val(url);
    if (url !== '' && url !== undefined) {
        $(url_selector).prop('readonly', true);
    } else {
        $(url_selector).prop('readonly', false);
    }
}

function clearSearchFilter(button)
{
    var form = $(button).closest('#filter_form');
    
    var inputs = form.find('input[type=text]');
    inputs.each(function() {
        var row = $(this);
        row.val('');
    });
    
    var selects = form.find('select');
    selects.each(function() {
        var row = $(this);
        row.val('');
    });
    
    form.submit();
}

function tryVideo(link, target)
{
    var parsed_link = $(link).val().split('/');
    if (parsed_link == '') {
        alert('Video link is empty');
        return;
    }
    parsed_link = parsed_link[parsed_link.length - 1];
    var target_link = 'https://www.youtube.com/embed/' + parsed_link + '?rel=0';
    
    $(target).attr('src', target_link);
    $(target).show();
    
    $(link).val(parsed_link);
}

function checkAllCheckboxes(initiator, selector)
{
    if ($(initiator).prop('checked') === true) {
        $(selector).prop('checked', true);
    } else {
        $(selector).prop('checked', false);
        $('#permissions_all').prop('checked', false);
    }
}

function addCodeFromTemplate(template_selector, target_selector, counter_selector, replacements)
{
    var counter = parseInt($(counter_selector).val());
    counter++;
    
    var html = $(template_selector).html();
    html = html.replace(/\{\$counter\}/g, counter);
    
    $(target_selector).append(html);
    $(counter_selector).val(counter);
}

function toggleStateBlocks(default_value)
{
    if (default_value == 1) {
        $('#final_box').slideUp();
        $('#final').val('');
        $('#final').prop('disabled', true);
    } else {
        $('#final_box').slideDown();
        $('#final').prop('disabled', false);
    }
}

function makeTableSortable(table_selector)
{
    $(table_selector).sortable({
        containerSelector: "table",
        itemPath: "> tbody",
        itemSelector: "tr"
    });
}

function changeSort(new_sort)
{
    $('#sort').val(new_sort);
    $('#sort_form').submit();
}

function submitGlobalSearch()
{
    var keyword = $('#global_search_keyword').val();
    if (keyword.length < 3) {
        return false;
    }
    
    $('#global_search_form').unbind('onsubmit');
    $('#global_search_form').submit();
}

function checkUserActivity()
{
    setInterval(
        function(){
            $.ajax({
                url: '/site/checkuseractivity',
                method: 'get',
                success: function(data){
                    if (data === '0') {
                        window.location.href = '/auth/logout';
                    }
                }
            });
        },
        30000
    );
}

function checkDate(id)
{
    var input = $('#' + id).find('input');
    if (input.prop('checked') == false) {
        input.prop('checked', true);
        $('#' + id).addClass('checked');
    } else {
        input.prop('checked', false);
        $('#' + id).removeClass('checked');
    }
}

function storeMenuState()
{
    var current_body_class = $('body').attr('class');
    $.ajax({
        url: '/site/storemenustate',
        data: 'state=' + (current_body_class === 'nav-md' ? 'closed' : 'opened'),
        method: 'get'
    });
}

//  Currency methods ===========================================================
function excludeCurrency(list_selector, id_to_exclude)
{
    var list_value = $(list_selector).val();
    var list_template = $('#currencies_list').html();
    
    $(list_selector).html(list_template);
    
    $(list_selector).children().each(function(){
        var option = $(this);
        if ($(this).val() == id_to_exclude && id_to_exclude != '') {
            $(this).remove();
        }
    });
    
    $(list_selector).val(list_value);
}

function togglCurrencies()
{
    var base_html = $('#id_base_currency').html();
    var base_value = $('#id_base_currency').val();
    
    var quote_html = $('#id_quote_currency').html();
    var quote_value = $('#id_quote_currency').val();
    
    $('#id_base_currency').html(quote_html);
    $('#id_base_currency').val(quote_value);
    
    $('#id_quote_currency').html(base_html);
    $('#id_quote_currency').val(base_value);
}
//  Currency methods ===========================================================

//  Crop image methods =========================================================
function getImageForCropper(id)
{
    var preview = $('#' + id + '_preview');
    $('body').on('change', '#' + id + '_image', function(){
        var imageReader = new FileReader();
        imageReader.readAsDataURL(document.getElementById(id + '_image').files[0]);

        imageReader.onload = function (oFREvent) {
            preview.attr('src', oFREvent.target.result).fadeIn();
            initImageCropper(id);
        };
    });
}

function initImageCropper(id)
{
    var default_x1 = 0;
    var default_y1 = 0;
    var default_w = 200;
    var default_h = 200;
    
    $('#' + id + '_x1').val(default_x1);
    $('#' + id + '_y1').val(default_y1);
    $('#' + id + '_w').val(default_w);
    $('#' + id + '_h').val(default_h);
    
    $('#' + id + '_preview').Jcrop({
        aspectRatio: true,
        setSelect: [
            default_x1,
            default_y1,
            default_x1 + default_w,
            default_y1 + default_h
        ],
        onSelect: function(frame){
            $('#' + id + '_x1').val(frame.x);
            $('#' + id + '_y1').val(frame.y);
            $('#' + id + '_w').val(frame.w);
            $('#' + id + '_h').val(frame.h);
        }
    });
}
//  Crop image methods =========================================================

function approvePairRequests()
{
    var allow_submit = false;
    
    $('.approve_request').each(function(){
        var row = $(this);
        
        if (row.prop('checked') == true) {
            allow_submit = true;
        }
    });
    
    if (allow_submit === false) {
        alert('Select at least one request');
        return;
    }
    
    $('#approve_form').submit();
}

function addPair()
{
    addCodeFromTemplate('#rates_tpl', '#rates_box', '#pairs_count');
    
    var count = $('#pairs_count').val();
    $("#rates_" + count).autocomplete({
        source: "/rates/autocomplete",
        minLength: 2
    });
}

function expandMenuSection(controller)
{
    var element = $('#menu_' + controller);
    if (element.length === 0) {
        return;
    }
    
    element.addClass('active');
    element.parent().find('.child_menu').slideDown();
}

function synchLangSwitchers($lang, id_initiator)
{
    var switchers = $('.lang_switcher_' + $lang);
    
    if (switchers.length === 0) {
        return;
    }
    
    switchers.each(function(){
        var row = $(this);
        var parent_conteier = row.closest('.lang_switchers_box');
        if (parent_conteier.attr('id') == 'lang_switchers_box_' + id_initiator) {
            return;
        }
        
        parent_conteier.find('.lang_switcher').removeClass('active');
        parent_conteier.find('.lang_switcher_' + $lang).addClass('active');
        
        parent_conteier.find('.lang_content').removeClass('active');
        parent_conteier.find('.lang_content_' + $lang).addClass('active');
        
        if (window.html_editors && Object.keys(window.html_editors).length > 0) {
            for (var key in window.html_editors) {
                window.html_editors[key].refresh();
            }
        }
    });
}

function refreshHtmlEditors()
{
    if (window.html_editors && Object.keys(window.html_editors).length > 0) {
        setTimeout(
            function(){
                for (var key in window.html_editors) {
                    window.html_editors[key].refresh();
                }
            },
            10
        );
        
    }
}

function collectDictionaryEntries()
{
    $.ajax({
        url: '/local/dictcollect',
        method: 'get',
        success: function(data){
            if (Object.keys(data).length > 0) {
                for (var i in data) {
                    $('#result_box').find('#result_' + i).html(data[i]);
                }
                $('#loading_box').hide();
                $('#result_box').show();
            } else {
                alert('Some problems are occured')
            }
        }
    });
}

function setUserPermissionsGroup(key)
{
    $('.groups_checkboxes').prop('checked', false);
    
    if (key == '') {
        return;
    }
    
    if (!groups[key]) {
        return;
    }
    
    $('#groups_' + groups[key]).prop('checked', true);
}

function loadReminderCalendar(year, month)
{
    $('#calendar_box').hide();
    $('#loading_box').show();
    
    var request_data = {
        year: year,
        month: month
    };
    
    $.ajax({
        url: '/reminder/list',
        async: true,
        data: request_data,
        method: 'get',
        dataType: 'html',
        success: function(data){
            if (data) {
                $('#calendar_box').html(data);
                
                $('#loading_box').hide();
                $('#calendar_box').show();
            }
        }
    });
}

function calculateGoalSavings(year)
{
    if (savings_by_years === undefined) {
        return;
    }
    
    $('#savings_no_payments').html(savings_by_years[year].savings_no_payments);
    $('#profit_no_payments').html(savings_by_years[year].profit_no_payments);
    if (savings_by_years[year].savings_with_payments) {
        $('#savings_with_payments').html(savings_by_years[year].savings_with_payments);
    }
    if (savings_by_years[year].profit_with_payments) {
        $('#profit_with_payments').html(savings_by_years[year].profit_with_payments);
    }
}

function loadCase(investor_id)
{
    $.ajax({
        url: '/investors/loadcase/' + investor_id,
        async: true,
        method: 'get',
        dataType: 'html',
        success: function(data){
            if (data) {
                $('#case_tab').html(data);
            }
        }
    });
}

function loadCharts(investor_id, date_range_from, date_range_to)
{
    var request_data = {
        date_range_from: date_range_from,
        date_range_to: date_range_to
    };
    
    $.ajax({
        url: '/investors/loadcharts/' + investor_id,
        async: true,
        data: request_data,
        method: 'get',
        dataType: 'html',
        success: function(data){
            if (data) {
                $('#chart_container').html(data);
            }
        }
    });
}

function findAvailableInvestors(recommendation_id)
{
    var request_data = $('#record_form').serializeArray();
    
    $.ajax({
        url: '/recommendations/getinvestors/' + recommendation_id,
        async: true,
        data: request_data,
        method: 'post',
        dataType: 'html',
        success: function(data){
            if (data) {
                $('#available_investors_box').html(data);
                
                calculateSelectedInvestors();
            }
        }
    });
}

function calculateSelectedInvestors()
{
    $('#selected_investors').html($('.investors_checkbox:checked').length);
}

function buildReportLink()
{
    var report_format = $('#report_format').val();
    var report_date_from = $('#report_date_from').val();
    var report_date_to = $('#report_date_to').val();
    
    var report_link_tpl = $('#report_link_tpl').html();
    report_link_tpl = report_link_tpl.replace('{format}', report_format);
    report_link_tpl = report_link_tpl.replace('{date_start}', report_date_from);
    report_link_tpl = report_link_tpl.replace('{date_end}', report_date_to);
    report_link_tpl = report_link_tpl.replaceAll('&amp;', '&');
    
    $('#report_link').attr('href', report_link_tpl);
}

function loadPositionsHistory(investor_id)
{
    var container = $('#history_container');
    if (container.length == 0) {
        return;
    }
    
    container.find('#history_records').hide();
    container.find('#history_preloader').show();
    
    var request_data = $('#history_form').serializeArray();
    
    $.ajax({
        url: '/investors/loadpositionshistory/' + investor_id,
        async: true,
        data: request_data,
        method: 'get',
        dataType: 'html',
        success: function(data){
            if (data) {
                container.find('#history_records').html(data);
                
                container.find('#history_preloader').hide();
                container.find('#history_records').show();
            }
        }
    });
}

function chackNewChatMessages()
{
    $.ajax({
        url: '/chat/checknewmessages',
        method: 'get',
        success: function(data){
            $('#header_alert_chat_new_messages').html(data);
            if (data > 0) {
                $('#header_alert_chat_new_messages_box').show();
            } else {
                $('#header_alert_chat_new_messages_box').hide();
            }
        }
    });
}

function loadCasesStat(mode)
{
    $.ajax({
        url: '/stat/loadstat?mode=' + mode,
        async: true,
        method: 'get',
        dataType: 'html',
        success: function(data){
            if (data) {
                $('#' + mode + '_loading').hide();
                $('#' + mode + '_stat_box').html(data);
            }
        }
    });
}

function chackNewRecommendations()
{
    $.ajax({
        url: '/recommendations/checknew',
        method: 'get',
        dataType: 'html',
        success: function(data){
            $('#header_alert_new_recommendations').html(data);
            if (data > 0) {
                $('#header_alert_new_recommendations_box').show();
            } else {
                $('#header_alert_new_recommendations_box').hide();
            }
        }
    });
}

function loadGoodsParams(category_id, product_id)
{
    var request_data = {
        category_id: category_id,
        product_id: product_id,
    };
    
    $.ajax({
        url: '/goods/loadparams',
        method: 'get',
        data: request_data,
        dataType: 'html',
        success: function(data){
            $('#params_box').html(data);
        }
    });
}

function calculateProductPrices()
{
    var buy_price = parseFloat($('#buy_price').val());
    var extra_charge_percent = parseFloat($('#extra_charge_percent').val());
    var discount_percent = parseFloat($('#discount_percent').val());
    
    var price = buy_price + (buy_price / 100 * extra_charge_percent);
    var inner_price = price - (price / 100 * discount_percent);
    
    if (isNaN(price) || isNaN(inner_price)) {
        alert('Wrong values given');
        return;
    }
    
    $('#price').val(price.toFixed(2));
    $('#inner_price').val(inner_price.toFixed(2));
}

function toggleVarsTypesValues(type)
{
    $('.value_box').hide();
    
    $('#value_' + type + '_box').show();
}