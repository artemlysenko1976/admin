<?php

return [
    'components' => [
        'errorHandler' => [
            'class' => 'app\backend\controllers\DefaultController',
            'errorAction' => 'default/error',
        ],
    ],
];
