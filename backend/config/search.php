<?php

/**
 * Array of models with global search support 
 */

return [
    'Articles',
    'MenusItems',
    'Pages',
    'Texts',
    'Metatags',
    'Scripts',
    'Redirects',
    'Users',
    'UsersGroups',
    'EmailTemplates',
    'Languages',
    'Dictionary',
];