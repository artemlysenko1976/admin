<?php

use Yii;

return [
//  Clients
    [
        'name' => Yii::t('app', 'Клиенты'),
        'url' => 'clients',
        'class' => 'fa-briefcase',
        'actions' => [
            [
                'name' => Yii::t('app', 'Список клиентов'),
                'url' => 'list',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить клиента'),
                'url' => 'add',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать клиента'),
                'url' => 'edit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Просмотреть клиента'),
                'url' => 'view',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить клиента'),
                'url' => 'delete',
                'display' => false,
                'check' => true,
            ],
        ],
    ],
//  Goodss
    [
        'name' => Yii::t('app', 'Товары'),
        'url' => 'goods',
        'class' => 'fa-barcode',
        'actions' => [
            [
                'name' => Yii::t('app', 'Список товаров'),
                'url' => 'goodslist',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить товар'),
                'url' => 'goodsadd',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать товар'),
                'url' => 'goodsedit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Просмотреть товар'),
                'url' => 'goodsview',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить товар'),
                'url' => 'goodsdelete',
                'display' => false,
                'check' => true,
            ],
            
            [
                'name' => Yii::t('app', 'Список категорий'),
                'url' => 'catslist',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить категорию'),
                'url' => 'catsadd',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать категорию'),
                'url' => 'catsedit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Просмотреть категорию'),
                'url' => 'catsview',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить категорию'),
                'url' => 'catsdelete',
                'display' => false,
                'check' => true,
            ],
            
            [
                'name' => Yii::t('app', 'Список параметров'),
                'url' => 'paramslist',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить параметр'),
                'url' => 'paramsadd',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать параметр'),
                'url' => 'paramsedit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Просмотреть параметр'),
                'url' => 'paramsview',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить параметр'),
                'url' => 'paramsdelete',
                'display' => false,
                'check' => true,
            ],
            
            [
                'name' => Yii::t('app', 'Список брендов'),
                'url' => 'brandslist',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить бренд'),
                'url' => 'brandsadd',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать бренд'),
                'url' => 'brandsedit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Просмотреть бренд'),
                'url' => 'brandsview',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить бренд'),
                'url' => 'brandsdelete',
                'display' => false,
                'check' => true,
            ],
        ],
    ],
//  Payments
    [
        'name' => Yii::t('app', 'Платежи'),
        'url' => 'payments',
        'class' => 'fa-usd',
        'actions' => [
            [
                'name' => Yii::t('app', 'Платежи'),
                'url' => 'paymentslist',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Просмотреть платеж'),
                'url' => 'paymentsview',
                'display' => false,
                'check' => true,
            ],
            
            [
                'name' => Yii::t('app', 'Валюты'),
                'url' => 'currencieslist',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить валюту'),
                'url' => 'currenciesadd',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать валюту'),
                'url' => 'currenciesedit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить валюту'),
                'url' => 'currenciesdelete',
                'display' => false,
                'check' => true,
            ],
            
            [
                'name' => Yii::t('app', 'Методы оплаты'),
                'url' => 'methodslist',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить метод'),
                'url' => 'methodsadd',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать метод'),
                'url' => 'methodsedit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить метод'),
                'url' => 'methodsdelete',
                'display' => false,
                'check' => true,
            ],
        ],
    ],
//  Localization
    [
        'name' => Yii::t('app', 'Локализация'),
        'url' => 'local',
        'class' => 'fa-globe',
        'actions' => [
            [
                'name' => Yii::t('app', 'Языки'),
                'url' => 'langslist',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить язык'),
                'url' => 'langsadd',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать язык'),
                'url' => 'langsedit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить язык'),
                'url' => 'langsdelete',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Валидировать язык'),
                'url' => 'langsvalidate',
                'display' => false,
                'check' => false,
            ],
            
            [
                'name' => Yii::t('app', 'Словарь'),
                'url' => 'dictlist',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить запись'),
                'url' => 'dictadd',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать запись'),
                'url' => 'dictedit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить запись'),
                'url' => 'dictdelete',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Валидировать запись'),
                'url' => 'dictvalidate',
                'display' => false,
                'check' => false,
            ],
            [
                'name' => Yii::t('app', 'Экспортировать словарь'),
                'url' => 'dictexport',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Импортировать словарь'),
                'url' => 'dictimport',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Собрать непереведенные записи'),
                'url' => 'dictcollect',
                'display' => true,
                'check' => true,
            ],
        ],
    ],
//  Countries
    [
        'name' => Yii::t('app', 'Страны'),
        'url' => 'countries',
        'class' => 'fa-list',
        'actions' => [
            [
                'name' => Yii::t('app', 'Список стран'),
                'url' => 'list',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить страну'),
                'url' => 'add',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать страну'),
                'url' => 'edit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить страну'),
                'url' => 'delete',
                'display' => false,
                'check' => true,
            ],
        ],
    ],
//  E-mail
    [
        'name' => 'E-mail',
        'url' => 'email',
        'class' => 'fa-at',
        'actions' => [
            [
                'name' => Yii::t('app', 'E-mail шаблоны'),
                'url' => 'list',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить шаблон'),
                'url' => 'add',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать шаблон'),
                'url' => 'edit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить шаблон'),
                'url' => 'delete',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Валидировать шаблон'),
                'url' => 'validate',
                'display' => false,
                'check' => false,
            ],
            [
                'name' => Yii::t('app', 'История рассылок'),
                'url' => 'history',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Просмотреть историческую запись'),
                'url' => 'historyview',
                'display' => false,
                'check' => true,
            ],
        ],
    ],
//  IP
//    [
//        'name' => 'IP adresses filter',
//        'url' => 'ip',
//        'class' => 'fa-filter',
//        'actions' => [
//            [
//                'name' => 'IP adresses list',
//                'url' => 'list',
//                'display' => true,
//                'check' => true,
//            ],
//            [
//                'name' => 'Add IP adresse',
//                'url' => 'add',
//                'display' => true,
//                'check' => true,
//            ],
//            [
//                'name' => 'Edit IP adresse',
//                'url' => 'edit',
//                'display' => false,
//                'check' => true,
//            ],
//            [
//                'name' => 'Delete IP adresse',
//                'url' => 'delete',
//                'display' => false,
//                'check' => true,
//            ],
//            [
//                'name' => 'Validate IP adresse',
//                'url' => 'validate',
//                'display' => false,
//                'check' => false,
//            ],
//        ],
//    ],
//  Administrators
    [
        'name' => Yii::t('app', 'Администраторы'),
        'url' => 'users',
        'class' => 'fa-user',
        'actions' => [
            [
                'name' => Yii::t('app', 'Администраторы'),
                'url' => 'list',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить администратора'),
                'url' => 'add',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать администратора'),
                'url' => 'edit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить администратора'),
                'url' => 'delete',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Валидировать администратора'),
                'url' => 'validate',
                'display' => false,
                'check' => false,
            ],
            [
                'name' => Yii::t('app', 'Просмотреть администратора'),
                'url' => 'view',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Лог действий'),
                'url' => 'activity',
                'display' => true,
                'check' => true,
            ],
        ],
    ],
//  Administrators groups
    [
        'name' => Yii::t('app', 'Группы админов'),
        'url' => 'groups',
        'class' => 'fa-group',
        'actions' => [
            [
                'name' => Yii::t('app', 'Группы'),
                'url' => 'list',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить группу'),
                'url' => 'add',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать группу'),
                'url' => 'edit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить группу'),
                'url' => 'delete',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Валидировать группу'),
                'url' => 'validate',
                'display' => false,
                'check' => false,
            ],
        ],
    ],
//  Settings
    [
        'name' => Yii::t('app', 'Настройки'),
        'url' => 'settings',
        'class' => 'fa-gear',
        'actions' => [
//            [
//                'name' => 'Site settings',
//                'url' => 'site',
//                'display' => true,
//                'check' => true,
//            ],
            
            [
                'name' => Yii::t('app', 'Переменные'),
                'url' => 'varslist',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Добавить переменную'),
                'url' => 'varsadd',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Редактировать переменную'),
                'url' => 'varsedit',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Удалить переменную'),
                'url' => 'varsdelete',
                'display' => false,
                'check' => true,
            ],
        ],
    ],
//  System information
    [
        'name' => Yii::t('app', 'Система'),
        'url' => 'system',
        'class' => 'fa-info',
        'actions' => [
            [
                'name' => Yii::t('app', 'Просмотреть системную информацию'),
                'url' => 'info',
                'display' => true,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Просмотреть информацию о PHP'),
                'url' => 'phpinfo',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Просмотреть информацию о сервере'),
                'url' => 'server',
                'display' => false,
                'check' => true,
            ],
            [
                'name' => Yii::t('app', 'Отчеты о работе кронов'),
                'url' => 'cron',
                'display' => true,
                'check' => true,
            ],
        ],
    ],
];