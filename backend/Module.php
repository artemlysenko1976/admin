<?php

namespace app\backend;

/**
 * backend module definition class
 */
class Module extends \yii\base\Module
{
    public $layout = 'main';
    
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\backend\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        
        \Yii::configure($this, require(__DIR__ . '/config/config.php'));
        \Yii::$app->errorHandler->errorAction = 'backend/default/error';
    }
}
