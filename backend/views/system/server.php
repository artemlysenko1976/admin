<?php

use Yii;

$this->title = Yii::t('app', 'Информация о сервере');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped non_list_table">
                <?php foreach ($info as $k => $v) { ?>
                <tr>
                    <td style="width:20%;"><?= $k ?></td>
                    <td><?= $v ?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>