<?php

use Yii;

$this->title = Yii::t('app', 'Информация о системе');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Фреймворк') ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-striped non_list_table">
                <tr>
                    <td style="width:20%;"><?= Yii::t('app', 'Название') ?></td>
                    <td>Yii2 version <?= $framework_version ?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('app', 'Среда') ?></td>
                    <td>
                        <?= YII_ENV ?>, 
                        <?= Yii::t('app', 'error level') ?>: <?= ERROR_LEVEL ?>, 
                        <?= Yii::t('app', 'debug mode') ?>: <?= YII_DEBUG ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Операционная система') ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-striped non_list_table">
                <tr>
                    <td style="width:20%;"><?= Yii::t('app', 'Название') ?></td>
                    <td><?= php_uname() ?></td>
                </tr>
                <tr>
                    <td>PHP</td>
                    <td>
                        <?= $php_version ?> 
                        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/phpinfo') ?>" class="glyphicon glyphicon-info-sign" target="_blank" title="view PHP info"></a>
                    </td>
                </tr>
                <tr>
                    <td><?= Yii::t('app', 'Часовой пояс') ?></td>
                    <td><?= date_default_timezone_get() ?></td>
                </tr>
            </table>
        </div>
    </div>
    
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Web сервер') ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-striped non_list_table">
                <tr>
                    <td style="width:20%;"><?= Yii::t('app', 'Название') ?></td>
                    <td>
                        <?= strip_tags($_SERVER['SERVER_SOFTWARE']) ?>
                        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/server') ?>" class="glyphicon glyphicon-info-sign" target="_blank" title="view server info"></a>
                    </td>
                </tr>
                <?php if (!empty($nginx_version)) { ?>
                <tr>
                    <td>Nginx</td>
                    <td><?= $nginx_version ?></td>
                </tr>
                <?php } ?>
                <?php if (!empty($apache_version)) { ?>
                <tr>
                    <td>Apache</td>
                    <td><?= $apache_version ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td><?= Yii::t('app', 'Server IP адрес') ?></td>
                    <td><?= $_SERVER['SERVER_ADDR'] ?></td>
                </tr>
                <?php if (!empty($geoip_db_info)) { ?>
                <tr>
                    <td>GeoIP</td>
                    <td><?= $geoip_db_info ?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'База данных') ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-striped non_list_table">
                <tr>
                    <td style="width:20%;"><?= Yii::t('app', 'Драйвер') ?></td>
                    <td><?= Yii::$app->db->driverName ?></td>
                </tr>
                <tr>
                    <td>DSN</td>
                    <td><?= Yii::$app->db->dsn ?></td>
                </tr>
                <?php if (!empty($mysql_version)) { ?>
                <tr>
                    <td>MySQL Server</td>
                    <td><?= $mysql_version ?></td>
                </tr>
                <?php } ?>
                <?php if (!empty($postgresql_version)) { ?>
                <tr>
                    <td>PostgreSQL Server</td>
                    <td><?= $postgresql_version ?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    
    <?php if (!empty($geoip_db_info)) { ?>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Определение локации') ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-striped non_list_table">
                <tr>
                    <td style="width:20%;">Library</td>
                    <td>GeoIP</td>
                </tr>
                <tr>
                    <td><?= Yii::t('app', 'База данных') ?></td>
                    <td><?= $geoip_db_info ?></td>
                </tr>
                <?php if (!empty($geoip_db_all_info)) { ?>
                    <?php foreach ($geoip_db_all_info as $item) { ?>
                    <tr>
                        <td><?= $item['description'] ?></td>
                        <td><?= $item['filename'] ?></td>
                    </tr>
                    <?php } ?>
                <?php } ?>
            </table>
        </div>
    </div>
    <?php } ?>
</div>