<?php

use yii\helpers\Url;
use app\models\CronReports;

$this->title = Yii::t('app', 'Отчеты о работе кронов');
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['name'])) {
    $this->params['js_code'][] = '$("#filter_name").val("' . Yii::$app->params['filter']['name'] . '");';
}
if (isset(Yii::$app->params['filter']['status'])) {
    $this->params['js_code'][] = '$("#filter_status").val("' . Yii::$app->params['filter']['status'] . '");';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post" action="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id) ?>">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-3">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" placeholder="type currency or pair name" class="form-control">
                </div>
                <div class="col-md-2">
                    <label for="filter_name"><?= Yii::t('app', 'Название крона') ?></label>
                    <select name="filter[name]" id="filter_name" class="form-control">
                        <option value="">-</option>
                        <?php foreach ($cron_names as $item) { ?>
                            <option value="<?= $item ?>"><?= $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="filter_status"><?= Yii::t('app', 'Статус') ?></label>
                    <select name="filter[status]" id="filter_status" class="form-control">
                        <option value="">-</option>
                        <?php foreach (CronReports::$statuses as $status => $label) { ?>
                            <option value="<?= $status ?>"><?= $label ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_timestamp_range"><?= Yii::t('app', 'Поиск по дате') ?></label><br>
                    <?= $this->render('/layouts/datepicker', [
                        'name' => 'filter[timestamp_range_from]',
                        'value' => Yii::$app->params['filter']['timestamp_range_from'],
                    ]) ?>
                    <?= $this->render('/layouts/datepicker', [
                        'name' => 'filter[timestamp_range_to]',
                        'value' => Yii::$app->params['filter']['timestamp_range_to'],
                    ]) ?>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Сбросить') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <div class="table_container">
            <table class="table table-striped table-bordered table-hover non_list_table">
                <tr>
                    <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Название крона поиск'), 'field' => 'name']) ?></th>
                    <th><?= Yii::t('app', 'Статус') ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Время начала'), 'field' => 'start_timestamp']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Время окончания'), 'field' => 'end_timestamp']) ?></th>
                    <th><?= Yii::t('app', 'Продолжительность') ?></th>
                    <th><?= Yii::t('app', 'Результат') ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Ошибки'), 'field' => 'error']) ?></th>
                </tr>
                <?php foreach ($records as $row) { ?>
                    <?php 
                    $class = '';
                    if ($row['status'] == 'with_error') {
                        $class = 'danger';
                    } elseif ($row['status'] == 'not_completed') {
                        $class = 'warning';
                    }
                    ?>
                    <tr class="<?= $class ?>">
                        <td><?= $row['name'] ?></td>
                        <td><?= CronReports::$statuses[$row['status']] ?></td>
                        <td><?= $this->render('/layouts/time', ['field' => $row['start_timestamp']]) ?></td>
                        <td><?= $this->render('/layouts/time', ['field' => $row['end_timestamp']]) ?></td>
                        <td><?= $row['duration'] ?></td>
                        <td>
                            <?php if (!empty($row['result'])) { ?>
                                <a href="javascript:void(0);" onclick="$('#result_<?= $row['id'] ?>_box').slideToggle();"><?= Yii::t('app', 'показать') ?></a>
                                <br>
                                <div id="result_<?= $row['id'] ?>_box" style="display:none;">
                                    <?= CronReports::showJsonData($row['result']) ?>
                                </div>
                            <?php } ?>
                        </td>
                        <td><?= CronReports::showJsonData($row['error']) ?></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    
        <?= $this->render('/layouts/pager', [
            'pages' => $pages
        ]) ?>
    <?php } else { ?>
        No records found
    <?php } ?>
</div>