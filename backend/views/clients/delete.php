<?php

use Yii;

$this->title = Yii::t('app', 'Удалить Клиента');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Клиенты'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    
    <?php if (empty($error)) { ?>
        <?= $this->render('/layouts/delete', [
            'data' => $data,
        ]); ?>
    <?php } else { ?>
        <?= $this->render('/entities/error', [
            'error' => $error
        ]) ?>
    <?php } ?>
</div>