<?php

use Yii;
use app\models\Clients;
use app\models\Users;
use app\library\Utils;

$this->title = Yii::t('app', 'Просмотр данных Клиента');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Клиенты'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

if (!empty($data)) {
}

?>

<div class="backend-default-index">
    <h1>
        #<?= $data['id'] ?> <?= $data['login'] ?> - <?= $data['first_name'] ?> <?= $data['last_name'] ?>
        
        <?php if (Users::checkAccessPermissions(Yii::$app->controller->id, 'delete') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/delete/' . $data['id']) ?>" class="btn btn-danger add_record_button">
            <span class="glyphicon glyphicon-remove"></span>
            <?= Yii::t('app', 'Удалить') ?>
        </a>
        <?php } ?>
        <?php if (Users::checkAccessPermissions(Yii::$app->controller->id, 'edit') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $data['id']) ?>" class="btn btn-default add_record_button">
            <span class="glyphicon glyphicon-edit"></span>
            <?= Yii::t('app', 'Редактировать') ?>
        </a>
        <?php } ?>
    </h1>
    <hr>
    
    <?php if (!empty($data)) { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-3">
                                <?php if (!empty($data['photo'])) { ?>
                                    <img src="/images/clients/<?= $data['id'] ?>/<?= $data['photo'] ?>?t=<?= time() ?>" alt="" class="thumbnail" style="height:150px;" onerror="this.src = '/images/no_photo.png';">
                                <?php } else { ?>
                                    <img src="/images/no_photo.png" alt="" class="thumbnail" style="height:150px;">
                                <?php } ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['login'] ?></label><br>
                                    <?= $data['login'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['first_name'] ?></label><br>
                                    <?= mb_strtoupper($data['last_name']) ?> 
                                    <?= $data['first_name'] ?>
                                    <?= (!empty($data['second_name']) ? $data['second_name'] : '') ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['birth_day'] ?></label><br>
                                    <?= $data['birth_day'] ?>
                                </div>
                                <?php if (!empty($data['gender'])) { ?>
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['gender'] ?></label><br>
                                    <?= Clients::$genders[$data['gender']] ?>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['country_id'] ?></label><br>
                                    <?= $data['country']->local->name ?>
                                </div>
                                <?php if (!empty($data['city'])) { ?>
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['city'] ?></label><br>
                                    <?= $data['city'] ?>
                                </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['lang'] ?></label><br>
                                    <?= $data['language']->name ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['email'] ?></label><br>
                                    <?= $data['email'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['phone'] ?></label><br>
                                    <?= $data['phone'] ?>
                                </div>
                                <?php if (!empty($data['site'])) { ?>
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['site'] ?></label><br>
                                    <a href="<?= $data['site'] ?>" target="_blank"><?= $data['site'] ?></a>
                                </div>
                                <?php } ?>
                                <?php if (!empty($data['organization_link'])) { ?>
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['organization_link'] ?></label><br>
                                    <a href="<?= $data['organization_link'] ?>" target="_blank"><?= $data['organization_link'] ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php if (!empty($data['description'])) { ?>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['description'] ?></label><br>
                                    <?= $data['description'] ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['status'] ?></label><br>
                                    <?= Clients::$statuses[$data['status']] ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Clients::attributeStaticLabels()['create_timestamp'] ?></label><br>
                                    <?= date(Yii::$app->params['date_time_format'], $data['create_timestamp']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="clear">
        <div class="form-group">
            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list') ?>" class="btn btn-default"><?= Yii::t('app', 'Назад') ?></a>
        </div>
    <?php } else { ?>
        <?= $this->render('/layouts/record_not_found'); ?>
    <?php } ?>
</div>