<?php

use Yii;
use app\models\Clients;

$referrer_string = '';

if (!empty($data)) {
    if (!empty($data->referrer)) {
        $referrer_string = '#' . $data->referrer_id . ' - ' . $data->referrer->last_name . ' ' . $data->referrer->first_name . ' (' . $data->referrer->login . ')';
    }
}

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        
        <?= $this->render('/layouts/form/string', [
            'name' => 'login',
            'id' => 'login',
            'label' => Clients::attributeStaticLabels()['login'],
            'value' => $data['login'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'email',
            'id' => 'email',
            'label' => Clients::attributeStaticLabels()['email'],
            'value' => $data['email'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'password',
            'label' => Clients::attributeStaticLabels()['password'],
            'value' => '',
            'type' => 'password',
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'password_confirm',
            'label' => Clients::attributeStaticLabels()['password_confirm'],
            'value' => '',
            'type' => 'password',
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/string', [
            'name' => 'first_name',
            'id' => 'first_name',
            'label' => Clients::attributeStaticLabels()['first_name'],
            'value' => $data['first_name'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'second_name',
            'id' => 'second_name',
            'label' => Clients::attributeStaticLabels()['second_name'],
            'value' => $data['second_name'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'last_name',
            'id' => 'last_name',
            'label' => Clients::attributeStaticLabels()['last_name'],
            'value' => $data['last_name'],
        ]) ?>
        <?= $this->render('/layouts/form/date', [
            'name' => 'birth_day',
            'id' => 'birth_day',
            'label' => Clients::attributeStaticLabels()['birth_day'],
            'value' => $data['birth_day'],
        ]) ?>
        <?= $this->render('/layouts/form/select', [
            'name' => 'gender',
            'id' => 'gender',
            'label' => Clients::attributeStaticLabels()['gender'],
            'value' => $data['gender'],
            'options' => Clients::$genders,
            'empty_option' => true,
        ]) ?>
        <?= $this->render('/layouts/form/select', [
            'name' => 'lang',
            'id' => 'lang',
            'label' => Clients::attributeStaticLabels()['lang'],
            'value' => $data['lang'],
            'options' => Yii::$app->params['langs'],
            'option_key' => 'code',
            'option_value' => 'name',
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/image', [
            'name' => 'photo',
            'id' => 'photo',
            'label' => Clients::attributeStaticLabels()['photo'],
            'image' => $data['photo'],
            'url' => '/images/clients/' . $data['id'] . '/' . $data['photo'],
            'delete' => true,
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/select', [
            'name' => 'country_id',
            'id' => 'country_id',
            'label' => Clients::attributeStaticLabels()['country_id'],
            'value' => $data['country_id'],
            'options' => $countries,
            'option_key' => 'id',
            'option_value' => 'local.name',
            'empty_option' => true,
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'city',
            'id' => 'city',
            'label' => Clients::attributeStaticLabels()['city'],
            'value' => $data['city'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/string', [
            'name' => 'phone',
            'id' => 'phone',
            'label' => Clients::attributeStaticLabels()['phone'],
            'value' => $data['phone'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'site',
            'id' => 'site',
            'label' => Clients::attributeStaticLabels()['site'],
            'value' => $data['site'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'organization_link',
            'id' => 'organization_link',
            'label' => Clients::attributeStaticLabels()['organization_link'],
            'value' => $data['organization_link'],
        ]) ?>
        <?= $this->render('/layouts/form/html', [
            'name' => 'description',
            'id' => 'description',
            'label' => Clients::attributeStaticLabels()['description'],
            'value' => $data['description'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/autocomplete', [
            'name' => 'referrer_id',
            'id' => 'referrer_id',
            'label' => Clients::attributeStaticLabels()['referrer_id'],
            'value' => $referrer_string,
            'url' => 'clients/autocomplete',
        ]) ?>
        <?= $this->render('/layouts/form/select', [
            'name' => 'status',
            'id' => 'status',
            'label' => Clients::attributeStaticLabels()['status'],
            'value' => $data['status'],
            'options' => Clients::$statuses,
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'data' => $data,
        ]) ?>
    </form>
    
    <?= $this->render('/layouts/form/documents_tpl', [
        'name' => 'docs]',
    ]) ?>
</div>