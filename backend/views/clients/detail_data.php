<?php

use yii\helpers\Url;

if (!empty($user)) {
    
}

?>

<?php if (!empty($user)) { ?>
<div class="x_panel">
    <?php if (!empty($title)) { ?>
    <div class="x_title">
        <h2><?= $title ?></h2>
        <div class="clearfix"></div>
    </div>
    <?php } ?>
    
    <div class="x_content">
        <div class="flex">
            <ul class="list-inline widget_profile_box">
                <li>&nbsp;</li>
                <li>
                    <a href="<?= Url::to(Yii::$app->params['urls']['backend'] . 'clients/view/' . $user->id) ?>" target="blank">
                        <?php if (!empty($user['photo'])) { ?>
                            <img src="/images/clients/<?= $user['id'] ?>/<?= $user['photo'] ?>?t=<?= time() ?>" alt="" class="img-circle profile_img" onerror="this.src = '/images/no_photo.png';">
                        <?php } else { ?>
                            <img src="/images/no_photo.png" alt="" class="img-circle profile_img">
                        <?php } ?>
                    </a>
                </li>
                <li>&nbsp;</li>
            </ul>
        </div>
        <br>
        <h3 class="name">
            <a href="<?= Url::to(Yii::$app->params['urls']['backend'] . 'clients/view/' . $user->id) ?>" target="blank">
                #<?= $user->id ?>
                <?= $user->first_name ?> <?= $user->last_name ?>
            </a>
        </h3>
        <p>
            <?= $user->email ?>
        </p>
        <?php if (!empty($user->phone)) { ?>
        <p>
            <?= $user->phone ?>
        </p>
        <?php } ?>
        <?php if (!empty($user->birth_day)) { ?>
        <p>
            <?= $user->birth_day ?>
        </p>
        <?php } ?>
    </div>
</div>
<?php } ?>