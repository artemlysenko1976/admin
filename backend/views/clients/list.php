<?php

use Yii;
use app\models\Clients;

$this->title = Yii::t('app', 'Клиенты');
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['gender'])) {
    $this->params['js_code'][] = '$("#filter_gender").val("' . Yii::$app->params['filter']['gender'] . '");';
}
if (isset(Yii::$app->params['filter']['status'])) {
    $this->params['js_code'][] = '$("#filter_status").val("' . Yii::$app->params['filter']['status'] . '");';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'add') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/add') ?>" type="submit" class="btn btn-primary add_record_button">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('app', 'Новая запись') ?>
        </a>
        <?php } ?>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post" action="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id) ?>">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-3">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-2">
                    <label for="filter_status"><?= Yii::t('app', 'Статус') ?></label>
                    <select type="text" name="filter[status]" id="filter_status" class="form-control">
                        <option value="">-</option>
                        <?php foreach (Clients::$statuses as $key => $item) { ?>
                            <option value="<?= $key ?>"><?= $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="filter_gender"><?= Yii::t('app', 'Пол') ?></label>
                    <select type="text" name="filter[gender]" id="filter_gender" class="form-control">
                        <option value="">-</option>
                        <?php foreach (Clients::$genders as $key => $item) { ?>
                            <option value="<?= $key ?>"><?= $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_timestamp_range"><?= Yii::t('app', 'Дата создания') ?></label><br>
                    <?= $this->render('/layouts/datepicker', [
                        'name' => 'filter[timestamp_range_from]',
                        'value' => Yii::$app->params['filter']['timestamp_range_from'],
                    ]) ?>
                    <?= $this->render('/layouts/datepicker', [
                        'name' => 'filter[timestamp_range_to]',
                        'value' => Yii::$app->params['filter']['timestamp_range_to'],
                    ]) ?>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Сбросить') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th><?= $this->render('/layouts/sort', ['label' => '#', 'field' => 'id']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', Clients::attributeStaticLabels()['photo']), 'field' => 'photo']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', Clients::attributeStaticLabels()['login']), 'field' => 'login']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', Clients::attributeStaticLabels()['last_name']), 'field' => 'last_name']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', Clients::attributeStaticLabels()['email']), 'field' => 'email']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', Clients::attributeStaticLabels()['phone']), 'field' => 'phone']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', Clients::attributeStaticLabels()['lang']), 'field' => 'lang']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', Clients::attributeStaticLabels()['status']), 'field' => 'status']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', Clients::attributeStaticLabels()['create_timestamp']), 'field' => 'create_timestamp']) ?></th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach ($records as $row) { ?>
                <tr>
                    <td><?= $row['id'] ?></td>
                    <td>
                        <?php if (!empty($row['photo'])) { ?>
                            <img src="/images/clients/<?= $row['id'] ?>/<?= $row['photo'] ?>?t=<?= time() ?>" alt="" class="thumbnail" style="height:70px;" onerror="this.src = '/images/no_avatar.png';">
                        <?php } else { ?>
                            <img src="/images/no_photo.png" alt="" class="thumbnail" style="height:70px;">
                        <?php } ?>
                    </td>
                    <td><?= $row['login'] ?></td>
                    <td><?= $row['last_name'] ?> <?= $row['first_name'] ?> <?= $row['second_name'] ?></td>
                    <td><?= $row['email'] ?></td>
                    <td><?= $row['phone'] ?></td>
                    <td><?= strtoupper($row['lang']) ?></td>
                    <td><?= Clients::$statuses[$row['status']] ?></td>
                    <td><?= date(Yii::$app->params['date_time_format'], $row['create_timestamp']) ?></td>
                    <td>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'view') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/view/' . $row['id']) ?>" class="glyphicon glyphicon-eye-open" title="<?= Yii::t('app', 'Просмотреть') ?>"></a>
                        <?php } ?>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'edit') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $row['id']) ?>" class="glyphicon glyphicon-edit" title="<?= Yii::t('app', 'Редактировать') ?>"></a>
                        <?php } ?>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'delete') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/delete/' . $row['id']) ?>" class="glyphicon glyphicon-remove" title="<?= Yii::t('app', 'Удалить') ?>"></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <?= $this->render('/layouts/pager', [
            'pages' => $pages
        ]) ?>
    <?php } else { ?>
        <?= Yii::t('app', 'Записи не найдены') ?>
    <?php } ?>
</div>