<?php

use Yii;

$this->title = Yii::t('app', 'Редактировать Клиента');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Клиенты'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    
    <?php if (empty($error)) { ?>
        <?php if (!empty($data)) { ?>
            <?= $this->render('form', [
                'data' => $data,
                'countries' => $countries,
            ]) ?>
        <?php } else { ?>
            <?= $this->render('/layouts/record_not_found') ?>
        <?php } ?>
    <?php } else { ?>
        <?= $this->render('/entities/error', [
            'error' => $error
        ]) ?>
    <?php } ?>
</div>