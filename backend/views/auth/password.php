<?php

$this->title = Yii::t('app', 'Change password');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    
    <div class="col-md-9">
        <form id="record_form" method="post" enctype="multipart/form-data">
            <?= $this->render('/layouts/form') ?>
            <?php if ($old_password === true) { ?>
            <div class="form-group">
                <label for="password_old"><?= Yii::t('app', 'Old password') ?></label>
                <input type="password" name="password_old" id="password_old" class="form-control">
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="password"><?= Yii::t('app', 'New password') ?></label>
                <input type="password" name="password" id="password" class="form-control">
            </div>
            <div class="form-group">
                <label for="password_confirm"><?= Yii::t('app', 'New password confirmation') ?></label>
                <input type="password" name="password_confirm" id="password_confirm" class="form-control">
            </div>
            <hr>
            <div class="form-group">
                <button type="button" id="submit_button" class="btn btn-primary" onclick="validate('<?= Yii::$app->controller->id ?>', 'password'); return false;"><?= Yii::t('app', 'Save') ?></button>
            </div>
        </form>
    </div>
</div>