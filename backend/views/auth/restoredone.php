<?php

$this->title = Yii::t('app', 'Restore password');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <p><?= Yii::t('app', 'If e-mail address You entered was found in our database the restore message has been sent to it') ?></p>
</div>