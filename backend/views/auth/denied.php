<?php

use Yii;

$this->title = Yii::t('app', 'Access denied');

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <p><?= Yii::t('app', 'You are not authorized to have access to content of this page') ?></p>
</div>