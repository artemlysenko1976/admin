<?php 

use Yii;
use yii\helpers\Url;
use yii\captcha\Captcha;

?>

<div class="row">
    <div class="col-md-2">
        <img src="/images/logo.png" alt="" />
    </div>
    <div class="col-md-8">
        <h1 class="login_h1">
            <?= Yii::$app->params['projectName'] ?><br>
            <small><?= Yii::t('app', 'Админпанель') ?></small>
        </h1>
    </div>
    <div class="col-md-2">
        
    </div>
</div>

<div class="backend-auth-login">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('app', 'Вход') ?></h3>
        </div>
        <div class="panel-body">
            <form method="post">
                <?= $this->render('/layouts/form.php') ?>
                <div class="form-group">
                    <label for="login"><?= Yii::t('app', 'E-mail') ?></label>
                    <input type="text" name="login" id="login" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password"><?= Yii::t('app', 'Пароль') ?></label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
                <?php if ($need_captcha === true) { ?>
                    <div class="form-group captcha_box">
                    <?= Captcha::widget([
                        'name' => 'captcha',
                    ]); ?>
                    </div>
                <?php } ?>
                <hr>
                <?php if (Yii::$app->session->hasFlash('error')) { ?>
                <div class="form_error"><?= Yii::$app->session->getFlash('error') ?></div>
                <br>
                <?php } ?>
                <button type="submit" class="btn btn-info"><?= Yii::t('app', 'Войти') ?></button>
                <a href="<?= Url::to(Yii::$app->params['urls']['backend'] . 'auth/restore') ?>" style="float:right;"><?= Yii::t('app', 'Забыли пароль?') ?></a>
            </form>
        </div>
    </div>
</div>