<?php

$this->title = Yii::t('app', 'Restore password');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    
    <div class="col-md-9">
        <form id="record_form" method="post" enctype="multipart/form-data">
            <?= $this->render('/layouts/form') ?>
            <div class="form-group">
                <label for="email"><?= Yii::t('app', 'E-mail') ?></label>
                <input type="text" name="email" id="email" class="form-control">
            </div>
            <hr>
            <div class="form-group">
                <button type="submit" id="submit_button" class="btn btn-primary"><?= Yii::t('app', 'Submit') ?></button>
                <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list') ?>" class="btn btn-default cancel_button"><?= Yii::t('app', 'Cancel') ?></a>
            </div>
        </form>
    </div>
</div>