<?php

use app\library\Utils;

/**
 * @var string 'string', 'text', 'html'
 */

$unique_id = Utils::generateRandomString(8);

if (empty($type)) {
    $type = 'string';
}

$tab_name = (isset($base_name) ? $base_name . '_' : '') . $name;
$tab_name = str_replace(
    ['][', '[', ']'], 
    '_', 
    $tab_name
);

if ($type == 'html') {
    foreach (Yii::$app->params['langs'] as $lang_k => $lang) {
        $id = 'l_' . $lang->code . '_' . $tab_name;
        $this->params['js_code'][] = '$("#tab_local_' . $tab_name . '_' . $lang->code . '_label").on("click", function(){'
            . 'setTimeout('
                . 'function(){'
                    . 'editor_' . $id . '[0].refresh();'
                . '},'
                . '100'
            . ');'
        . '});';
    }
}

?>

<?php if (count(Yii::$app->params['langs']) > 1) { ?>
<div id="lang_switchers_box_<?= $unique_id ?>" class="lang_switchers_box">
    <ul id="local_<?= $tab_name ?>_tabs" class="nav nav-tabs" role="tablist">
    <?php foreach (Yii::$app->params['langs'] as $lang_k => $lang) { ?>
        <li id="tab_local_<?= $tab_name ?>_<?= $lang->code ?>_label" role="presentation" class="<?= ($lang_k == 0 ? 'active' : '') ?> lang_switcher lang_switcher_<?= $lang->code ?>">
            <a href="#local_<?= $tab_name ?>_<?= $lang->code ?>_tab" aria-controls="local_<?= $tab_name ?>_<?= $lang->code ?>_tab" role="tab" data-toggle="tab" onclick="synchLangSwitchers('<?= $lang->code ?>', '<?= $unique_id ?>');">
                <?= strtoupper($lang->code) ?>
            </a>
        </li>
    <?php } ?>
    </ul>
    <div class="tab-content">
    <?php foreach (Yii::$app->params['langs'] as $lang_k => $lang) { 
        $field_name = (isset($base_name) ? $base_name . '[l]' : 'l')
            . '[' . $lang->code . '][' . $name . ']';
        $field_id = str_replace(
            ['][', '[', ']'], 
            '_', 
            substr($field_name, 0, -1)
        );
    ?>
        <div role="tabpanel" class="tab-pane <?= ($lang_k == 0 ? 'active' : '') ?> lang_content lang_content_<?= $lang->code ?> lang_content_<?= $type ?>" id="local_<?= $tab_name ?>_<?= $lang->code ?>_tab">
            <div class="form-group">
            <?php if ($type == 'string') { ?>
                <input type="text" name="<?= $field_name ?>" id="<?= $field_id ?>" value="<?= $value[$lang->code][$name] ?>" class="form-control">
            <?php } elseif ($type == 'text') { ?>
                <textarea name="<?= $field_name ?>" id="<?= $field_id ?>" class="form-control" rows="10"><?= $value[$lang->code][$name] ?></textarea>
            <?php } elseif ($type == 'html') { ?>
                <?= $this->render('/layouts/html_editor', [
                    'name' => $field_name,
                    'value' => $value[$lang->code][$name]
                ]) ?>
                <p class="help-block"><a href="javascript:void(0);" onclick="editor_<?= $id ?>[0].refresh();" class="fa fa-refresh" title="<?= Yii::t('app', 'Если HTML редактор неактивен, кликните здесь') ?>"></a></p>
            <?php } ?>
            </div>
        </div>
    <?php } ?>
    </div>
</div>
<?php } else { ?>
    <?php foreach (Yii::$app->params['langs'] as $lang_k => $lang) {
        $field_name = (isset($base_name) ? $base_name . '[l]' : 'l')
            . '[' . $lang->code . '][' . $name . ']';
        $field_id = str_replace(
            ['][', '[', ']'], 
            '_', 
            substr($field_name, 0, -1)
        );
    ?>
        <div class="form-group">
        <?php if ($type == 'string') { ?>
            <input type="text" name="l[<?= $lang->code ?>][<?= $name ?>]" id="l_<?= $lang->code ?>_<?= $name ?>" value="<?= $value[$lang->code][$name] ?>" class="form-control">
        <?php } elseif ($type == 'text') { ?>
            <textarea name="<?= $field_name ?>" id="<?= $field_id ?>" class="form-control"><?= $value[$lang->code][$name] ?></textarea>
        <?php } elseif ($type == 'html') { ?>
            <?= $this->render('/layouts/html_editor', [
                'name' => 'l[' . $lang->code . '][' . $name . ']',
                'value' => $value[$lang->code][$name]
            ]) ?>
            <p class="help-block"><a href="javascript:void(0);" class="fa fa-refresh" onclick="editor_<?= $id ?>[0].refresh();" title="<?= Yii::t('app', 'Если HTML редактор неактивен, кликните здесь') ?>"></a></p>
        <?php } ?>
        </div>
    <?php } ?>
<?php } ?>