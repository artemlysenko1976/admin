<?php

$controls = [
    'bold',
    'italic',
    'underline',
    'strikethrough',
    'style',
    '|',
    'bullets',
    'numbering',
    '|',
    'alignleft',
    'center',
    'alignright',
    'justify',
    '|',
    'undo',
    'redo',
    '|',
    'rule',
    'image',
    'link',
    'unlink',
    '|',
    'cut',
    'copy',
    'paste',
    'pastetext',
    '|',
    'source',
];

$id = str_replace(['][', '[', ']'], ['_', '_', ''], $name);
$this->params['js_code'][] = 'var editor_' . $id . ' = $("#' . $id . '").cleditor({
        controls: // controls to add to the toolbar
            "' . implode(' ', $controls) . '"
    });
    if (!html_editors) {
        var html_editors = {};
    }
    
    html_editors["' . $id . '"] = editor_' . $id . '[0];';

?>

<textarea name="<?= $name ?>" id="<?= $id ?>" class="jqte-test" style="display:none;"><?= $value ?></textarea>