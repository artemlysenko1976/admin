<?php

use Yii;

$alerts_types = [
    'success' => 'success',
    'error' => 'danger',
    'warning' => 'warning',
    'info' => 'info',
];

$alerts = [];
foreach ($alerts_types as $type => $class) {
    if (Yii::$app->session->hasFlash($type)) {
        $alerts[$class] = Yii::$app->session->getFlash($type);
        if (!is_array($alerts[$class])) {
            $alerts[$class] = [$alerts[$class]];
        }
    }
}

?>

<?php if (!empty($alerts)) { ?>
    <?php foreach ($alerts as $class => $class_alerts) { ?>
        <?php foreach ($class_alerts as $flash_v) { ?>
            <div class="row alert_box">
                <div class="alert alert-<?= $class ?>" role="alert"><?= $flash_v ?></div>
            </div>
        <?php } ?>
    <?php } ?>
<?php } ?>