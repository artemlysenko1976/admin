<?php

use Yii;
use app\models\AbstractModel;

?>

<div class="row" style="margin-bottom:10px;">
    <div class="col-md-12" style="text-align:right;">
        <?= Yii::t('app', 'записей на странице') ?>
        &nbsp;
        <div class="dt-buttons btn-group">
            <form id="limit_form" method="post">
                <?= $this->render('/layouts/form') ?>
                <input type="hidden" name="list_limit" id="list_limit" value="">
                <?php foreach (AbstractModel::$list_limits as $limit) { ?>
                    <?php
                        $class = 'default';
                        if (!empty(Yii::$app->params['list_limit'])) {
                            if ($limit == Yii::$app->params['list_limit']) {
                                $class = 'primary';
                            }
                        } else {
                            if ($limit == AbstractModel::$list_limits[0]) {
                                $class = 'primary';
                            }
                        }
                    ?>
                    <button type="button" class="btn btn-<?= $class ?> buttons-html5 btn-sm" onclick="$('#limit_form').find('#list_limit').val(<?= $limit ?>); $('#limit_form').submit();">
                        <span><?= $limit ?></span>
                    </button>
                <?php } ?>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>