<?php

use Yii;

if (empty($action)) {
    $action = 'list';
}

?>

<?php if (!empty($data)) { ?>
    <?php if (!empty($warning)) { ?>
        <div class="alert alert-warning" role="alert">
            <span class="glyphicon glyphicon-warning-sign"></span>
            <?= $warning ?>
        </div>
    <?php } ?>
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <input type="hidden" name="delete" id="delete" value="1">
        <p><?= Yii::t('app', 'Удалить эту запись?') ?></p>
        <button type="submit" id="submit_button" class="btn btn-danger"><?= Yii::t('app', 'Удалить') ?></button>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . $action) ?>" class="btn btn-default cancel_button"><?= Yii::t('app', 'Отменить') ?></a>
    </form>
<?php } else { ?>
    <?= $this->render('/layouts/record_not_found') ?>
<?php } ?>