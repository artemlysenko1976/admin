<?php

use Yii;

$this->params['js_code'][] = 'getImageForCropper("' . $name . '");';

?>

<label class="control-label" for="<?= $name ?>_image"><?= Yii::t('app', 'Image') ?></label><br>
<?php if (!empty($file)) { ?>
    <img src="<?= $file ?>?t=<?= time() ?>" alt="<?= $name ?>" class="thumbnail">
    <div class="checkbox">
        <label>
            <input type="checkbox" name="<?= $name ?>[delete]" id="<?= $name ?>_delete" value="1"> 
            <?= Yii::t('app', 'delete image') ?>
        </label>
    </div>
<?php } ?>
<input type="file" name="<?= $name ?>[image]" id="<?= $name ?>_image" />
<div class="crop_image_box">
    <img id="<?= $name ?>_preview" accept="image/x-png,image/gif,image/jpeg" style="">
</div>

<input type="hidden" name="<?= $name ?>[x]" id="<?= $name ?>_x1" value="">
<input type="hidden" name="<?= $name ?>[y]" id="<?= $name ?>_y1" value="">
<input type="hidden" name="<?= $name ?>[width]" id="<?= $name ?>_w" value="">
<input type="hidden" name="<?= $name ?>[height]" id="<?= $name ?>_h" value="">