<?php

use Yii;
use app\library\Export;

?>

<div class="col-md-9">
    <p><?= Yii::t('app', 'Для фильтрации экспортируемых данных используются результаты поиска в соответствующем разделе') ?></p>
    <form id="export_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <div class="form-group">
            <label for="format"><?= Yii::t('app', 'Формат экспорта') ?></label>
            <select name="format" id="format" class="form-control">
            <?php foreach (Export::$available_formats as $item) { ?>
                <option value="<?= $item ?>"><?= $item ?></option>
            <?php } ?>
            </select>
        </div>
        <?php if (!empty($include)) { ?>
            <?= $this->render($include) ?>
        <?php } ?>
        <hr>
        <div class="form-group">
            <button type="submit" id="submit_button" class="btn btn-primary"><?= Yii::t('app', 'Экспортировать') ?></button>
            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . (!empty($link) ? $link : 'list')) ?>" class="btn btn-default cancel_button"><?= Yii::t('app', 'Отменить') ?></a>
        </div>
    </form>
</div>