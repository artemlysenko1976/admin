<?php

/**
 * Datepicker input
 * 
 * @var $name string
 * @var $value string|integer
 * @var $time boolean
 * @var $readonly boolean default false
 * @var $onclick string
 * @var $onchange string
 * @var $class string
 */

if (is_numeric($value)) {
    $value = date(Yii::$app->params['date_time_format'], $value);
}
$readonly_string = ($readonly === true ? 'readonly' : '');

$replacements = [
    '][' => '_', 
    '[' => '_', 
    ']' => '',
];

if ($time === true) {
    $date_name = $name . '[date]';
    $date_id = str_replace(array_keys($replacements), array_values($replacements), $name) . '_date';
    $hour_name = $name . '[hour]';
    $hour_id = str_replace(array_keys($replacements), array_values($replacements), $name) . '_hour';
    $minute_name = $name . '[minute]';
    $minute_id = str_replace(array_keys($replacements), array_values($replacements), $name) . '_minute';
    
    if (!empty($value)) {
        $time_value = end(explode(' ', $value));
        $time_value = explode(':', $time_value);
        if (!empty($time_value[0])) {
            $this->params['js_code'][] = '$("#' . $hour_id . '").val("' . $time_value[0] . '");';
        }
        if (!empty($time_value[1])) {
            $this->params['js_code'][] = '$("#' . $minute_id . '").val("' . $time_value[1] . '");';
        }
        $value = reset(explode(' ', $value));
    }
} else {
    $date_name = $name;
    $date_id = str_replace(array_keys($replacements), array_values($replacements), $name);
}

if ($readonly !== true) {
    $this->params['js_code'][] = '$("#' . $date_id . '").datepicker({
        dateFormat: "yy-mm-dd"
    });';
}

$events = [];
if (!empty($onchange)) {
    $events[] = 'onchange="' . $onchange . '"';
}
if (!empty($onclick)) {
    $events[] = 'onclick="' . $onclick . '"';
}
$events = implode(' ', $events);

?>

<div class="has-feedback" style="display:inline-block;">
    <input type="text" name="<?= $date_name ?>" id="<?= $date_id ?>" value="<?= $value ?>" class="form-control <?= ($class ?? '') ?>" <?= $events ?> <?= $readonly_string ?> style="padding-left:6px; padding-right:40px; width:120px; display:inline-block">
    <span class="fa fa-calendar form-control-feedback right" aria-hidden="true" style="right:0;"></span>
</div>
<?php if ($time === true) { ?>
    <select name="<?= $hour_name ?>" id="<?= $hour_id ?>" class="form-control <?= ($class ?? '') ?>" <?= $events ?> <?= $readonly_string ?> style="width:60px; display:inline-block">
        <option value="">-</option>
        <?php for ($i = 0; $i < 24; $i++) {
            echo '<option value="' . ($i < 10 ? str_pad($i, 2, '0', STR_PAD_LEFT) : $i) . '">' . ($i < 10 ? str_pad($i, 2, '0', STR_PAD_LEFT) : $i) . '</option>';
        } ?>
    </select> : 
    <select name="<?= $minute_name ?>" id="<?= $minute_id ?>" class="form-control <?= ($class ?? '') ?>" <?= $events ?> <?= $readonly_string ?> style="width:60px; display:inline-block">
        <option value="">-</option>
        <?php for ($i = 0; $i < 60; $i++) {
            echo '<option value="' . ($i < 10 ? str_pad($i, 2, '0', STR_PAD_LEFT) : $i) . '">' . ($i < 10 ? str_pad($i, 2, '0', STR_PAD_LEFT) : $i) . '</option>';
        } ?>
    </select>
<?php } ?>