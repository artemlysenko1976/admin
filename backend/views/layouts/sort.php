<?php

$sort = 'ASC';
if (!empty(Yii::$app->params['sort'])) {
    $current_field = reset(explode(' ', Yii::$app->params['sort']));
    if ($current_field == $field) {
        $current_sort = end(explode(' ', Yii::$app->params['sort']));
        $sort = ($current_sort == 'ASC' ? 'DESC' : 'ASC');
    }
}

?>

<a href="javascript:void(0);" onclick="changeSort('<?= $field ?> <?= $sort ?>');" title="Change sort by '<?= $label ?>'">
    <?= $label ?>
    <?php if (!empty($current_sort)) { ?>
        <span class="glyphicon glyphicon-chevron-<?= ($current_sort == 'ASC' ? 'up' : 'down') ?>"></span>
    <?php } ?>
</a>