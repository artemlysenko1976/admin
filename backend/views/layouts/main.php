<?php

/* @var $this \yii\web\View */
/* @var $content string */

use Yii;
use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

if (!empty(Yii::$app->session->get('auth'))) {
    $this->params['js_code'][] = "checkUserActivity();";
    $this->params['js_code'][] = "$('#menu_toggle').click(storeMenuState)";
    if (Users::checkAccessPermissions('chat', 'index') === true) {
        $this->params['js_code'][] = "chackNewChatMessages();";
        $this->params['js_code'][] = "setInterval(chackNewChatMessages, 10000);";
    }
    if (Users::checkAccessPermissions('recommendations', 'list') === true) {
        $this->params['js_code'][] = "chackNewRecommendations();";
        $this->params['js_code'][] = "setInterval(chackNewRecommendations, 10000);";
    }
}
$this->params['js_code'][] = "expandMenuSection('" . Yii::$app->controller->id . "')";

$backend_menu_state = Yii::$app->session->get('backend_menu_state');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="icon" type="image/png" href="/images/icons/Favicon_16px.png">
</head>
<body class="<?= (empty($backend_menu_state) || $backend_menu_state == 'opened' ? 'nav-md' : 'nav-sm') ?>">
<?php $this->beginBody() ?>
    <div class="container body">
        <div class="main_container">
            <?php if (Yii::$app->controller->id == 'auth' && Yii::$app->controller->action->id == 'login') { ?>
                <div class="col-md-12">
                    <?= $content ?>
                </div>
            <?php } else { ?>
            <!-- left column -->
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="<?= Url::to(Yii::$app->params['urls']['backend']) ?>" class="site_title"><span><?= Yii::$app->params['projectName'] ?></span></a>
                    </div>

                    <div class="clearfix"></div>

                    <?php if (!empty(Yii::$app->session->get('auth'))) { ?>
                    <div class="profile clearfix">
                        <div class="profile_info">
                            <span><?= Yii::t('app', 'Добро пожаловать') ?>,</span>
                            <h2><?= Yii::$app->session->get('auth')['name'] ?></h2>
                        </div>
                    </div>
                    
                    <br />

                    <!-- Menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li>
                                    <a href="<?= Url::to(Yii::$app->params['urls']['backend']) ?>">
                                        <i class="fa fa-home"></i> <?= Yii::t('app', 'Стартовая') ?>
                                    </a>
                                </li>
                            </ul>
                            <?php if (!empty(Yii::$app->params['menu'])) { ?>
                                <?php foreach (Yii::$app->params['menu'] as $menu_controller) { ?>
                                    <ul class="nav side-menu">
                                        <li id="menu_<?= $menu_controller['url'] ?>">
                                            <a>
                                                <i class="fa <?= $menu_controller['class'] ?>"></i> 
                                                <?= $menu_controller['name'] ?>
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="nav child_menu">
                                            <?php foreach ($menu_controller['actions'] as $menu_action) { ?>
                                                <?php $url = Url::to(Yii::$app->params['urls']['backend'] . $menu_controller['url'] . '/' . $menu_action['url']); ?>
                                                <?php if ($menu_action['display'] == true) { ?>
                                                    <li><a href="<?= $url ?>"><?= $menu_action['name'] ?></a></li>
                                                <?php } ?>
                                            <?php } ?>
                                            </ul>
                                        </li>
                                    </ul>        
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- END Left column -->
        
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <?php if (!empty(Yii::$app->session->get('auth'))) { ?>
                        <ul id="top_right_info_box" class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <?= Yii::$app->session->get('auth')['name'] ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="<?= Url::to(Yii::$app->params['urls']['backend'] . 'auth/password') ?>"><i class="fa fa-lock pull-right"></i> <?= Yii::t('app', 'Сменить пароль') ?></a></li>
                                    <li><a href="<?= Url::to(Yii::$app->params['urls']['backend'] . 'auth/logout') ?>"><i class="fa fa-sign-out pull-right"></i> <?= Yii::t('app', 'Выйти') ?></a></li>
                                </ul>
                            </li>
                            <li class="dropdown clock_box" title="<?= Yii::t('app', 'Server time') ?>">
                                <span id="hours"><?= date('H', time()) ?></span>
                                :
                                <span id="min"><?= date('i', time()) ?></span>
                                :
                                <span id="sec"><?= date('s', time()) ?></span>
                                <small><?= date_default_timezone_get() ?></small>
                            </li>
                            <li id="header_alert_box" class="dropdown header_alert_box">
                                <?php if (Users::checkAccessPermissions('chat', 'index') === true) { ?>
                                <span id="header_alert_chat_new_messages_box" class="badge header_alert">
                                    <a href="<?= Url::to(Yii::$app->params['urls']['backend'] . 'chat/index') ?>" title="<?= Yii::t('app', 'Новые сообщения в чате') ?>">
                                        <span class="fa fa-pencil"></span>
                                        <span id="header_alert_chat_new_messages">0</span>
                                    </a>
                                </span>
                                <?php } ?>
                                <?php if (Users::checkAccessPermissions('recommendations', 'list') === true) { ?>
                                <span id="header_alert_new_recommendations_box" class="badge header_alert">
                                    <a href="<?= Url::to(Yii::$app->params['urls']['backend'] . 'recommendations/list?new=1') ?>" title="<?= Yii::t('app', 'Новые заявки') ?>">
                                        <span class="fa fa-money"></span>
                                        <span id="header_alert_new_recommendations">0</span>
                                    </a>
                                </span>
                                <?php } ?>
                            </li>
                        </ul>
                        <?php } ?>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <?= $this->render('/layouts/alerts') ?>
                <?= Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('app', 'Стартовая'), 
                        'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'])
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
                <div class="bottom_delimiter"></div>
                <br>
            </div>
            
            <!-- footer content -->
            <footer>
                <div class="pull-right">
                    <p class="pull-left">&copy; <?= Yii::$app->params['projectName'] ?> <?= date('Y') ?></p>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
            <?php } ?>
        </div>
    </div>
    
    <div id="dictionary_entries" style="display:none;">
        <span id="server_error"><?= Yii::t('app', 'Server error') ?></span>
        <span id="success"><?= Yii::t('app', 'Success') ?></span>
        <span id="success_operation"><?= Yii::t('app', 'Data was saved successfully') ?></span>
    </div>
        
    <form id="sort_form" method="post" style="display:none;">
        <?= $this->render('/layouts/form') ?>
        <input type="hidden" name="sort" id="sort" value="">
    </form>
    
    <?php $this->endBody() ?>
    
    <?php if (!empty($this->params['js_code'])) { ?>
        <?= "\n" . '<script>' ?>
        <?php foreach ($this->params['js_code'] as $js_code) { ?>
            <?= "\n" . $js_code ?>
        <?php } ?>
        <?= "\n" . '</script>' ?>
    <?php } ?>
</body>
</html>
<?php $this->endPage() ?>