<?php

use yii\widgets\LinkPager;

?>

<?= LinkPager::widget([
    'pagination' => $pages,
    'nextPageLabel' => '&rsaquo;',
    'prevPageLabel' => '&lsaquo;',
    'firstPageLabel' => '&laquo;',
    'lastPageLabel' => '&raquo;',
]); ?>