<?php

use Yii;

if (empty($action)) {
    $action = 'edit';
}

?>

<div class="modal fade" id="history_window" tabindex="-1" role="dialog" aria-labelledby="history_window_label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="history_window_label"><?= $name ?> - <?= Yii::t('app', 'История') ?></h4>
            </div>
            <div class="modal-body">
                <div class="panel-group" id="history_accordion" role="tablist" aria-multiselectable="true">
                <?php foreach ($history as $history_data) { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="history_item_label_<?= $history_data['id'] ?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#history_accordion" href="#history_item_content_<?= $history_data['id'] ?>" aria-expanded="true" aria-controls="history_item_content_<?= $history_data['id'] ?>">
                                    <?= $history_data['create_time'] ?> by <?= $history_data['user_name'] ?>
                                </a>
                                <a href="<?= \yii\helpers\Url::to((!empty($url) ? $url : Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . $action . '/' . $history_data['record_id']) . '?restore=' . $history_data['id']) ?>" title="Restore this record" class="glyphicon glyphicon-repeat add_record_button"></a>
                            </h4>
                        </div>
                        <div id="history_item_content_<?= $history_data['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="history_item_label_<?= $history_data['id'] ?>">
                            <table class="table">
                                <tbody>
                                <?php foreach ($history_data['data'] as $key => $item) {
                                    if ($key == 'id' || $key == 'L') {
                                        continue;
                                    } ?>
                                    <tr>
                                        <th style="width:25%;"><?= $key ?></th>
                                        <td style="text-align:left;"><?= nl2br(\yii\helpers\Html::encode($item)) ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <?php if (isset($history_data['data']['L'])) { ?>
                                <div>
                                    <ul id="<?= $history_data['id'] ?>_tabs" class="nav nav-tabs" role="tablist">
                                    <?php foreach ($history_data['data']['L'] as $lang => $value) { ?>
                                        <li id="tab_<?= $history_data['id'] ?>_<?= $lang ?>_label" role="presentation" <?= ($lang == \app\models\Languages::getDefaultLang()->code ? 'class="active"' : '') ?>><a href="#<?= $history_data['id'] ?>_<?= $lang ?>_tab" role="tab" data-toggle="tab"><?= strtoupper($lang) ?></a></li>
                                    <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                    <?php foreach ($history_data['data']['L'] as $lang => $values) { ?>
                                        <div role="tabpanel" class="tab-pane <?= ($lang == \app\models\Languages::getDefaultLang()->code ? 'active' : '') ?>" id="<?= $history_data['id'] ?>_<?= $lang ?>_tab">
                                            <table class="table">
                                                <tbody>
                                                <?php foreach ($values as $value_k => $value_v) { ?>
                                                    <tr>
                                                        <th style="width:25%;"><?= $value_k ?></th>
                                                        <td style="text-align:left;"><?= nl2br(\yii\helpers\Html::encode($value_v)) ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>