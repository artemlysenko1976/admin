<?php 

use app\library\Form;

/**
 * String input
 * 
 * @var $name string
 * @var $id string
 * @var $label string
 * @var $value string|int|float
 * @var $type string 'text', 'password', etc.
 * @var $readonly boolean default false
 * @var $onclick string
 * @var $onchange string
 * @var $placeholder string
 * @var $class string
 * @var $hint string
 */

$name = trim($name);
if (empty($type)) {
    $type = 'text';
}
if (empty($id)) {
    $id = $name;
}
if (!isset($value)) {
    $value = '';
}
if (empty($label)) {
    $label = ucfirst(str_replace(['_'], [' '], $name));
}
$class_string = (!empty($class) ? $class : '');
$onclick_string = Form::createEventlistenersString('onclick', $onclick);
$onchange_string = Form::createEventlistenersString('onchange', $onchange);
$placeholder_string = Form::createAttributeString('placeholder', $placeholder);
$readonly_string = ($readonly === true ? 'readonly' : '');

?>

<div class="form-group <?= (!empty($icon) ? 'has-feedback' : '') ?>">
    <label for="<?= $id ?>" class="control-label"><?= $label ?></label>
    <input type="<?= $type ?>" name="<?= $name ?>" id="<?= $id ?>" value="<?= $value ?>" class="form-control <?= $class_string ?>" <?= $onclick_string ?> <?= $onchange_string ?> <?= $placeholder_string ?> <?= $readonly_string ?>>
    <?php if (!empty($icon)) { ?>
        <span class="fa <?= $icon ?> form-control-feedback right" aria-hidden="true" style="right:0;"></span>
    <?php } ?>
    <?php if (!empty($hint) && is_string($hint)) { ?>
        <p class="help-block"><?= $hint ?></p>
    <?php } ?>
</div>