<?php 

use Yii;

/**
 * String input
 * 
 * @var $name string
 * @var $label string
 * @var $value string|integer
 * @var $time boolean
 * @var $readonly boolean default false
 * @var $onclick string
 * @var $onchange string
 * @var $class string
 * @var $hint string
 */

$name = trim($name);
if (empty($id)) {
    $id = $name;
}
if (empty($type)) {
    $type = 'text';
}
if (empty($value)) {
    $value = '';
} else {
    if (is_int($value)) {
        $value = date(Yii::$app->params['date_time_format'], $value);
    }
}
if (empty($label)) {
    $label = ucfirst(str_replace(['_'], [' '], $name));
}
if (boolval($time) === true) {
    $id_for_label = $id;
} else {
    $id_for_label = $id . '_label';
}

?>

<div class="form-group">
    <label id="<?= $id_for_label ?>" class="control-label"><?= $label ?></label><br>
    <?= $this->render('/layouts/datepicker', [
        'name' => $name,
        'value' => $value,
        'time' => boolval($time),
        'onclick' => $onclick,
        'onchange' => $onchange,
        'class' => $class,
        'readonly' => $readonly,
    ]) ?>
    <?php if (!empty($hint) && is_string($hint)) { ?>
        <p class="help-block"><?= $hint ?></p>
    <?php } ?>
</div>