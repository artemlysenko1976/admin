<?php 

use app\library\Form;

/** 
 * Select with options list
 * 
 * @var $name string 
 * @var $id string
 * @var $label string
 * @var $value string|int|float
 * @var $options array
 * @var $option_key string
 * @var $option_value string|array
 * @var $empty_option boolean
 * @var $readonly boolean default false
 * @var $onclick string
 * @var $onchange string
 * @var $class string
 * @var $hint string
 */

$name = trim($name);
if (empty($id)) {
    $id = $name;
}
if (isset($value)) {
    $this->params['js_code'][] = '$("#' . $id . '").val("' . $value . '");';
}
if (empty($label)) {
    $label = ucfirst(str_replace(['_'], [' '], $name));
}
$class_string = (!empty($class) ? $class : '');
$onclick_string = Form::createEventlistenersString('onclick', $onclick);
$onchange_string = Form::createEventlistenersString('onchange', $onchange);
$readonly_string = ($readonly === true ? 'readonly' : '');

if (!isset($options)) {
    throw new Exception('Options array is required');
}
$options_array = Form::prepareOptions(
    $options, 
    (isset($option_key) ? $option_key : null), 
    (isset($option_value) ? $option_value : null), 
    (!empty($empty_option) ? true : false)
);

?>

<div class="form-group">
    <label for="<?= $id ?>" class="control-label"><?= $label ?></label>
    <select name="<?= $name ?>" id="<?= $id ?>" class="form-control <?= $class_string ?>" <?= $onclick_string ?> <?= $onchange_string ?> <?= $placeholder_string ?> <?= $readonly_string ?>>
        <?php foreach ($options_array as $k => $v) { ?>
            <option value="<?= $k ?>"><?= $v ?></option>
        <?php } ?>
    </select>
    <?php if (!empty($hint) && is_string($hint)) { ?>
        <p class="help-block"><?= $hint ?></p>
    <?php } ?>
</div>