<?php

/**
 * Adding documents fields
 * 
 * @var $name string
 * @var $label string
 * @var $value array
 */

if (empty($name)) {
    $name = 'docs';
}
$processed_name = str_replace(['[', ']'], ['_', ''], $name);

if (!empty($value)) {
    if (!is_array($value)) {
        throw new \Exception('Wrong documents array given');
    }
} else {
    $value = [];
}

?>

<div class="form-group">
    <label id="<?= $processed_name ?>"><?= $label ?></label>
    <div role="tabpanel" class="tab-pane" id="params_tab">
        <div id="docs_form_box">
        <?php if (!empty($value)) { ?>
            <?php foreach ($value as $index => $doc) { ?>
            <div class="form-group" id="new_param_<?= $index ?>_box">
                <input type="text" name="<?= $name ?>[<?= $index ?>][name]" id="docs_<?= $index ?>_name" value="<?= $doc['name'] ?>" class="form-control product_param_input" placeholder="<?= Yii::t('app', 'Название документа') ?>">
                <input type="text" name="<?= $name ?>[<?= $index ?>][link]" id="docs_<?= $index ?>_value" value="<?= $doc['link'] ?>" class="form-control product_param_input" placeholder="<?= Yii::t('app', 'Ссылка на документ') ?>">
                <a href="javascript:void(0);" onclick="$('#new_param_<?= $index ?>_box').remove();" class="glyphicon glyphicon-remove"></a>
            </div>
            <?php } ?>
        <?php } ?>
        </div>
        <input type="hidden" id="docs_counter" value="<?= count($value) ?>">
        <button type="button" id="add_param_button" class="btn btn-default" onclick="addCodeFromTemplate('#new_product_param_tpl', '#docs_form_box', '#docs_counter');" style="margin-bottom:10px;"><?= Yii::t('app', 'Добавить документ') ?></button>
    </div>
</div>