<?php

/**
 * Adding documents fields template
 * 
 * @var $name string
 */

if (empty($name)) {
    $name = 'docs';
}

?>

<div style="display:none;">
    <div id="new_product_param_tpl">
        <div class="form-group" id="new_param_{$counter}_box">
            <input type="text" name="<?= $name ?>[{$counter}][name]" id="docs_{$counter}_name" value="" class="form-control product_param_input" placeholder="<?= Yii::t('app', 'Название документа') ?>">
            <input type="text" name="<?= $name ?>[{$counter}][link]" id="docs_{$counter}_value" value="" class="form-control product_param_input" placeholder="<?= Yii::t('app', 'Ссылка на документ') ?>">
            <a href="javascript:void(0);" onclick="$('#new_param_{$counter}_box').remove();" class="glyphicon glyphicon-remove"></a>
        </div>
    </div>
</div>