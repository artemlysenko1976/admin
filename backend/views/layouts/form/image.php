<?php 

use Yii;
use app\library\Form;

/**
 * Image input and preview
 * 
 * @var $name string
 * @var $id string
 * @var $label string
 * @var $image string
 * @var $url string
 * @var $accept string
 * @var $delete boolean
 * @var $delete_name string
 * @var $readonly boolean default false
 * @var $onclick string
 * @var $onchange string
 * @var $placeholder string
 * @var $class string
 * @var $hint string
 */

$name = trim($name);
if (empty($id)) {
    $id = $name;
}
if (empty($label)) {
    $label = ucfirst(str_replace(['_'], [' '], $name));
}
if (empty($accept)) {
    $accept = 'image/jpeg,image/png,image/gif';
}
if (empty($delete_name)) {
    $delete_name = 'delete_' . $name;
}
$class_string = (!empty($class) ? $class : '');
$onclick_string = Form::createEventlistenersString('onclick', $onclick);
$onchange_string = Form::createEventlistenersString('onchange', $onchange);
$placeholder_string = Form::createAttributeString('placeholder', $placeholder);
$readonly_string = ($readonly === true ? 'readonly' : '');

?>

<div class="form-group">
    <label for="<?= $id ?>" class="control-label"><?= $label ?></label>
    <input type="file" name="<?= $name ?>" id="<?= $id ?>" accept="<?= $accept ?>" class="<?= $class_string ?>" <?= $onclick_string ?> <?= $onchange_string ?> <?= $placeholder_string ?> <?= $readonly_string ?>>
    <?php if (!empty($image) && !empty($url)) { ?>
    <div class="image_preview_box" style="display:block;">
        <div class="thumbnail">
            <img src="<?= $url ?>?t=<?= time() ?>" class="image_preview">
        </div>
        <?php if ($delete === true && $readonly !== true) { ?>
        <br>
        <label for="delete_image">
            <input type="checkbox" name="<?= $delete_name ?>" id="<?= $delete_name ?>" value="1">
            <label for="<?= $delete_name ?>"><?= Yii::t('app', 'удалить изображение') ?></label>
        </label>
        <?php } ?>
    </div>
    <?php } ?>
    <?php if (!empty($hint) && is_string($hint)) { ?>
        <p class="help-block"><?= $hint ?></p>
    <?php } ?>
</div>