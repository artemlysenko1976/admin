<?php 

use Yii;

/**
 * String input with autocomplete
 * 
 * @var $name string
 * @var $id string
 * @var $label string
 * @var $value string|int|float
 * @var $url string
 * @var $readonly boolean default false
 * @var $onclick string
 * @var $onchange string
 * @var $placeholder string
 * @var $class string
 * @var $hint string
 * @var $icon string
 */

if (empty($id)) {
    $id = $name;
}
if (empty($url)) {
    throw new \Exception('No autocomplete URL given');
}
if (empty($icon)) {
    $icon = 'fa-list';
}

$events = [];
if (!empty($onchange)) {
    if (is_array($onchange)) {
        foreach ($onchange as $v) {
            $events[] = $v;
        }
    } elseif (is_string($onchange)) {
        $events[] = $onchange;
    }
}

$js_code = '$("#' . $id . '").autocomplete({
    source: "/backend/' . $url . '",
    minLength: 2';
    if (!empty($events)) {
        $js_code .= ', select: function( event, ui ) {';
            $js_code .= implode('; ', $events);
        $js_code .= '}';
    }
$js_code .= '});';

$this->params['js_code'][] = $js_code

?>

<?= $this->render('/layouts/form/string', [
    'name' => $name,
    'id' => $id,
    'label' => $label,
    'value' => $value,
    'type' => 'text',
    'onclick' => $onclick,
    'onchange' => $onchange,
    'class' => $class,
    'placeholder' => Yii::t('app', 'введите текст для поиска...'),
    'icon' => $icon,
    'hint' => (!empty($hint) ? $hint : null),
    'readonly' => $readonly,
]) ?>