<?php 

use app\library\Form;

/** 
 * Group of radio checkboxes
 * 
 * @var $name string 
 * @var $id string
 * @var $label string
 * @var $value string|int|float|array
 * @var $options array
 * @var $option_key string
 * @var $option_value string
 * @var $readonly boolean default false
 * @var $onclick string
 * @var $onchange string
 * @var $class string
 * @var $hint string
 */

$name = trim($name);
if (empty($id)) {
    $id = $name;
}
if (isset($value)) {
    if (!is_array($value)) {
        $value = [$value];
    }
    foreach ($value as $value_v) {
        if (!is_scalar($value_v)) {
            throw new Exception('Value must be a scalar or array of scalars');
        }
        $this->params['js_code'][] = '$("#' . Form::createOptionId($name, $value_v) . '").attr("checked", true);';
    }
}
if (empty($label)) {
    $label = ucfirst(str_replace(['_'], [' '], $name));
}
$class_string = (!empty($class) ? $class : '');
$onclick_string = Form::createEventlistenersString('onclick', $onclick);
$onchange_string = Form::createEventlistenersString('onchange', $onchange);
$readonly_string = ($readonly === true ? 'readonly' : '');

if (!isset($options)) {
    throw new Exception('Options array is required');
}
$options_array = Form::prepareOptions(
    $options, 
    (isset($option_key) ? $option_key : null), 
    (isset($option_value) ? $option_value : null), 
    (!empty($empty_option) ? true : false)
);

?>

<div class="form-group">
    <label id="<?= $id ?>" class="control-label"><?= $label ?></label>
    <div class="row">
        <?php foreach ($options_array as $k => $v) { ?>
            <?php 
                $option_id = $name . '_' . str_replace([' '], ['_'], $k);
            ?>
            <div class="col-md-3">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="<?= $name ?>[]" id="<?= Form::createOptionId($name, $k) ?>" value="<?= $k ?>" class="<?= $class_string ?>" <?= $onclick_string ?> <?= $onchange_string ?> <?= $readonly_string ?>> 
                        <?= $v ?>
                    </label>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php if (!empty($hint) && is_string($hint)) { ?>
        <p class="help-block"><?= $hint ?></p>
    <?php } ?>
</div>