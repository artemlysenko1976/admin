<?php 

use Yii;

/**
 * Submit and cancel buttons
 * 
 * @var $id string
 * @var $validate_controller string
 * @var $validate_action string
 * @var $cancel_action string
 * @var $data object
 */

if (empty($id)) {
    $id = 'submit_button';
}
if (empty($validate_controller)) {
    $validate_controller = Yii::$app->controller->id;
}
if (empty($validate_action)) {
    $validate_action = 'add';
}
if (empty($cancel_action)) {
    $cancel_action = 'list';
}

?>

<div class="form-group">
    <button type="button" id="submit_button" class="btn btn-primary" onclick="validate('<?= $validate_controller ?>', '<?= $validate_action ?>'); return false;"><?= Yii::t('app', 'Сохранить') ?></button>
    <?php if (!empty(Yii::$app->request->get('restore')) && !empty($data)) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id . '/' . $data['id']) ?>" class="btn btn-default cancel_button"><?= Yii::t('app', 'Вернуться к текущему варианту') ?></a>
    <?php } ?>
    <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . $cancel_action) ?>" class="btn btn-default cancel_button"><?= Yii::t('app', 'Отменить') ?></a>
</div>