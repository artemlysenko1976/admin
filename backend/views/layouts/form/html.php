<?php 

use Yii;
use app\library\Form;

/**
 * WYSIWYG HTML editor
 * 
 * @var $name string
 * @var $id string
 * @var $label string
 * @var $value string|integer|float
 * @var $hint string
 */

$name = trim($name);
if (empty($id)) {
    $id = $name;
}
if (empty($value)) {
    $value = '';
}
if (empty($label)) {
    $label = ucfirst(str_replace(['_'], [' '], $name));
}

?>

<div class="form-group">
    <label for="<?= $id ?>" class="control-label"><?= $label ?></label>
    <?= $this->render('/layouts/html_editor', [
        'name' => $name,
        'id' => $id,
        'value' => $value
    ]) ?>
    <p class="help-block"><a href="javascript:void(0);" onclick="editor_<?= $id ?>[0].refresh();" class="fa fa-refresh" title="<?= Yii::t('app', 'Если HTML редактор неактивен, кликните здесь') ?>"></a></p>
    <?php if (!empty($hint) && is_string($hint)) { ?>
        <p class="help-block"><?= $hint ?></p>
    <?php } ?>
</div>