<?php 

use app\library\Form;

/**
 * Textarea
 * 
 * @var $name string
 * @var $id string
 * @var $label string
 * @var $value string|integer|float
 * @var $readonly boolean default false
 * @var $onclick string
 * @var $onchange string
 * @var $class string
 * @var $placeholder string
 * @var $rows integer
 * @var $hint string
 */

$name = trim($name);
if (empty($id)) {
    $id = $name;
}
if (empty($value)) {
    $value = '';
}
if (empty($label)) {
    $label = ucfirst(str_replace(['_'], [' '], $name));
}
if (empty($rows) || !is_numeric($rows) || $rows <= 0) {
    $rows = 5;
}
$class_string = (!empty($class) ? $class : '');
$onclick_string = Form::createEventlistenersString('onclick', $onclick);
$onchange_string = Form::createEventlistenersString('onchange', $onchange);
$placeholder_string = Form::createAttributeString('placeholder', $placeholder);
$readonly_string = ($readonly === true ? 'readonly' : '');

?>

<div class="form-group">
    <label for="<?= $id ?>" class="control-label"><?= $label ?></label>
    <textarea name="<?= $name ?>" id="<?= $id ?>" class="form-control <?= $class_string ?>" rows="<?= $rows ?>" <?= $onclick_string ?> <?= $onchange_string ?> <?= $placeholder_string ?> <?= $readonly_string ?>><?= $value ?></textarea>
    <?php if (!empty($hint) && is_string($hint)) { ?>
        <p class="help-block"><?= $hint ?></p>
    <?php } ?>
</div>