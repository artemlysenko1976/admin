<?php 

use app\library\Form;

/**
 * Two radio buttons to switch TRUE and FALSE value
 * 
 * @var $name string
 * @var $id string
 * @var $label string
 * @var $label_true string
 * @var $label_false string
 * @var $value integer|boolean (true, false, 1, 0)
 * @var $readonly boolean default false
 * @var $onclick string
 * @var $onchange string
 * @var $class string
 * @var $hint string
 */

$name = trim($name);
if (empty($id)) {
    $id = $name;
}
$id_true = $id . '_true';
$id_false = $id . '_false';

if ($value === true || $value === false || $value === 1 || $value === 0) {
    if (boolval($value) === true) {
        $this->params['js_code'][] = '$("#' . $id_true . '").attr("checked", true);';
        $this->params['js_code'][] = '$("#' . $id_true . '").closest("label").addClass("active");';
    } else {
        $this->params['js_code'][] = '$("#' . $id_false . '").attr("checked", true);';
        $this->params['js_code'][] = '$("#' . $id_false . '").closest("label").addClass("active");';
    }
} else {
    $this->params['js_code'][] = '$("#' . $id_true . '").attr("checked", true);';
    $this->params['js_code'][] = '$("#' . $id_true . '").closest("label").addClass("active");';
}
if (empty($label)) {
    $label = ucfirst(str_replace(['_'], [' '], $name));
}
if (empty($label_true)) {
    $label_true = '<span class="glyphicon glyphicon-ok"></span>';
}
if (empty($label_false)) {
    $label_false = '<span class="glyphicon glyphicon-remove"></span>';
}
$class_string = (!empty($class) ? $class : '');
$onclick_string = Form::createEventlistenersString('onclick', $onclick);
$onchange_string = Form::createEventlistenersString('onchange', $onchange);
$readonly_string = ($readonly === true ? 'readonly' : '');

?>

<div class="form-group">
    <label id="<?= $id ?>" class="control-label"><?= $label ?></label><br>
    <div class="btn-group" data-toggle="buttons">
        <label class="btn btn-default">
            <input type="radio" name="<?= $name ?>" id="<?= $id_true ?>" value="1" class="<?= $class_string ?>" <?= $onclick_string ?> <?= $onchange_string ?> <?= $readonly_string ?> autocomplete="off">
            <?= $label_true ?>
        </label>
        <label class="btn btn-default">
            <input type="radio" name="<?= $name ?>" id="<?= $id_false ?>" value="0" class="<?= $class_string ?>" <?= $onclick_string ?> <?= $onchange_string ?> <?= $readonly_string ?> autocomplete="off">
            <?= $label_false ?>
        </label>
    </div>
    <?php if (!empty($hint) && is_string($hint)) { ?>
        <p class="help-block"><?= $hint ?></p>
    <?php } ?>
</div>