<?php 

use app\library\Form;

/**
 * File uploading input
 * 
 * @var $name string
 * @var $id string
 * @var $label string
 * @var $file string
 * @var $accept string
 * @var $multiple boolean
 * @var $readonly boolean default false
 * @var $onclick string
 * @var $onchange string
 * @var $placeholder string
 * @var $class string
 * @var $hint string
 */

$name = trim($name);
if (empty($id)) {
    $id = $name;
}
if (empty($label)) {
    $label = ucfirst(str_replace(['_'], [' '], $name));
}
$accept = (!empty($accept) ? 'accept="' . $accept . '"' : '');
$multiple = ($multiple == true ? 'multiple=""' : '');

$class_string = (!empty($class) ? $class : '');
$onclick_string = Form::createEventlistenersString('onclick', $onclick);
$onchange_string = Form::createEventlistenersString('onchange', $onchange);
$placeholder_string = Form::createAttributeString('placeholder', $placeholder);
$readonly_string = ($readonly === true ? 'readonly' : '');

?>

<div class="form-group">
    <label for="<?= $id ?>" class="control-label"><?= $label ?></label>
    <input type="file" name="<?= $name ?>" id="<?= $id ?>" <?= $accept ?> class="<?= $class_string ?>" <?= $onclick_string ?> <?= $onchange_string ?> <?= $placeholder_string ?> <?= $multiple ?> <?= $readonly_string ?>>
    <?php if (!empty($hint) && is_string($hint)) { ?>
        <p class="help-block"><?= $hint ?></p>
    <?php } ?>
</div>