<?php

use Yii;
use app\models\Users;

$this->title = Yii::t('app', 'Просмотр администратора');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Администраторы'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

if (!empty($data)) {
    $docs = null;
    if (!empty($data['docs'])) {
        $docs = json_decode($data['docs'], true);
    }
}

?>

<div class="backend-default-index">
    <h1>
        #<?= $data['id'] ?> <?= $data['last_name'] ?> <?= $data['first_name'] ?>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'delete') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/delete/' . $data['id']) ?>" class="btn btn-danger add_record_button">
            <span class="glyphicon glyphicon-remove"></span>
            <?= Yii::t('app', 'Удалить') ?>
        </a>
        <?php } ?>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'edit') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $data['id']) ?>" class="btn btn-default add_record_button">
            <span class="glyphicon glyphicon-edit"></span>
            <?= Yii::t('app', 'Редактировать') ?>
        </a>
        <?php } ?>
    </h1>
    <hr>
    
    <?php if (!empty($data)) { ?>
<!-- General information -->
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?= Yii::t('app', 'Общая исформация') ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-3">
                                <?php if (!empty($data['photo'])) { ?>
                                    <img src="/images/users/<?= $data['photo'] ?>?t=<?= time() ?>" alt="" class="thumbnail" style="height:150px;" onerror="this.src = '/images/no_avatar.png';">
                                <?php } else { ?>
                                    <img src="/images/no_photo.png" alt="" class="thumbnail" style="height:150px;">
                                <?php } ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Yii::t('app', 'Имя') ?></label><br>
                                    <?= mb_strtoupper($data['last_name']) ?> 
                                    <?= $data['first_name'] ?>
                                    <?= (!empty($data['second_name']) ? $data['second_name'] : '') ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Yii::t('app', 'Роль') ?></label><br>
                                    <?= Users::$roles[$data['role']] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Yii::t('app', 'Права доступа') ?></label><br>
                                    <?php if ($data['basic'] == 1) { ?>
                                        <?= Yii::t('app', 'Все права') ?>
                                    <?php } else { ?>
                                        <?php foreach ($data['groups'] as $groups_v) { ?>
                                            <?php if ($groups_v->group->id > 1) { ?>
                                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . 'groups/edit/' . $groups_v->group->id) ?>" target="_blank">
                                            <?php } ?>
                                                <?= $groups_v->group->name ?>
                                            <?php if ($groups_v->group->id > 1) { ?>
                                            </a>
                                            <?php } ?>
                                            <br>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Yii::t('app', 'Активность') ?></label><br>
                                    <?= $this->render('/layouts/boolean', ['field' => $data['active']]) ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Yii::t('app', 'E-mail') ?></label><br>
                                    <?= $data['email'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Yii::t('app', 'Телефон') ?></label><br>
                                    <?= $data['phone'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Yii::t('app', 'Дата рождения') ?></label><br>
                                    <?= $data['birth_day'] ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Yii::t('app', 'Серия и номер паспорта') ?></label><br>
                                    <?= $data['passport'] ?>
                                </div>
                                <?php if (!empty($docs)) { ?>
                                <div class="form-group">
                                    <label class="control-label"><?= Yii::t('app', 'Ссылки на документы') ?></label><br>
                                    <?php foreach ($docs as $doc) { ?>
                                        <span class="fa fa-file"></span>
                                        <a href="<?= $doc['link'] ?>" target="_blank"><?= $doc['name'] ?></a>
                                        <br>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <hr>
        <div class="form-group">
            <a href="<?= \yii\helpers\Url::to('/backend/' . Yii::$app->controller->id . '/list') ?>" class="btn btn-default"><?= Yii::t('app', 'Назад') ?></a>
        </div>
    <?php } else { ?>
        <?= $this->render('/layouts/record_not_found'); ?>
    <?php } ?>
</div>