<?php

use Yii;

$this->title = Yii::t('app', 'Добавить администратора');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Администраторы'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('form',[
        'permissions_groups' => $permissions_groups
    ]) ?>
</div>