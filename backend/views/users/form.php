<?php

use app\models\Users;
use Yii;

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <?php if ($data['basic'] == 0) { ?>
            <?= $this->render('/layouts/form/select', [
                'name' => 'role',
                'label' => Users::attributeStaticLabels()['role'],
                'value' => $data['role'],
                'options' => Users::$roles,
                'empty_option' => true,
                'onchange' => 'setUserPermissionsGroup(this.value)',
            ]) ?>
        <?php } ?>
        <hr>
        <?= $this->render('/layouts/form/string', [
            'name' => 'email',
            'label' => Users::attributeStaticLabels()['email'],
            'value' => $data['email'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'password',
            'label' => Users::attributeStaticLabels()['password'],
            'value' => '',
            'type' => 'password',
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'password_confirm',
            'label' => Users::attributeStaticLabels()['password_confirm'],
            'value' => '',
            'type' => 'password',
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/string', [
            'name' => 'first_name',
            'label' => Users::attributeStaticLabels()['first_name'],
            'value' => $data['first_name'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'second_name',
            'label' => Users::attributeStaticLabels()['second_name'],
            'value' => $data['second_name'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'last_name',
            'label' => Users::attributeStaticLabels()['last_name'],
            'value' => $data['last_name'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/string', [
            'name' => 'phone',
            'label' => Users::attributeStaticLabels()['phone'],
            'value' => $data['phone'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/image', [
            'name' => 'photo',
            'label' => Users::attributeStaticLabels()['photo'],
            'image' => $data['photo'],
            'url' => '/images/users/' . $data['photo'],
            'delete' => true,
        ]) ?>
        <?php if ($data['basic'] == 0) { ?>
            <hr>
            <?= $this->render('/layouts/form/checkbox', [
                'name' => 'groups',
                'label' => Users::attributeStaticLabels()['groups'],
                'options' => $permissions_groups,
                'option_key' => 'id',
                'option_value' => 'name',
                'value' => $data['groups_ids'],
                'class' => 'groups_checkboxes',
            ]) ?>
            <?= $this->render('/layouts/form/boolean', [
                'name' => 'active',
                'label' => Users::attributeStaticLabels()['active'],
                'value' => $data['active'],
            ]) ?>
        <?php } ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'data' => $data,
        ]) ?>
    </form>
    
    <script>
        var groups = {};
        <?php foreach ($permissions_groups as $group) { ?>
            <?php if (empty($group->key)) { 
                continue;
            } ?>
            groups.<?= $group->key ?> = <?= $group->id ?>;
        <?php } ?>
    </script>
    
    <?= $this->render('/layouts/form/documents_tpl') ?>
</div>