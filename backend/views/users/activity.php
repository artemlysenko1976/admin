<?php

use Yii;

$this->title = Yii::t('app', 'Лог активности');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Администраторы'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['operation'])) {
    $this->params['js_code'][] = '$("#filter_operation").val("' . Yii::$app->params['filter']['operation'] . '");';
}
if (isset(Yii::$app->params['filter']['user_id'])) {
    $this->params['js_code'][] = '$("#filter_user_id").val("' . Yii::$app->params['filter']['user_id'] . '");';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post" action="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id) ?>">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-3">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-2">
                    <label for="filter_user_id"><?= Yii::t('app', 'Администратор') ?></label>
                    <select type="text" name="filter[user_id]" id="filter_user_id" class="form-control">
                        <option value="">-</option>
                        <?php foreach ($users as $item) { ?>
                            <option value="<?= $item->id ?>"><?= $item->name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="filter_operation"><?= Yii::t('app', 'Действие') ?></label>
                    <select type="text" name="filter[operation]" id="filter_operation" class="form-control">
                        <option value="">-</option>
                        <?php foreach ($operations as $item) { ?>
                            <option value="<?= $item ?>"><?= $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_date_range"><?= Yii::t('app', 'Даты') ?></label><br>
                    <?= $this->render('/layouts/datepicker', [
                        'name' => 'filter[date_range_from]',
                        'value' => Yii::$app->params['filter']['date_range_from'],
                    ]) ?>
                    <?= $this->render('/layouts/datepicker', [
                        'name' => 'filter[date_range_to]',
                        'value' => Yii::$app->params['filter']['date_range_to'],
                    ]) ?>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Reset filter') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Администратор'), 'field' => 'user_name']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Действие'), 'field' => 'operation']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Объект'), 'field' => 'object_name_']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Время'), 'field' => 'create_time']) ?></th>
            </tr>
            <?php foreach ($records as $row) { ?>
                <tr>
                    <td style="width:auto;"><?= $row['user_name'] ?> (#<?= $row['user_id'] ?>)</td>
                    <td><?= $row['operation'] ?></td>
                    <td>
                    <?php if (!empty($row['object_id'])) { ?>
                        <?= $row['object_name_'] ?>
                        (#<?= $row['object_id'] ?>)
                    <?php } ?>
                    </td>
                    <td style="width:auto; text-align:left;"><?= $row['create_time'] ?></td>
                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
        <?= Yii::t('app', 'No records found') ?>
    <?php } ?>
    
    <?= $this->render('/layouts/pager', [
        'pages' => $pages
    ]) ?>
</div>