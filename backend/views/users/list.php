<?php

use Yii;
use app\models\Users;

$this->title = Yii::t('app', 'Администраторы');
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['active'])) {
    $this->params['js_code'][] = '$("#filter_active").val("' . Yii::$app->params['filter']['active'] . '");';
}
if (isset(Yii::$app->params['filter']['role'])) {
    $this->params['js_code'][] = '$("#filter_role").val("' . Yii::$app->params['filter']['role'] . '");';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'add') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/add') ?>" type="submit" class="btn btn-primary add_record_button">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('app', 'Новая запись') ?>
        </a>
        <?php } ?>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post" action="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id) ?>">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-3">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="filter_role"><?= Yii::t('app', 'Роль') ?></label>
                    <select type="text" name="filter[role]" id="filter_role" class="form-control">
                        <option value="">-</option>
                        <?php foreach (Users::$roles as $k => $v) { ?>
                            <option value="<?= $k ?>"><?= Yii::t('app', $v) ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_active"><?= Yii::t('app', 'Аксивность') ?></label>
                    <select type="text" name="filter[active]" id="filter_active" class="form-control">
                        <option value="">-</option>
                        <option value="1"><?= Yii::t('app', 'Активный') ?></option>
                        <option value="0"><?= Yii::t('app', 'неактивный') ?></option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Сбросить') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th><?= $this->render('/layouts/sort', ['label' => '#', 'field' => 'id']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Users::attributeStaticLabels()['photo'], 'field' => 'photo']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Users::attributeStaticLabels()['name'], 'field' => 'name']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Users::attributeStaticLabels()['role'], 'field' => 'role']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Users::attributeStaticLabels()['email'], 'field' => 'email']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Users::attributeStaticLabels()['phone'], 'field' => 'phone']) ?></th>
                <th><?= Users::attributeStaticLabels()['groups'] ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Users::attributeStaticLabels()['active'], 'field' => 'active']) ?></th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach ($records as $row) { ?>
                <tr>
                    <td><?= $row['id'] ?></td>
                    <td>
                        <?php if (!empty($row['photo'])) { ?>
                            <img src="/images/users/<?= $row['photo'] ?>?t=<?= time() ?>" alt="" class="thumbnail" style="height:70px;" onerror="this.src = '/images/no_avatar.png';">
                        <?php } else { ?>
                            <img src="/images/no_photo.png" alt="" class="thumbnail" style="height:70px;">
                        <?php } ?>
                    </td>
                    <td><?= $row['name'] ?></td>
                    <td><?= Users::$roles[$row['role']] ?></td>
                    <td><?= $row['email'] ?></td>
                    <td><?= $row['phone'] ?></td>
                    <td>
                    <?php if ($row['basic'] == 1) { ?>
                        <?= Yii::t('app', 'Все права') ?>
                    <?php } else { ?>
                        <?php foreach ($row['groups'] as $groups_v) { ?>
                            <?php if ($groups_v->group->id > 1) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . 'groups/edit/' . $groups_v->group->id) ?>" target="_blank">
                            <?php } ?>
                                <?= $groups_v->group->name ?>
                            <?php if ($groups_v->group->id > 1) { ?>
                            </a>
                            <?php } ?>
                            <br>
                        <?php } ?>
                    <?php } ?>
                    </td>
                    <td><?= $this->render('/layouts/boolean', ['field' => $row['active']]) ?></td>
                    <td>
                        <?php if ($row['basic'] == 0 && app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'view') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/view/' . $row['id']) ?>" class="glyphicon glyphicon-eye-open" title="<?= Yii::t('app', 'Просмотреть') ?>"></a>
                        <?php } ?>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'edit') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $row['id']) ?>" class="glyphicon glyphicon-edit" title="<?= Yii::t('app', 'Редактировать') ?>"></a>
                        <?php } ?>
                        <?php if ($row['basic'] == 0 && app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'delete') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/delete/' . $row['id']) ?>" class="glyphicon glyphicon-remove" title="<?= Yii::t('app', 'Удалить') ?>"></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <?= $this->render('/layouts/pager', [
            'pages' => $pages
        ]) ?>
    <?php } else { ?>
        <?= Yii::t('app', 'Записи не найдены') ?>
    <?php } ?>
</div>