<?php

use Yii;

$this->title = 'Добавить переменную';
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Переменные'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/varslist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('varsform') ?>
</div>