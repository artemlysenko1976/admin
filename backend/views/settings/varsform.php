<?php

use app\models\Variables;

$this->params['js_code'][] = 'setTimeout(function(){toggleVarsTypesValues($("#type").val());}, 500);';

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'key',
            'id' => 'key',
            'label' => Variables::attributeStaticLabels()['key'],
            'value' => $data['key'],
        ]) ?>
        <?= $this->render('/layouts/form/select', [
            'name' => 'type',
            'label' => Variables::attributeStaticLabels()['type'],
            'value' => $data['type'],
            'options' => Variables::$types,
            'onchange' => 'toggleVarsTypesValues(this.value);'
        ]) ?>
        <div id="value_STRING_box" class="value_box" style="display:none;">
            <?= $this->render('/layouts/form/string', [
                'name' => 'value[STRING]',
                'id' => 'value_STRING',
                'label' => Variables::attributeStaticLabels()['value'],
                'value' => ($data['type'] == 'STRING' ? $data['value'] : ''),
            ]) ?>
        </div>
        <div id="value_INTEGER_box" class="value_box" style="display:none;">
            <?= $this->render('/layouts/form/string', [
                'name' => 'value[INTEGER]',
                'id' => 'value_INTEGER',
                'label' => Variables::attributeStaticLabels()['value'],
                'value' => ($data['type'] == 'INTEGER' ? $data['value'] : ''),
            ]) ?>
        </div>
        <div id="value_FLOAT_box" class="value_box" style="display:none;">
            <?= $this->render('/layouts/form/string', [
                'name' => 'value[FLOAT]',
                'id' => 'value_FLOAT',
                'label' => Variables::attributeStaticLabels()['value'],
                'value' => ($data['type'] == 'FLOAT' ? $data['value'] : ''),
            ]) ?>
        </div>
        <div id="value_BOOLEAN_box" class="value_box" style="display:none;">
            <?= $this->render('/layouts/form/boolean', [
                'name' => 'value[BOOLEAN]',
                'id' => 'value_BOOLEAN',
                'label' => Variables::attributeStaticLabels()['value'],
                'value' => ($data['type'] == 'BOOLEAN' ? intval($data['value']) : ''),
            ]) ?>
        </div>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'validate_action' => 'varsadd',
            'cancel_action' => 'varslist',
            'data' => $data,
        ]) ?>
    </form>
</div>