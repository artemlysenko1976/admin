<?php

use Yii;
use app\models\Variables;

$this->title = Yii::t('app', 'Переменные');
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['type'])) {
    $this->params['js_code'][] = '$("#filter_type").val("' . Yii::$app->params['filter']['type'] . '");';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'varsadd') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/varsadd') ?>" type="submit" class="btn btn-primary add_record_button">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('app', 'Новая запись') ?>
        </a>
        <?php } ?>
    </h1>
    
    <div class="alert alert-warning" role="alert">
        <span class="glyphicon glyphicon-alert"></span>
        &nbsp;&nbsp;
        <?= Yii::t('app', 'ВНИМАНИЕ! Удаление переменных, а так же любое неосторожное их редактирование может привести к проблемам в работе системы') ?>
    </div>
    
    <div class="x_panel">
        <form id="filter_form" method="post">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-3">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="filter_type"><?= Yii::t('app', 'Тип') ?></label>
                    <select name="filter[type]" id="filter_type" class="form-control">
                        <option value="">-</option>
                        <?php foreach (Variables::$types as $k => $item) { ?>
                            <option value="<?= $k ?>"><?= $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Сбросить') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <div class="table_container">
            <table class="table table-striped table-bordered table-hover">
                <tr>
                    <th><?= $this->render('/layouts/sort', ['label' => '#', 'field' => 'id']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Variables::attributeStaticLabels()['type'], 'field' => 'type']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Variables::attributeStaticLabels()['key'], 'field' => 'key']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Variables::attributeStaticLabels()['value'], 'field' => 'value']) ?></th>
                    <th>&nbsp;</th>
                </tr>
                <?php foreach ($records as $row) { ?>
                    <tr>
                        <td><?= $row['id'] ?></td>
                        <td><?= $row['type'] ?></td>
                        <td><?= $row['key'] ?></td>
                        <td>
                            <?php if ($row['type'] == 'BOOLEAN') { ?>
                                <?= $this->render('/layouts/boolean', ['field' => $row['value']]) ?>
                            <?php } else { ?>
                                <?= $row['value'] ?>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'varsedit') === true) { ?>
                                <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/varsedit/' . $row['id']) ?>" class="glyphicon glyphicon-edit" title="Edit"></a>
                            <?php } ?>
                            <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'varsdelete') === true) { ?>
                                <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/varsdelete/' . $row['id']) ?>" class="glyphicon glyphicon-remove" title="Delete"></a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?= $this->render('/layouts/pager', [
                'pages' => $pages
            ]) ?>
        </div>
    <?php } else { ?>
        <?= Yii::t('app', 'Записи не найдены') ?>
    <?php } ?>
</div>