<?php

use Yii;

$this->title = Yii::t('app', 'Site Settings');
$this->params['breadcrumbs'][] = $this->title;

if (!empty($data)) {
    $this->params['js_code'][] = '$("#maintenance").val(' . $data->maintenance . ')';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
    </h1>
    
    <div class="col-md-9">
        <form id="record_form" method="post" enctype="multipart/form-data">
            <?= $this->render('/layouts/form') ?>
            <?php if (!empty($data)) {
                echo '<input type="hidden" name="id" id="id" value="' . $data['id'] . '">'; 
            } ?>
            <?= $this->render('/layouts/form/boolean', [
                'name' => 'maintenance',
                'label' => Yii::t('app', 'Maintenance mode'),
                'value' => $data['maintenance'],
                'readonly' => !$full_edit,
            ]) ?>
            <hr>
            <div class="form-group">
                <button type="button" id="submit_button" class="btn btn-primary" onclick="validate('<?= Yii::$app->controller->id ?>', 'add'); return false;"><?= Yii::t('app', 'Save') ?></button>
            </div>
        </form>
    </div>
</div>