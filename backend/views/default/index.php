<?php

use Yii;
use yii\helpers\Url;

?>

<div class="backend-default-index">
    <h1><?= Yii::$app->params['projectName'] ?> <small><?= Yii::t('app', 'Админпанель') ?></small></h1>
    <hr>
    <?php if (!empty(Yii::$app->params['menu'])) { ?>
        <div class="x_panel">
            <div class="x_title">
                <h2><?= Yii::t('app', 'Доступные операции') ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <?php $counter = 0 ?>
                <?php foreach (Yii::$app->params['menu'] as $k => $item) { ?>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div>
                                    <i class="fa <?= $item['class'] ?>"></i>&nbsp;
                                    <b><?= $item['name'] ?></b>
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php foreach ($item['actions'] as $action) { ?>
                                    <?php if ($action['check'] == true) { ?>
                                    <div><?= $action['name'] ?></div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php if (($counter + 1) % 4 == 0) { ?>
                        </div>
                        <div class="row">
                    <?php } ?>
                    <?php $counter++; ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>