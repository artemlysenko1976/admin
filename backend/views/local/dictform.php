<?php

use Yii;

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'key',
            'label' => Yii::t('app', 'Ключ'),
            'value' => $data['key'],
        ]) ?>
        <div class="form-group">
            <label>Entry</label>
            <?= $this->render('/layouts/local_field', [
                'name' => Yii::t('app', 'Запись'),
                'value' => $data['locals'],
                'type' => 'text',
            ]) ?>
        </div>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'validate_action' => 'dictadd',
            'cancel_action' => 'dictlist/' . $parent_record->id,
            'data' => $data,
        ]) ?>
    </form>
</div>