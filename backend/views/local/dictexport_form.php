<?php

use Yii;

?>

<div class="form-group">
    <label for="lang"><?= Yii::t('app', 'Язык') ?></label>
    <select name="lang" id="lang" class="form-control">
    <?php foreach (Yii::$app->params['langs'] as $item) { ?>
        <?php if ($item->basic == 0) { ?>
        <option value="<?= $item->code ?>"><?= $item->name ?> - <?= strtoupper($item->code) ?></option>
        <?php } ?>
    <?php } ?>
    </select>
</div>
<div class="form-group">
    <label for="untranslated"><?= Yii::t('app', 'Экспортировать записи') ?></label>
    <select name="untranslated" id="untranslated" class="form-control">
        <option value="1"><?= Yii::t('app', 'Непереведенные') ?></option>
        <option value="0"><?= Yii::t('app', 'Все') ?></option>
    </select>
</div>