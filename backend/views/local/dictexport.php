<?php

$this->title = Yii::t('app', 'Экспорт словаря');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Словарь'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('/layouts/export', [
        'link' => 'dictlist',
        'include' => '/local/dictexport_form',
    ]); ?>
</div>