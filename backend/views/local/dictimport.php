<?php

use Yii;

$this->title = Yii::t('app', 'Импорт словаря');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Словарь'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <div class="col-md-9">
        <form id="export_form" method="post" enctype="multipart/form-data">
            <?= $this->render('/layouts/form') ?>
            <div class="form-group">
                <label for="file"><?= Yii::t('app', 'Файл') ?></label>
                <input type="file" name="file" id="file" class="file_input" accept=".csv,.xls,.xlsx">
                <p class="help-block"><?= Yii::t('app', 'доступные типы файлов') ?>: .csv, .xls, .xlsx</p>
                <p class="help-block"><?= Yii::t('app', 'доступные поля') ?>: "Key", "Entry"</p>
            </div>
            <div class="form-group">
                <label for="lang"><?= Yii::t('app', 'Язык') ?></label>
                <select name="lang" id="lang" class="form-control">
                    <?php foreach (Yii::$app->params['langs'] as $item) { ?>
                        <?php if ($item->basic == 0) { ?>
                            <option value="<?= $item->code ?>"><?= $item->name ?> - <?= strtoupper($item->code) ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
            <hr>
            <div class="form-group">
                <button type="submit" id="submit_button" class="btn btn-primary"><?= Yii::t('app', 'Экспортировать') ?></button>
                <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist') ?>" class="btn btn-default cancel_button"><?= Yii::t('app', 'Отменить') ?></a>
            </div>
        </form>
    </div>
</div>