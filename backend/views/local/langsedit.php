<?php

use Yii;

$this->title = Yii::t('app', 'Редактировать язык');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Языки'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <?php if (!empty(Yii::$app->request->get('restore'))) { ?>
        <span class="label label-info"><?= Yii::t('app', 'Восстановить') ?></span>
        <?php } ?>
        <?php if (!empty($record_history)) { ?>
        <button type="button" class="btn btn-default add_record_button" data-toggle="modal" data-target="#history_window">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            <?= Yii::t('app', 'Новая запись') ?>
        </button>
        <?php } ?>
    </h1>
    
    <?php if (!empty($data)) {
        echo $this->render('langsform', ['data' => $data]);
    } else {
        echo $this->render('/layouts/record_not_found');
    } ?>
    
    <?php if (!empty($record_history)) {
        echo $this->render('/layouts/record_history', [
            'name' => $data->getObjectName(),
            'history' => $record_history,
            'action' => 'langsedit'
        ]);
    } ?>
</div>