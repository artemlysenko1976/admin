<?php

$this->title = Yii::t('app', 'Доавить запись');
$this->params['breadcrumbs'][] = [
    'label' => 'Словарь', 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('dictform') ?>
</div>