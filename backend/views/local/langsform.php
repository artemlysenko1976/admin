<?php

use Yii;

if (!empty($data)) {
    $this->params['js_code'][] = '$("#active").val("' . $data['active'] . '");';
}

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'name',
            'label' => Yii::t('app', 'Название'),
            'value' => $data['name'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'code',
            'label' => Yii::t('app', 'ISO код'),
            'value' => $data['code'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'code_full',
            'label' => Yii::t('app', 'ISO код 2'),
            'value' => $data['code_full'],
            'hint' => "'en-US', 'zh-CN'...",
        ]) ?>
        <?= $this->render('/layouts/form/boolean', [
            'name' => 'active',
            'label' => Yii::t('app', 'активный'),
            'value' => $data['active'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'validate_action' => 'langsadd',
            'cancel_action' => 'langslist',
            'data' => $data,
        ]) ?>
    </form>
</div>