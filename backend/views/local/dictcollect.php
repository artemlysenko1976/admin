<?php

use Yii;

$this->title = Yii::t('app', 'Непереведенные записи');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Словарь'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist')
];
$this->params['breadcrumbs'][] = $this->title;

$this->params['js_code'][] = "collectDictionaryEntries();";

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <hr>
    <div class="col-md-9">
        <div id="loading_box" style="text-align:center; margin-top:50px;">
            <img src="/images/loading.gif" alt="" style="width:100px;">
        </div>
        <div id="result_box" style="display:none;">
            <div class="form-group">
                <label><?= Yii::t('app', 'Проверенные директории') ?></label>
                <p id="result_dirs" class="form-control-static"></p>
            </div>
            <div class="form-group">
                <label><?= Yii::t('app', 'Проверенные файлы') ?></label>
                <p id="result_files" class="form-control-static"></p>
            </div>
            <div class="form-group">
                <label><?= Yii::t('app', 'Найденные записи') ?></label>
                <p id="result_found" class="form-control-static"></p>
            </div>
            <div class="form-group">
                <label><?= Yii::t('app', 'Сохраненные непереведенные записи') ?></label>
                <p id="result_stored" class="form-control-static"></p>
            </div>
            <hr>
            <a href="" class="btn btn-primary"><?= Yii::t('app', 'Обновить') ?></a>
        </div>
    </div>
</div>