<?php

use Yii;

$this->title = Yii::t('app', 'Языки');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'langsadd') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/langsadd') ?>" type="submit" class="btn btn-primary add_record_button">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('app', 'Новая запись') ?>
        </a>
        <?php } ?>
    </h1>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th><?= $this->render('/layouts/sort', ['label' => '#', 'field' => 'id']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Название'), 'field' => 'name']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'ISO код'), 'field' => 'code']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'ISO код 2'), 'field' => 'code_full']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Активный'), 'field' => 'active']) ?></th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach ($records as $row) { ?>
                <tr>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['code'] ?></td>
                    <td><?= $row['code_full'] ?></td>
                    <td><?= $this->render('/layouts/activity', ['row' => $row]) ?></td>
                    <td>
                        <?php if ($row['basic'] == 0 && app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'langsedit') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/langsedit/' . $row['id']) ?>" class="glyphicon glyphicon-edit" title="<?= Yii::t('app', 'Редактировать') ?>"></a>
                        <?php } ?>
                        <?php if ($row['basic'] == 0 && app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'langsdelete') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/langsdelete/' . $row['id']) ?>" class="glyphicon glyphicon-remove" title="<?= Yii::t('app', 'Удалить') ?>"></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <?= $this->render('/layouts/pager', [
            'pages' => $pages
        ]) ?>
    <?php } else { ?>
        <?= Yii::t('app', 'Записи не найдены') ?>
    <?php } ?>
</div>