<?php

use Yii;

$this->title = Yii::t('app', 'Словарь');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'dictadd') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictadd') ?>" type="submit" class="btn btn-primary add_record_button">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('app', 'Новая запись') ?>
        </a>
        <?php } ?>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'dictimport') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictimport') ?>" type="submit" class="btn btn-default add_record_button">
            <span class="glyphicon glyphicon-floppy-save"></span>
            <?= Yii::t('app', 'Импорь') ?>
        </a>
        <?php } ?>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'dictexport') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictexport') ?>" type="submit" class="btn btn-default add_record_button">
            <span class="glyphicon glyphicon-hdd"></span>
            <?= Yii::t('app', 'Экспорт') ?>
        </a>
        <?php } ?>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'dictcollect') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictcollect') ?>" type="submit" class="btn btn-default add_record_button">
            <span class="glyphicon glyphicon-list"></span>
            <?= Yii::t('app', 'Непереведенные записи') ?>
        </a>
        <?php } ?>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post" action="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id) ?>">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-5">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Сбросить') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th><?= $this->render('/layouts/sort', ['label' => '#', 'field' => 'id']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Ключ'), 'field' => 'key']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Запись'), 'field' => 'entry']) ?></th>
                <th><?= Yii::t('app', 'Непереведенные версии') ?></th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach ($records as $row) { ?>
                <tr>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['key'] ?></td>
                    <td><?= $row['entry'] ?></td>
                    <td>
                        <?php $versions = explode(',', $row['versions']); ?>
                        <?php foreach (Yii::$app->params['langs'] as $lang) { ?>
                            <?php if (!in_array($lang->code, $versions)) { ?>
                                <?= strtoupper($lang->code) ?>
                                &nbsp;
                            <?php } ?>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'dictedit') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictedit/' . $row['id']) ?>" class="glyphicon glyphicon-edit" title="<?= Yii::t('app', 'Редактировать') ?>"></a>
                        <?php } ?>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'dictdelete') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictdelete/' . $row['id']) ?>" class="glyphicon glyphicon-remove" title="<?= Yii::t('app', 'Удалить') ?>"></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <?= $this->render('/layouts/pager', [
            'pages' => $pages
        ]) ?>
    <?php } else { ?>
        <?= Yii::t('app', 'Записи не найдены') ?>
    <?php } ?>
</div>