<?php

use Yii;
use app\models\Countries;

$this->title = Yii::t('app', 'Страны');
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['region'])) {
    $this->params['js_code'][] = '$("#filter_region").val("' . Yii::$app->params['filter']['region'] . '");';
}
if (isset(Yii::$app->params['filter']['active'])) {
    $this->params['js_code'][] = '$("#filter_active").val("' . Yii::$app->params['filter']['active'] . '");';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'add') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/add') ?>" type="submit" class="btn btn-primary add_record_button">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('app', 'Новая запись') ?>
        </a>
        <?php } ?>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-3">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="filter_region"><?= Yii::t('app', 'Регион') ?></label>
                    <select name="filter[region]" id="filter_region" class="form-control">
                        <option value="">-</option>
                        <?php foreach ($regions as $item) { ?>
                            <option value="<?= $item ?>"><?= $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_active"><?= Yii::t('app', 'Аксивность') ?></label>
                    <select type="text" name="filter[active]" id="filter_active" class="form-control">
                        <option value="">-</option>
                        <option value="1"><?= Yii::t('app', 'Активный') ?></option>
                        <option value="0"><?= Yii::t('app', 'неактивный') ?></option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Сбросить') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <div class="table_container">
            <table class="table table-striped table-bordered table-hover">
                <tr>
                    <th><?= $this->render('/layouts/sort', ['label' => '#', 'field' => 'id']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Countries::attributeStaticLabels()['name'], 'field' => 'name']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Countries::attributeStaticLabels()['region'], 'field' => 'region']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Countries::attributeStaticLabels()['iso_code'], 'field' => 'iso_code']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Countries::attributeStaticLabels()['iso_code_2'], 'field' => 'iso_code_2']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Countries::attributeStaticLabels()['phone_code'], 'field' => 'phone_code']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Countries::attributeStaticLabels()['active'], 'field' => 'active']) ?></th>
                    <th>&nbsp;</th>
                </tr>
                <?php foreach ($records as $row) { ?>
                    <tr>
                        <td><?= $row['id'] ?></td>
                        <td><?= $row['local']['name'] ?></td>
                        <td><?= $row['region'] ?></td>
                        <td><?= $row['iso_code'] ?></td>
                        <td><?= $row['iso_code_2'] ?></td>
                        <td><?= (!empty($row['phone_code']) ? '+' : '') ?><?= $row['phone_code'] ?></td>
                        <td><?= $this->render('/layouts/boolean', ['field' => $row['active']]) ?></td>
                        <td>
                            <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'edit') === true) { ?>
                                <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $row['id']) ?>" class="glyphicon glyphicon-edit" title="Edit"></a>
                            <?php } ?>
                            <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'delete') === true) { ?>
                                <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/delete/' . $row['id']) ?>" class="glyphicon glyphicon-remove" title="Delete"></a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?= $this->render('/layouts/pager', [
                'pages' => $pages
            ]) ?>
        </div>
    <?php } else { ?>
        <?= Yii::t('app', 'Записи не найдены') ?>
    <?php } ?>
</div>