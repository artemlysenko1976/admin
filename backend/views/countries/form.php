<?php

use app\models\Countries;

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <div class="form-group">
            <label id="name"><?= Countries::attributeStaticLabels()['name'] ?></label>
            <?= $this->render('/layouts/local_field', [
                'name' => 'name',
                'value' => $data['locals'],
                'type' => 'string',
            ]) ?>
        </div>
        <?= $this->render('/layouts/form/select', [
            'name' => 'region',
            'id' => 'region',
            'label' => Countries::attributeStaticLabels()['region'],
            'value' => $data['region'],
            'options' => $regions,
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'iso_code_2',
            'id' => 'iso_code_2',
            'label' => Countries::attributeStaticLabels()['iso_code_2'],
            'value' => $data['iso_code_2'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'iso_code',
            'id' => 'iso_code',
            'label' => Countries::attributeStaticLabels()['iso_code'],
            'value' => $data['iso_code'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'phone_code',
            'id' => 'phone_code',
            'label' => Countries::attributeStaticLabels()['phone_code'],
            'value' => $data['phone_code'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/boolean', [
            'name' => 'active',
            'label' => Countries::attributeStaticLabels()['active'],
            'value' => $data['active'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'data' => $data,
        ]) ?>
    </form>
</div>