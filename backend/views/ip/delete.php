<?php

use Yii;

$this->title = Yii::t('app', 'Delete IP address');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'IP addresses filter'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('/layouts/delete', [
        'data' => $data,
    ]); ?>
</div>