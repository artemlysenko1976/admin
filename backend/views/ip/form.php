<?php

use Yii;
use app\models\Ips;

if (!empty($data)) {
    $this->params['js_code'][] = '$("#operation").val("' . $data->operation . '")';
    $this->params['js_code'][] = '$("#active").val(' . $data->active . ')';
}

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'ip',
            'label' => Yii::t('app', 'IP address'),
            'value' => $data['ip'],
        ]) ?>
        <?= $this->render('/layouts/form/select', [
            'name' => 'operation',
            'label' => Yii::t('app', 'Operation'),
            'options' => Ips::$operations,
            'value' => $data['operation'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/string', [
            'name' => 'comment',
            'label' => Yii::t('app', 'Comment'),
            'value' => $data['comment'],
        ]) ?>
        <?= $this->render('/layouts/form/boolean', [
            'name' => 'active',
            'label' => Yii::t('app', 'Activity'),
            'value' => $data['active'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'data' => $data,
        ]) ?>
    </form>
</div>