<?php

use Yii;

$this->title = Yii::t('app', 'IP addresses filter');
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['active'])) {
    $this->params['js_code'][] = '$("#filter_active").val("' . Yii::$app->params['filter']['active'] . '");';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'add') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/add') ?>" type="submit" class="btn btn-primary add_record_button">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('app', 'New record') ?>
        </a>
        <?php } ?>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-5">
                    <label for="filter_keyword"><?= Yii::t('app', 'Keyword') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Reset filter') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Search') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th><?= $this->render('/layouts/sort', ['label' => '#', 'field' => 'id']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'IP'), 'field' => 'ip']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Operation'), 'field' => 'operation']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Comment'), 'field' => 'comment']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Active'), 'field' => 'active']) ?></th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach ($records as $row) { ?>
                <tr>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['ip'] ?></td>
                    <td><?= $row['operation'] ?></td>
                    <td><?= $row['comment'] ?></td>
                    <td><?= $this->render('/layouts/boolean', ['field' => $row['active']]) ?></td>
                    <td>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'edit') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $row['id']) ?>" class="glyphicon glyphicon-edit" title="<?= Yii::t('app', 'Edit') ?>"></a>
                        <?php } ?>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'delete') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/delete/' . $row['id']) ?>" class="glyphicon glyphicon-remove" title="<?= Yii::t('app', 'Delete') ?>"></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
        <?= Yii::t('app', 'No records found') ?>
    <?php } ?>
    
    <?= $this->render('/layouts/pager', [
        'pages' => $pages
    ]) ?>
</div>