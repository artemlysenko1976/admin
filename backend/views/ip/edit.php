<?php

use Yii;

$this->title = Yii::t('app', 'Edit IP address');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'IP addresses filter'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <?php if (!empty(Yii::$app->request->get('restore'))) { ?>
        <span class="label label-info"><?= Yii::t('app', 'Restore') ?></span>
        <?php } ?>
        <?php if (!empty($record_history)) { ?>
        <button type="button" class="btn btn-default add_record_button" data-toggle="modal" data-target="#history_window">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            <?= Yii::t('app', 'View history') ?>
        </button>
        <?php } ?>
    </h1>
    
    <?php if (!empty($data)) { ?>
        <?= $this->render('form', [
            'data' => $data,
        ]) ?>
    <?php } else { ?>
        <?= $this->render('/layouts/record_not_found') ?>
    <?php } ?>
    
    <?php if (!empty($record_history)) { ?>
        <?= $this->render('/layouts/record_history', [
            'name' => $data->getObjectName(),
            'history' => $record_history
        ]) ?>
    <?php } ?>
</div>