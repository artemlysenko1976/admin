<?php

use Yii;

$this->title = Yii::t('app', 'Удалить категорию');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Категории товаров'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catslist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('/layouts/delete', [
        'data' => $data,
        'action' => 'catslist',
    ]); ?>
</div>