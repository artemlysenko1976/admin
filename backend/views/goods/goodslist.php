<?php

use Yii;
use app\models\Goods;
use app\models\GoodsLocal;

$this->title = Yii::t('app', 'Товары');
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['category_id'])) {
    $this->params['js_code'][] = '$("#filter_category_id").val("' . Yii::$app->params['filter']['category_id'] . '");';
}
if (isset(Yii::$app->params['filter']['active'])) {
    $this->params['js_code'][] = '$("#filter_active").val("' . Yii::$app->params['filter']['active'] . '");';
}
if (isset(Yii::$app->params['filter']['brand_id'])) {
    $this->params['js_code'][] = '$("#filter_brand_id").val("' . Yii::$app->params['filter']['brand_id'] . '");';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'goodsadd') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodsadd') ?>" type="submit" class="btn btn-primary add_record_button">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('app', 'Новая запись') ?>
        </a>
        <?php } ?>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-3">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="filter_category_id"><?= Yii::t('app', 'Категория') ?></label>
                    <select type="text" name="filter[category_id]" id="filter_category_id" class="form-control">
                        <option value="">-</option>
                        <?php foreach ($categories as $id => $item) { ?>
                            <option value="<?= $id ?>"><?= $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="filter_brand_id"><?= Yii::t('app', 'Бренд') ?></label>
                    <select type="text" name="filter[brand_id]" id="filter_brand_id" class="form-control">
                        <option value="">-</option>
                        <?php foreach ($brands as $item) { ?>
                            <option value="<?= $item->id ?>"><?= $item->name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="filter_active"><?= Yii::t('app', 'Аксивность') ?></label>
                    <select type="text" name="filter[active]" id="filter_active" class="form-control">
                        <option value="">-</option>
                        <option value="1"><?= Yii::t('app', 'Активный') ?></option>
                        <option value="0"><?= Yii::t('app', 'неактивный') ?></option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Сбросить') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <div class="table_container">
            <table class="table table-striped table-bordered table-hover">
                <tr>
                    <th><?= $this->render('/layouts/sort', ['label' => '#', 'field' => 'id']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Goods::attributeStaticLabels()['photo'], 'field' => 'photo']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Goods::attributeStaticLabels()['name'], 'field' => GoodsLocal::tableName() . '.name']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Goods::attributeStaticLabels()['category_id'], 'field' => 'category_name']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Goods::attributeStaticLabels()['brand_id'], 'field' => 'brand_name']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Goods::attributeStaticLabels()['price'], 'field' => 'price']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Goods::attributeStaticLabels()['inner_price'], 'field' => 'inner_price']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Goods::attributeStaticLabels()['buy_price'], 'field' => 'buy_price']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Goods::attributeStaticLabels()['quantity'], 'field' => 'price']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Goods::attributeStaticLabels()['client_id'], 'field' => 'client_id']) ?></th>
                    <th><?= $this->render('/layouts/sort', ['label' => Goods::attributeStaticLabels()['active'], 'field' => 'active']) ?></th>
                    <th>&nbsp;</th>
                </tr>
                <?php foreach ($records as $row) { ?>
                    <tr>
                        <td><?= $row['id'] ?></td>
                        <td>
                            <?php if (!empty($row['photo'])) { ?>
                                <img src="/images/goods/<?= $row['id'] ?>/<?= $row['photo'] ?>?t=<?= time() ?>" alt="" class="thumbnail" style="height:70px;" onerror="this.src = '/images/no_image.png';">
                            <?php } else { ?>
                                <img src="/images/no_image.png" alt="" class="thumbnail" style="height:70px;">
                            <?php } ?>
                        </td>
                        <td><?= $row['rellocal']['name'] ?></td>
                        <td><?= $row['category_name'] ?></td>
                        <td><?= $row['brand_name'] ?></td>
                        <td><?= $row['price'] ?></td>
                        <td><?= $row['inner_price'] ?></td>
                        <td><?= $row['buy_price'] ?></td>
                        <td><?= $row['quantity'] ?></td>
                        <td>
                            <?php if (!empty($row['client_id'])) { ?>
                                <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . 'clients/view/' . $row['client_id']) ?>" target="_blank">
                                    <?= $row['client_last_name'] ?> <?= $row['client_first_name'] ?>
                                </a>
                            <?php } else { ?>
                                <?= Yii::t('app', 'Магазин') ?>
                            <?php } ?>
                        </td>
                        <td><?= $this->render('/layouts/boolean', ['field' => $row['active']]) ?></td>
                        <td>
                            <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'goodsview') === true) { ?>
                                <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodsview/' . $row['id']) ?>" class="glyphicon glyphicon-eye-open" title="<?= Yii::t('app', 'Просмотреть') ?>"></a>
                            <?php } ?>
                            <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'goodsedit') === true) { ?>
                                <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodsedit/' . $row['id']) ?>" class="glyphicon glyphicon-edit" title="<?= Yii::t('app', 'Редактировать') ?>"></a>
                            <?php } ?>
                            <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'goodsdelete') === true) { ?>
                                <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodsdelete/' . $row['id']) ?>" class="glyphicon glyphicon-remove" title="<?= Yii::t('app', 'Ущалить') ?>"></a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?= $this->render('/layouts/pager', [
                'pages' => $pages
            ]) ?>
        </div>
    <?php } else { ?>
        <?= Yii::t('app', 'Записи не найдены') ?>
    <?php } ?>
</div>