<?php

use Yii;
use app\models\Brands;

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'name',
            'label' => Brands::attributeStaticLabels()['name'],
            'value' => $data['name'],
        ]) ?>
        <?= $this->render('/layouts/form/image', [
            'name' => 'logo',
            'label' => Brands::attributeStaticLabels()['logo'],
            'image' => $data['logo'],
            'url' => '/images/brands/' . $data['logo'],
            'delete' => true,
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/boolean', [
            'name' => 'active',
            'label' => Brands::attributeStaticLabels()['active'],
            'value' => $data['active'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'validate_action' => 'brandsadd',
            'cancel_action' => 'brandslist',
            'data' => $data,
        ]) ?>
    </form>
</div>