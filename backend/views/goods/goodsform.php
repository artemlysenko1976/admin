<?php

use app\models\Goods;

$client_string = '';

if (!empty($data)) {
    if (!empty($data->client)) {
        $client_string = '#' . $data->client_id . ' - ' . $data->client->last_name . ' ' . $data->client->first_name . ' (' . $data->client->login . ')';
    }
    
    $this->params['js_code'][] = "loadGoodsParams(" . $data->category_id . ", " . $data->id . ");";
}

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <div class="form-group">
            <label id="name"><?= Goods::attributeStaticLabels()['name'] ?></label>
            <?= $this->render('/layouts/local_field', [
                'name' => 'name',
                'value' => $data['locals'],
                'type' => 'string',
            ]) ?>
        </div>
        <div class="form-group">
            <label id="name"><?= Goods::attributeStaticLabels()['description'] ?></label>
            <?= $this->render('/layouts/local_field', [
                'name' => 'description',
                'value' => $data['locals'],
                'type' => 'html',
            ]) ?>
        </div>
        <hr>
        <?= $this->render('/layouts/form/select', [
            'name' => 'category_id',
            'id' => 'category_id',
            'label' => Goods::attributeStaticLabels()['category_id'],
            'value' => $data['category_id'],
            'options' => $categories,
            'empty_option' => true,
            'onchange' => "loadGoodsParams(this.value" . (!empty($data->id) ? ', ' . $data->id : '') . ");",
        ]) ?>
        <div class="form-group">
            <label id="name"><?= Goods::attributeStaticLabels()['parameters'] ?></label>
            <div id="params_box"></div>
        </div>
        <hr>
        <?= $this->render('/layouts/form/image', [
            'name' => 'photo',
            'label' => Goods::attributeStaticLabels()['photo'],
            'image' => $data['photo'],
            'url' => '/images/goods/' . $data['id'] . '/' . $data['photo'],
            'delete' => true,
        ]) ?>
        <?= $this->render('/layouts/form/video', [
            'name' => 'video',
            'label' => Goods::attributeStaticLabels()['video'],
            'value' => $data['video'],
            'hint' => Yii::t('app', 'Введите код видео в YouTube, на пример "ioud934amXM" для видео с адресом https://youtu.be/ioud934amXM')
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/string', [
            'name' => 'buy_price',
            'label' => Goods::attributeStaticLabels()['buy_price'],
            'value' => $data['buy_price'],
            'onchange' => 'calculateProductPrices();'
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'extra_charge_percent',
            'label' => Goods::attributeStaticLabels()['extra_charge_percent'],
            'value' => (!empty($data['extra_charge_percent']) ? $data['extra_charge_percent'] : Yii::$app->params['goods']['extra_charge_percent']),
            'onchange' => 'calculateProductPrices();'
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'discount_percent',
            'label' => Goods::attributeStaticLabels()['discount_percent'],
            'value' => (!empty($data['discount_percent']) ? $data['discount_percent'] : Yii::$app->params['goods']['discount_percent']),
            'onchange' => 'calculateProductPrices();'
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'price',
            'label' => Goods::attributeStaticLabels()['price'],
            'value' => $data['price'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'inner_price',
            'label' => Goods::attributeStaticLabels()['inner_price'],
            'value' => $data['inner_price'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'old_price',
            'label' => Goods::attributeStaticLabels()['old_price'],
            'value' => $data['old_price'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/select', [
            'name' => 'brand_id',
            'id' => 'brand_id',
            'label' => Goods::attributeStaticLabels()['brand_id'],
            'value' => $data['brand_id'],
            'options' => $brands,
            'option_key' => 'id',
            'option_value' => 'name',
            'empty_option' => true,
        ]) ?>
        <?= $this->render('/layouts/form/autocomplete', [
            'name' => 'client_id',
            'label' => Goods::attributeStaticLabels()['client_id'],
            'value' => $client_string,
            'url' => 'clients/autocomplete',
            'hint' => Yii::t('app', 'выберите поставщика из клиентов, или оставьте пустым - в этом случае товар будет добавлен от имени магазина'),
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'quantity',
            'label' => Goods::attributeStaticLabels()['quantity'],
            'value' => $data['quantity'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/boolean', [
            'name' => 'active',
            'label' => Goods::attributeStaticLabels()['active'],
            'value' => $data['active'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'data' => $data,
            'validate_action' => 'goodsadd',
            'cancel_action' => 'goodslist',
        ]) ?>
    </form>
</div>