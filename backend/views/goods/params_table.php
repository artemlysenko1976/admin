<?php

use Yii;

?>

<?php if (!empty($parameters)) { ?>
    <table id="list_table" class="table table-striped table-bordered table-hover non_list_table">
        <tbody>
        <?php foreach ($parameters as $id => $item) { ?>
            <tr>
                <td style="width:40%;"><?= $item['name'] ?></td>
                <td>
                    <input type="text" name="parameters[<?= $id ?>]" id="parameters_<?= $id ?>" value="<?= $item['value'] ?>" class="form-control">
                </td>
                <td style="width:20px;">
                    <span class="glyphicon glyphicon-move order_handle"></span>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <script>
        $("#list_table").sortable({
            containerSelector: "table",
            itemPath: "> tbody",
            itemSelector: "tr",
            placeholder: "<tr class=\"order_placeholder\"><td colspan=\"15\">&nbsp;</td></tr>",
            handle: ".order_handle"
        });
    </script>
<?php } else { ?>
    <p><?= Yii::t('app', 'для выбранной категории параметры не заданы') ?></p>
<?php } ?>
