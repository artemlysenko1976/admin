<?php

use Yii;
use app\models\Goods;
use app\models\Users;
use app\library\Utils;

$this->title = Yii::t('app', 'Просмотр данных Товара');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Товары'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodslist')
];
$this->params['breadcrumbs'][] = $this->title;

if (!empty($data)) {
}

?>

<div class="backend-default-index">
    <h1>
        #<?= $data['id'] ?> <?= $data['local']['name'] ?>
        
        <?php if (Users::checkAccessPermissions(Yii::$app->controller->id, 'goodsdelete') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodsdelete/' . $data['id']) ?>" class="btn btn-danger add_record_button">
            <span class="glyphicon glyphicon-remove"></span>
            <?= Yii::t('app', 'Удалить') ?>
        </a>
        <?php } ?>
        <?php if (Users::checkAccessPermissions(Yii::$app->controller->id, 'goodsedit') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodsedit/' . $data['id']) ?>" class="btn btn-default add_record_button">
            <span class="glyphicon glyphicon-edit"></span>
            <?= Yii::t('app', 'Редактировать') ?>
        </a>
        <?php } ?>
    </h1>
    <hr>
    
    <?php if (!empty($data)) { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-3">
                                <?php if (!empty($data['photo'])) { ?>
                                    <img src="/images/goods/<?= $data['id'] ?>/<?= $data['photo'] ?>?t=<?= time() ?>" alt="" class="thumbnail" style="height:150px;" onerror="this.src = '/images/no_photo.png';">
                                <?php } else { ?>
                                    <img src="/images/no_image.png" alt="" class="thumbnail" style="height:150px;">
                                <?php } ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['name'] ?></label><br>
                                    <?= $data['local']['name'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['brand_id'] ?></label><br>
                                    <?php if (!empty($data['brand']->logo)) { ?>
                                        <img src="/images/brands/<?= $data['brand']->logo ?>" alt="" style="height:30px;" onerror="this.src = '/images/no_avatar.png';">
                                    <?php } ?>
                                    <?= $data['brand']->name ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['category_id'] ?></label><br>
                                    <?= $data->category['local']['name'] ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['price'] ?></label><br>
                                    <?= $data['price'] ?>
                                </div>
                                <?php if (!empty($data['old_price'])) { ?>
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['old_price'] ?></label><br>
                                    <?= $data['old_price'] ?>
                                </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['buy_price'] ?></label><br>
                                    <?= $data['buy_price'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['inner_price'] ?></label><br>
                                    <?= $data['inner_price'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['extra_charge_percent'] ?></label><br>
                                    <?= $data['extra_charge_percent'] ?>%
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['discount_percent'] ?></label><br>
                                    <?= $data['discount_percent'] ?>%
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['client_id'] ?></label><br>
                                    <?php if (!empty($data['client'])) { ?>
                                        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . 'clients/view/' . $data['client_id']) ?>" target="_blank">
                                            <?= $data['client']['last_name'] ?> <?= $data['client']['first_name'] ?>
                                        </a>
                                    <?php } else { ?>
                                        <?= Yii::t('app', 'Магазин') ?>
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['quantity'] ?></label><br>
                                    <?= $data['quantity'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['active'] ?></label><br>
                                    <?= $this->render('/layouts/boolean', ['field' => $data['active']]); ?>
                                </div>
                            </div>
                        </div>
                        <?php if (!empty($data['local']['description'])) { ?>
                        <hr>
                        <div class="row">
                            <?php if (!empty($data['video'])) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['video'] ?></label><br>
                                    <iframe width="350" height="200" src="https://www.youtube.com/embed/<?= $data['video'] ?>?rel=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['description'] ?></label><br>
                                    <?= $data['local']['description'] ?>
                                </div>
                            </div>
                            <?php if (!empty($data['parameters'])) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><?= Goods::attributeStaticLabels()['parameters'] ?></label><br>
                                    <?php foreach ($data['parameters'] as $param) { ?>
                                    <div class="row">
                                        <div class="col-md-6"><i><?= $param['rellocal']['name'] ?></i>:</div>
                                        <div class="col-md-6"><?= $param['value'] ?></div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <hr class="clear">
        <div class="form-group">
            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodslist') ?>" class="btn btn-default"><?= Yii::t('app', 'Назад') ?></a>
        </div>
    <?php } else { ?>
        <?= $this->render('/layouts/record_not_found'); ?>
    <?php } ?>
</div>