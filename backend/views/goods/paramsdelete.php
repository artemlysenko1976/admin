<?php

use Yii;

$this->title = 'Удалить параметр';
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Параметры товаров'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paramslist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('/layouts/delete', [
        'data' => $data,
        'action' => 'paramslist',
    ]); ?>
</div>