<?php

use Yii;

if (empty($level))
    $level = 0;
?>

<tr>
    <td>
        <?= $row['id'] ?>
        <input type="hidden" name="ordering[]" id="ordering_<?= $row['id'] ?>" value="<?= $row['id'] ?>">
    </td>
    <td>
        <?php if ($level > 0) { ?>
            <?php for ($i = 0; $i < $level; $i++) { ?>
                &nbsp;&nbsp;&nbsp;&nbsp;
            <?php } ?>
            <span class="glyphicon glyphicon-menu-right"></span>&nbsp;
        <?php } ?>
        <?= $row['local']['name'] ?>
    </td>
    <td><?= $this->render('/layouts/boolean', ['field' => $row['active']]) ?></td>
    <td>
        <span class="glyphicon glyphicon-move order_handle"></span>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'catsedit') === true) { ?>
            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catsedit/' . $row['id']) ?>" class="glyphicon glyphicon-edit" title="Edit"></a>
        <?php } ?>
        <?php if (count($row['children']) == 0 && app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'catsdelete') === true) { ?>
            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catsdelete/' . $row['id']) ?>" class="glyphicon glyphicon-remove" title="Delete"></a>
        <?php } ?>
    </td>
</tr>
<?php if (count($row['children']) > 0) { ?>
    <?php foreach ($row['children'] as $subrow) { ?>
        <?= $this->render('catslist_row', [
            'row' => $subrow, 
            'level' => $level + 1
        ]) ?>
    <?php } ?>
<?php } ?>