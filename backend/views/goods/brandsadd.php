<?php

use Yii;

$this->title = Yii::t('app', 'Добавить Бренд');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Бренды'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/brandslist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('brandsform') ?>
</div>