<?php

use Yii;

$this->title = 'Добавить товар';
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Товары'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodslist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('goodsform', [
        'categories' => $categories,
        'brands' => $brands,
    ]) ?>
</div>