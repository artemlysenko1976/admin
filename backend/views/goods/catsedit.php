<?php

use Yii;

$this->title = Yii::t('app', 'Редактировать категорию');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Категории товаров'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catslist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <?php if (!empty(Yii::$app->request->get('restore'))) { ?>
        <span class="label label-info"><?= Yii::t('app', 'Восстановить') ?></span>
        <?php } ?>
        <?php if (!empty($record_history)) { ?>
        <button type="button" class="btn btn-default add_record_button" data-toggle="modal" data-target="#history_window">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            <?= Yii::t('app', 'Просмотреть историю') ?>
        </button>
        <?php } ?>
    </h1>
    
    <?= $this->render('catsform', [
        'data' => $data,
        'parent_items' => $parent_items,
    ]) ?>
    
    <?php if (!empty($record_history)) {
        echo $this->render('/layouts/record_history', [
            'name' => $data->getObjectName(),
            'history' => $record_history,
            'action' => 'catsmethodsedit',
            'url' => \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catsedit/' . $data['id'])
        ]);
    } ?>
</div>