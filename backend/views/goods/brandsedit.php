<?php

$this->title = Yii::t('app', 'Редактировать Бренд');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Бренды'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/brandslist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <?php if (!empty(Yii::$app->request->get('restore'))) { ?>
        <span class="label label-info"><?= Yii::t('app', 'Восстановить') ?></span>
        <?php } ?>
        <?php if (!empty($record_history)) { ?>
        <button type="button" class="btn btn-default add_record_button" data-toggle="modal" data-target="#history_window">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            <?= Yii::t('app', 'Просмотреть историю') ?>
        </button>
        <?php } ?>
    </h1>
    
    <?php if (!empty($data)) { ?>
        <?= $this->render('brandsform', [
            'data' => $data
        ]) ?>
    <?php } else { ?>
        <?= $this->render('/layouts/record_not_found') ?>
    <?php } ?>
    
    <?php if (!empty($record_history)) { ?>
        <?= $this->render('/layouts/record_history',[
            'name' => $data->getObjectName(),
            'history' => $record_history,
            'action' => 'brandsedit',
        ]) ?>
    <?php } ?>
</div>