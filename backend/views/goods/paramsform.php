<?php

use app\models\GoodsParameters;

if (!empty($data)) {
    $categories_map = $data->getCategories();
    if (!empty($categories_map)) {
        foreach ($categories_map as $cat_record) {
            $this->params['js_code'][] = "$('#categories_" . $cat_record->id . "').attr('checked', true)";
        }
    }
}

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <div class="form-group">
            <label id="name"><?= GoodsParameters::attributeStaticLabels()['name'] ?></label>
            <?= $this->render('/layouts/local_field', [
                'name' => 'name',
                'value' => $data['locals'],
                'type' => 'string',
            ]) ?>
        </div>
        <hr>
        <div class="form-group">
            <label id="categories"><?= GoodsParameters::attributeStaticLabels()['categories'] ?></label>
            <div class="form-group">
                <?php foreach ($categories as $id => $item) { ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="categories[]" id="categories_<?= $id ?>" value="<?= $id ?>" class="categories_checkboxes"> 
                        <?= $item ?>
                    </label>
                </div>
                <?php } ?>
            </div>
        </div>
        <hr>
        <?= $this->render('/layouts/form/boolean', [
            'name' => 'active',
            'label' => GoodsParameters::attributeStaticLabels()['active'],
            'value' => $data['active'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'data' => $data,
            'validate_action' => 'paramsadd',
            'cancel_action' => 'paramslist',
        ]) ?>
    </form>
</div>