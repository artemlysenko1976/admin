<?php

use app\models\GoodsCategories;

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <div class="form-group">
            <label class="control-label" id="name" for="name"><?= GoodsCategories::attributeStaticLabels()['name'] ?></label>
            <?= $this->render('/layouts/local_field', [
                'name' => 'name',
                'value' => $data['locals'],
                'type' => 'string',
            ]) ?>
        </div>
        <?= $this->render('/layouts/form/select', [
            'name' => 'parent_id',
            'label' => GoodsCategories::attributeStaticLabels()['parent_id'],
            'options' => $parent_items,
            'option_key' => 'id',
            'option_value' => 'local.name',
            'empty_option' => true,
            'value' => $data['parent_id'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/boolean', [
            'name' => 'active',
            'label' => GoodsCategories::attributeStaticLabels()['active'],
            'value' => $data['active'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'validate_action' => 'catsadd',
            'cancel_action' => 'catslist',
            'data' => $data,
        ]) ?>
    </form>
</div>