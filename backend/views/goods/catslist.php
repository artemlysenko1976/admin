<?php

use Yii;
use app\models\GoodsCategories;

$this->title = Yii::t('app', 'Категории товаров');
$this->params['breadcrumbs'][] = $this->title;

$this->params['js_code'][] = '$("#list_table").sortable({
    containerSelector: "table",
    itemPath: "> tbody",
    itemSelector: "tr",
    placeholder: "<tr class=\"order_placeholder\"><td colspan=\"15\">&nbsp;</td></tr>",
    handle: ".order_handle",
    onDrop: function ($item, container, _super, event) {
        $("#ordering_form").submit();
    }
});';

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'catsadd') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catsadd') ?>" type="submit" class="btn btn-primary add_record_button">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('app', 'Новая запись') ?>
        </a>
        <?php } ?>
    </h1>
    
    <form id="ordering_form" method="post">
        <?= $this->render('/layouts/form') ?>
        <?php if (!empty($records)) { ?>
            <table id="list_table" class="table table-striped table-bordered table-hover">
                <tr>
                    <th>#</th>
                    <th><?= GoodsCategories::attributeStaticLabels()['name'] ?></th>
                    <th><?= GoodsCategories::attributeStaticLabels()['active'] ?></th>
                    <th>&nbsp;</th>
                </tr>
                <tbody>
                <?php foreach ($records as $row) { ?>
                    <?= $this->render('catslist_row', [
                        'row' => $row, 
                        'level' => 0
                    ]) ?>
                <?php } ?>
                </tbody>
            </table>
        <?php } else { ?>
            <?= Yii::t('app', 'Записи не найдены') ?>
        <?php } ?>
    </form>
</div>