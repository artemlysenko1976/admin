<?php

use Yii;

$this->title = Yii::t('app', 'Добавить Метод');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Методы оплаты'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/methodslist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('methodsform', [
        'currencies' => $currencies,
    ]) ?>
</div>