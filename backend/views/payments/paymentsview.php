<?php

use Yii;
use app\models\Payments;
use app\models\Users;

$this->title = Yii::t('app', 'Просмотр платежа');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Платежи'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paymentslist')
];
$this->params['breadcrumbs'][] = $this->title;

if (!empty($data)) {
    $client = $data->client;
    $currency = $data->currency;
    $method = $data->method;
//    $order = $data->order;
    
    if (!empty($data->server_response)) {
        $server_response = json_decode($data->server_response, true);
        if (!empty($server_response)) {
            $server_response = print_r($server_response, true);
        } else {
            $server_response = $data->server_response;
        }
    }
}

?>

<div class="backend-default-index">
    <h1>
        #<?= $data['id'] ?> <?= $data['amount'] ?> <?= $data['currency']['code'] ?>
    </h1>
    <hr>
    
    <?php if (!empty($data)) { ?>
        <div class="row">
            <div class="col-md-9">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['status'] ?></label><br>
                                    <?= Payments::$statuses[$data['status']] ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['type'] ?></label><br>
                                    <?= Payments::$types[$data['type']] ?>
                                </div>
                            </div>
                            <?php if (!empty($data['order_id'])) { ?>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['order_id'] ?></label><br>
                                    <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . 'orders/view/' . $data['order_id']) ?>" target="_blank">
                                        #<?= $data['order_id'] ?>
                                    </a>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['create_timestamp'] ?></label><br>
                                    <?= date(Yii::$app->params['date_time_format'], $data['create_timestamp']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['method_id'] ?></label><br>
                                    <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/methodsedit/' . $data['method_id']) ?>" target="_blank">
                                        <?= $method['local']['name'] ?>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['currency_id'] ?></label><br>
                                    <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/currenciesedit/' . $data['currency_id']) ?>" target="_blank">
                                        <?= $currency['local']['name'] ?> <?= $currency['code'] ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['comment'] ?></label><br>
                                    <?php if (!empty($data['comment'])) { ?>
                                        <?= nl2br($data['comment']) ?>
                                    <?php } else { ?>
                                        -
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <form id="record_form" method="post" enctype="multipart/form-data">
                                        <?= $this->render('/layouts/form') ?>
                                        <?= $this->render('/layouts/form/id', [
                                            'data' => $data,
                                        ]) ?>
                                        <?= $this->render('/layouts/form/text', [
                                            'name' => 'comment',
                                            'label' => Yii::t('app', 'Добавить комментарий'),
                                            'value' => '',
                                        ]) ?>
                                        <?= $this->render('/layouts/form/button', [
                                            'validate_action' => 'paymentsadd',
                                            'cancel_action' => 'paymentslist',
                                            'data' => $data,
                                        ]) ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <?php if (!empty($data['error'])) { ?>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 red">
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['error'] ?></label><br>
                                    <?= nl2br($data['error']) ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if (!empty($server_response)) { ?>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['server_response'] ?></label><br>
                                    <pre><?= $server_response ?></pre>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['amount'] ?></label><br>
                                    <h3>
                                        <?= $data['amount'] ?>
                                        <small><?= $currency['code'] ?></small>
                                    </h3>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['comission'] ?></label><br>
                                    <?= $data['comission'] ?>
                                    <?= $currency['code'] ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= Payments::attributeStaticLabels()['discount'] ?></label><br>
                                    <?= $data['discount'] ?>
                                    <?= $currency['code'] ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if (!empty($client)) { ?>
                    <?= $this->render('/clients/detail_data', [
                        'user' => $client,
                        'title' => Payments::attributeStaticLabels()['client_id'],
                    ]); ?>
                <?php } ?>
            </div>
        </div>
        <hr class="clear">
        <div class="form-group">
            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paymentslist') ?>" class="btn btn-default"><?= Yii::t('app', 'Назад') ?></a>
        </div>
    <?php } else { ?>
        <?= $this->render('/layouts/record_not_found'); ?>
    <?php } ?>
</div>