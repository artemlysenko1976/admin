<?php

use Yii;
use app\models\PaymentsMethods;

$currencies_ids = [];

if (!empty($data)) {
    $existed_currencies = $data['currencies'];
    if (!empty($existed_currencies)) {
        foreach ($existed_currencies as $existed_currency) {
            $currencies_ids[] = $existed_currency->id;
        }
    }
}

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <div class="form-group">
            <label class="control-label" id="name" for="name"><?= PaymentsMethods::attributeStaticLabels()['name'] ?></label>
            <?= $this->render('/layouts/local_field', [
                'name' => 'name',
                'value' => $data['locals'],
                'type' => 'string',
            ]) ?>
        </div>
        <?= $this->render('/layouts/form/string', [
            'name' => 'key',
            'label' => PaymentsMethods::attributeStaticLabels()['key'],
            'value' => $data['key'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/string', [
            'name' => 'comission_fixed',
            'label' => PaymentsMethods::attributeStaticLabels()['comission_fixed'],
            'value' => (!empty($data['comission_fixed']) ? $data['comission_fixed'] : 0),
        ]) ?>
        <?= $this->render('/layouts/form/select', [
            'name' => 'comission_currency_id',
            'label' => PaymentsMethods::attributeStaticLabels()['comission_currency_id'],
            'value' => $data['comission_currency_id'],
            'options' => $currencies,
            'option_key' => 'id',
            'option_value' => 'local.name',
            'empty_option' => true,
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'comission_percent',
            'label' => PaymentsMethods::attributeStaticLabels()['comission_percent'],
            'value' => (!empty($data['comission_percent']) ? $data['comission_percent'] : 0),
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/checkbox', [
            'name' => 'currencies',
            'id' => 'currencies',
            'label' => PaymentsMethods::attributeStaticLabels()['currencies'],
            'value' => $currencies_ids,
            'options' => $currencies,
            'option_key' => 'id',
            'option_value' => 'local.name',
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/boolean', [
            'name' => 'active',
            'label' => PaymentsMethods::attributeStaticLabels()['active'],
            'value' => $data['active'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'validate_action' => 'methodsadd',
            'cancel_action' => 'methodslist',
            'data' => $data,
        ]) ?>
    </form>
</div>