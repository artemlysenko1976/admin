<?php

use Yii;
use app\models\Currencies;

$editable = true;
$methods_ids = [];

if (!empty($data)) {
    if ($data['basic'] == 1) {
        $editable = false;
    }
    
    $existed_methods = $data['methods'];
    if (!empty($existed_methods)) {
        foreach ($existed_methods as $existed_method) {
            $methods_ids[] = $existed_method->id;
        }
    }
}

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <div class="form-group">
            <label class="control-label" id="name" for="name"><?= Currencies::attributeStaticLabels()['name'] ?></label>
            <?= $this->render('/layouts/local_field', [
                'name' => 'name',
                'value' => $data['locals'],
                'type' => 'string',
            ]) ?>
        </div>
        <?php if ($editable === true) { ?>
            <?= $this->render('/layouts/form/string', [
                'name' => 'code',
                'label' => Currencies::attributeStaticLabels()['code'],
                'value' => $data['code'],
            ]) ?>
            <?= $this->render('/layouts/form/string', [
                'name' => 'rate',
                'label' => Currencies::attributeStaticLabels()['rate'],
                'value' => $data['rate'],
            ]) ?>
        <?php } ?>
        <hr>
        <?= $this->render('/layouts/form/checkbox', [
            'name' => 'methods',
            'id' => 'methods',
            'label' => Currencies::attributeStaticLabels()['methods'],
            'value' => $methods_ids,
            'options' => $methods,
            'option_key' => 'id',
            'option_value' => 'local.name',
        ]) ?>
        <?php if ($editable === true) { ?>
            <hr>
            <?= $this->render('/layouts/form/boolean', [
                'name' => 'fiat',
                'label' => Currencies::attributeStaticLabels()['fiat'],
                'value' => $data['fiat'],
            ]) ?>
            <?= $this->render('/layouts/form/boolean', [
                'name' => 'active',
                'label' => Currencies::attributeStaticLabels()['active'],
                'value' => $data['active'],
            ]) ?>
        <?php } ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'validate_action' => 'currenciesadd',
            'cancel_action' => 'currencieslist',
            'data' => $data,
        ]) ?>
    </form>
</div>