<?php

use Yii;

$this->title = Yii::t('app', 'Добавить Валюту');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Валюты'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/currencieslist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('currenciesform', [
        'methods' => $methods,
    ]) ?>
</div>