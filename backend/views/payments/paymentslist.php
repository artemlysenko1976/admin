<?php

use Yii;
use app\models\Payments;

$this->title = Yii::t('app', 'Платежи');
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['method_id'])) {
    $this->params['js_code'][] = '$("#filter_method_id").val("' . Yii::$app->params['filter']['method_id'] . '");';
}
if (isset(Yii::$app->params['filter']['currency_id'])) {
    $this->params['js_code'][] = '$("#filter_currency_id").val("' . Yii::$app->params['filter']['currency_id'] . '");';
}
if (isset(Yii::$app->params['filter']['status'])) {
    $this->params['js_code'][] = '$("#filter_status").val("' . Yii::$app->params['filter']['status'] . '");';
}
if (isset(Yii::$app->params['filter']['type'])) {
    $this->params['js_code'][] = '$("#filter_type").val("' . Yii::$app->params['filter']['type'] . '");';
}

$this->params['js_code'][] = '$("#filter_client").autocomplete({
    source: "/clients/autocomplete",
    minLength: 2
});';

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post" action="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id) ?>">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-3">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-3 has-feedback">
                    <label for="filter_client"><?= Yii::t('app', 'Клиент') ?></label>
                    <input type="text" name="filter[client]" id="filter_client" value="<?= Yii::$app->params['filter']['client'] ?>" placeholder="<?= Yii::t('app', 'введите текст для поиска') ?>" class="form-control">
                    <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>
                <div class="col-md-3">
                    <label for="filter_method_id"><?= Yii::t('app', 'Метод оплаты') ?></label>
                    <select type="text" name="filter[method_id]" id="filter_method_id" class="form-control">
                        <option value="">-</option>
                        <?php foreach ($methods as $item) { ?>
                            <option value="<?= $item->id ?>"><?= $item->local->name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_currency_id"><?= Yii::t('app', 'Валюта') ?></label>
                    <select type="text" name="filter[currency_id]" id="filter_currency_id" class="form-control">
                        <option value="">-</option>
                        <?php foreach ($currencies as $item) { ?>
                            <option value="<?= $item->id ?>"><?= $item->local->name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_status"><?= Yii::t('app', 'Статус') ?></label>
                    <select type="text" name="filter[status]" id="filter_status" class="form-control">
                        <option value="">-</option>
                        <?php foreach (Payments::$statuses as $k => $item) { ?>
                            <option value="<?= $k ?>"><?= $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_type"><?= Yii::t('app', 'Тип') ?></label>
                    <select type="text" name="filter[type]" id="filter_type" class="form-control">
                        <option value="">-</option>
                        <?php foreach (Payments::$types as $k => $item) { ?>
                            <option value="<?= $k ?>"><?= $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_timestamp_range"><?= Yii::t('app', 'Дата создания') ?></label><br>
                    <?= $this->render('/layouts/datepicker', [
                        'name' => 'filter[timestamp_range_from]',
                        'value' => Yii::$app->params['filter']['timestamp_range_from'],
                    ]) ?>
                    <?= $this->render('/layouts/datepicker', [
                        'name' => 'filter[timestamp_range_to]',
                        'value' => Yii::$app->params['filter']['timestamp_range_to'],
                    ]) ?>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Сбросить') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th><?= $this->render('/layouts/sort', ['label' => '#', 'field' => 'id']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Payments::attributeStaticLabels()['client_id'], 'field' => 'client_last_name']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Payments::attributeStaticLabels()['order_id'], 'field' => 'order_id']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Payments::attributeStaticLabels()['method_id'], 'field' => 'method_id']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Payments::attributeStaticLabels()['amount'], 'field' => 'amount']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Payments::attributeStaticLabels()['comission'], 'field' => 'comission']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Payments::attributeStaticLabels()['discount'], 'field' => 'comission']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Payments::attributeStaticLabels()['type'], 'field' => 'type']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Payments::attributeStaticLabels()['status'], 'field' => 'status']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Payments::attributeStaticLabels()['create_timestamp'], 'field' => 'create_timestamp']) ?></th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach ($records as $row) { ?>
                <tr>
                    <td><?= $row['id'] ?></td>
                    <td>
                        <?php if (!empty($row['client_last_name'])) { ?>
                        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . 'clients/view/' . $row['client_id']) ?>" target="_blank">
                            #<?= $row['client_id'] ?> <?= $row['client_last_name'] ?> <?= $row['client_first_name'] ?>
                        </a>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (!empty($row['order_id'])) { ?>
                        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . 'orders/view/' . $row['order_id']) ?>" target="_blank">
                            #<?= $row['order_id'] ?>
                        </a>
                        <?php } ?>
                    </td>
                    <td><?= $row['method_name'] ?></td>
                    <td><?= $row['amount'] ?> <?= $row['currency_code'] ?></td>
                    <td><?= (!empty($row['comission']) ? $row['comission'] . ' ' . $row['currency_code'] : '-') ?></td>
                    <td><?= (!empty($row['discount']) ? $row['discount'] . ' ' . $row['currency_code'] : '-') ?></td>
                    <td><?= Payments::$types[$row['type']] ?></td>
                    <td><?= Payments::$statuses[$row['status']] ?></td>
                    <td><?= $this->render('/layouts/time', ['field' => $row['create_timestamp']]) ?></td>
                    <td>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'paymentsview') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paymentsview/' . $row['id']) ?>" class="glyphicon glyphicon-eye-open" title="<?= Yii::t('app', 'Просмотреть') ?>"></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <?= $this->render('/layouts/pager', [
            'pages' => $pages
        ]) ?>
    <?php } else { ?>
        <?= Yii::t('app', 'Записи не найдены') ?>
    <?php } ?>
</div>