<?php

use Yii;
use app\models\PaymentsMethods;
use app\models\PaymentsMethodsLocal;

$this->title = Yii::t('app', 'Методы оплаты');
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['currency_id'])) {
    $this->params['js_code'][] = '$("#filter_currency_id").val("' . Yii::$app->params['filter']['currency_id'] . '");';
}
if (isset(Yii::$app->params['filter']['active'])) {
    $this->params['js_code'][] = '$("#filter_active").val("' . Yii::$app->params['filter']['active'] . '");';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'methodsadd') === true) { ?>
        <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/methodsadd') ?>" type="submit" class="btn btn-primary add_record_button">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('app', 'Новая запись') ?>
        </a>
        <?php } ?>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post" action="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id) ?>">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-4">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="filter_currency_id"><?= Yii::t('app', 'Валюты') ?></label>
                    <select type="text" name="filter[currency_id]" id="filter_currency_id" class="form-control">
                        <option value="">-</option>
                        <?php foreach ($currencies as $item) { ?>
                            <option value="<?= $item->id ?>"><?= $item->local->name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_active"><?= Yii::t('app', 'Аксивность') ?></label>
                    <select type="text" name="filter[active]" id="filter_active" class="form-control">
                        <option value="">-</option>
                        <option value="1"><?= Yii::t('app', 'Активный') ?></option>
                        <option value="0"><?= Yii::t('app', 'неактивный') ?></option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Сбросить') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th><?= $this->render('/layouts/sort', ['label' => '#', 'field' => 'id']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => PaymentsMethods::attributeStaticLabels()['name'], 'field' => PaymentsMethodsLocal::tableName() . '.name']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => PaymentsMethods::attributeStaticLabels()['key'], 'field' => 'key']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => PaymentsMethods::attributeStaticLabels()['comission_fixed'], 'field' => 'comission_fixed']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => PaymentsMethods::attributeStaticLabels()['comission_percent'], 'field' => 'comission_percent']) ?></th>
                <th><?= PaymentsMethods::attributeStaticLabels()['currencies'] ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => PaymentsMethods::attributeStaticLabels()['active'], 'field' => 'active']) ?></th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach ($records as $row) { ?>
                <tr>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['rellocal']['name'] ?></td>
                    <td><?= $row['key'] ?></td>
                    <td>
                        <?= $row['comission_fixed'] ?>
                        <?php if ($row['comission_fixed'] > 0) { ?>
                            <?= $row['comission_currency'] ?>
                        <?php } ?>
                    </td>
                    <td><?= $row['comission_percent'] ?>%</td>
                    <td><?= $row['currencies'] ?></td>
                    <td><?= $this->render('/layouts/boolean', ['field' => $row['active']]) ?></td>
                    <td>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'methodsedit') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/methodsedit/' . $row['id']) ?>" class="glyphicon glyphicon-edit" title="<?= Yii::t('app', 'Редактировать') ?>"></a>
                        <?php } ?>
                        <?php if (app\models\Users::checkAccessPermissions(Yii::$app->controller->id, 'methodsdelete') === true) { ?>
                            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/methodsdelete/' . $row['id']) ?>" class="glyphicon glyphicon-remove" title="<?= Yii::t('app', 'Удалить') ?>"></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <?= $this->render('/layouts/pager', [
            'pages' => $pages
        ]) ?>
    <?php } else { ?>
        <?= Yii::t('app', 'Записи не найдены') ?>
    <?php } ?>
</div>