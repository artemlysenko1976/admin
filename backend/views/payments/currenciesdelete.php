<?php

$this->title = Yii::t('app', 'Удалить Валюту');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Валюты'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/currencieslist')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('/layouts/delete', [
        'data' => $data,
        'action' => 'currencieslist',
    ]); ?>
</div>