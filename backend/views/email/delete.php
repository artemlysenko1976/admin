<?php

$this->title = Yii::t('app', 'Удалить E-mail шаблон');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'E-mail шаблоны'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('/layouts/delete', [
        'data' => $data,
    ]); ?>
</div>