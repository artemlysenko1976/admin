<?php

use Yii;

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <div class="form-group">
            <label class="control-label" for="subject"><?= Yii::t('app', 'Тема') ?></label>
            <?= $this->render('/layouts/local_field', [
                'name' => 'subject',
                'value' => $data['locals'],
                'type' => 'string',
            ]) ?>
            <p class="help-block"><?= htmlspecialchars(Yii::t('app', 'вы можете вставить переменные, используя РНР синтаксис <b><?= $var_name ?>')) ?></b></p>
        </div>
        <div class="form-group">
            <label class="control-label" for="content"><?= Yii::t('app', 'Контент') ?></label>
            <?= $this->render('/layouts/local_field', [
                'name' => 'content',
                'value' => $data['locals'],
                'type' => 'html',
            ]) ?>
            <p class="help-block"><?= Yii::t('app', 'Вы можете использовать РНР синтаксис включая переменные, циклы, условия и пр. внутри сообщения') ?></p>
        </div>
        <hr>
        <?= $this->render('/layouts/form/string', [
            'name' => 'email_from',
            'label' => Yii::t('app', 'E-mail (от)'),
            'value' => $data['email_from'],
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'name_from',
            'label' => Yii::t('app', 'Им (от)'),
            'value' => $data['name_from'],
        ]) ?>
        <?= $this->render('/layouts/form/text', [
            'name' => 'email_to',
            'label' => Yii::t('app', 'E-mails (кому)'),
            'value' => $data['email_to'],
            'hint' => 'Input one e-mail per line',
        ]) ?>
        <?= $this->render('/layouts/form/text', [
            'name' => 'email_copy',
            'label' => Yii::t('app', 'E-mails (BCC)'),
            'value' => $data['email_copy'],
            'hint' => 'Input one e-mail per line',
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/string', [
            'name' => 'key',
            'label' => Yii::t('app', 'Ключ'),
            'value' => $data['key'],
        ]) ?>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'data' => $data,
        ]) ?>
    </form>
</div>