<?php

use Yii;
use yii\helpers\Url;

$this->title = Yii::t('app', 'История рассылок');
$this->params['breadcrumbs'][] = $this->title;

if (isset(Yii::$app->params['filter']['user_id'])) {
    $this->params['js_code'][] = '$("#filter_user_id").val("' . Yii::$app->params['filter']['user_id'] . '");';
}

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= $pages->totalCount ?></span>
    </h1>
    
    <div class="x_panel">
        <form id="filter_form" method="post" action="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id) ?>">
            <?= $this->render('/layouts/form') ?>
            <div class="row">
                <div class="col-md-4">
                    <label for="filter_keyword"><?= Yii::t('app', 'Текстовой поиск') ?></label>
                    <input type="text" name="filter[keyword]" id="filter_keyword" value="<?= Yii::$app->params['filter']['keyword'] ?>" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="filter_user_id"><?= Yii::t('app', 'Отправить' )?></label>
                    <select type="text" name="filter[user_id]" id="filter_user_id" class="form-control">
                        <option value="">-</option>
                        <option value="0"><?= Yii::t('app', 'Системное') ?></option>
                        <?php foreach ($users as $item) { ?>
                            <option value="<?= $item->id ?>"><?= $item->name ?> (#<?= $item->id ?>)</option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="filter_date_range"><?= Yii::t('app', 'Период') ?></label><br>
                    <?= $this->render('/layouts/datepicker', [
                        'name' => 'filter[date_range_from]',
                        'value' => Yii::$app->params['filter']['date_range_from'],
                    ]) ?>
                    <?= $this->render('/layouts/datepicker', [
                        'name' => 'filter[date_range_to]',
                        'value' => Yii::$app->params['filter']['date_range_to'],
                    ]) ?>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <br>
                    <button type="button" class="btn btn-default glyphicon glyphicon-repeat" onclick="clearSearchFilter(this);" title="<?= Yii::t('app', 'Сбросить') ?>"></button>
                    <button type="submit" class="btn btn-success"><?= Yii::t('app', 'Искать') ?></button>
                </div>
            </div>
        </form>
    </div>
    
    <?php if (!empty($records)) { ?>
        <?= $this->render('/layouts/records_limit') ?>
        
        <table class="table table-striped table-bordered table-hover non_list_table">
            <tr>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Тема'), 'field' => 'subject']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Отправить'), 'field' => 'user_name']) ?></th>
                <th><?= Yii::t('app', 'Recipients') ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Время'), 'field' => 'create_time']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Ошибка'), 'field' => 'error']) ?></th>
                <th><?= $this->render('/layouts/sort', ['label' => Yii::t('app', 'Результат'), 'field' => 'result']) ?></th>
            </tr>
            <?php foreach ($records as $row) { ?>
                <?php $recipients = json_decode($row['recipients']); ?>
                <tr>
                    <td><a href="<?= Url::to(Yii::$app->params['urls']['backend'] . 'email/historyview/' . $row['id']) ?>"><?= $row['subject'] ?></a></td>
                    <td>
                        <?php if (!empty($row['user_id'])) { ?>
                            <?= $row['user_name'] ?> (#<?= $row['user_id'] ?>)
                        <?php } else { ?>
                            System
                        <?php } ?>
                    </td>
                    <td><?= implode(', ', $recipients) ?></td>
                    <td><?= $row['create_time'] ?></td>
                    <td><?= $row['error'] ?></td>
                    <td>
                        <?php if ($row['result'] == 1) { ?>
                            <span class="label label-success"><?= Yii::t('app', 'Успех') ?></span>
                        <?php } else { ?>
                            <span class="label label-danger"><?= Yii::t('app', 'Ошибка') ?></span>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <?= $this->render('/layouts/pager', [
            'pages' => $pages
        ]) ?>
    <?php } else { ?>
        <?= Yii::t('app', 'Записи не найдены') ?>
    <?php } ?>
</div>