<?php

use Yii;

$this->title = Yii::t('app', 'Просмотреть историческую запись');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'История рассылок'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/history')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1>
        <?= $data['subject'] ?>
    </h1>
    
    <?php if (!empty($data)) { ?>
        <div class="row">
            <pre><?= $data['content'] ?></pre>
        </div>
        <div class="row">
            <div class="col-md-2"><?= Yii::t('app', 'Время') ?>:</div>
            <div class="col-md-10"><?= $data['create_time'] ?></div>
        </div>
        <div class="row">
            <div class="col-md-2"><?= Yii::t('app', 'Отправить') ?>:</div>
            <div class="col-md-10">
                <?php if (!empty($data['user_id'])) { ?>
                    <?= $data['user_name'] ?> (#<?= $data['user_id'] ?>)
                <?php } else { ?>
                    <?= Yii::t('app', 'Системное') ?>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"><?= Yii::t('app', 'Получатели') ?>:</div>
            <?php $recipients = json_decode($data['recipients']); ?>
            <div class="col-md-10"><?= implode(', ', $recipients) ?></div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-2"><?= Yii::t('app', 'Результат') ?>:</div>
            <div class="col-md-10">
                <?php if ($data['result'] == 1) { ?>
                    <span class="label label-success"><?= Yii::t('app', 'Успех') ?></span>
                <?php } else { ?>
                    <span class="label label-danger"><?= Yii::t('app', 'Ошибка') ?></span>
                <?php } ?>
            </div>
        </div>
        <?php if ($data['result'] == 0 && !empty($data['error'])) { ?>
            <div class="row">
                <div class="col-md-2"><?= Yii::t('app', 'Ошибка') ?>:</div>
                <div class="col-md-10"><?= $data['error'] ?></div>
            </div>
        <?php } ?>
        <hr>
        <div class="form-group">
            <a href="<?= \yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/history') ?>" class="btn btn-default"><?= Yii::t('app', 'Назад') ?></a>
        </div>
    <?php } else { ?>
        <?= $this->render('/layouts/record_not_found') ?>
    <?php } ?>
</div>