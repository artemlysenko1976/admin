<?php

use Yii;

?>

<div class="backend-default-index">
    <h1><?= Yii::t('app', 'Error') ?></h1>
    <p><?= $exception ?></p>
</div>