<?php

$this->title = 'Search by "' . $keyword . '"';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1>
        <?= $this->title ?>
        <span class="badge"><?= count($records) ?></span>
    </h1>
    <hr>
    
    <?php if (!empty($records)) { ?>
        <table class="table table-striped table-bordered table-hover non_list_table">
            <tr>
                <th>Object</th>
            </tr>
            <?php foreach ($records as $url => $object) { ?>
                <tr>
                    <td>
                        <a href="<?= $url ?>" target="_blank"><?= $object ?></a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
        No matches found
    <?php } ?>
</div>