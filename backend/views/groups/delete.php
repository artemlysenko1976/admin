<?php

use Yii;

$this->title = Yii::t('app', 'Удалить группу');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Группы администраторов'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('/layouts/delete', [
        'data' => $data,
        'warning' => ($depended_users > 0 ? 'This group is linked to ' . $depended_users . ' users' : '')
    ]); ?>
</div>