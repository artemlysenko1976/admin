<?php

use Yii;

if (!empty($data)) {
    foreach ($data->getPermissions() as $controller_name => $actions) {
        foreach ($actions as $action_name) {
            $this->params['js_code'][] = '$("#permissions_' . $controller_name . '_' . $action_name . '").prop("checked", true)';
        }
    }
}

?>

<div class="col-md-9">
    <form id="record_form" method="post" enctype="multipart/form-data">
        <?= $this->render('/layouts/form') ?>
        <?= $this->render('/layouts/form/id', [
            'data' => $data,
        ]) ?>
        <?= $this->render('/layouts/form/string', [
            'name' => 'name',
            'label' => Yii::t('app', 'Название'),
            'value' => $data['name'],
        ]) ?>
        <div class="form-group">
            <label class="control-label" for="permissions"><?= Yii::t('app', 'Права доступа') ?></label>
            <div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="permissions_all" onclick="checkAllCheckboxes(this, '.permissions');"> Check all permissions
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?php 
                    $col_count = ceil(count($menu_tree) / 3);
                    $index = 0;
                    ?>
                    <?php foreach ($menu_tree as $k => $item) { ?>
                        <?php $index++; ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="permissions_<?= $item['url'] ?>_all" class="permissions" onclick="checkAllCheckboxes(this, '.permissions_<?= $item['url'] ?>');"> &nbsp;
                                        <i class="fa <?= $item['class'] ?>"></i>&nbsp;
                                        <b><?= $item['name'] ?></b>
                                    </label>
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php foreach ($item['actions'] as $action) { ?>
                                    <?php if ($action['check'] == true) { ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="permissions[<?= $item['url'] ?>][]" id="permissions_<?= $item['url'] ?>_<?= $action['url'] ?>" value="<?= $action['url'] ?>" class="permissions permissions_<?= $item['url'] ?>" onclick="checkAllCheckboxes(this, '');"> 
                                            <?= $action['name'] ?>
                                        </label>
                                    </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <?php if ($index % $col_count == 0) { ?>
                            </div>
                            <div class="col-md-4">
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <hr>
        <?= $this->render('/layouts/form/button', [
            'data' => $data,
        ]) ?>
    </form>
</div>