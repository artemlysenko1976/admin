<?php

use Yii;

$this->title = Yii::t('app', 'Добавить группу');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Группы администраторов'), 
    'url' => yii\helpers\Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list')
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="backend-default-index">
    <h1><?= $this->title ?></h1>
    <?= $this->render('form', [
        'menu_tree' => $menu_tree
    ]) ?>
</div>