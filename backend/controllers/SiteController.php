<?php

namespace app\backend\controllers;

use Yii;
use yii\helpers\Url;
use app\library\GlobalSearch;
use app\models\Users;

class SiteController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionSearch()
    {
        if (Yii::$app->request->getIsPost()) {
            $keyword = Yii::$app->request->post('keyword');
            
            return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id . '?keyword=' . $keyword));
        }
        
        $keyword = Yii::$app->request->get('keyword');
        
        $obj = new GlobalSearch();
        $records = $obj->search($keyword);
        
        return $this->render('search', [
            'keyword' => $keyword,
            'records' => $records,
        ]);
    }
    
    public function actionCheckuseractivity()
    {
        if (!empty(Yii::$app->session->get('auth'))) {
            $user_data = Users::findOne([
                'id' => Yii::$app->session->get('auth')['id'],
                'active' => 1,
                'deleted' => 0,
            ]);
            if (empty($user_data)) {
                $result = 0;
            } else {
                $result = 1;
            }
        } else {
            $result = 0;
        }
        
        return \Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_HTML,
            'data' => $result,
        ]);
    }
    
//  WYSIWYG upload images methods
    public function actionUploadimages()
    {
        $fileName = $_FILES['imageName']['name'];
        $ext = substr(strrchr(strtolower($fileName), '.'), 1);
        $extArr = ['jpg', 'jpeg', 'gif', 'png', 'bmp', 'svg'];
        
        if (in_array($ext, $extArr)) {
            $tmpFileName = $_FILES['imageName']['tmp_name'];
            $newName =  $fileName . '_' . time() . '.' . $ext;
            move_uploaded_file($tmpFileName, $this->getUploadImagesDir() . $newName);
            $output =  '<div id="image">/images/upload/' . $newName . '</div>';
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_HTML,
                'data' => $output,
            ]);
        } else {
            
        }
    }
    
    public function actionViewimages()
    {
        $images = glob($this->getUploadImagesDir() . "*.*"); 

        usort($images, function ($a, $b) {
            return filemtime($b) - filemtime($a);
        });

        $img_names = [];
        foreach ($images as $image) {
            $image = '/images/upload/' . end(explode('/', $image));
            $img_names[] = $image;
        }
        
        return \Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'data' => $img_names,
        ]);
    }
    
    public function getUploadImagesDir()
    {
        return Yii::$app->basePath . '/web/images/upload/';
    }
    
    public function actionStoremenustate()
    {
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->session->set('backend_menu_state', Yii::$app->request->get('state'));
        }
    }
}