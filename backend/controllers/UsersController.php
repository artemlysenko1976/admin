<?php

namespace app\backend\controllers;

use yii;
use app\backend\controllers\AbstractController;
use yii\helpers\Url;
use app\models\Users;
use app\models\UsersActivity;
use app\models\UsersGroups;
use app\models\UsersGroupsMap;

/**
 * UsersController
 */
class UsersController extends AbstractController
{
    /**
     * Redirect to list action
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
    }
    
    /**
     * Displays list of records
     * @return string
     */
    public function actionList()
    {
        $filter_callbacks = [
            'keyword' => function($filter) {
                $fields = [
                    'name',
                    'first_name',
                    'second_name',
                    'last_name',
                    'email',
                    'phone',
                    'passport',
                    'docs',
                ];
                $where = [
                    'conditions' => "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            }
        ];
        
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $filter_callbacks);
        $where['conditions'][] = "deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["basic DESC", "name ASC"]);
        
        $obj = new Users();
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit']);
        
        return $this->render('list',            [
            'records' => $list['records'],
            'pages' => $list['pages'],
        ]);
    }
    
    /**
     * Add record records
     * @return string
     */
    public function actionAdd()
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = Users::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Users();
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
            }
        }
        
        $permissions_groups = UsersGroups::findAll(['deleted' => 0]);
        
        return $this->render('add', [
            'permissions_groups' => $permissions_groups
        ]);
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionEdit($id)
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = Users::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Users();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
            }
        }
        
        $data = Users::findOne(['id' => $id, 'deleted' => 0]);
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $id));
                }
            }
        }
        
        $permissions_groups = UsersGroups::findAll(['deleted' => 0]);
        
        return $this->render('edit', [
            'data' => $data,
            'record_history' => $record_history,
            'permissions_groups' => $permissions_groups
        ]);
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionDelete($id)
    {
        $data = Users::findOne([
            'id' => $id, 
            'deleted' => 0, 
            'basic' => 0
        ]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Users();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        return $this->render('delete', [
            'data' => $data
        ]);
    }
    
    /**
     * View record
     * @return string
     */
    public function actionView($id)
    {
        $data = Users::findOne([
            'id' => $id, 
            'deleted' => 0, 
            'basic' => 0
        ]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        return $this->render('view', [
            'data' => $data
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionValidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Users::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
    /**
     * Displays users activity
     * @return string
     */
    public function actionActivity()
    {
        $filter_callbacks = [
            'keyword' => function($filter) {
                $fields = [
                    'user_name',
                    'operation',
                    'object_name_'
                ];
                $where = [
                    'conditions' => "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            }
        ];
        
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $filter_callbacks);
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["create_time DESC"]);
        
        $obj = new UsersActivity();
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit']);
        
        $operations = UsersActivity::getOperations();
        $users = Users::find()
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('activity', [
            'records' => $list['records'],
            'pages' => $list['pages'],
            'operations' => $operations,
            'users' => $users,
        ]);
    }
}