<?php

namespace app\backend\controllers;

use yii;
use app\models\Countries;
use yii\helpers\Url;
use app\models\Clients;

/**
 * ClientsController
 */
class ClientsController extends AbstractController
{
    /**
     * Redirect to list action
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
    }
    
    /**
     * Displays list of records
     * @return string
     */
    public function actionList()
    {
        $obj = new Clients();
        
        $select = [
            Clients::tableName() . ".*",
        ];
        $joins = [];
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $obj->getFilterCallbacks(), false);
        $where['conditions'][] = "deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["last_name ASC"]);
        
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit'], $joins, $select);
        
        $countries = Countries::find()
            ->where([
                'active' => 01,
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('list', [
            'records' => $list['records'],
            'pages' => $list['pages'],
            'countries' => $countries,
        ]);
    }
    
    /**
     * Add record records
     * @return string
     */
    public function actionAdd()
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = Clients::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Clients();
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
            }
        }
        
        $countries = Countries::find()
            ->where([
                'active' => 01,
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('add', [
            'countries' => $countries,
        ]);
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionEdit($id)
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = Clients::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Clients();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
            }
        }
        
        $data = Clients::findOne([
            'id' => $id, 
            'deleted' => 0
        ]);
        
        if (!empty($data['investor'])) {
//            $record_history = $data->getHistory();
//            
////  Restore mode
//            $restore_id = Yii::$app->request->get('restore');
//            if (!empty($restore_id)) {
//                $data = $data->getRestoreData($restore_id);
//                if (empty($data)) {
//                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $id));
//                }
//            }
        }
        
        $countries = Countries::find()
            ->where([
                'active' => 01,
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('edit', [
            'data' => $data,
            'record_history' => $record_history,
            'countries' => $countries,
        ]);
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionDelete($id)
    {
        $data = Clients::findOne([
            'id' => $id, 
            'deleted' => 0,
        ]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Clients();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        return $this->render('delete', [
            'data' => $data
        ]);
    }
    
    /**
     * View record
     * @return string
     */
    public function actionView($id)
    {
        $data = Clients::findOne([
            'id' => $id, 
            'deleted' => 0,
        ]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        return $this->render('view', [
            'data' => $data,
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionValidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Clients::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
//  Ajax methods ===============================================================
    /**
     * Load case details
     * @return string
     */
//    public function actionLoadcase($id)
//    {
//        if (Yii::$app->request->getIsAjax()) {
//            
//            
//            return \Yii::createObject([
//                'class' => 'yii\web\Response',
//                'format' => \yii\web\Response::FORMAT_HTML,
//                'data' => $output,
//            ]);
//        }
//    }
    
    /**
     * Action for Ajax autocomplete search
     * @return array
     */
    public function actionAutocomplete()
    {
        if (Yii::$app->request->getIsAjax()) {
            $keyword = Yii::$app->request->get('term');
            $list = Clients::autocompleteSearch($keyword, (Yii::$app->request->get()['conditions']));
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $list,
            ]);
        }
    }
}