<?php

namespace app\backend\controllers;

use yii;
use app\backend\controllers\AbstractController;
use yii\helpers\Url;
use app\models\Languages;
use app\models\Dictionary;
use app\models\DictionaryLocal;

/**
 * LocalController
 */
class LocalController extends AbstractController
{
    /**
     * Redirect to list action
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/langslist'));
    }
    
//  Languages ==================================================================
    /**
     * Displays list of records
     * @return string
     */
    public function actionLangslist()
    {
        $where['conditions'][] = 'deleted = 0';
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["basic DESC", "name ASC"]);
        
        $obj = new Languages();
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit']);
        
        return $this->render('langslist', [
            'records' => $list['records'],
            'pages' => $list['pages'],
        ]);
    }
    
    /**
     * Add record
     * @return string
     */
    public function actionLangsadd()
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = Languages::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Languages();
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/langslist'));
            }
        }
        
        return $this->render('langsadd');
    }
    
    /**
     * Edit record
     * @return string
     */
    public function actionLangsedit($id)
    {
        $data = Languages::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/langslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $errors = Languages::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Languages();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/langslist'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $id));
                }
            }
        }
        
        return $this->render('langsedit', [
            'data' => $data,
            'record_history' => $record_history,
        ]);
    }
    
    /**
     * Delete record
     * @return string
     */
    public function actionLangsdelete($id)
    {
        $data = Languages::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/langslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Languages();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/langslist'));
        }
        
        return $this->render('langsdelete', [
            'data' => $data,
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionLangsvalidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Languages::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
//  Dictionary =================================================================
    /**
     * Displays list of records
     * @return string
     */
    public function actionDictlist()
    {
        $obj = new Dictionary();
        $obj->buildDictFiles();
        
        $select = [
            Dictionary::tableName() . ".*",
            "dl.entry",
            "(SELECT GROUP_CONCAT(lang) FROM " . DictionaryLocal::tableName() . " WHERE main_id = " . Dictionary::tableName() . ".id AND entry != '') AS versions"
        ];
        $joins = [
            [
                "JOIN",
                DictionaryLocal::tableName() . " AS dl",
                "dl.main_id = " . Dictionary::tableName() . ".id AND dl.lang = '" . Yii::$app->params['lang']->code . "'"
            ],
        ];
        
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $obj->getFilterCallbacks());
        $where['conditions'][] = "deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["key ASC"]);
        
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit'], $joins, $select);
        
        return $this->render('dictlist', [
            'records' => $list['records'],
            'pages' => $list['pages'],
        ]);
    }
    
    /**
     * Add record
     * @return string
     */
    public function actionDictadd()
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = Dictionary::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Dictionary();
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist'));
            }
        }
        
        return $this->render('dictadd');
    }
    
    /**
     * Edit record
     * @return string
     */
    public function actionDictedit($id)
    {
        $data = Dictionary::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $errors = Dictionary::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Dictionary();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $id));
                }
            }
        }
        
        return $this->render('dictedit', [
            'data' => $data,
            'record_history' => $record_history,
        ]);
    }
    
    /**
     * Delete record
     * @return string
     */
    public function actionDictdelete($id)
    {
        $data = Dictionary::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Dictionary();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist'));
        }
        
        return $this->render('dictdelete', [
            'data' => $data,
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionDictvalidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Dictionary::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
    /**
     * Exports list of records
     * @return string
     */
    public function actionDictexport()
    {
        if (Yii::$app->request->getIsPost()) {
//  Set filter
            $session_filter = Yii::$app->session->get('filter');
            $filter = [];
            if (isset($session_filter['/backend/' . Yii::$app->controller->id . '/dictlist'])) {
                $filter = $session_filter['/backend/' . Yii::$app->controller->id . '/dictlist'];
            }
            
//  Set current lang to message lang for sending answer
            $current_lang = Yii::$app->params['lang'];
            $message_lang = Languages::findOne(['code' => Yii::$app->request->post('lang')]);
            Yii::$app->language = $message_lang->code;
            Yii::$app->params['lang'] = $message_lang;
            
//  Create conditions
            $obj = new Dictionary();
            $where = $this->createConditionFromFilter($filter, $obj->getFilterCallbacks());
            $where['conditions'][] = "deleted = 0";
            if (Yii::$app->request->post('untranslated') == 1) {
                $where['conditions'][] = "(entry IS NULL OR entry = '')";
            }
            
//  Get records
            $list = $obj->getPagerList($where, ["key ASC"], 1, \app\library\Export::$records_limit);
            
//  Output
            $export_obj = \app\library\Export::factory(Yii::$app->request->post('format'));
            $export_obj->build($list['records'], 'dictionary_' . Yii::$app->request->post('lang'));
            
//  Restore current lang
            Yii::$app->language = $current_lang->code;
            Yii::$app->params['lang'] = $current_lang;
            
            exit();
        }
        
        return $this->render('dictexport');
    }
    
    /**
     * Import list of records
     * @return string
     */
    public function actionDictimport()
    {
        if (Yii::$app->request->getIsPost()) {
            $obj = new Dictionary();
            $result = $obj->import('file', Yii::$app->request->post('lang'));
            
            if (empty($result['errors'])) {
                Yii::$app->session->setFlash('success', $result['inserted'] . ' records updated successfully');
                return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictlist'));
            } else {
                Yii::$app->session->setFlash('error', implode('<br>', $result['errors']));
                return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/dictimport'));
            }
        }
        
        return $this->render('dictimport');
    }
    
    /**
     * Delete record
     * @return string
     */
    public function actionDictcollect()
    {
        if (Yii::$app->request->getIsAjax()) {
            $result = Dictionary::collectUntranslatedEntries();
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $result,
            ]);
        }
        
        return $this->render('dictcollect');
    }
}