<?php

namespace app\backend\controllers;

use yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Users;
use app\models\Settings;
use app\models\Languages;
use app\models\AbstractModel;

class AbstractController extends Controller
{
    public $layout = 'main';
    
    protected $view_params = [];
    
    protected $auth;
    
    public static $system_controllers = [
        'auth',
        'site',
    ];
    
    public function beforeAction($action)
    {
        define('BACKEND', true);
        
//  Clean up current URL
        $cleaned_current_url = reset(explode('?', Url::current()));
        
//  Get site settings
        Yii::$app->params['settings'] = Settings::findOne(1);
        
//  Get languages
        Yii::$app->params['langs'] = Languages::find()
            ->where(['deleted' => 0])
            ->orderBy("basic DESC, name ASC")
            ->all();
        
//  Set current language
        Yii::$app->params['lang'] = Languages::getAdminLang();
        Yii::$app->language = Yii::$app->params['lang']->code;
        
//  Unauthorized ---------------------------------------------------------------
        if (empty(Yii::$app->session->get('auth'))) {
            if ($cleaned_current_url == Yii::$app->params['urls']['backend'] . 'auth/externalauth') {
                $this->enableCsrfValidation = false;
            }
            
            if (!in_array(Yii::$app->controller->id, self::$system_controllers)) {
                if (!Yii::$app->request->getIsAjax()) {
                    Yii::$app->session->set('redirect_url', Url::current());
                }
                $this->redirect(Url::to([Yii::$app->params['urls']['backend'] . 'auth/login']), 301);
                Yii::$app->end();
            }
            
            return parent::beforeAction($action);
        }
        
//  Authorized -----------------------------------------------------------------
//  Authorization
        $this->auth = Yii::$app->session->get('auth');
        $this->auth['last_activity_time'] = time();

//  Check if current user still active
        $user_data = Users::findOne([
            'id' => Yii::$app->session->get('auth')['id'],
            'active' => 1,
            'deleted' => 0,
        ]);
        if (empty($user_data)) {
            $this->redirect(Url::to([Yii::$app->params['urls']['backend'] . 'auth/logout']), 301);
            return false;
        }
        
//  Remove unavailable langs
        if (!empty(Yii::$app->session->get('auth')['available_langs'])) {
            foreach (Yii::$app->params['langs'] as $k => $v) {
                if (!in_array($v->code, Yii::$app->session->get('auth')['available_langs'])) {
                    unset(Yii::$app->params['langs'][$k]);
                }
            }
        }

//  Get menu
        $menu = $this->getMenu();
        Yii::$app->params['menu'] = $menu;

//  Check permissions
        $allow_access = Users::checkAccessPermissions(Yii::$app->controller->id, Yii::$app->controller->action->id);
        if ($allow_access === false) {
            $this->redirect(Url::to([Yii::$app->params['urls']['backend'] . 'auth/denied']), 301);
            return false;
        }

//  Filter request
        $filter = Yii::$app->request->post('filter');
        $session_filter = Yii::$app->session->get('filter');
        if (!empty($filter)) {
            if (!empty($session_filter)) {
                $session_filter[$cleaned_current_url] = $filter;
            } else {
                $session_filter = [
                    $cleaned_current_url => $filter
                ];
            }
            Yii::$app->session->set('filter', $session_filter);

            $this->redirect(Url::current(), 301);
            return false;
        } else {
            if (!empty($session_filter[$cleaned_current_url])) {
                $current_filter = $session_filter[$cleaned_current_url];
                Yii::$app->params['filter'] = $current_filter;
            }
        }

//  Sort request
        $sort = Yii::$app->request->post('sort');
        $session_sort = Yii::$app->session->get('sort');
        if (!empty($sort)) {
            if (!empty($session_sort)) {
                $session_sort[$cleaned_current_url] = $sort;
            } else {
                $session_sort = [
                    $cleaned_current_url => $sort
                ];
            }
            Yii::$app->session->set('sort', $session_sort);

            $this->redirect(Url::current(), 301);
            return false;
        } else {
            if (!empty($session_sort[$cleaned_current_url])) {
                $current_sort = $session_sort[$cleaned_current_url];
                Yii::$app->params['sort'] = $current_sort;
            }
        }

//  Limit request
        $limit = Yii::$app->request->post('list_limit');
        $session_limit = Yii::$app->session->get('list_limit');
        if (!empty($limit)) {
            if (!empty($session_limit)) {
                $session_limit[$cleaned_current_url] = $limit;
            } else {
                $session_limit = [
                    $cleaned_current_url => $limit
                ];
            }
            Yii::$app->session->set('list_limit', $session_limit);

            $this->haedRedirect(Url::current(), 301);
            return false;
        } else {
            if (!empty($session_limit[$cleaned_current_url])) {
                $current_limit = $session_limit[$cleaned_current_url];
                Yii::$app->params['list_limit'] = $current_limit;
            }
        }
        
    	return parent::beforeAction($action);
    }
    
    protected function getMenu()
    {
        if (empty(Yii::$app->session->get('auth')['permissions'])) {
            return [];
        }
        
        $permissions = Yii::$app->session->get('auth')['permissions'];
        
//  Get menu tree
        $menu = include_once Yii::$app->basePath . '/backend/config/menu.php';
        foreach ($menu as $controller_k => $controller_data) {
            foreach ($controller_data['actions'] as $action_k => $action_data) {
                if (!in_array($action_data['url'], $permissions[$controller_data['url']])) {
                    unset($menu[$controller_k]['actions'][$action_k]);
                }
            }
        }
        
//  Clean up empty controllers
        foreach ($menu as $controller_k => $controller_data) {
            if (empty($controller_data['actions'])) {
                unset($menu[$controller_k]);
            }
        }
        
        return $menu;
    }
    
    protected function resultRedirect($result, $url, $message='')
    {
        if (!empty($result)) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Operation complete successfully') . (!empty($message) ? ' - ' . $message : ''));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Operation failed') . (!empty($message) ? ' - ' . $message : ''));
        }
        
        return $this->redirect($url);
    }
    
    /**
     * Builds search condition from filter data
     * @param array $filter
     * @param array $callbacks
     * @return array
     */
    public function createConditionFromFilter($filter, $callbacks=[], $escape_field_names=true)
    {
        if (empty($filter)) {
            return [];
        }
        
        $output = [
            'conditions' => [],
            'binds' => []
        ];
        
        foreach ($filter as $filter_k => $filter_v) {
            if ($filter_v === '') {
                continue;
            }
            
            if (isset($callbacks[$filter_k])) {
                $result = $callbacks[$filter_k]($filter_v);
                if (!empty($result['conditions'])) {
                    $output['conditions'][] = $result['conditions'];
                }
                if (!empty($result['binds'])) {
                    $output['binds'] = array_merge($output['binds'], $result['binds']);
                }
            } elseif (isset(AbstractModel::getSearchCallbacks()[$filter_k])) {
                $result = AbstractModel::getSearchCallbacks()[$filter_k]($filter_v);
                if (!empty($result['conditions'])) {
                    $output['conditions'][] = $result['conditions'];
                }
                if (!empty($result['binds'])) {
                    $output['binds'] = array_merge($output['binds'], $result['binds']);
                }
            } else {
                $escape = ($escape_field_names === true ? "`" : "");
                
                $output['conditions'][] = $escape . $filter_k . $escape . " = :" .$filter_k;
                $output['binds'][":" .$filter_k] = $filter_v;
            }
        }
        
        return $output;
    }
    
    protected function haedRedirect(string $url, int $status_code=302)
    {
        http_response_code($status_code);
        header("Location: " . $url);
        exit();
    }
}