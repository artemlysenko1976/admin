<?php

namespace app\backend\controllers;

use yii;
use yii\web\Controller;
use app\backend\controllers\AbstractController;
use yii\helpers\Url;
use app\models\Users;
use app\models\UsersGroups;
use app\models\UsersGroupsMap;

/**
 * GroupsController
 */
class GroupsController extends AbstractController
{
    /**
     * Redirect to list action
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
    }
    
    /**
     * Displays list of records
     * @return string
     */
    public function actionList()
    {
        $filter_callbacks = [
            'keyword' => function($filter) {
                $fields = [
                    'name',
                ];
                $where = [
                    'conditions' => "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            }
        ];
        
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $filter_callbacks);
        $where['conditions'][] = "deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["basic DESC", "name ASC"]);
        
        $obj = new UsersGroups();
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit']);
        
        return $this->render(
            'list',
            [
                'records' => $list['records'],
                'pages' => $list['pages'],
            ]
        );
    }
    
    /**
     * Add record records
     * @return string
     */
    public function actionAdd()
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = UsersGroups::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new UsersGroups();
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
            }
        }
        
        $menu_tree = UsersGroups::getAvailableSections();
        
        return $this->render(
            'add',
            [
                'menu_tree' => $menu_tree
            ]
        );
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionEdit($id)
    {
        $data = UsersGroups::findOne(['id' => $id, 'deleted' => 0, 'basic' => 0]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $errors = UsersGroups::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new UsersGroups();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $id));
                }
            }
        }
        
        $menu_tree = UsersGroups::getAvailableSections();
        
        return $this->render(
            'edit',
            [
                'data' => $data,
                'record_history' => $record_history,
                'menu_tree' => $menu_tree
            ]
        );
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionDelete($id)
    {
        $data = UsersGroups::findOne([
            'id' => $id, 
            'deleted' => 0, 
            'basic' => 0
        ]);
        if (empty($data) || !empty($data->key)) {
            $this->redirect(Url::to('/backend/' . Yii::$app->controller->id . '/list'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new UsersGroups();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        $depended_users = UsersGroupsMap::find(['group_id' => $data->id])
            ->count();
    
        return $this->render(
            'delete',
            [
                'data' => $data,
                'depended_users' => $depended_users,
            ]
        );
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionValidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = UsersGroups::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
}