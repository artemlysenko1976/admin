<?php

namespace app\backend\controllers;

use yii;
use app\backend\controllers\AbstractController;
use app\models\CronReports;

/**
 * SystemController
 */
class SystemController extends AbstractController
{
    /**
     * System information
     * @return string
     */
    public function actionInfo()
    {
        try {
            $framework_version = Yii::getVersion();
            $php_version = phpversion();
            $os_version = PHP_OS;
        } catch (\Exception $e) {
            
        }
        
        try {
            $nginx_version = shell_exec('nginx -v');
        } catch (\Exception $ex) {
            
        }
        
        try {
            $apache_version = shell_exec('apache2 -v');
        } catch (\Exception $ex) {
            
        }
        
        try {
            $mysql_version = shell_exec('mysql --version');
        } catch (\Exception $ex) {
            
        }
        
        try {
            $postgresql_version = shell_exec('psql --version');
        } catch (\Exception $ex) {
            
        }
        
        try {
            if (is_callable(geoip_database_info)) {
                $geoip_db_info = geoip_database_info();
                $geoip_db_all_info = geoip_db_get_all_info();
            }
        } catch (\Exception $ex) {
            
        }
        
        return $this->render('info', [
            'framework_version' => $framework_version,
            'php_version' => $php_version,
            'nginx_version' => $nginx_version,
            'apache_version' => $apache_version,
            'mysql_version' => $mysql_version,
            'postgresql_version' => $postgresql_version,
            'os_version' => $os_version,
            'geoip_db_info' => $geoip_db_info,
            'geoip_db_all_info' => $geoip_db_all_info,
        ]);
    }
    
    /**
     * PHP information
     * @return string
     */
    public function actionPhpinfo()
    {
        echo phpinfo();
        exit();
    }
    
    /**
     * Server information
     * @return string
     */
    public function actionServer()
    {
        $info = [];
        foreach ($_SERVER as $k => $v) {
            $k = ucfirst(strtolower(str_replace('_', ' ', $k)));
            $info[$k] = $v;
        }
        
        return $this->render('server', [
            'info' => $info,
        ]);
    }
    
    /**
     * Displays list of records
     * @return string
     */
    public function actionCron()
    {
        $obj = new CronReports();
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $obj->getFilterCallbacks(), false);
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["start_timestamp DESC"]);
        
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit']);
        
        $cron_names = CronReports::getUniqueColValues('name');
        
        return $this->render('cron', [
            'records' => $list['records'],
            'pages' => $list['pages'],
            'cron_names' => $cron_names,
        ]);
    }
}