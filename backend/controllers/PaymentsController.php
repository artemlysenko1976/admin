<?php

namespace app\backend\controllers;

use yii;
use yii\helpers\Url;
use app\models\Payments;
use app\models\PaymentsMethods;
use app\models\PaymentsMethodsLocal;
use app\models\PaymentsMethodsMap;
use app\models\Clients;
use app\models\Currencies;
use app\models\CurrenciesLocal;

/**
 * PaymentsController
 */
class PaymentsController extends AbstractController
{
    /**
     * Redirect to list action
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paymentslist'));
    }
    
#   Payments ===================================================================
    /**
     * Displays list of records
     * @return string
     */
    public function actionPaymentslist()
    {
        $obj = new Payments();
        
        $select = [
            Payments::tableName() . ".*",
            "cl.first_name AS client_first_name",
            "cl.last_name AS client_last_name",
            "pml.name AS method_name",
            "cu.code AS currency_code",
        ];
        $joins = [
            [
                "LEFT JOIN",
                Clients::tableName() . " AS cl",
                "cl.id = " . Payments::tableName() . ".client_id"
            ],
//            [
//                "JOIN",
//                Orders::tableName() . " AS o",
//                "o.id = " . Payments::tableName() . ".order_id"
//            ],
            [
                "JOIN",
                PaymentsMethodsLocal::tableName() . " AS pml",
                "pml.main_id = " . Payments::tableName() . ".method_id AND pml.lang = '" . Yii::$app->params['lang']['code'] . "'"
            ],
            [
                "JOIN",
                Currencies::tableName() . " AS cu",
                "cu.id = " . Payments::tableName() . ".currency_id"
            ],
        ];
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $obj->getFilterCallbacks(), false);
        $where['conditions'][] = Payments::tableName() . ".deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["create_timestamp DESC"]);
        
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit'], $joins, $select);
        
        $currencies = Currencies::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        $methods = PaymentsMethods::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('paymentslist', [
            'records' => $list['records'],
            'pages' => $list['pages'],
            'methods' => $methods,
            'currencies' => $currencies,
        ]);
    }
    
    /**
     * View record
     * @return string
     */
    public function actionPaymentsview($id)
    {
        $data = Payments::findOne([
            'id' => $id, 
            'deleted' => 0,
        ]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paymentslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $errors = Payments::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Payments();
                $result = $obj->addComment(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id . '/' . $id));
            }
        }
        
        return $this->render('paymentsview', [
            'data' => $data,
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionPaymentsvalidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Payments::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
#   Currencies =================================================================
    /**
     * Displays list of records
     * @return string
     */
    public function actionCurrencieslist()
    {
        $obj = new Currencies();
        
        $select = [
            Currencies::tableName() . ".*",
            "(SELECT STRING_AGG(name,', ') "
            . "FROM " . PaymentsMethodsLocal::tableName() . " AS pml "
            . "JOIN " . PaymentsMethodsMap::tableName() . " AS pmm ON pmm.method_id = pml.main_id "
            . "WHERE pml.lang = '" . Yii::$app->params['lang']['code'] . "' "
            . "AND pmm.currency_id = " . Currencies::tableName() . ".id) AS methods",
        ];
        $joins = [
            [
                "LEFT JOIN",
                PaymentsMethodsMap::tableName() . " AS pmm",
                "pmm.currency_id = " . Currencies::tableName() . ".id"
            ]
        ];
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $obj->getFilterCallbacks(), false);
        $where['conditions'][] = "deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["basic DESC", "name ASC"]);
        
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit'], $joins, $select);
        
        $methods = PaymentsMethods::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('currencieslist', [
            'records' => $list['records'],
            'pages' => $list['pages'],
            'methods' => $methods,
        ]);
    }
    
    /**
     * Add record records
     * @return string
     */
    public function actionCurrenciesadd()
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = Currencies::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Currencies();
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/currencieslist'));
            }
        }
        
        $methods = PaymentsMethods::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('currenciesadd', [
            'methods' => $methods,
        ]);
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionCurrenciesedit($id)
    {
        $data = Currencies::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/currencieslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $errors = Currencies::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Currencies();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/currencieslist'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/currenciesedit/' . $id));
                }
            }
        }
        
        $methods = PaymentsMethods::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('currenciesedit', [
            'data' => $data,
            'record_history' => $record_history,
            'methods' => $methods,
        ]);
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionCurrenciesdelete($id)
    {
        $data = Currencies::findOne([
            'id' => $id, 
            'basic' => 0,
            'deleted' => 0,
        ]);
        if (empty($data)) {
            $this->redirect(Url::to('/backend/' . Yii::$app->controller->id . '/currencieslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Currencies();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/currencieslist'));
        }
        
        return $this->render('currenciesdelete', [
            'data' => $data
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionCurrenciesvalidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Currencies::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
#   Methods ====================================================================
    /**
     * Displays list of records
     * @return string
     */
    public function actionMethodslist()
    {
        $obj = new PaymentsMethods();
        
        $select = [
            PaymentsMethods::tableName() . ".*",
            "c.code AS comission_currency",
            "(SELECT STRING_AGG(name,', ') "
            . "FROM " . CurrenciesLocal::tableName() . " AS cl "
            . "JOIN " . PaymentsMethodsMap::tableName() . " AS pmm ON pmm.currency_id = cl.main_id "
            . "WHERE cl.lang = '" . Yii::$app->params['lang']['code'] . "' "
            . "AND pmm.method_id = " . PaymentsMethods::tableName() . ".id) AS currencies",
        ];
        $joins = [
            [
                "LEFT JOIN",
                PaymentsMethodsMap::tableName() . " AS pmm",
                "pmm.method_id = " . PaymentsMethods::tableName() . ".id"
            ],
            [
                "LEFT JOIN",
                Currencies::tableName() . " AS c",
                "c.id = " . PaymentsMethods::tableName() . ".comission_currency_id"
            ],
        ];
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $obj->getFilterCallbacks(), false);
        $where['conditions'][] = PaymentsMethods::tableName() . ".deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["name ASC"]);
        
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit'], $joins, $select);
        
        $currencies = Currencies::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('methodslist', [
            'records' => $list['records'],
            'pages' => $list['pages'],
            'currencies' => $currencies,
        ]);
    }
    
    /**
     * Add record records
     * @return string
     */
    public function actionMethodsadd()
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = PaymentsMethods::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new PaymentsMethods();
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/methodslist'));
            }
        }
        
        $currencies = Currencies::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('methodsadd', [
            'currencies' => $currencies,
        ]);
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionMethodsedit($id)
    {
        $data = PaymentsMethods::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/methodslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $errors = PaymentsMethods::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new PaymentsMethods();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/methodslist'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/methodsedit/' . $id));
                }
            }
        }
        
        $currencies = Currencies::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('methodsedit', [
            'data' => $data,
            'record_history' => $record_history,
            'currencies' => $currencies,
        ]);
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionMethodsdelete($id)
    {
        $data = PaymentsMethods::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            $this->redirect(Url::to('/backend/' . Yii::$app->controller->id . '/methodslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new PaymentsMethods();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/methodslist'));
        }
        
        return $this->render('methodsdelete', [
            'data' => $data
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionMethodsvalidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = PaymentsMethods::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
}