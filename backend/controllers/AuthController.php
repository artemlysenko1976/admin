<?php

namespace app\backend\controllers;

use yii;
use yii\web\Controller;
use app\backend\controllers\AbstractController;
use yii\helpers\Url;
use app\models\Users;
use app\models\UsersActivity;
use app\models\LoginHistory;
use app\models\PasswordRestores;
use app\models\CrossdomainAuthorizations;
use app\library\Utils;

/**
 * AuthController
 */
class AuthController extends AbstractController
{
    protected static $captcha_var = '__captcha/backend/site/captcha';
    
//  Auth methods ===============================================================
    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
//  If already authorized
        if (!empty(Yii::$app->session->get('auth'))) {
            return $this->redirect(Url::to([Yii::$app->params['urls']['backend']]), 301);
        }
        
//  Check login tries
        $history_obj = new LoginHistory();
        $tries_over = $history_obj->checkTries(Yii::$app->request->getUserIP());
        
//  Quick authorization ================================
        $code = Yii::$app->request->get('code');
        if (!empty($code)) {
//  Get restore data
            $restore_data = PasswordRestores::find()
                ->where([
                    'code' => $code,
                    'used' => 0
                ])
                ->andWhere("valid_to >= '" . date('Y-m-d H:i:s', time()) . "'")
                ->one();
            if (empty($restore_data)) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Wrong authorization link'));
                return $this->redirect(Url::to(['auth/login']), 301);
            }
            
//  Get user data
            $data = Users::find()
                ->where([
                    'id' => $restore_data->user_id,
                    'deleted' => 0,
                    'active' => 1
                ])
                ->one();
            if (empty($data)) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'User not found'));
                return $this->redirect(Url::to(['auth/login']), 301);
            }
            
//  Deactivate restore data
            $restore_data->used = 1;
            $restore_data->save(false);
            
//  Get permissions
            $permissions = Users::getPermissions($data);
            
//  Transform to array
            $data = $data->toArray();
            
            if (!empty($permissions)) {
                $data['permissions'] = $permissions;
            }
            
            if (!empty($data['available_langs'])) {
                $data['available_langs'] = json_decode($data['available_langs']);
            }
            
//  Set session data
            $data['login_time'] = time();
            Yii::$app->session->set('auth', $data);
            
//  Log activity
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_SIGN_IN);
            
//  Log login result to history
            $history_obj->log($data);
            $history_obj->updateResult(true);
            
            Yii::$app->session->set(self::$captcha_var, null);
            
//  Redirect
            if (!empty(Yii::$app->request->get('redirect'))) {
                return $this->redirect(Yii::$app->request->get('redirect'), 301);
            } elseif (Yii::$app->session->has('redirect_url')) {
                $redirect_url = Yii::$app->session->get('redirect_url');
                Yii::$app->session->set('redirect_url', null);
                
                return $this->redirect($redirect_url, 301);
            } else {
                return $this->redirect(Url::to([Yii::$app->params['urls']['backend']]), 301);
            }
        }
        
//  Normal authorization =======================================================
        $session_captcha = Yii::$app->session->get(self::$captcha_var);
        
        if (Yii::$app->request->isPost) {
//  Log login history
            $history_obj->log(Yii::$app->request->post());
            
//  Check params
            $email = Yii::$app->request->post('login');
            $password = Yii::$app->request->post('password');
            $captcha = Yii::$app->request->post('captcha');
            
            if (empty($email) || empty($password)) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Wrong e-mail or password'));
                return $this->redirect(Url::to(['auth/login']), 301);
            }
            
//  Check captcha
            if ($tries_over === true || !empty(Yii::$app->session->get(self::$captcha_var))) {
                if (empty($captcha) || $captcha !== $session_captcha) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Wrong confirmation code'));
                    return $this->redirect(Url::to(['auth/login']), 301);
                }
            }
            
//  Get data
            $data = Users::find()
                ->where(['email' => $email])
                ->one();
            
            if (empty($data)) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Wrong e-mail or password'));
                return $this->redirect(Url::to(['auth/login']), 301);
            }
            
            if ($data->password !== md5($password) || $data->active == 0 || $data->deleted == 1) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Wrong e-mail or password'));
                return $this->redirect(Url::to(['auth/login']), 301);
            }
            
//  Get permissions
            $permissions = Users::getPermissions($data);
            
//  Transform to array
            $data = $data->toArray();
            
            if (!empty($permissions)) {
                $data['permissions'] = $permissions;
            }
            
            if (!empty($data['available_langs'])) {
                $data['available_langs'] = json_decode($data['available_langs']);
            }
            
//  Set session data
            $data['login_time'] = time();
            Yii::$app->session->set('auth', $data);
            
//  Log activity
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_SIGN_IN);
            
//  Log login result to history
            $history_obj->updateResult(true);
            
            Yii::$app->session->set(self::$captcha_var, null);
            
//  Redirect
            if (Yii::$app->session->has('redirect_url')) {
                $redirect_url = Yii::$app->session->get('redirect_url');
                Yii::$app->session->set('redirect_url', null);
                
                return $this->redirect($redirect_url, 301);
            } else {
                return $this->redirect(Url::to([Yii::$app->params['urls']['backend']]), 301);
            }
        }
        
        return $this->render(
            'login',
            ['need_captcha' => $tries_over || !empty($session_captcha)]
        );
    }
    
    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
//  If not authrized yet
        if (empty(Yii::$app->session->get('auth'))) {
            return $this->redirect(Url::to(['auth/login']), 301);
        }
        
//  Log activity
        $activity = new UsersActivity();
        $activity->log(UsersActivity::OPERATION_SIGN_OUT);
        
//  Clean up session
        Yii::$app->session->set('auth', null);
        
//  Redirect
        return $this->redirect(Url::to(['auth/login']), 301);
    }
    
//  External auth methods ======================================================
    public function actionExternalauth()
    {
//  Init output
        $output = [
            'result' => false,
            'token' => '',
            'error' => '',
        ];
        
//  Get auth params
        $email = Yii::$app->request->post('login');
        $password = Yii::$app->request->post('password');
        $url = Yii::$app->request->post('url');
        $hash = Yii::$app->request->post('hash');
        
        if (empty($email) || empty($password) || empty($hash)) {
            $output['error'] = 'Login and Password are required';
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $output,
            ]);
        }
        
//  Check hash
        $generated_hash = Users::generateCrossDomainHash($email, $password);
        
        if ($generated_hash != $hash) {
            $output['error'] = 'Request data error';
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $output,
            ]);
        }
        
//  Get user
        $data = Users::find()
            ->where([
                'email' => $email,
                'password' => $password,
                'active' => 1,
                'deleted' => 0,
            ])
            ->asArray()
            ->one();
        
        if (empty($data)) {
            $output['error'] = 'User not found';
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $output,
            ]);
        }
        
//  Store auth token
        $token = Utils::generateRandomString(32);
        
        $auth_obj = new CrossdomainAuthorizations([
            'id_user' => $data['id'],
            'url' => $url,
            'token' => $token,
            'create_timestamp' => time(),
            'valid_timestamp' => (time() + CrossdomainAuthorizations::$record_life_time),
            'status' => 'new',
        ]);
        $auth_record_result = $auth_obj->save(false);
        
        if (empty($auth_record_result)) {
            $output['error'] = 'Unable to store session';
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $output,
            ]);
        }
        
//  Output
        $output['result'] = true;
        $output['token'] = $token;
        
        return \Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'data' => $output,
        ]);
    }
    
    public function actionExternallogin()
    {
//  Check user session
        if (!empty(Yii::$app->session->get('auth'))) {
            $this->redirect(Url::to([Yii::$app->params['urls']['backend']]), 301);
        }
        
//  Check token
        $token = Yii::$app->request->get('token');
        
        if (empty($token)) {
            $this->redirect(Url::to([Yii::$app->params['urls']['backend'] . 'auth/login']), 301);
            Yii::$app->end();
        }
        
//  Get auth record
        $auth_record = CrossdomainAuthorizations::find()
            ->where([
                'token' => $token,
                'status' => 'new',
            ])
            ->andWhere("valid_timestamp >= " . time())
            ->limit(1)
            ->one();
        
        if (empty($auth_record)) {
            $this->redirect(Url::to([Yii::$app->params['urls']['backend'] . 'auth/login']), 301);
            Yii::$app->end();
        }
        
//  Get user
        $data = Users::find()
            ->where([
                'id' => $auth_record->id_user,
                'active' => 1,
                'deleted' => 0,
            ])
            ->one();

        if (empty($data)) {
            $this->redirect(Url::to([Yii::$app->params['urls']['backend'] . 'auth/login']), 301);
            Yii::$app->end();
        }
        
//  Get permissions
        $permissions = Users::getPermissions($data);
        $data = $data->toArray();
        if (!empty($permissions)) {
            $data['permissions'] = $permissions;
        }

//  Set session data
        $data['login_time'] = time();
        Yii::$app->session->set('auth', $data);
        
//  Log activity
        $activity = new UsersActivity();
        $activity->log(UsersActivity::OPERATION_SIGN_IN);
        
//  Update auth record status
        $auth_record->status = 'used';
        $auth_record->save(false);
        
        $this->redirect(Url::to([Yii::$app->params['urls']['backend']]), 301);
    }
    
//  Password methods ===========================================================
    /**
     * Password
     * @return string
     */
    public function actionPassword()
    {
//  If not authorized
        if (empty(Yii::$app->session->get('auth'))) {
            return $this->redirect(Url::to([Yii::$app->params['urls']['backend']]), 301);
        }
        
        $old_password = (!empty(Yii::$app->session->get('restore_password')) ? false : true);
        
        if (Yii::$app->request->isPost) {
            $obj = new Users();
            $result = $obj->changePassword(Yii::$app->request->post('password'), Yii::$app->session->get('auth')['id']);
            
            Yii::$app->session->set('restore_password', null);
            
            return $this->resultRedirect($result, Url::to([Yii::$app->params['urls']['backend']]));
        }
        
        return $this->render(
            'password',
            ['old_password' => $old_password]
        );
    }
    
    /**
     * Restore
     * @return string
     */
    public function actionRestore()
    {
//  If already authorized
        if (!empty(Yii::$app->session->get('auth'))) {
            return $this->redirect(Url::to([Yii::$app->params['urls']['backend']]), 301);
        }
        
        if (Yii::$app->request->isPost) {
//  Check params
            $email = Yii::$app->request->post('email');
            if (empty($email) || filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
                return $this->redirect(Url::to(['auth/restoredone']), 301);
            }
            
//  Get data
            $data = Users::find()
                ->where(['email' => $email])
                ->andWhere(['deleted' => 0])
                ->andWhere(['active' => 1])
                ->one();
            if (empty($data)) {
                return $this->redirect(Url::to(['auth/restoredone']), 301);
            }
            
//  Create restore code
            $code = PasswordRestores::createRestoreCode($data);
            
//  Send restore e-mail
            Yii::$app->mailer->messageFromTemplate(
                'restore_password',
                ['link' => Url::to(Yii::$app->params['urls']['backend'] . 'auth/login?code=' . $code . '&redirect=/backend/auth/password', true)],
                $data->email
            );
            
//  Redirect
            Yii::$app->session->set('restore_password', true);
            return $this->redirect(Url::to(['auth/restoredone']), 301);
        }
        
        return $this->render('restore');
    }
    
    /**
     * Restoredone
     * @return string
     */
    public function actionRestoredone()
    {
        return $this->render('restoredone');
    }
    
    /**
     * Validate password
     * @return string
     */
    public function actionValidatepassword()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Users::validatePassword(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
//  Additional methods =========================================================
    /**
     * Access denied
     * @return string
     */
    public function actionDenied()
    {
        return $this->render('denied');
    }
}