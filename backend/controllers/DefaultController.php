<?php

namespace app\backend\controllers;

use yii;
use app\backend\controllers\AbstractController;
use app\models\Reminder;

/**
 * Default controller for the `backend` module
 */
class DefaultController extends AbstractController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'menu' => Yii::$app->params['menu'],
        ]);
    }
}