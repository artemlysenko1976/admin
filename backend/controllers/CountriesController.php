<?php

namespace app\backend\controllers;

use yii;
use app\backend\controllers\AbstractController;
use yii\helpers\Url;
use app\models\Countries;

/**
 * CountriesController
 */
class CountriesController extends AbstractController
{
    /**
     * Redirect to list action
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
    }
    
    /**
     * Displays list of records
     * @return string
     */
    public function actionList()
    {
        $filter_callbacks = [
            'keyword' => function($filter) {
                $fields = [
                    'name',
                    'iso_code',
                    'phone_code',
                ];
                $where = [
                    'conditions' => "(" . implode(" ILIKE :keyword OR ", $fields) . " ILIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            }
        ];
        
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $filter_callbacks, false);
        $where['conditions'][] = "deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["name ASC"]);
        
        $obj = new Countries();
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit']);
        
        $regions = Countries::getRegions();
        
        return $this->render('list', [
            'records' => $list['records'],
            'pages' => $list['pages'],
            'regions' => $regions,
        ]);
    }
    
    /**
     * Add record
     * @return string
     */
    public function actionAdd()
    {
        if (Yii::$app->request->getIsPost()) {
            $obj = new Countries();
            $errors = $obj->validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
            }
        }
        
        $regions = Countries::getRegions();
        
        return $this->render('add', [
            'regions' => $regions,
        ]);
    }
    
    /**
     * Edit record
     * @return string
     */
    public function actionEdit($id)
    {
        $data = Countries::findOne($id);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Countries();
            $errors = $obj->validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $id));
                }
            }
        }
        
        $regions = Countries::getRegions();
        
        return $this->render('edit', [
            'data' => $data,
            'record_history' => $record_history,
            'regions' => $regions,
        ]);
    }
    
    /**
     * Delete record
     * @return string
     */
    public function actionDelete($id)
    {
        $data = Countries::findOne($id);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Countries();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        return $this->render('delete', [
            'data' => $data,
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionValidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $obj = new Countries();
            $errors = $obj->validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
}