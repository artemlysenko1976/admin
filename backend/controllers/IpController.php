<?php

namespace app\backend\controllers;

use yii;
use yii\web\Controller;
use app\backend\controllers\AbstractController;
use yii\helpers\Url;
use app\models\Ips;

/**
 * IpController
 */
class IpController extends AbstractController
{
    /**
     * Redirect to list action
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
    }
    
    /**
     * Displays list of records
     * @return string
     */
    public function actionList()
    {
        $filter_callbacks = [
            'keyword' => function($filter) {
                $fields = [
                    'ip',
                    'comment',
                ];
                $where = [
                    'conditions' => "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            }
        ];
        
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $filter_callbacks);
        $where['conditions'][] = "deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["ip ASC"]);
        
        $obj = new Ips();
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit']);
        
        return $this->render(
            'list',
            [
                'records' => $list['records'],
                'pages' => $list['pages'],
            ]
        );
    }
    
    /**
     * Add record
     * @return string
     */
    public function actionAdd()
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = Ips::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Ips();
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
            }
        }
        
        return $this->render('add');
    }
    
    /**
     * Edit record
     * @return string
     */
    public function actionEdit($id)
    {
        $data = Ips::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $errors = Ips::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Ips();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $id));
                }
            }
        }
        
        return $this->render(
            'edit',
            [
                'data' => $data,
                'record_history' => $record_history,
            ]
        );
    }
    
    /**
     * Delete record
     * @return string
     */
    public function actionDelete($id)
    {
        $data = Ips::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Ips();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        return $this->render(
            'delete',
            [
                'data' => $data,
            ]
        );
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionValidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Ips::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
}