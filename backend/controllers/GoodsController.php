<?php

namespace app\backend\controllers;

use yii;
use yii\helpers\Url;
use app\models\Goods;
use app\models\GoodsLocal;
use app\models\GoodsCategories;
use app\models\GoodsCategoriesLocal;
use app\models\GoodsParameters;
use app\models\GoodsParametersLocal;
use app\models\GoodsParametersMap;
use app\models\Clients;
use app\models\Brands;

/**
 * GoodsController
 */
class GoodsController extends AbstractController
{
    /**
     * Redirect to list action
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodslist'));
    }
    
#   Goods ======================================================================
    /**
     * Displays list of records
     * @return string
     */
    public function actionGoodslist()
    {
        $obj = new Goods();
        
        $select = [
            Goods::tableName() . ".*",
            "gcl.name AS category_name",
            "c.first_name AS client_first_name",
            "c.last_name AS client_last_name",
            "b.name AS brand_name",
        ];
        $joins = [
            [
                "JOIN",
                GoodsLocal::tableName() . " AS gl",
                "gl.main_id = " . Goods::tableName() . ".id AND gl.lang = '" . Yii::$app->params['lang']['code'] . "'"
            ],
            [
                "JOIN",
                GoodsCategoriesLocal::tableName() . " AS gcl",
                "gcl.main_id = " . Goods::tableName() . ".category_id AND gcl.lang = '" . Yii::$app->params['lang']['code'] . "'"
            ],
            [
                "LEFT JOIN",
                Clients::tableName() . " AS c",
                "c.id = " . Goods::tableName() . ".client_id"
            ],
            [
                "LEFT JOIN",
                Brands::tableName() . " AS b",
                "b.id = " . Goods::tableName() . ".brand_id"
            ],
            
        ];
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $obj->getFilterCallbacks(), false);
        $where['conditions'][] = Goods::tableName() . ".deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["gl.name ASC"]);

        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit'], $joins, $select);
        
        $cats_obj = new GoodsCategories();
        $categories = $cats_obj->getNestedList([
            'deleted' => 0
        ]);
        
        $brands = Brands::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('goodslist', [
            'records' => $list['records'],
            'pages' => $list['pages'],
            'categories' => $categories,
            'brands' => $brands,
        ]);
    }
    
    /**
     * Add record
     * @return string
     */
    public function actionGoodsadd()
    {
        if (Yii::$app->request->getIsPost()) {
            $obj = new Goods();
            $errors = $obj->validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodslist'));
            }
        }
        
        $cats_obj = new GoodsCategories();
        $categories = $cats_obj->getNestedList([
            'deleted' => 0
        ]);
        
        $brands = Brands::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('goodsadd', [
            'categories' => $categories,
            'brands' => $brands,
        ]);
    }
    
    /**
     * Edit record
     * @return string
     */
    public function actionGoodsedit($id)
    {
        $data = Goods::findOne($id);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Goods();
            $errors = $obj->validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodslist'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodsedit/' . $id));
                }
            }
        }
        
        $cats_obj = new GoodsCategories();
        $categories = $cats_obj->getNestedList([
            'deleted' => 0
        ]);
        
        $brands = Brands::find()
            ->where([
                'deleted' => 0,
            ])
            ->orderBy("name ASC")
            ->all();
        
        return $this->render('goodsedit', [
            'data' => $data,
            'record_history' => $record_history,
            'categories' => $categories,
            'brands' => $brands,
        ]);
    }
    
    /**
     * Delete record
     * @return string
     */
    public function actionGoodsdelete($id)
    {
        $data = Goods::findOne([
            'id' => $id,
            'deleted' => 0,
        ]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Goods();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodslist'));
        }
        
        return $this->render('goodsdelete', [
            'data' => $data,
        ]);
    }
    
    /**
     * View record
     * @return string
     */
    public function actionGoodsview($id)
    {
        $data = Goods::findOne([
            'id' => $id, 
            'deleted' => 0,
        ]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/goodslist'));
        }
        
        return $this->render('goodsview', [
            'data' => $data,
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionGoodsvalidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $obj = new Goods();
            $errors = $obj->validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
    public function actionLoadparams()
    {
        if (Yii::$app->request->getIsAjax()) {
            $parameters = GoodsCategories::loadParams(
                Yii::$app->request->get('category_id'),
                Yii::$app->request->get('product_id')
            );
            
            $output = $this->renderAjax('params_table', [
                'parameters' => $parameters,
            ]);
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_HTML,
                'data' => $output,
            ]);
        }
    }
    
#   Categories =================================================================
    /**
     * Displays list of records
     * @return string
     */
    public function actionCatslist()
    {
        $obj = new GoodsCategories();
        
        if (Yii::$app->request->getIsPost()) {
            if (!empty(Yii::$app->request->post('ordering'))) {
                $result = $obj->changeOrdering(Yii::$app->request->post('ordering'));
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catslist'));
            }
        }
        
        $condition = [
            'deleted' => 0,
        ];
        $list = $obj->getTree($condition, ["ordering ASC"]);
        
        return $this->render('catslist', [
            'records' => $list,
        ]);
    }
    
    /**
     * Add record
     * @return string
     */
    public function actionCatsadd()
    {
        $obj = new GoodsCategories();
        
        if (Yii::$app->request->getIsPost()) {
            $errors = $obj->validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catslist'));
            }
        }
        
        $parent_items = $obj->getNestedList([
            'deleted' => 0
        ]);
        
        return $this->render('catsadd', [
            'parent_items' => $parent_items,
        ]);
    }
    
    /**
     * Edit record
     * @return string
     */
    public function actionCatsedit($id)
    {
        $data = GoodsCategories::findOne($id);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catslist'));
        }
        
        $obj = new GoodsCategories();
        
        if (Yii::$app->request->getIsPost()) {
            $errors = $obj->validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catslist'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catsedit/' . $id));
                }
            }
        }
        
        $parent_items = $obj->getNestedList([
            'deleted' => 0,
            "id != " . $data->id
        ]);
        
        return $this->render('catsedit', [
            'data' => $data,
            'record_history' => $record_history,
            'parent_items' => $parent_items,
        ]);
    }
    
    /**
     * Delete record
     * @return string
     */
    public function actionCatsdelete($id)
    {
        $data = GoodsCategories::findOne([
            'id' => $id,
            'deleted' => 0,
        ]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catslist'));
        }
        
        $child_records = GoodsCategories::find()
            ->where([
                'parent_id' => $data->id,
                'deleted' => 0,
            ])
            ->count();
        if (!empty($child_records)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new GoodsCategories();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/catslist'));
        }
        
        return $this->render('catsdelete', [
            'data' => $data,
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionCatsvalidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $obj = new GoodsCategories();
            $errors = $obj->validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
#   Parameters =================================================================
    /**
     * Displays list of records
     * @return string
     */
    public function actionParamslist()
    {
        $obj = new GoodsParameters();
        
        $select = [
            GoodsParameters::tableName() . ".*",
            "gpl.name",
            "COUNT(gpm.id) AS categories_count"
        ];
        $joins = [
            [
                "JOIN",
                GoodsParametersLocal::tableName() . " AS gpl",
                "gpl.main_id = " . GoodsParameters::tableName() . ".id AND gpl.lang = '" . Yii::$app->params['lang']['code'] . "'"
            ],
            [
                "LEFT JOIN",
                GoodsParametersMap::tableName() . " AS gpm",
                "gpm.parameter_id = " . GoodsParameters::tableName() . ".id"
            ],
            
        ];
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $obj->getFilterCallbacks(), false);
        $where['conditions'][] = "deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["name ASC"]);
        $groups = [
            GoodsParameters::tableName() . ".id",
            "gpl.name",
        ];
        
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit'], $joins, $select, $groups);
        
        $cats_obj = new GoodsCategories();
        $categories = $cats_obj->getNestedList([
            'deleted' => 0
        ]);
        
        return $this->render('paramslist', [
            'records' => $list['records'],
            'pages' => $list['pages'],
            'categories' => $categories,
        ]);
    }
    
    /**
     * Add record
     * @return string
     */
    public function actionParamsadd()
    {
        if (Yii::$app->request->getIsPost()) {
            $obj = new GoodsParameters();
            $errors = $obj->validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paramslist'));
            }
        }
        
        $cats_obj = new GoodsCategories();
        $categories = $cats_obj->getNestedList([
            'deleted' => 0
        ]);
        
        return $this->render('paramsadd', [
            'categories' => $categories,
        ]);
    }
    
    /**
     * Edit record
     * @return string
     */
    public function actionParamsedit($id)
    {
        $data = GoodsParameters::findOne($id);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paramslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new GoodsParameters();
            $errors = $obj->validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paramslist'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paramsedit/' . $id));
                }
            }
        }
        
        $cats_obj = new GoodsCategories();
        $categories = $cats_obj->getNestedList([
            'deleted' => 0
        ]);
        
        return $this->render('paramsedit', [
            'data' => $data,
            'record_history' => $record_history,
            'categories' => $categories,
        ]);
    }
    
    /**
     * Delete record
     * @return string
     */
    public function actionParamsdelete($id)
    {
        $data = GoodsParameters::findOne([
            'id' => $id,
            'deleted' => 0,
        ]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paramslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new GoodsParameters();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/paramslist'));
        }
        
        return $this->render('paramsdelete', [
            'data' => $data,
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionParamsvalidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $obj = new GoodsParameters();
            $errors = $obj->validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
#   Brands =====================================================================
    /**
     * Displays list of records
     * @return string
     */
    public function actionBrandslist()
    {
        $obj = new Brands();
        
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $obj->getFilterCallbacks(), false);
        $where['conditions'][] = "deleted = 0";
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["name ASC"]);
        
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit']);
        
        return $this->render('brandslist', [
            'records' => $list['records'],
            'pages' => $list['pages'],
        ]);
    }
    
    /**
     * Add record records
     * @return string
     */
    public function actionBrandsadd()
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = Brands::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Brands();
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/brandslist'));
            }
        }
        
        return $this->render('brandsadd');
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionBrandsedit($id)
    {
        $data = Brands::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/brandslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $errors = Brands::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Brands();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/brandslist'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/brandsedit/' . $id));
                }
            }
        }
        
        return $this->render('brandsedit', [
            'data' => $data,
            'record_history' => $record_history
        ]);
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionBrandsdelete($id)
    {
        $data = Brands::findOne(['id' => $id, 'deleted' => 0]);
        if (empty($data)) {
            $this->redirect(Url::to('/backend/' . Yii::$app->controller->id . '/brandslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Brands();
            $result = $obj->deleteRecord($id);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/brandslist'));
        }
        
        return $this->render('brandsdelete', [
            'data' => $data
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionBrandsvalidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Brands::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
}