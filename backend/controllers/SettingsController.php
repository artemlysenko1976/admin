<?php

namespace app\backend\controllers;

use yii;
use yii\helpers\Url;
use app\models\Settings;
use app\models\Variables;

/**
 * SettingsController
 */
class SettingsController extends AbstractController
{
    /**
     * Redirect to site action
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/site'));
    }
    
#   Site settings ==============================================================
    /**
     * Site record
     * @return string
     */
    public function actionSite()
    {
        $data = Settings::findOne(1);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/list'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $errors = Settings::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Settings();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/site'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/edit/' . $id));
                }
            }
        }
        
        return $this->render('site', [
            'data' => $data,
            'record_history' => $record_history,
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionValidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Settings::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
    
#   Variables ==================================================================
    /**
     * Displays list of records
     * @return string
     */
    public function actionVarslist()
    {
        $obj = new Variables();
        
        $where = $this->createConditionFromFilter(Yii::$app->params['filter'], $obj->getFilterCallbacks(), false);
        $sort = (!empty(Yii::$app->params['sort']) ? [Yii::$app->params['sort']] : ["key ASC"]);
        
        $list = $obj->getPagerList($where, $sort, Yii::$app->request->get('page', 1), Yii::$app->params['list_limit']);
        
        return $this->render('varslist', [
            'records' => $list['records'],
            'pages' => $list['pages'],
        ]);
    }
    
    /**
     * Add record records
     * @return string
     */
    public function actionVarsadd()
    {
        if (Yii::$app->request->getIsPost()) {
            $errors = Variables::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Variables();
                $id = $obj->addRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($id, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/varslist'));
            }
        }
        
        return $this->render('varsadd');
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionVarsedit($id)
    {
        $data = Variables::findOne($id);
        if (empty($data)) {
            $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/varslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $errors = Variables::validateRecord(Yii::$app->request->post());
            if (empty($errors)) {
                $obj = new Variables();
                $result = $obj->editRecord(Yii::$app->request->post());
                
                return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/varslist'));
            }
        }
        
        if (!empty($data)) {
            $record_history = $data->getHistory();
            
//  Restore mode
            $restore_id = Yii::$app->request->get('restore');
            if (!empty($restore_id)) {
                $data = $data->getRestoreData($restore_id);
                if (empty($data)) {
                    return $this->redirect(Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/varsedit/' . $id));
                }
            }
        }
        
        return $this->render('varsedit', [
            'data' => $data,
            'record_history' => $record_history,
        ]);
    }
    
    /**
     * Edit record records
     * @return string
     */
    public function actionVarsdelete($id)
    {
        $data = Variables::findOne([
            'id' => $id, 
        ]);
        if (empty($data)) {
            $this->redirect(Url::to('/backend/' . Yii::$app->controller->id . '/varslist'));
        }
        
        if (Yii::$app->request->getIsPost()) {
            $obj = new Variables();
            $result = $obj->deleteRecord($id, false);
            
            return $this->resultRedirect($result, Url::to(Yii::$app->params['urls']['backend'] . Yii::$app->controller->id . '/varslist'));
        }
        
        return $this->render('varsdelete', [
            'data' => $data
        ]);
    }
    
    /**
     * Validate record
     * @return string
     */
    public function actionVarsvalidate()
    {
        if (Yii::$app->request->getIsAjax()) {
            $errors = Variables::validateRecord(Yii::$app->request->post());
            
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'data' => $errors,
            ]);
        }
    }
}