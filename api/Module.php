<?php

namespace app\api;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\api\controllers';
    
    public function init()
    {
        parent::init();
        
        \Yii::configure($this, require(__DIR__ . '/config/config.php'));
        \Yii::$app->errorHandler->errorAction = 'api/default/error';
    }
}