<?php

return [
    'Get candles for case chart' => [
        'url' => '/api/chart/get/{investor_id}',
        'method' => 'GET',
        'parameters' => [
            'date_from' => [
                'type' => 'string',
                'description' => 'format: yyyy-mm-dd',
            ],
            'date_to' => [
                'type' => 'string',
                'description' => 'format: yyyy-mm-dd',
            ],
        ],
        'return' => [
            'type' => 'array',
            'description' => '[case: [currency: $currency, candles: [...]], positions: [$position_name: [currency: $currency, candles: [...]], ...]]',
        ],
    ],
    'Upload files to message' => [
        'url' => '/api/chat/uploadfiles/{message_id}',
        'method' => 'POST',
        'body parameters' => [
            'chat_id' => [
                'type' => 'integer',
            ],
            'investor_id' => [
                'type' => 'integer',
                'description' => 'format: yyyy-mm-dd',
            ],
            'files' => [
                'type' => 'array',
                'description' => 'array of files: [origin_file_name: base64_encode($file_content), ...]',
            ],
        ],
        'return' => [
            'type' => 'integer',
            'description' => 'quantity of uploaded files',
        ],
    ],
    'Upload investor images' => [
        'url' => '/api/investor/uploadimages/{$investor_id}',
        'method' => 'POST',
        'body parameters' => [
            'photo' => [
                'type' => 'string',
                'description' => 'base64_encode($file_content)',
            ],
            'passport_photo' => [
                'type' => 'string',
                'description' => 'base64_encode($file_content)',
            ],
            'selfy' => [
                'type' => 'string',
                'description' => 'base64_encode($file_content)',
            ],
        ],
        'return' => [
            'type' => 'array',
            'description' => '[photo: 1|0, passport_photo: 1|0, selfy: 1|0]',
        ],
    ],
    'Save investor goal' => [
        'url' => '/api/goal/save/{$investor_id}',
        'method' => 'POST',
        'body parameters' => [
            'name' => [
                'type' => 'string',
            ],
            'goal_age' => [
                'type' => 'integer',
            ],
            'init_contribution' => [
                'type' => 'float',
            ],
            'montly_dividents' => [
                'type' => 'float',
            ],
            'investor_income' => [
                'type' => 'float',
            ],
            'monthly_payments_percent' => [
                'type' => 'float',
            ],
            'month_payments_start' => [
                'type' => 'integer',
            ],
            'month_dividents_start' => [
                'type' => 'integer',
            ],
            'goal_amount' => [
                'type' => 'float',
            ],
        ],
        'return' => [
            'type' => 'string',
            'description' => 'NULL if succes, JSON encoded errors if error',
        ],
    ],
];