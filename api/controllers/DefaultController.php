<?php

namespace app\api\controllers;

use yii;
use app\api\controllers\AbstractController;

class DefaultController extends AbstractController
{
    public $modelClass = 'app\models\UsersActivity';
        
    public function actions()
    {
        return [
//            'error' => [
//                'class' => 'yii\web\ErrorAction',
//            ],
        ];
    }
    
    public function actionIndex()
    {
        $data = [
            'title' => 'API endpoints description',
            'endpoints' => include_once Yii::$app->basePath . '/api/config/endpoints.php',
        ];
        
        return $this->output($data);
    }
    
    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            $exception = new \yii\web\NotFoundHttpException('Page not found');
        }
        
        return $this->outputError($exception->getMessage(), $exception->statusCode);
    }
    
    public function actionForbidden()
    {        
        $a = new \GuzzleHttp\Psr7\Request();
        throw new \yii\web\ForbiddenHttpException('Operation not allowed');
    }
}