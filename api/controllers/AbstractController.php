<?php

namespace app\api\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Languages;
use app\models\SystemDictionary;

class AbstractController extends ActiveController
{
    protected $body_params;
    
    public function beforeAction($action)
    {
        if (isset($this->verbs()[Yii::$app->controller->action->id]) && !in_array(Yii::$app->request->getMethod(), $this->verbs()[Yii::$app->controller->action->id])) {
            return Yii::$app->runAction('api/default/error');
        }
        
//  Get body params
        $this->body_params = json_decode(Yii::$app->request->getRawBody(), true);
        
//  Set current language
        Yii::$app->params['lang'] = Languages::find()
            ->where(['deleted' => 0])
            ->orderBy("basic DESC")
            ->one();
        Yii::$app->language = Yii::$app->params['lang']->code;
        
    	return parent::beforeAction($action);
    }
    
    public function actions()
    {
        $actions = parent::actions();
        
        return $actions;
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    
    public function checkAccess($action, $model = null, $params = [])
    {
//        $request_token = Yii::$app->request->get('token');
//        $valid_token = $this->createToken();
//        
//        if ($request_token !== $valid_token) {
//            throw new \yii\web\ForbiddenHttpException('Access denied');
//        }
    }
    
    public function actionDispatch($id=null)
    {
        $this->checkAccess(Yii::$app->controller->action->id);
        
        switch (Yii::$app->request->getMethod()) {
            case 'GET':
            case 'HEAD':
                if (empty($id)) {
                    return Yii::$app->runAction('api/' . Yii::$app->controller->id . '/index');
                } else {
                    return Yii::$app->runAction('api/' . Yii::$app->controller->id . '/view', ['id' => $id]);
                }
                break;
                
            case 'POST':
                return Yii::$app->runAction('api/' . Yii::$app->controller->id . '/create');
                break;
                
            case 'PUT':
            case 'PATCH':
                return Yii::$app->runAction('api/' . Yii::$app->controller->id . '/update', ['id' => $id]);
                break;
            
            case 'DELETE':
                return Yii::$app->runAction('api/' . Yii::$app->controller->id . '/delete', ['id' => $id]);
                break;
            
            default:
                return Yii::$app->runAction('api/' . Yii::$app->controller->id . '/forbidden');
        }
    }
    
    /**
     * Creates secure token from request parameters to verify it
     * Token format:
     * md5(
     *      $http_method
     *      . $controller
     *      . $id (if exists)
     *      . $param_0_name
     *      . $param_1_name
     *      . $param_2_name
     *        ...
     *      . $api_secret_key (from /config/web.php)
     * )
     * @return string
     */
    protected function createToken()
    {
        $token = '';
        $token .= strtolower(Yii::$app->request->getMethod());
//        $token .= Yii::$app->controller->id;
//        $token .= Yii::$app->request->get('id');
        
        if (!empty($this->body_params)) {
            foreach ($this->body_params as $k => $v) {
                $token .= $k;
            }
        }
        
        $token .= Yii::$app->params['api_secret_key'];
        
        return md5($token);
    }
    
    /**
     * Log API request to file
     * @param mixed $response
     * @return boolean
     */
    protected function log($response)
    {
        if (API_LOG === false) {
            return false;
        }
        
//  Init
        $content = '';
        $separator = "\n";
        
//  Time
        $content .= date(Yii::$app->params['date_time_format'], time()) . $separator;
        $content .= microtime(true) . $separator;
        $content .= $separator;
        
//  GET
        $get = Yii::$app->request->get();
        unset($get['r']);
        $content .= 'GET' . $separator;
        $content .= (!empty($get) ? print_r($get, true) : '') . $separator;
        
//  POST
        $content .= 'POST' . $separator;
        $content .= (!empty(Yii::$app->request->post()) ? print_r(Yii::$app->request->post(), true) : '') . $separator;
        
//  Body
        $content .= 'Body' . $separator;
        $content .= (!empty($this->body_params) ? print_r($this->body_params, true) : '') . $separator;
        
//  Response
        if (is_string($response) && strpos($response, '{') === 0) {
            $response_parsed = json_decode($response, true);
            if (!empty($response_parsed)) {
                $response = $response_parsed;
            }
        }
        $content .= 'Response' . $separator;
        $content .= print_r($response, true) . $separator;
        
//  File name
        $file_name = 'api_' . Yii::$app->controller->id . '_' . Yii::$app->controller->action->id . '.log';
        
//  Write log file
        try {
            $file = fopen(Yii::$app->basePath . '/logs/' . $file_name, 'w');
            if (empty($file)) {
                return false;
            }
            fwrite($file, $content);
            fclose($file);
        } catch (\Exception $ex) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Finalize API actions
     * @param mixed $response
     * @return \yii\web\Response
     */
    protected function output($response)
    {
        $output = [
            'status' => 'ok',
            'error' => '',
            'data' => $response,
        ];
        
        $this->log($output);
        
        return \Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'data' => $output,
        ]);
    }
    
    /**
     * Output error
     * @param string $message
     * @param integer $http_code HTTP status code
     * @return \yii\web\Response
     */
    protected function outputError(string $message='', int $http_code=207)
    {
        $output = [
            'status' => 'error',
            'error' => $message,
            'data' => null,
        ];
        
        $this->log($output);
        
        $response_obj = Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'data' => $output,
        ]);
        $response_obj->setStatusCode($http_code);
        
        return $response_obj;
    }
}