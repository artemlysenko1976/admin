<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="background:#EDEDED; padding:10px; font-family:sans-serif; color:#73879C;">
    <?php $this->beginBody() ?>
    <table style="background:#F7F7F7; border:none; width:80%; margin:0 auto;">
        <tr style="background:#2A3F54; color:#ffffff;">
            <td style="padding:10px;">
                <h1 style="padding:0; margin:0; font-weight:normal;"><?= Yii::$app->params['projectName'] ?></h1>
            </td>
        </tr>
        <tr>
            <td style="padding:10px;">
                <?= $content ?>
            </td>
        </tr>
        <tr>
            <td style="padding:10px 10px 30px 10px; text-align:right;">
                <hr style="border:0; border-top:1px solid #EDEDED; margin:20px 0;">
                <em><?= Yii::t('app', 'Best regards') ?></em><br>
                <em><?= Yii::t('app', '{project} Team', ['project' => Yii::$app->params['projectName']]) ?></em>
            </td>
        </tr>
        <tr style="background:#ffffff;">
            <td style="padding:10px; text-align:left;">&copy; <?= Yii::$app->params['projectName'] ?> <?= date('Y') ?></td>
        </tr>
    </table>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>