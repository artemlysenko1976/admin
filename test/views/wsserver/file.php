<?php

?>

<div class="backend-default-index">
    <h1><?= Yii::$app->params['current_menu_item']['name'] ?></h1>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="file" class="control-label">File</label>
                <input type="file" name="file" id="file">
            </div>
            <hr>
            <button id="" class="btn btn-default" onclick="client.uploadFile();">Send</button>
        </div>
    </div>
</div>

<script src="/js/ws_client.js"></script>
<script>
    var client = new WSClient(1, 'user', '<?= Yii::$app->params['ws_client_js'] ?>');
</script>