<?php

if (!empty($result)) {
    $parsed_response = json_decode($result['response']->content, true);
}

?>

<div class="backend-default-index">
    <h1><?= Yii::$app->params['current_menu_item']['name'] ?></h1>
    <hr>
    <?php if (!empty($result)) { ?>
        <?php if (empty($result['error'])) { ?>
<!-- url -->
            <h2>
                <span class="label label-<?= ($result['http_code'] == 200 ? 'success' : 'danger') ?>">
                    <?= $result['http_code'] ?>
                </span>
                &nbsp;&nbsp;
                <?= $result['url'] ?>
                <div style="float:right;">
                    <strong><?= round($result['request_time'], 3) ?></strong>
                    <snall>seconds</snall>
                </div>
            </h2>
            <hr>
            <div class="row">
<!-- Request parameters -->
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Request parameters</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content" style="display:none;">
                            <div class="form-group">
                                <pre><?= print_r($result['request_params'], true) ?></pre>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (!empty($result['request_body'])) { ?>
<!-- Request body -->
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Request body</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content" style="display:none;">
                            <div class="form-group">
                                <pre><?= $result['request_body'] ?></pre>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
<!-- Raw response -->
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Raw response</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <pre><?= $result['raw_response'] ?></pre>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (!empty($result['parsed_response'])) { ?>
<!-- Parsed response -->
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Parsed response</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                <pre style="max-height:700px;"><?= print_r($result['parsed_response'], true) ?></pre>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
<!-- Headers -->
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Headers</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                            <?php foreach ($result['headers'] as $headers_k => $headers_v) { ?>
                                <div class="row">
                                    <div class="col-md-2" style="text-align:right;"><strong><?= $headers_k ?></strong></div>
                                    <div class="col-md-10"><?= $headers_v ?></div>
                                </div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <h2 class="red">ERROR</h2>
            <p class="red"><?= $result['error'] ?></p>
        <?php } ?>
    <?php } else { ?>
        Result is empty
    <?php } ?>
</div>