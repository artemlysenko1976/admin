<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= (empty($backend_menu_state) || $backend_menu_state == 'opened' ? 'nav-md' : 'nav-sm') ?>">
<?php $this->beginBody() ?>
    <div class="container body">
        <div class="main_container">
            <!-- left column -->
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="<?= Url::to('/test') ?>" class="site_title"><span><?= Yii::$app->params['projectName'] ?></span></a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="profile clearfix">
                        <div class="profile_info">
                            <span>Welcome to</span>
                            <h2>TEST module</h2>
                        </div>
                    </div>
                    
                    <br />

                    <!-- Menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li>
                                    <a href="<?= Url::to('/test/') ?>">
                                        <i class="fa fa-home"></i> Home
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::to('/backend/') ?>">
                                        <i class="fa fa-cog"></i> Backend
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::to('/api/') ?>">
                                        <i class="fa fa-code"></i> API
                                    </a>
                                </li>
                            </ul>
                            <?php if (!empty(Yii::$app->params['menu'])) { ?>
                                <?php foreach (Yii::$app->params['menu'] as $menu_controller) { ?>
                                    <ul class="nav side-menu">
                                        <li id="menu_<?= $menu_controller['url'] ?>">
                                            <a>
                                                <i class="fa fa-genderless"></i> 
                                                <?= $menu_controller['name'] ?>
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="nav child_menu">
                                            <?php foreach ($menu_controller['actions'] as $menu_action) { ?>
                                                <?php $url = Url::to('/test/' . $menu_controller['url'] . '/' . $menu_action['url']); ?>
                                                <li><a href="<?= $url ?>"><?= $menu_action['name'] ?></a></li>
                                            <?php } ?>
                                            </ul>
                                        </li>
                                    </ul>        
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Left column -->
        
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav style="height:60px;">
                        <ul class="nav navbar-nav navbar-left">
                            <li class="clock_box">
                                <h2 class="label label-danger">TEST module</h2>
                            </li>
                        </ul>
                        <ul id="top_right_info_box" class="nav navbar-nav navbar-right">
                            <li class="dropdown clock_box" title="Server time">
                                <span id="hours"><?= date('H', time()) ?></span>
                                :
                                <span id="min"><?= date('i', time()) ?></span>
                                :
                                <span id="sec"><?= date('s', time()) ?></span>
                                <small><?= date_default_timezone_get() ?></small>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <?= $content ?>
                <div class="bottom_delimiter"></div>
                <br>
            </div>
            
            <!-- footer content -->
            <footer>
                <div class="pull-right">
                    <p class="pull-left"><?= Yii::$app->params['projectName'] ?> - TEST module</p>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>
    
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>