<?php

use Yii;

?>

<div class="container">
    <div class="col-lg-12">
        <div class="logo">
            <h1><?= $name ?></h1>
        </div>
        <p class="lead text-muted"><?= $message ?></p>
        <?php if (YII_DEBUG === true) { ?>
        <p style="text-align:left;"><?= $exception ?></p>
        <?php } ?>
        <div class="clearfix"></div>
    </div>
</div>