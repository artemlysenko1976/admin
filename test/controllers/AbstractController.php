<?php

namespace app\test\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;

class AbstractController extends Controller
{
    public $layout = 'main';
    
    public function actions()
    {
        $actions = parent::actions();
        
        return $actions;
    }
    
    public function beforeAction($action)
    {
        if (WEB_TEST !== true) {
            return $this->redirect('/');
        }
        
        Yii::$app->params['menu'] = include_once Yii::$app->basePath . '/test/config/menu.php';
        
        $current_url = str_replace('/test/', '', Url::current());
        foreach (Yii::$app->params['menu'] as $menu_controller) {
            foreach ($menu_controller['actions'] as $menu_action) {
                if ($current_url == $menu_controller['url'] . '/' . $menu_action['url']) {
                    Yii::$app->params['current_menu_item'] = $menu_action;
                    break 2;
                }
            }
        }
        
    	return parent::beforeAction($action);
    }
    
    public function createApiToken($method, $url, $params=null)
    {
        $token = '';
        $token .= strtolower($method);
        $token .= $url;
        
        if (!empty($params)) {
            foreach ($params as $k => $v) {
                $token .= $k;
            }
        }
        
        $token .= Yii::$app->params['api_secret_key'];
        
        return strtoupper(md5($token));
    }
}