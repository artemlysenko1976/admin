<?php

namespace app\test\controllers;

use yii;
use app\test\controllers\AbstractController;
use app\library\test\HttpRequest;

class GoalController extends AbstractController
{
    public function actionSave()
    {
        $domain = DOMAIN_LOCAL;
        $method = HttpRequest::METHOD_POST;
        
        $investor_id = 2;
        
        $data = [
            'name' => 'Суперцель',
            'goal_age' => 55,
            'init_contribution' => 600000,
            'montly_dividents' => 100000,
            'investor_income' => 3300,
            'monthly_payments_percent' => 15,
            'month_payments_start' => 1,
            'month_dividents_start' => 6,
            'goal_amount' => 4000000,
        ];
        
        $url = $domain . 'api/goal/save/' . $investor_id;
        
        $result = HttpRequest::test(
            $url, 
            $data, 
            $method,
            HttpRequest::FORMAT_JSON,
            true
        );
        
        return $this->render('/layouts/http', [
            'result' => $result,
        ]);
    }
}