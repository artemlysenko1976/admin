<?php

namespace app\test\controllers;

use yii;
use app\test\controllers\AbstractController;

class DefaultController extends AbstractController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
}