<?php

return [
//  Charts
    [
        'name' => 'Charts',
        'url' => 'charts',
        'actions' => [
            [
                'name' => 'Get investor chart',
                'url' => 'get',
            ],
        ],
    ],
];