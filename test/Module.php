<?php

namespace app\test;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\test\controllers';
    
    public function init()
    {
        parent::init();
        
        \Yii::configure($this, require(__DIR__ . '/config/config.php'));
        \Yii::$app->errorHandler->errorAction = 'test/default/error';
    }
}