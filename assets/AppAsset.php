<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/jquery-ui-1.9.2.custom.min.css',
        'css/cleditor/jquery.cleditor.css',
        'css/font-awesome/css/font-awesome.min.css',
        'css/custom.min.css',
        'vendors/pnotify/dist/pnotify.css',
        'vendors/pnotify/dist/pnotify.buttons.css',
        'vendors/pnotify/dist/pnotify.nonblock.css',
        '/css/jquery.Jcrop.min.css',
        
        'css/site.css',
    ];
    public $js = [
        'js/jquery-ui-1.9.2.custom.min.js',
        'js/scripts.js',
        'js/jquery-sortable.js',
        'js/bootstrap.min.js',
        'js/jquery.cleditor.js',
        'js/cleditor-imageupload-plugin.js',
        'https://cdn.jsdelivr.net/npm/luxon@1.24.1',
        'https://cdn.jsdelivr.net/npm/chart.js@3.0.0-beta.3/dist/chart.js',
        'https://cdn.jsdelivr.net/npm/chartjs-adapter-luxon@0.2.1',
        'js/chartjs-chart-financial.js',
        'js/clock.js',
        'js/custom.js',
        'vendors/pnotify/dist/pnotify.js',
        '/js/jquery.Jcrop.min.js',
        '/js/chat.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
