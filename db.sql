CREATE TYPE public.payments_statuses AS ENUM
   ('NEW',
    'IN_PROGRESS',
    'DONE',
    'ERROR');

CREATE TYPE public.payments_types AS ENUM
   ('ORDER',
    'REFILL');

CREATE TABLE public.payments
(
  id serial NOT NULL,
  client_id integer NOT NULL DEFAULT 0,
  order_id integer NOT NULL,
  method_id integer NOT NULL,
  currency_id integer NOT NULL,
  amount numeric NOT NULL,
  comission numeric,
  discount numeric,
  server_response text,
  error text,
  comment text,
  create_timestamp integer NOT NULL,
  status payments_statuses NOT NULL DEFAULT 'NEW'::payments_statuses,
  deleted integer NOT NULL DEFAULT 0,
  type payments_types,
  CONSTRAINT payments_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE TYPE public.variables_types AS ENUM
   ('STRING',
    'INTEGER',
    'FLOAT',
    'BOOLEAN');

CREATE TABLE public.variables
(
  id serial NOT NULL,
  key character varying,
  value character varying,
  type variables_types,
  CONSTRAINT variables_pk PRIMARY KEY (id),
  CONSTRAINT variables_uq UNIQUE (key)
)
WITH (
  OIDS=FALSE
);