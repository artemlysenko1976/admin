<?php

namespace app\models;

use Yii;
use app\models\PaymentsMethodsLocal;
use app\models\Currencies;

/**
 * This is the model class for table "payments_methods".
 *
 * @property integer $id
 * @property string $key
 * @property float $comission_fixed
 * @property integer $comission_currency_id
 * @property float $comission_percent
 * @property integer $active
 * @property integer $deleted
 */
class PaymentsMethods extends \app\models\AbstractCommonDbLocal
{
    public $object_name = 'Payment method {id}';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_methods';
    }
    
    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Ключ',
            'name' => Yii::t('app', 'Название'),
            'comission_fixed' => Yii::t('app', 'Фиксированная комиссия'),
            'comission_currency_id' => Yii::t('app', 'Валюта комиссии'),
            'comission_percent' => Yii::t('app', 'Процент комиссии'),
            'active' => Yii::t('app', 'Активность'),
            'currencies' => Yii::t('app', 'Валюты'),
        ];
    }
    
    public function getCurrencies()
    {
        if (empty($this->id)) {
            return;
        }
        
        $list = Currencies::find()
            ->from(Currencies::tableName() . " AS c")
            ->join(
                "JOIN",
                PaymentsMethodsMap::tableName() . " AS pmm",
                "pmm.currency_id = c.id"
            )
            ->where([
                "pmm.method_id" => $this->id,
            ])
            ->all();
        
        return $list;
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    PaymentsMethodsLocal::tableName() . '.name',
                    self::tableName() . '.key',
                ];
                $where = [
                    'conditions' => "(" . implode(" ILIKE :keyword OR ", $fields) . " ILIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            },
            'active' => function($filter) {
                $where = [
                    'conditions' => self::tableName() . ".active = :active",
                    'binds' => [':active' => $filter]
                ];
                
                return $where;
            },
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        foreach ($data['l'] as $lang => $field) {
            $entry = trim($field['name']);
            if (empty($entry)) {
                $errors['name'] = Yii::t('app', "Заполните название");
                break;
            }
        }
        
        $data['key'] = trim($data['key']);
        if (!preg_match('/^([a-z0-9_]{2,20})$/', $data['key'])) {
            $errors['key'] = Yii::t('app', "Неверный формат ключа");
        } else {
            $check_query = self::find()
                ->select(['id'])
                ->where([
                    'deleted' => 0,
                    'key' => $data['key'],
                ])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere("id != " . $data['id']);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['key'] = Yii::t('app', "Такой ключ уже используеся");
            }
        }
        
        $data['comission_fixed'] = trim($data['comission_fixed']);
        if (!is_numeric($data['comission_fixed']) || $data['comission_fixed'] < 0) {
            $errors['comission_fixed'] = Yii::t('app', "Значение должно быть 0 или позитивной целым или десятичным числом");
        } else {
            if ($data['comission_fixed'] > 0 && empty($data['comission_currency_id'])) {
                $errors['comission_currency_id'] = Yii::t('app', "Выберите валюту комиссии");
            }
        }
        
        $data['comission_percent'] = trim($data['comission_percent']);
        if (!is_numeric($data['comission_percent']) || $data['comission_percent'] < 0) {
            $errors['comission_percent'] = Yii::t('app', "Значение должно быть 0 или позитивной целым или десятичным числом");
        }
        
        return $errors;
    }
    
    public function getCommission($id, array $order, $total_amount=null)
    {
        $output = 0;
        
        if (empty($order) || empty($order['total_amount'])) {
            return $output;
        }
        
        if (empty($total_amount)) {
            $total_amount = $order['total_amount'] - $order['payment_commission'];
        }
        
        $data = self::findOne([
            'id' => $id,
            'deleted' => 0,
            'active' => 1,
        ]);
        if (empty($data)) {
            return $output;
        }
        
        if ($data->comission_fixed == 0 && $data->comission_percent == 0) {
            return $output;
        }
        
        if ($data->comission_percent > 0) {
            $output += round($total_amount / 100 * $data->comission_percent, 2);
        }
        
        if ($data->comission_fixed > 0) {
            $comission_fixed = $data->comission_fixed;
            
            $output += $comission_fixed;
        }
        
        return $output;
    }
    
    public function addRecord(array $data)
    {
        if (!empty($data['currencies'])) {
            $currencies = $data['currencies'];
            unset($data['currencies']);
        }
        
        $id = parent::addRecord($data, false);
        
        if (!empty($id)) {
            $this->saveCurrencies($id, $currencies);
            $data['currencies'] = $currencies;
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_ADD, $id, "Payment Method " . $id);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $id, $data);
        }
        
        return $id;
    }
    
    public function editRecord(array $data)
    {
        $old_data = self::findOne($data['id']);
        if (empty($old_data)) {
            return false;
        }
        
        if (!empty($data['currencies'])) {
            $currencies = $data['currencies'];
            unset($data['currencies']);
        }
        
        $result = parent::editRecord($data, false);
        
        if (!empty($result)) {
            $this->saveCurrencies($old_data->id, $currencies);
            $data['currencies'] = $currencies;
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_EDIT, $old_data->id, "Payment Method " . $old_data->id);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $old_data->id, $data);
        }
        
        return $result;
    }
    
    protected function saveCurrencies(int $method_id, array $currencies=null)
    {
        PaymentsMethodsMap::deleteAll([
            'method_id' => $method_id,
        ]);
        
        if (!empty($currencies)) {
            foreach ($currencies as $currency_id) {
                $record = new PaymentsMethodsMap([
                    'currency_id' => $currency_id,
                    'method_id' => $method_id,
                ]);
                $record->save();
            }
        }
    }
}