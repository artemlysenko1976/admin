<?php

namespace app\models;

use Yii;
use app\models\Clients;
//use app\models\Orders;
use app\models\PaymentsMethods;
use app\models\Currencies;
use app\library\Utils;

/**
 * This is the model class for table "payments".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $order_id
 * @property integer $method_id
 * @property integer $currency_id
 * @property float $amount
 * @property float $comission
 * @property float $discount
 * @property string $server_response
 * @property string $error
 * @property string $comment
 * @property integer $create_timestamp
 * @property string $status
 * @property string $type
 * @property integer $deleted
 */
class Payments extends \app\models\AbstractCommonDb
{
    public static $statuses = [
        'NEW' => 'Новый',
        'IN_PROGRESS' => 'В процессе',
        'DONE' => 'OK',
        'ERROR' => 'Ошибка',
    ];
    
    public static $types = [
        'ORDER' => 'по заказу',
        'REFILL' => 'пополнение счета',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments';
    }
    
    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => Yii::t('app', 'Клиент'),
            'order_id' => Yii::t('app', 'Заказ'),
            'method_id' => Yii::t('app', 'Метод оплаты'),
            'currency_id' => Yii::t('app', 'Валюта'),
            'amount' => Yii::t('app', 'Сумма'),
            'comission' => Yii::t('app', 'Комиссия'),
            'discount' => Yii::t('app', 'Скидка'),
            'server_response' => Yii::t('app', 'Ответ сервера'),
            'error' => Yii::t('app', 'Ошибка'),
            'comment' => Yii::t('app', 'Комментарий'),
            'create_timestamp' => Yii::t('app', 'Время создания'),
            'status' => Yii::t('app', 'Статус'),
            'type' => Yii::t('app', 'Тип'),
            'deleted' => 'Deleted',
        ];
    }
    
    public function getClient()
    {
        return $this->hasOne(
            Clients::className(), 
            ['id' => 'order_id']
        );
    }
    
//    public function getOrder()
//    {
//        return $this->hasOne(
//            Orders::className(), 
//            ['id' => 'client_id']
//        );
//    }
    
    public function getMethod()
    {
        return $this->hasOne(
            PaymentsMethods::className(), 
            ['id' => 'method_id']
        );
    }
    
    public function getCurrency()
    {
        return $this->hasOne(
            Currencies::className(), 
            ['id' => 'currency_id']
        );
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    self::tableName() . '.server_response',
                    self::tableName() . '.error',
                    self::tableName() . '.comment',
                ];
                $binds = [
                    ':keyword' => '%' . $filter . '%'
                ];
                $conditions = [
                    "(" . implode(" ILIKE :keyword OR ", $fields) . " ILIKE :keyword)"
                ];
                
                $where = [
                    'conditions' => "(" . implode(" OR ", $conditions) . ")",
                    'binds' => $binds
                ];
                
                return $where;
            },
            'client' => function($filter) {
                $client_id = Utils::getIdFromAutocomplete($filter);
                $where = [
                    'conditions' => self::tableName() . ".client_id = :client_id",
                    'binds' => [':client_id' => intval($client_id)]
                ];
                
                return $where;
            },
            'status' => function($filter) {
                $where = [
                    'conditions' => self::tableName() . ".status = :status",
                    'binds' => [':status' => $filter]
                ];
                
                return $where;
            },
            'timestamp_range_from' => function($filter) {
                $where = [
                    'conditions' => self::tableName() . ".create_timestamp >= :date_from",
                    'binds' => [':date_from' => strtotime($filter . ' 00:00:00')]
                ];
                return $where;
            },
            'timestamp_range_to' => function($filter) {
                $where = [
                    'conditions' => self::tableName() . ".create_timestamp <= :date_to",
                    'binds' => [':date_to' => strtotime($filter . ' 23:59:59')]
                ];
                return $where;
            },
        ];
    }
    
    public static function showServerResponse(array $data, int $level=0)
    {
        $output = [];
        
        foreach ($data as $k => $v) {
            $row = '';
            for ($i = 0; $i < $level; $i++) {
                $row .= '-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $row .= $k . ': ';
            if (empty($v)) {
                $row .= '-';
            } else {
                if (!is_array($v)) {
                    $row .= $v;
                } else {
                    $output[] = $row;
                    $output = array_merge($output, self::showServerResponse($v, $level+1));
                }
            }
            
            $output[] = $row;
        }
        
        return $output;
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        $data['comment'] = trim($data['comment']);
        if (empty($data['comment'])) {
            $errors['comment'] = Yii::t('app', "Заполните комментарий");
        }
        
        return $errors;
    }
    
    public function addComment($data)
    {
        $old_data = self::findOne($data['id']);
        
        if (empty($old_data)) {
            return false;
        }
        
        $comment = '';
        
        if (!empty($old_data->comment)) {
            $comment .= $old_data->comment;
            $comment .= "\r\n";
        }
        
        $comment .= $data['comment'];
        
        $old_data->comment = $comment;
        $result = $old_data->save();
        
        return $result;
    }
}

