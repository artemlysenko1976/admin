<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "ips".
 *
 * @property integer $id
 * @property string $ip
 * @property string $operation
 * @property string $comment
 * @property integer $active
 * @property integer $deleted
 */
class Ips extends \app\models\AbstractModel
{
    public $object_name = 'IP {ip}';
    
    public static $global_search_fields = [
        'ip',
        'operation',
        'comment',
    ];
    public static $global_search_permissions = [
        'ip',
        'edit',
    ];
    
    public static $operations = [
        'allow',
        'deny',
    ];
    
    public $section_name = 'ips';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ips';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'IP',
            'operation' => 'Operation',
            'comment' => 'Comment',
            'active' => 'Active',
            'deleted' => 'Deleted',
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
//  IP
        $data['ip'] = trim($data['ip']);
        if (empty($data['ip'])) {
            $errors['ip'] = Yii::t('app', "IP address is required");
        } elseif (filter_var($data['ip'], FILTER_VALIDATE_IP) == false) {
            $errors['ip'] = Yii::t('app', "IP address has invalid format");
        }
        
        return $errors;
    }
    
    public function addRecord(array $data, $log=true)
    {
        $id = parent::addRecord($data);
        if (!empty($id)) {
            $this->generateIpCache();
            
            $this->notifyFrontend();
        }
        
        return $id;
    }
    
    public function editRecord(array $data, $log=true)
    {
        $result = parent::editRecord($data);
        if (!empty($result)) {
            $this->generateIpCache();
            
            $this->notifyFrontend();
        }
        
        return $result;
    }
    
    public function deleteRecord($id, $soft=true, $log=true)
    {
        $data = self::findOne($id);
        if (empty($data)) {
            return false;
        }
        
        $result = parent::deleteRecord($id);
        if (!empty($result)) {
            $this->generateIpCache();
        }
        
        return $result;
    }
    
    public function generateIpCache()
    {
//  Allowed IPs
        $allowed_ips = self::findAll([
            'operation' => 'allow',
            'active' => 1,
            'deleted' => 0,
        ]);
        if (!empty($allowed_ips)) {
            $file_content = '<?php' . "\r\n";
            $file_content .= 'return [' . "\r\n";
            foreach ($allowed_ips as $item) {
                $file_content .= '"' . $item->ip . '",' . "\r\n";
            }
            $file_content .= '];' . "\r\n";
            
            $allowed_cache_file = fopen(self::getAllowedFilePath(), 'w');
            if (!empty($allowed_cache_file)) {
                fwrite($allowed_cache_file, $file_content);
            }
            fclose($allowed_cache_file);
        } else {
            if (file_exists(self::getAllowedFilePath())) {
                unlink(self::getAllowedFilePath());
            }
        }
        
//  Disallowed IPs
        $disallowed_ips = self::findAll([
            'operation' => 'deny',
            'active' => 1,
            'deleted' => 0,
        ]);
        if (!empty($disallowed_ips)) {
            $file_content = '<?php' . "\r\n";
            $file_content .= 'return [' . "\r\n";
            foreach ($disallowed_ips as $item) {
                $file_content .= '"' . $item->ip . '",' . "\r\n";
            }
            $file_content .= '];' . "\r\n";
            
            $disallowed_cache_file = fopen(self::getDisallowedFilePath(), 'w');
            if (!empty($disallowed_cache_file)) {
                fwrite($disallowed_cache_file, $file_content);
            }
            fclose($disallowed_cache_file);
        } else {
            if (file_exists(self::getDisallowedFilePath())) {
                unlink(self::getDisallowedFilePath());
            }
        }
    }
    
    public static function getAllowedFilePath()
    {
        return Yii::$app->basePath . '/cache/allowed_ips.php';
    }
    
    public static function getDisallowedFilePath()
    {
        return Yii::$app->basePath . '/cache/disallowed_ips.php';
    }
}
