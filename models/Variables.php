<?php

namespace app\models;

use Yii;
use app\library\Utils;

/**
 * This is the model class for table "variables".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property string $type
 */
class Variables extends \app\models\AbstractCommonDb
{
    public static $types = [
        'STRING' => 'строка',
        'INTEGER' => 'целое число',
        'FLOAT' => 'число с точкой',
        'BOOLEAN' => 'логический',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'variables';
    }
    
    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'key' => Yii::t('app', 'Ключ'),
            'value' => Yii::t('app', 'Значение'),
            'type' => Yii::t('app', 'Тип'),
        ];
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    self::tableName() . '.key',
                    self::tableName() . '.value',
                ];
                $binds = [
                    ':keyword' => '%' . $filter . '%'
                ];
                $conditions = [
                    "(" . implode(" ILIKE :keyword OR ", $fields) . " ILIKE :keyword)"
                ];
                
                $where = [
                    'conditions' => "(" . implode(" OR ", $conditions) . ")",
                    'binds' => $binds
                ];
                
                return $where;
            },
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        $data['key'] = trim($data['key']);
        if (!preg_match('/^([0-9a-z_]{2,50})$/', $data['key'])) {
            $errors['key'] = Yii::t('app', 'Неверный формат ключа');
        } else {
            $check_query = self::find()
                ->where([
                    "key" => $data['key'],
                ])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere("id != " . $data['id']);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['key'] = Yii::t('app', 'Этот ключ уже используется');
            }
        }
        
        if ($data['value'] !== '') {
            $value = $data['value'][$data['type']];
            
            switch ($data['type']) {
                case 'INTEGER':
                    if (!preg_match('/^(\-?[0-9]{1,12})$/', $value)) {
                        $errors['value_INTEGER'] = Yii::t('app', 'Значение должно быть целым числом');
                    }
                    break;
                    
                case 'FLOAT':
                    if (!preg_match('/^(\-?[0-9]{1,12}\.[0-9]{1,12})$/', $value)) {
                        $errors['value_FLOAT'] = Yii::t('app', 'Значение должно быть числом с точкой');
                    }
                    break;
                    
                case 'BOOLEAN':
                    if (!preg_match('/^([0-1]{1})$/', $value)) {
                        $errors['value_BOOLEAN'] = Yii::t('app', 'Значение должно быть логическим');
                    }
                    break;
            }
        }
        
        return $errors;
    }
    
    public function addRecord(array $data)
    {
        $data['value'] = $this->processValue($data['value'][$data['type']], $data['type']);
        
        $id = parent::addRecord($data);
        
        return $id;
    }
    
    public function editRecord(array $data)
    {
        $data['value'] = $this->processValue($data['value'][$data['type']], $data['type']);
        
        $result = parent::editRecord($data);
        
        return $result;
    }
    
    protected function processValue(string $value, string $type)
    {
        switch ($type) {
            case 'INTEGER':
                $output = intval($value);
                break;

            case 'FLOAT':
                $output = floatval($value);
                break;

            case 'BOOLEAN':
                $output = (boolval($value) == true ? 1 : 0);
                break;
            
            default :
                $output = $value;
        }
        
        return $output;
    }
}