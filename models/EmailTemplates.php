<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "email_templates".
 *
 * @property integer $id
 * @property string $key
 * @property string $email_from
 * @property string $name_from
 * @property string $email_to
 * @property string $email_copy
 * @property integer $deleted
 */
class EmailTemplates extends \app\models\AbstractLocal
{
    public $object_name = 'E-mail template {key}';
    
    public static $global_search_fields = [
        'key',
        'email_from',
        'name_from',
        'email_to',
        'email_copy',
    ];
    public static $global_search_l_fields = [
        'subject',
        'content',
    ];
    public static $global_search_permissions = [
        'email',
        'edit',
    ];
    
    public $section_name = 'email_templates';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_templates';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'email_from' => 'Email From',
            'email_to' => 'Email To',
            'deleted' => 'Deleted',
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        $data['key'] = trim($data['key']);
        if (empty($data['key'])) {
            $errors['key'] = Yii::t('app', "Key is required");
        } else {
            $check_query = self::find()
                ->select(['id'])
                ->where([
                    'deleted' => 0,
                    "key" => $data['key']
                ])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere("id != " . $data['id']);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['key'] = Yii::t('app', "This key already exists");
            }
        }
        
        $default_lang = Languages::getDefaultLang();
        
        $content = trim($data['l'][$default_lang->code]['subject']);
        if (empty($content)) {
            $errors['l_' . $default_lang->code . '_subject'] = Yii::t('app', "Default language entry is required");
        }
        
        $content = trim(strip_tags($data['l'][$default_lang->code]['content']));
        if (empty($content)) {
            $errors['l_' . $default_lang->code . '_content'] = Yii::t('app', "Default language entry is required");
        }
        
        $data['email_from'] = trim($data['email_from']);
        $data['email_to'] = trim($data['email_to']);
        if (empty($data['email_from']) && empty($data['email_to'])) {
            $errors['email_to'] = Yii::t('app', "Input E-mail (From) or E-mails (To)");
        } else {
            if (!empty($data['email_from'])) {
                if (filter_var($data['email_from'], FILTER_VALIDATE_EMAIL) === false) {
                    $errors['email_from'] = Yii::t('app', "Wrong e-mail format");
                }
            }
            if (!empty($data['email_to'])) {
                $email_to = explode("\r\n", $data['email_to']);
                foreach ($email_to as $v) {
                    if (filter_var($v, FILTER_VALIDATE_EMAIL) === false) {
                        $errors['email_to'] = Yii::t('app', "Wrong e-mail format");
                    }
                }
            }
        }
        
        if (!empty($data['name_from']) && empty($data['email_from'])) {
            $errors['email_from'] = Yii::t('app', "If Name (From) is set E-mail (From) is required");
        }
        
        if (!empty($data['email_copy'])) {
            $email_copy = explode("\r\n", $data['email_copy']);
            foreach ($email_copy as $v) {
                if (filter_var($v, FILTER_VALIDATE_EMAIL) === false) {
                    $errors['email_copy'] = Yii::t('app', "Wrong e-mail format");
                }
            }
        }
        
        return $errors;
    }
    
    public function addRecord(array $data, $log=false)
    {   
        foreach ($data['l'] as $lang => $fields) {
            $data['l'][$lang]['content'] = self::preservePhpTags($fields['content']);
        }
        
        $id = parent::addRecord($data);
        if (!empty($id)) {
            self::renderTemplate($id);
            
            $this->notifyFrontend();
        }
        
        return $id;
    }
    
    public function editRecord(array $data, $log=false)
    {
        foreach ($data['l'] as $lang => $fields) {
            $data['l'][$lang]['content'] = self::preservePhpTags($fields['content']);
        }
        
        $result = parent::editRecord($data);
        if (!empty($result)) {
            self::renderTemplate($data['id']);
            
            $this->notifyFrontend();
        }
        
        return $result;
    }
    
    public function deleteRecord($id, $soft=true, $log=true)
    {
        $data = self::findOne($id);
        
        $result = parent::deleteRecord($id, $soft, $log);
        if (!empty($data)) {
            $file_name = Yii::$app->basePath . '/mail/generated/' . $data->key . '.php';
            if (is_file($file_name)) {
                unlink($file_name);
            }
        }
        
        return $result;
    }
    
    public static function getTemplateData($key)
    {
        $data = self::findOne([
            'key' => $key,
            'deleted' => 0
        ]);
        if (!empty($data)) {
            self::renderTemplate($data->id, false);
        }
        
        return $data;
    }
    
    public static function renderTemplate($id, $force=true)
    {
        if (empty($id) || !is_numeric($id) || $id <= 0) {
            throw new \Exception('Unable to render template file - invalid template ID');
        }
        
        $data = self::findOne($id);
        if (empty($data)) {
            throw new \Exception('Unable to render template file - can not load template data');
        }
        
        try {
            foreach ($data->locals as $lang => $local_data) {
                $file_name = Yii::$app->basePath . '/mail/generated/' . $lang . '/' . $data->key . '.php';
                if (is_file($file_name) && $force === false) {
                    return true;
                }

                $file = fopen($file_name, 'w');
                if (!is_resource($file)) {
                    throw new \Exception('Unable to render template file - can not create file ' . $file_name);
                }
                fwrite($file, $local_data->content);
                fclose($file);
                chmod($file_name, 0777);
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
        
        return true;
    }
}
