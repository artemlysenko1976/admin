<?php

namespace app\models;

use Yii;
use app\library\ClientStat;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property integer $maintenance
 */
class Settings extends \app\models\AbstractModel
{
    public $object_name = 'Settings';
    
    public $section_name = 'settings';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'maintenance' => 'Maintanance',
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        return $errors;
    }
    
    public function getHistoryCallbacks()
    {
        $callbacks = parent::getHistoryCallbacks();
        $callbacks['maintenance'] = function($value) {
            return ($value == 1 ? 'On' : 'Off');
        };
        
        return $callbacks;
    }
    
    public function editSettings(array $data, $section='settings')
    {
        $result = parent::editRecord($data);
        
        $this->notifyFrontend();
        
        return $result;
    }
}