<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goods_parameters_map".
 *
 * @property integer $id
 * @property integer $parameter_id
 * @property integer $category_id
 */
class GoodsParametersMap extends \app\models\AbstractCommonDb
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_parameters_map';
    }
}