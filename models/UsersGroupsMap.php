<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_groups_map".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $group_id
 */
class UsersGroupsMap extends \app\models\AbstractModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_groups_map';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'group_id' => 'Group ID',
        ];
    }
    
    public function getGroup()
    {
        return $this->hasOne(
            UsersGroups::className(), 
            ['id' => 'group_id']
        );
    }
}
