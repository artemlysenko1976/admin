<?php

namespace app\models;

use Yii;

/**
 * Class for common DB with typical localization
 *
 */
class AbstractCommonDbLocal extends AbstractPostgreSQLLocal
{
    public static function getDb() 
    {
        return Yii::$app->db_common;
    }
}