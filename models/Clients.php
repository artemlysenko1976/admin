<?php

namespace app\models;

use Yii;
use app\models\Languages;
use app\models\Countries;
use app\library\Validator;
use app\library\Utils;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\web\UploadedFile;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property int $referrer_id
 * @property string $login
 * @property string $first_name
 * @property string $second_name
 * @property string $last_name
 * @property string $birth_day
 * @property string $email
 * @property string $phone
 * @property string $photo
 * @property string $site
 * @property string $organization_link
 * @property string $gender
 * @property int $country_id
 * @property string $city
 * @property string $description
 * @property string $lang
 * @property string $status
 * @property int $create_timestamp
 * @property int $deleted
 */
class Clients extends AbstractCommonDb
{
    public $object_name = 'Client {id}';
    
    public static $statuses = [
        'ACTIVE' => 'Активный',
        'BANNED' => 'Забаненный',
    ];
    
    public static $genders = [
        'MALE' => 'Мужчина',
        'FTMALE' => 'Женщина',
        'OTHER' => 'Другое',
    ];
    
    public static $photo_path = '/web/images/clients/';
    public static $photo_size = [
        1500,
        1500,
    ];
    
    public static $login_rules = [
        'regexp' => '/^([a-zA-Z0-9_\-\.]{2,16})$/',
        'min_chars' => 2,
        'max_chars' => 16,
    ];
    
    public static $password_rules = [
        'min_chars' => 5,
        'max_chars' => 12,
    ];
    
    public static $name_rules = [
        'regexp' => '/^([^\d@#\$%\^&\*\(\)\[\]:;\'",\._\+]{2,50})$/',
        'min_chars' => 2,
        'max_chars' => 50,
    ];
    
    public static $city_rules = [
        'regexp' => '/^([^@#\$%\^&\*\(\)\[\]:;\'",\._\+]{2,50})$/',
        'min_chars' => 2,
        'max_chars' => 50,
    ];
    
    public static $description_rules = [
        'min_chars' => 10,
        'max_chars' => 10000,
    ];
    
    public static $available_image_extensions = [
        'jpg',
        'jpeg',
        'gif',
        'png',
    ];
    public static $max_image_size = 5000000; //  5 mb
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }
    
    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'referrer_id' => Yii::t('app', 'Реферрер'),
            'login' => Yii::t('app', 'Никнейм'),
            'password' => Yii::t('app', 'Пароль'),
            'password_confirm' => Yii::t('app', 'Подтверждение пароля'),
            'first_name' => Yii::t('app', 'Имя'),
            'second_name' => Yii::t('app', 'Отчество'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'birth_day' => Yii::t('app', 'Дата рождения'),
            'email' => Yii::t('app', 'E-mail'),
            'phone' => Yii::t('app', 'Номер телефона'),
            'photo' => Yii::t('app', 'Фотография'),
            'site' => Yii::t('app', 'Вебсайт'),
            'organization_link' => Yii::t('app', 'Ссылка на место работы'),
            'gender' => Yii::t('app', 'Пол'),
            'country_id' => Yii::t('app', 'Страна'),
            'city' => Yii::t('app', 'Город'),
            'description' => Yii::t('app', 'О себе'),
            'lang' => Yii::t('app', 'Язык'),
            'status' => Yii::t('app', 'Статус'),
            'create_timestamp' => Yii::t('app', 'Время создания'),
            'deleted' => 'Deleted',
        ];
    }
    
    public function getReferrer()
    {
        return $this->hasOne(
            self::className(), 
            ['id' => 'referrer_id']
        );
    }
    
    public function getLanguage()
    {
        return $this->hasOne(
            Languages::className(), 
            ['code' => 'lang']
        );
    }
    
    public function getCountry()
    {
        return $this->hasOne(
            Countries::className(), 
            ['id' => 'country_id']
        );
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    'login',
                    'first_name',
                    'second_name',
                    'last_name',
                    'email',
                    'phone',
                    'site',
                    'organization_link',
                    'city',
                    'description',
                ];
                $where = [
                    'conditions' => "(" . implode(" ILIKE :keyword OR ", $fields) . " ILIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            },
            'timestamp_range_from' => function($filter) {
                $where = [
                    'conditions' => self::tableName() . ".create_timestamp >= :create_timestamp_from",
                    'binds' => [':create_timestamp_from' => strtotime($filter . ' 00:00:00')]
                ];
                return $where;
            },
            'timestamp_range_to' => function($filter) {
                $where = [
                    'conditions' => self::tableName() . ".create_timestamp <= :create_timestamp_to",
                    'binds' => [':create_timestamp_to' => strtotime($filter . ' 23:59:59')]
                ];
                return $where;
            },
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        $data['email'] = trim($data['email']);
        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = Yii::t('app', "Неверный формат E-mail");
        } else {
            $check_query = self::find()
                ->select(['id'])
                ->where([
                    'deleted' => 0,
                    'email' => $data['email'],
                ])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere("id != " . $data['id']);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['email'] = Yii::t('app', "Такой E-mail уже используеся");
            }
        }
        
        $data['password'] = trim($data['password']);
        $data['password_confirm'] = trim($data['password_confirm']);
        
        if (empty($data['id'])) {
            if (empty($data['password'])) {
                $errors['password'] = Yii::t('app', "Введите Пароль");
            }
        }
        
        if (!empty($data['password'])) {
            if (strlen($data['password']) < self::$password_rules['min_chars'] || strlen($data['password']) > self::$password_rules['max_chars']) {
                $errors['password'] = Yii::t('app', "Пароль должен содержать от {min} до {max} символов", [
                    'min' => self::$password_rules['min_chars'],
                    'max' => self::$password_rules['max_chars'],
                ]);
            }
            
            if (empty($data['password_confirm'])) {
                $errors['password_confirm'] = Yii::t('app', "Введите Подтверждение пароля");
            } else {
                if ($data['password_confirm'] !== $data['password']) {
                    $errors['password_confirm'] = Yii::t('app', "Неверное подтверждение пароля");
                }
            }
        }
        
        $data['login'] = trim($data['login']);
        if (!preg_match(self::$login_rules['regexp'], $data['login'])) {
            $errors['login'] = Yii::t('app', "Никнейм должен состоять из латинских букв, цыфр, '.', '_', '-' (от {min} до {max} символов)", [
                'min' => self::$login_rules['min_chars'],
                'max' => self::$login_rules['max_chars'],
            ]);
        } else {
            $check_query = self::find()
                ->select(['id'])
                ->where([
                    'deleted' => 0,
                    'login' => $data['login'],
                ])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere("id != " . $data['id']);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['login'] = Yii::t('app', "Такой Никнейм уже используеся");
            }
        }
        
        $data['first_name'] = trim($data['first_name']);
        if (empty($data['first_name'])) {
            $errors['first_name'] = Yii::t('app', "Заполните Имя");
        } else {
            if (!preg_match(self::$name_rules['regexp'], $data['first_name'])) {
                $errors['first_name'] = Yii::t('app', "Имя должно состоять из букв (от {min} до {max} символов)", [
                    'min' => self::$name_rules['min_chars'],
                    'max' => self::$name_rules['max_chars'],
                ]);
            }
        }
        
        $data['second_name'] = trim($data['second_name']);
        if (!empty($data['second_name'])) {
            if (!preg_match(self::$name_rules['regexp'], $data['second_name'])) {
                $errors['second_name'] = Yii::t('app', "Отчество должно состоять из букв (от {min} до {max} символов)", [
                    'min' => self::$name_rules['min_chars'],
                    'max' => self::$name_rules['max_chars'],
                ]);
            }
        }
        
        $data['last_name'] = trim($data['last_name']);
        if (empty($data['last_name'])) {
            $errors['last_name'] = Yii::t('app', "Заполните Фамилию");
        } else {
            if (!preg_match(self::$name_rules['regexp'], $data['last_name'])) {
                $errors['last_name'] = Yii::t('app', "Фамилия должна состоять из букв (от {min} до {max} символов)", [
                    'min' => self::$name_rules['min_chars'],
                    'max' => self::$name_rules['max_chars'],
                ]);
            }
        }
        
        $data['birth_day'] = trim($data['birth_day']);
        if (empty($data['birth_day'])) {
            $errors['birth_day'] = Yii::t('app', "Заполните Дату рождения");
        } else {
            if (Validator::validateCalendarDate($data['birth_day']) == false) {
                $errors['birth_day'] = Yii::t('app', "Неверный формат даты рождения");
            }
        }
        
        $data['phone'] = trim($data['phone']);
        if (Validator::validatePhoneNumber($data['phone']) == false) {
            $errors['phone'] = Yii::t('app', "Неверный формат номера телефона");
        } else {
            $check_query = self::find()
                ->select(['id'])
                ->where([
                    'deleted' => 0,
                    'phone' => $data['phone'],
                ])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere("id != " . $data['id']);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['phone'] = Yii::t('app', "Такой Номер телефона уже используеся");
            }
        }
        
        $data['city'] = trim($data['city']);
        if (!empty($data['city'])) {
            if (!preg_match(self::$city_rules['regexp'], $data['city'])) {
                $errors['city'] = Yii::t('app', "Город должен состоять из букв (от {min} до {max} символов)", [
                    'min' => self::$city_rules['min_chars'],
                    'max' => self::$city_rules['max_chars'],
                ]);
            }
        }
        
        $data['site'] = trim($data['site']);
        if (!empty($data['site'])) {
            if (!filter_var($data['site'], FILTER_VALIDATE_URL)) {
                $errors['site'] = Yii::t('app', "Неверный формат ссылки");
            }
        }
        
        $data['organization_link'] = trim($data['organization_link']);
        if (!empty($data['organization_link'])) {
            if (!filter_var($data['organization_link'], FILTER_VALIDATE_URL)) {
                $errors['organization_link'] = Yii::t('app', "Неверный формат ссылки");
            }
        }
        
        $data['description'] = trim($data['description']);
        if (!empty($data['description'])) {
            if (mb_strlen($data['description']) < self::$description_rules['min_chars'] || mb_strlen($data['about']) > self::$description_rules['max_chars']) {
                $errors['description'] = Yii::t('app', "Описание должно содержать от {min} до {max} символов", [
                    'min' => self::$description_rules['min_chars'],
                    'max' => self::$description_rules['max_chars'],
                ]);
            }
        }
        
        if (!empty($data['referrer_id'])) {
            $referrer_id = Utils::getIdFromAutocomplete($data['referrer_id']);
            if (empty($referrer_id) || !is_numeric($referrer_id)) {
                $errors['referrer_id'] = Yii::t('app', "Выберите реферрера из списка");
            } else {
                $check_query = self::find()
                    ->select([
                        'id',
                        'referrer_id',
                    ])
                    ->where([
                        'id' => $referrer_id,
                        'deleted' => 0,
                    ])
                    ->limit(1);
                $check = $check_query->one();
                if (empty($check)) {
                    $errors['referrer_id'] = Yii::t('app', "Такой реферрер не найден");
                } else {
                    if (!empty($data['id']) && $check['id'] == $data['id']) {
                        $errors['referrer_id'] = Yii::t('app', "Клиент не может быть реферрером для самого себя");
                    } elseif (!empty($data['id']) && $check['referrer_id'] == $data['id']) {
                        $errors['referrer_id'] = Yii::t('app', "Этот клиент уже является реферралом редактируемого");
                    }
                }
            }
        }
        
        return $errors;
    }
    
    public function addRecord(array $data, $log=true)
    {
        unset($data['password_confirm']);
        if (!empty($data['password'])) {
            $data['password'] = self::encodePassword($data['password']);
        }
        
        if (!empty($data['referrer_id'])) {
            $data['referrer_id'] = Utils::getIdFromAutocomplete($data['referrer_id']);
        } else {
            $data['referrer_id'] = 0;
        }
        
        $data['create_timestamp'] = time();
        
        $id = parent::addRecord($data, false);
        
        if (!empty($id)) {
            $this->createDir($id);
            
//  Photo
            $uploaded_file = UploadedFile::getInstanceByName('photo');
            if (!empty($uploaded_file)) {
                $file_name = 'photo.' . $uploaded_file->extension;
                $images_result = Image::getImagine()
                    ->open($uploaded_file->tempName)
                    ->thumbnail(new Box(self::$photo_size[0], self::$photo_size[1]))
                    ->save(Yii::$app->basePath . self::$photo_path . $id . '/' . $file_name, ['quality' => 100]);
                if (!empty($images_result)) {
                    $data['photo'] = $file_name;
                    $this->photo = $file_name;
                    $this->save(false);
                }
            }
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_ADD, $id, "Client " . $id);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $id, $data);
        }
        
        return $id;
    }
    
    public function editRecord(array $data, $log=true)
    {
//  Get old data
        $old_data = self::findOne($data['id']);
        if (empty($old_data)) {
            return false;
        }
        
        unset($data['password_confirm']);
        if (!empty($data['password'])) {
            $data['password'] = self::encodePassword($data['password']);
        } else {
            $data['password'] = $pld_data->password;
        }
        
        if (!empty($data['referrer_id'])) {
            $data['referrer_id'] = Utils::getIdFromAutocomplete($data['referrer_id']);
        }
        
//  Delete photo
        if (!empty($data['delete_photo'])) {
            unlink(Yii::$app->basePath . self::$photo_path . $old_data->id . '/' . $old_data->photo);
            $data['photo'] = null;
            unset($data['delete_photo']);
        }
        
//  Photo
        $uploaded_file = UploadedFile::getInstanceByName('photo');
        if (!empty($uploaded_file)) {
            if (!empty($old_data->photo)) {
                unlink(Yii::$app->basePath . self::$photo_path . $old_data->id . '/' . $old_data->photo);
            }
            
            $file_name = 'photo.' . $uploaded_file->extension;
            $images_result = Image::getImagine()
                ->open($uploaded_file->tempName)
                ->thumbnail(new Box(self::$photo_size[0], self::$photo_size[1]))
                ->save(Yii::$app->basePath . self::$photo_path . $old_data->id . '/' . $file_name, ['quality' => 100]);
            if (!empty($images_result)) {
                $data['photo'] = $file_name;
            }
        }
        
        $result = parent::editRecord($data);
        
        return $result;
    }
    
    protected function createDir(int $id)
    {
        $dir_name = Yii::$app->basePath . self::$photo_path . $id . '/';
        if (!is_dir($dir_name)) {
            mkdir($dir_name);
            chmod($dir_name, 0777);
        }
    }
    
    public static function encodePassword($password)
    {
        if (empty($password)) {
            return '';
        }
        
        return md5($password);
    }
    
    public function uploadApiImage(string $type, string $content)
    {
//  Check
        if (empty($this->id)) {
            return false;
        }
        
        if (!in_array($type, ['photo', 'passport_photo', 'selfy'])) {
            return false;
        }
        
        if (!empty($this->$type)) {
            unlink(Yii::$app->basePath . self::$photo_path . $this->id . '/' . $this->$type);
        }
        
//  Set image params
        $extension = Utils::getImageTypeFromBinaryData($content);
        $file_name = $type . '.' . $extension;
        $file_path = Yii::$app->basePath . self::$photo_path . $this->id . '/' . $file_name;
        
//  Save origin image
        $file = fopen($file_path, 'w');
        $result = fwrite($file, $content);
        fclose($file);
        
        if (empty($result)) {
            return false;
        }
        
//  Resize image
        $images_result = Image::getImagine()
            ->open($file_path)
            ->thumbnail(new Box(self::$photo_size[0], self::$photo_size[1]))
            ->save($file_path, ['quality' => 100]);
        
        if (empty($images_result)) {
            return false;
        }
        
        if (!empty($images_result)) {
            $this->$type = $file_name;
            $this->save(false);
        }
        
        return true;
    }
    
    /**
     * Search for autocomplete
     * @param string $keyword
     * @return array
     */
    public static function autocompleteSearch($keyword, array $additional_conditions=null)
    {
        if (empty($keyword)) {
            return [];
        }

        $list = [];
        $conditions = [];

        if (is_numeric($keyword)) {
//  Numeric keyword
            $conditions[] = self::tableName() . ".id = :keyword";
            $binds = [':keyword' => intval($keyword)];
        } else {
//  Other
            $fields = [
                'login',
                'first_name',
                'second_name',
                'last_name',
                'email',
                'phone',
                'site',
                'organization_link',
                'city',
                'description',
            ];
            
            $conditions[] = "(" . implode(" ILIKE :keyword OR ", $fields) . " ILIKE :keyword)";
            $binds = [':keyword' => '%' . $keyword . '%'];
        }
        
//  Additional conditions
        if (!empty($additional_conditions)) {
            foreach ($additional_conditions as $k => $v) {
                $conditions[] = "(" . $k . " = :" . $k . ")";
                $binds = [':' . $k => $v];
            }
        }
        
//  Make query
        $result = self::find()
            ->select([
                "id",
                'first_name',
                'last_name',
                'login',
            ])
            ->where([
                'deleted' => 0,
            ])
            ->andWhere("(" . implode(" OR ", $conditions) . ")", $binds)
            ->asArray()
            ->all();
        
//  Build output
        if (!empty($result)) {
            foreach ($result as $result_row) {
                $list[] = '#' . $result_row['id'] . ' - ' . $result_row['last_name'] . ' ' . $result_row['first_name'] . ' (' . $result_row['login'] . ')';
            }
        }

        return $list;
    }
}