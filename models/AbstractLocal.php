<?php

namespace app\models;

use yii;
use yii\db\Query;
use yii\db\ActiveQuery;
use app\models\UsersActivity;
use app\models\RecordsHistory;
use app\models\Languages;
use yii\helpers\Url;

/**
 * Abstract Localized Model
 */
class AbstractLocal extends \app\models\AbstractModel
{
    public $local_restore;
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_id' => 'Main ID',
            'lang' => 'Language',
        ];
    }
    
    /**
     * Create relation by main_id and language to join local table
     * @return relation
     */
    public function getLocal()
    {
        $local_class_name = static::className() . 'Local';
        
        return $this->hasOne($local_class_name::className(), [
                'main_id' => 'id',
            ])
            ->where([
                'lang' => Yii::$app->params['lang']->code
            ]);
    }
    
    /**
     * Create relation by main_id and language to join local table
     * For usage in multiple selection to get rows with empty local data
     * @return relation
     */
    public function getRellocal()
    {
        $local_class_name = static::className() . 'Local';
        
        return $this->hasOne($local_class_name::className(), [
                'main_id' => 'id',
            ]);
    }
    
    /**
     * Create relation by main_id
     * @return relation
     */
    public function getLocals()
    {
        if (!empty($this->local_restore)) {
            return $this->local_restore;
        }
        
        $local_class_name = static::className() . 'Local';
        
        $data = $local_class_name::className()::find()
            ->where(['main_id' => $this->id])
            ->all();
        
        $output = [];
        foreach ($data as $data_v) {
            $output[$data_v->lang] = $data_v;
            
        }
        
        return $output;
    }
    
    /**
     * Override to make predefined joins
     */
    public static function find()
    {
//  Make parent
        $query = parent::find();
        
//  Join local table
//        $query->joinWith('local');
        $query->joinWith([
            'rellocal' => function($query){
                $query->onCondition("lang = '" . Yii::$app->params['lang']->code . "'");
            }
        ]);
        
        return $query;
    }
    
    /**
     * Inserts localized data
     * @param array $data
     * @param boolean $log if TRUE then logs will be wrote down
     * @return int|boolean
     */
    public function addRecord(array $data, $log=true)
    {
//  Get loca data
        if (isset($data['l'])) {
            $local_data = $data['l'];
            unset($data['l']);
        }
        
//  Save main data
        $id = parent::addRecord($data, false);
        if (!empty($id)) {
//  Save local data
            if (!empty($local_data)) {
                foreach ($local_data as $lang => $values) {
                    $class_name = self::className() . 'Local';
                    $local_obj = new $class_name;
                    $local_obj->main_id = $id;
                    $local_obj->lang = $lang;
                    foreach ($values as $values_k => $values_v) {
                        if (is_string($values_v)) {
                            $values_v = trim($values_v);
                        }
                        $local_obj->$values_k = $values_v;
                    }
                    $local_obj->save(false);
                }
            }
            
            if ($log === true) {
//  Set local data for logging
                if (!empty($local_data)) {
                    $data['l'] = $local_data;
                }
                
//  Log user activity
                $object_name = $this->object_name;
                if (!empty($object_name)) {
                    preg_match('/\{(.*)\}/', $object_name, $object_property);
                    if (!empty($object_property)) {
                        $object_name = str_replace($object_property[0], $this->{$object_property[1]}, $object_name);
                    }
                }

                $activity = new UsersActivity();
                $activity->log(UsersActivity::OPERATION_ADD, $id, $object_name);
                
//  Log history
                $history = new RecordsHistory();
                $history->log($this->tableName(), $id, $data);
            }
        }
        
        return $id;
    }
    
    /**
     * Updates localized data
     * @param array $data
     * @param boolean $log if TRUE then logs will be wrote down
     * @return boolean
     */
    public function editRecord(array $data, $log=true)
    {
//  Get loca data
        if (isset($data['l'])) {
            $local_data = $data['l'];
            unset($data['l']);
        }
        
//  Make parent update
        $result = parent::editRecord($data, false);
        if (!empty($result)) {
            $class_name = self::className() . 'Local';
            
//  Get old local data
            $old_local_data = $class_name::findAll(['main_id' => $data['id']]);
            if (!empty($old_local_data)) {
                foreach ($old_local_data as $old_local_data_v) {
                    $old_local_data_v->delete();
                }
            }
            
//  Save local data
            if (!empty($local_data)) {
                foreach ($local_data as $lang => $values) {
                    $local_obj = new $class_name;
                    $local_obj->main_id = $data['id'];
                    $local_obj->lang = $lang;
                    foreach ($values as $values_k => $values_v) {
                        if (is_string($values_v)) {
                            $values_v = trim($values_v);
                        }
                        $local_obj->$values_k = $values_v;
                    }
                    $local_obj->save(false);
                }
            }
            
            if ($log === true) {
//  Set local data for logging
                if (!empty($local_data)) {
                    $data['l'] = $local_data;
                }
                
//  Log user activity
                $object_name = $this->object_name;
                if (!empty($object_name)) {
                    preg_match('/\{(.*)\}/', $object_name, $object_property);
                    if (!empty($object_property)) {
                        $object_name = str_replace($object_property[0], $this->{$object_property[1]}, $object_name);
                    }
                }

                $activity = new UsersActivity();
                $activity->log(UsersActivity::OPERATION_EDIT, $data['id'], $object_name);
                
//  Log history
                $history = new RecordsHistory();
                $history->log($this->tableName(), $data['id'], $data);
            }
        }
        
        return $result;
    }
    
//  Global search =======================================
    /**
     * Making global search by current model by localized text fields
     * @param string $keyword Keyword
     * @return array
     */
    public function makeGlobalLikeSearch($keyword, $module='backend')
    {
//  Check permissions
        if (empty(static::$global_search_permissions)) {
            return [];
        }
        
        if (Users::checkAccessPermissions(static::$global_search_permissions[0], static::$global_search_permissions[1]) === false) {
            return [];
        }
        
        $records = parent::makeGlobalLikeSearch($keyword, $module);
        if (!empty(static::$global_search_l_fields)) {
            $conditions_deleted = ($this->hasAttribute('deleted') ? ['deleted' => 0] : []);
            $conditions = "(`" . implode("` LIKE :keyword OR `", static::$global_search_l_fields) . "` LIKE :keyword)";
            $binds = [':keyword' => '%' . $keyword . '%'];
            $like_records = (new Query())
                ->select([
                    static::tableName() . '.*',
                ])
                ->from(static::tableName())
                ->join('JOIN', static::tableName() . '_l', static::tableName() . "_l.main_id = " . static::tableName() . '.id')
                ->where($conditions_deleted)
                ->andWhere($conditions, $binds)
                ->all();
            
            if (!empty($like_records)) {
                foreach ($like_records as $like_record) {
                    $object_name = $this->getObjectNameFrom($like_record);
                    $records[Url::to('/backend/' . static::$global_search_permissions[0] . '/' . static::$global_search_permissions[1] . '/' . $like_record['id'])] = $object_name;
                }
            }
        }
        
        return $records;
    }
}