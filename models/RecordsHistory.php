<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "records_history".
 *
 * @property integer $id
 * @property string $table
 * @property integer $record_id
 * @property integer $user_id
 * @property string $data
 * @property string $create_time
 */
class RecordsHistory extends \app\models\AbstractModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'records_history';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table' => 'Table',
            'record_id' => 'Record ID',
            'user_id' => 'User ID',
            'data' => 'Data',
            'create_time' => 'Create Time',
        ];
    }
    
    public function log($table, $record_id, array $data)
    {
        $auth = Yii::$app->session->get('auth');
        if (empty($auth)) {
            throw new \Exception('Trying to log history of unauthrized user');
        }
        if (empty($table)) {
            throw new \Exception('Trying to log history without table');
        }
        if (empty($record_id)) {
            throw new \Exception('Trying to log history without record id');
        }
        if (empty($data)) {
            throw new \Exception('Trying to log history with no data');
        }
        
        if (!empty($data['_csrf'])) {
            unset($data['_csrf']);
        }
        
        $this->user_id = $auth['id'];
        $this->user_name = $auth['name'];
        $this->table = $table;
        $this->record_id = $record_id;
        $this->data = serialize($data);
        $this->create_time = date('Y-m-d H:i:s');
        
        $result = $this->save(false);
        
        return $result;
    }
}
