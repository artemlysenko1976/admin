<?php

namespace app\models;

use Yii;
use ReflectionClass;

/**
 * This is the model class for table "users_activity".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $user_name
 * @property string $operation
 * @property integer $object_id
 * @property string $object_name_
 * @property string $create_time
 */
class UsersActivity extends \app\models\AbstractModel
{
    const OPERATION_SIGN_IN = 'sign in';
    const OPERATION_SIGN_OUT = 'sign out';
    const OPERATION_ADD = 'add';
    const OPERATION_EDIT = 'edit';
    const OPERATION_DELETE = 'delete';
//    const OPERATION_VIEW = 'view';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_activity';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'user_name' => 'User Name',
            'operation' => 'Operation',
            'object_id' => 'Object ID',
            'object_name' => 'Object Name',
            'create_time' => 'Create Time',
        ];
    }
    
    public function log($operation, $object_id=null, $object_name=null)
    {
        $auth = Yii::$app->session->get('auth');
        if (empty($auth)) {
            throw new \Exception('Trying to log activity of unauthrized user');
        }
        if (empty($operation)) {
            throw new \Exception('Trying to log activity without operation');
        }
        
        $this->user_id = $auth['id'];
        $this->user_name = $auth['name'];
        $this->operation = $operation;
        if (!empty($object_id)) {
            $this->object_id = $object_id;
        }
        if (!empty($object_name)) {
            $this->object_name_ = $object_name;
        }
        $this->create_time = date('Y-m-d H:i:s');
        
        $result = $this->save(false);
        
        return $result;
    }
    
    public static function getOperations()
    {
        $oClass = new ReflectionClass(__CLASS__);
        $consts = $oClass->getConstants();
        $operations = [];
        foreach ($consts as $k => $v) {
            if (strpos($k, 'OPERATION_') === 0) {
                $operations[] = $v;
            }
        }
        
        return $operations;
    }
    
    public static function getUsers()
    {
        $users = [];
        
        $records = self::find()
            ->select(["DISTINCT(user_name)"])
            ->orderBy("user_name ASC")
            ->asArray()
            ->all();
        foreach ($records as $record) {
            $users[] = $record['user_name'];
        }
        
        return $users;
    }
}