<?php

namespace app\models;

use Yii;
use app\models\GoodsCategoriesLocal;
use app\models\GoodsParameters;
use app\models\GoodsParametersLocal;
use app\models\GoodsParametersMap;

/**
 * This is the model class for table "goods_categories".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $ordering
 * @property integer $active
 * @property integer $deleted
 */
class GoodsCategories extends \app\models\AbstractCommonDbLocal
{
    public $object_name = 'Goods category {id}';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_categories';
    }
    
    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительская категория',
            'name' => Yii::t('app', 'Название'),
            'active' => Yii::t('app', 'Активность'),
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        foreach ($data['l'] as $lang => $field) {
            $entry = trim($field['name']);
            if (empty($entry)) {
                $errors['name'] = Yii::t('app', "Заполните название");
                break;
            }
        }
        
        if (!empty($data['id']) && !empty($data['parent_id'])) {
            $parent_record = self::findOne($data['parent_id']);
            if (empty($parent_record)) {
                $errors['parent_id'] = Yii::t('app', "Родительская категория не найдена");
            } else {
                while (!empty($parent_record->parent_id)) {
                    if ($parent_record->parent_id == $data['id']) {
                        $errors['parent_id'] = Yii::t('app', "Редактируемая категория является родительской к выбранной");
                        break;
                    } else {
                        $parent_record = self::findOne($parent_record->parent_id);
                        if (empty($parent_record)) {
                            break;
                        }
                    }
                }
            }
        }
        
        return $errors;
    }
    
    public function addRecord(array $data)
    {
        $data['ordering'] = $this->getNextOrdering();
        if (empty($data['parent_id'])) {
            $data['parent_id'] = 0;
        }
        
        $id = parent::addRecord($data);
        
        return $id;
    }
    
    public function editRecord(array $data)
    {
        if (empty($data['parent_id'])) {
            $data['parent_id'] = 0;
        }
        
        $result = parent::editRecord($data);
        
        return $result;
    }
    
    /**
     * Search for autocomplete
     * @param string $keyword
     * @return array
     */
    public static function autocompleteSearch($keyword)
    {
        if (empty($keyword)) {
            return [];
        }
        
        $list = [];
        $conditions = [];
        
//  Numeric keyword
        if (is_numeric($keyword)) {
            $conditions[] = self::tableName() . ".id = :keyword";
            $binds = [':keyword' => intval($keyword)];
        }
//  Other
        else {
            $fields = [
                'cl.name',
            ];
            
            $conditions[] = "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)";
            $binds = [':keyword' => '%' . $keyword . '%'];
        }
        
//  Make query
        $query = self::find()
            ->select([
                "c.id",
                "cl.name",
            ])
            ->from([
                self::tableName() . " AS c",
            ])
            ->join(
                "JOIN",
                GoodsCategoriesLocal::tableName() . " AS cl",
                "cl.main_id = c.id AND cl.lang = '" . Yii::$app->language . "'" 
            )
            ->where([
                'c.active' => 1,
                'c.deleted' => 0,
            ])
            ->andWhere("(" . implode(" OR ", $conditions) . ")", $binds)
            ->orderBy("cl.name ASC")
            ->asArray();
        
        $result = $query->all();
        
//  Build output
        if (!empty($result)) {
            foreach ($result as $result_row) {
                $list[] = $result_row['id'] . ' | ' . $result_row['name'];
            }
        }
        
        return $list;
    }
    
    public function getNestedList(array $condition=null)
    {
        $tree_list = $this->getTree($condition, ["ordering ASC"]);
        if (empty($tree_list)) {
            return [];
        }
        
        $output = $this->getNestedListRecursive($tree_list);
        
        unset($tree_list);
        
        return $output;
    }
    
    protected function getNestedListRecursive(array $tree_list, array $output=[], int $level=0)
    {
        $prefix = '';
        if ($level > 0) {
            for ($i = 0; $i < $level; $i++) {
                $prefix .= '&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $prefix .= '- ';
        }
        
        foreach ($tree_list as $record) {
            $output[$record->id] = $prefix . $record->local->name;
            
            if (!empty($record->children)) {
                $output = $this->getNestedListRecursive($record->children, $output, $level + 1);
            }
        }
        
        return $output;
    }
    
    public static function loadParams(int $category_id, int $product_id=null)
    {
        $params = [];
        
        while (!empty($category_id)) {
            $list = GoodsParameters::find()
                ->select([
                    "gp.id",
                    "gc.parent_id",
                ])
                ->from(GoodsParameters::tableName() . " AS gp")
                ->join(
                     "JOIN",
                    GoodsParametersMap::tableName() . " AS gpm",
                    "gpm.parameter_id = gp.id"
                )
                ->join(
                     "JOIN",
                    self::tableName() . " AS gc",
                    "gc.id = gpm.category_id"
                )
                ->where([
                    "gpm.category_id" => $category_id,
                    "gp.active" => 1,
                    "gp.deleted" => 0,
                ])
                ->orderBy("name ASC")
                ->asArray()
                ->all();
            
            if (empty($list)) {
                return $params;
            }
            
            $category_id = $list[0]['parent_id'];
            
            foreach ($list as $row) {
                $params[$row['id']] = [
                    'name' => $row['rellocal']['name'],
                    'value' => '',
                ];
            }
        }
        
        $output = [];
        
        if (!empty($product_id)) {
            $product_params = GoodsDescriptions::find()
                ->where([
                    'product_id' => $product_id,
                ])
                ->orderBy("id ASC")
                ->all();
            
            if (!empty($product_params)) {
                $existed_params = [];
                foreach ($product_params as $product_param) {
                    if (!isset($params[$product_param['parameter_id']])) {
                        continue;
                    }
                    
                    $existed_params[$product_param['parameter_id']] = [
                        'name' => $params[$product_param['parameter_id']]['name'],
                        'value' => $product_param['value'],
                    ];
                    
                    unset($params[$product_param['parameter_id']]);
                }
                
                if (!empty($existed_params)) {
                    foreach ($existed_params as $k => $v) {
                        $output[$k] = $v;
                    }
                }
            }
        }
        
        foreach ($params as $k => $v) {
            $output[$k] = $v;
        }
        
        return $output;
    }
}