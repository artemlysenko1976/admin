<?php

namespace app\models;

use Yii;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\web\UploadedFile;

/**
 * This is the model class for table "brands".
 *
 * @property integer $id
 * @property integer $name
 * @property integer $logo
 * @property integer $active
 * @property integer $deleted
 */
class Brands extends \app\models\AbstractCommonDb
{
    public $object_name = 'Brand {name}';
    
    public static $photo_path = '/web/images/brands/';
    public static $photo_size = [
        1500,
        1500,
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brands';
    }
    
    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Название'),
            'logo' => Yii::t('app', 'Лого'),
            'active' => Yii::t('app', 'Активность'),
        ];
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    'name',
                ];
                $where = [
                    'conditions' => "(" . implode(" ILIKE :keyword OR ", $fields) . " ILIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            },
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        if (empty($data['name'])) {
            $errors['name'] = Yii::t('app', "Заполните название");
        } else {
            $check_query = self::find()
                ->select(['id'])
                ->where([
                    'deleted' => 0,
                    'name' => $data['name'],
                ])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere("id != " . $data['id']);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['name'] = Yii::t('app', "Такое название уже используеся");
            }
        }
        
        return $errors;
    }
    
    public function addRecord(array $data)
    {
        
        $id = parent::addRecord($data, false);
        
        if (!empty($id)) {
//  Photo
            $uploaded_file = UploadedFile::getInstanceByName('logo');
            if (!empty($uploaded_file)) {
                $file_name = $id . '.' . $uploaded_file->extension;
                $images_result = Image::getImagine()
                    ->open($uploaded_file->tempName)
                    ->thumbnail(new Box(self::$photo_size[0], self::$photo_size[1]))
                    ->save(Yii::$app->basePath . self::$photo_path . $file_name, ['quality' => 100]);
                if (!empty($images_result)) {
                    $data['logo'] = $file_name;
                    $this->logo = $file_name;
                    $this->save(false);
                }
            }
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_ADD, $id, "Brand " . $data['name']);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $id, $data);
        }
        
        return $id;
    }
    
    public function editRecord(array $data)
    {
        $old_data = self::findOne($data['id']);
        if (empty($old_data)) {
            return false;
        }
        
//  Delete photo
        if (!empty($data['delete_logo'])) {
            unlink(Yii::$app->basePath . self::$photo_path . $old_data->logo);
            $data['logo'] = null;
            unset($data['delete_logo']);
        }
        
//  Photo
        $uploaded_file = UploadedFile::getInstanceByName('logo');
        if (!empty($uploaded_file)) {
            if (!empty($old_data->logo)) {
                unlink(Yii::$app->basePath . self::$photo_path . $old_data->logo);
            }
            
            $file_name = $old_data->id . '.' . $uploaded_file->extension;
            $images_result = Image::getImagine()
                ->open($uploaded_file->tempName)
                ->thumbnail(new Box(self::$photo_size[0], self::$photo_size[1]))
                ->save(Yii::$app->basePath . self::$photo_path . $file_name, ['quality' => 100]);
            if (!empty($images_result)) {
                $data['logo'] = $file_name;
            }
        }
        
        $result = parent::editRecord($data, false);
        
        if (!empty($result)) {
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_EDIT, $data['id'], "Brand " . $data['name']);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $old_data->id, $data);
        }
        
        return $result;
    }
}