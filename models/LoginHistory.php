<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "login_history".
 *
 * @property integer $id
 * @property string $login
 * @property string $ip
 * @property integer $result
 * @property string $module
 * @property timestamp $create_time
 */
class LoginHistory extends \app\models\AbstractModel
{
    /**
     * Maximal count of authorization tries
     * @var integer 
     */
    protected static $max_tries = 5;

    /**
     * Time range during which $max_tries will be calculated
     * @var integer 
     */
    protected static $max_time = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'login_history';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'ip' => 'Ip',
            'result' => 'Result',
            'module' => 'Module',
            'create_time' => 'Create Time',
        ];
    }
    
    public function log($post, $module='backend')
    {
        $this->login = $post['login'] ?? $post['email'];
        $this->ip = Yii::$app->request->getUserIP();
        $this->module = $module;
        $this->create_time = date('Y-m-d H:i:s', time());
        
        $result = $this->save(false);
        
        return $result;
    }
    
    public function updateResult(bool $login_result)
    {
        $this->result = intval($login_result);
        $this->create_time = $this->create_time;
        $result = $this->save(false);
        
        return $result;
    }
    
    public function checkTries($ip)
    {
        $result = self::find()
            ->where(['ip' => $ip])
            ->andWhere("create_time >= '" . date('Y-m-d H:i:s', (time() - self::$max_time)) . "'")
            ->count();
    
        return ($result >= self::$max_tries ? true : false);
    }
}