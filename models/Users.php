<?php

namespace app\models;

use yii;
use yii\db\Query;
use app\models\UsersGroupsMap;
use app\interfaces\Authorizable;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\web\UploadedFile;
use app\library\Validator;;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $role
 * @property string $name
 * @property string $password
 * @property string $email
 * @property string $phone
 * @property string $first_name
 * @property string $second_name
 * @property string $last_name
 * @property string $photo
 * @property integer $active
 * @property integer $basic
 * @property integer $deleted
 */
class Users extends \app\models\AbstractModel implements Authorizable
{
    public $object_name = 'User {name}';
    
     public static $global_search_fields = [
        'name',
        'email',
    ];
    public static $global_search_permissions = [
        'users',
        'edit',
    ];
    
    public static $roles = [
        
    ];
    
    public static $photo_path = '/web/images/users/';
    public static $photo_size = [
        1500,
        1500,
    ];
    
    public static $password_min_chars = 5;
    public static $password_max_chars = 12;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'role' => Yii::t('app', 'Роль'),
            'name' => Yii::t('app', 'Имя'),
            'first_name' => Yii::t('app', 'Имя'),
            'second_name' => Yii::t('app', 'Отчество'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'email' => Yii::t('app', 'E-mail'),
            'password' => Yii::t('app', 'Пароль'),
            'password_confirm' => Yii::t('app', 'Подтверждение пароля'),
            'phone' => Yii::t('app', 'Номер телефона'),
            'photo' => Yii::t('app', 'Фотография'),
            'groups' => Yii::t('app', 'Группы прав доступа'),
            'active' => 'Активный',
            'deleted' => 'Deleted',
        ];
    }
    
    public function getHistoryCallbacks()
    {
        $callbacks = parent::getHistoryCallbacks();
        $callbacks['password'] = function($value) {
            return null;
        };
        $callbacks['groups'] = function($value) {
            $groups = self::find()
                ->select(['name'])
                ->from(UsersGroups::tableName())
                ->where(['id' => $value])
                ->all();
            
            if (empty($groups)) {
                return false;
            }
            
            $output = [];
            foreach ($groups as $v) {
                $output[] = $v['name'];
            }
            
            return implode(', ', $output);
        };
        
        return $callbacks;
    }
    
    public function getRestoreCallbacks()
    {
        $callbacks = parent::getRestoreCallbacks();
        $callbacks['password'] = function($value) {
            return null;
        };
        $callbacks['groups'] = function($value) {
            $output = [];
            foreach ($value as $group_id) {
                $record = new UsersGroupsMap();
                $record->user_id = $this->id;
                $record->group_id = $group_id;
                
                $output[] = $record;
            }
            
            return $output;
        };
        
        return $callbacks;
    }
    
    public function getGroups_ids()
    {
        if (empty($this->id)) {
            return false;
        }
        
        $groups_ids = UsersGroupsMap::find()
            ->select([
                'group_id',
            ])
            ->where([
                'user_id' => $this->id,
            ])
            ->column();
        
        return $groups_ids;
    }
    
    public function getGroups()
    {
        return $this->hasMany(
            UsersGroupsMap::className(), 
            ['user_id' => 'id']
        );
    }
    
    public function setGroups(array $groups)
    {
        $this->groups = $groups;
        
        return $this;
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        $data['first_name'] = trim($data['first_name']);
        if (empty($data['first_name'])) {
            $errors['first_name'] = Yii::t('app', "Введите Имя");
        }
        
        $data['last_name'] = trim($data['last_name']);
        if (empty($data['last_name'])) {
            $errors['last_name'] = Yii::t('app', "Введите Фамилию");
        }
        
        $data['email'] = trim($data['email']);
        if (empty($data['email'])) {
            $errors['email'] = Yii::t('app', "Введите E-mail");
        } else {
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $errors['email'] = Yii::t('app', "Неверный формат E-mail");
            } else {
                $check_query = self::find()
                    ->select(['id'])
                    ->where([
                        'deleted' => 0,
                        'email' => $data['email'],
                    ])
                    ->limit(1);
                if (!empty($data['id'])) {
                    $check_query->andWhere("id != " . $data['id']);
                }
                $check = $check_query->one();
                if (!empty($check)) {
                    $errors['email'] = Yii::t('app', "Этот E-mail уже используется");
                }
            }
        }
        
        $data['phone'] = trim($data['phone']);
        if (!empty($data['phone'])) {
            if (Validator::validatePhoneNumber($data['phone']) == false) {
                $errors['phone'] = Yii::t('app', "Неверный формат телефона");
            }
        }
        
        $data['password'] = trim($data['password']);
        $data['password_confirm'] = trim($data['password_confirm']);
        
        if (empty($data['id'])) {
            if (empty($data['password'])) {
                $errors['password'] = Yii::t('app', "Введите Пароль");
            }
        }
        
        if (!empty($data['password'])) {
            if (strlen($data['password']) < self::$password_min_chars || strlen($data['password']) > self::$password_max_chars) {
                $errors['password'] = Yii::t('app', "Пароль должен содержать от {min} до {max} символов", [
                    'min' => self::$password_min_chars,
                    'max' => self::$password_max_chars,
                ]);
            }
            
            if (empty($data['password_confirm'])) {
                $errors['password_confirm'] = Yii::t('app', "Введите Подтверждение пароля");
            } else {
                if ($data['password_confirm'] !== $data['password']) {
                    $errors['password_confirm'] = Yii::t('app', "Неверное подтверждение пароля");
                }
            }
        }
        
        return $errors;
    }
    
    public function validatePassword($data)
    {
        $errors = [];
        
        if (isset($data['password_old'])) {
            $data['password_old'] = trim($data['password_old']);
            
            if (empty($data['password_old'])) {
                $errors['password_old'] = Yii::t('app', "Введите старый пароль");
            } else {
                $check = (new Query())
                    ->select(['id'])
                    ->from(self::tableName())
                    ->where(['password' => self::encodePassword($data['password_old'])])
                    ->andWhere(['deleted' => 0])
                    ->andWhere(['id' => Yii::$app->session->get('auth')['id']])
                    ->limit(1)
                    ->one();

                if (empty($check)) {
                    $errors['password_old'] = Yii::t('app', "Неверный старый пароль");
                }
            }
        }
        
        $data['password'] = trim($data['password']);
        $data['password_confirm'] = trim($data['password_confirm']);
        
        if (empty($data['password'])) {
            $errors['password'] = Yii::t('app', "Введите новый пароль");
        }
        
        if (!empty($data['password'])) {
            if (strlen($data['password']) < self::$password_min_chars || strlen($data['password']) > self::$password_max_chars) {
                $errors['password'] = Yii::t('app', "Новый пароль должен содержать от {min} до {max} символов", [
                    'min' => self::$password_min_chars,
                    'max' => self::$password_max_chars,
                ]);
            }
            
            if (empty($data['password_confirm'])) {
                $errors['password_confirm'] = Yii::t('app', "Введите Подтверждение пароля");
            } else {
                if ($data['password_confirm'] !== $data['password']) {
                    $errors['password_confirm'] = Yii::t('app', "Неверное Подтверждение пароля");
                }
            }
        }
        
        return $errors;
    }
    
    public function addRecord(array $data, $log=true)
    {
        $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
        
//  Process password
        unset($data['password_confirm']);
        if (!empty($data['password'])) {
            $data['password'] = self::encodePassword($data['password']);
        }
        
//  Get permissions groups
        if (!empty($data['groups'])) {
            $groups = $data['groups'];
            unset($data['groups']);
        }
        
//  Save data
        $id = parent::addRecord($data, false);
        
        if (!empty($id)) {
//  Save permissions groups map
            if (!empty($groups)) {
                foreach ($groups as $group_id) {
                    $map_obj = new UsersGroupsMap();
                    $map_obj->user_id = $id;
                    $map_obj->group_id = $group_id;
                    $map_obj->save(false);
                }
                
                $data['groups'] = $groups;
            }
            
//  Photo
            $uploaded_file = UploadedFile::getInstanceByName('photo');
            if (!empty($uploaded_file)) {
                $file_name = $id . '.' . $uploaded_file->extension;
                $images_result = Image::getImagine()
                    ->open($uploaded_file->tempName)
                    ->thumbnail(new Box(self::$photo_size[0], self::$photo_size[1]))
                    ->save(Yii::$app->basePath . self::$photo_path . $file_name, ['quality' => 100]);
                if (!empty($images_result)) {
                    $this->photo = $file_name;
                    $this->save(false);
                }
            }
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_ADD, $id, "User " . $data['name']);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $id, $data);
        }
        
        return $id;
    }
    
    public function editRecord(array $data, $log=true)
    {
//  Get old data
        $pld_data = self::findOne($data['id']);
        if (empty($pld_data)) {
            return false;
        }
        
        $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
        
//  Process password
        unset($data['password_confirm']);
        if (!empty($data['password'])) {
            $data['password'] = self::encodePassword($data['password']);
        } else {
            $data['password'] = $pld_data->password;
        }
        
//  Get permissions groups
        if (!empty($data['groups'])) {
            $groups = $data['groups'];
            unset($data['groups']);
        }
        
//  Delete photo
        if (!empty($data['delete_photo'])) {
            unlink(Yii::$app->basePath . self::$photo_path . $data['photo']);
            $data['photo'] = null;
            unset($data['delete_photo']);
        }
        
//  Photo
        $uploaded_file = UploadedFile::getInstanceByName('photo');
        if (!empty($uploaded_file)) {
            if (!empty($pld_data->photo)) {
                unlink(Yii::$app->basePath . self::$photo_path . $pld_data->photo);
            }
            
            $file_name = $data['id'] . '.' . $uploaded_file->extension;
            $images_result = Image::getImagine()
                ->open($uploaded_file->tempName)
                ->thumbnail(new Box(self::$photo_size[0], self::$photo_size[1]))
                ->save(Yii::$app->basePath . self::$photo_path . $file_name, ['quality' => 100]);
            if (!empty($images_result)) {
                $data['photo'] = $file_name;
            }
        }
        
        $result = parent::editRecord($data, false);
        
        if (!empty($result)) {
//  Delete old permissions groups map
            $old_map = UsersGroupsMap::findAll(['user_id' => $data['id']]);
            if (!empty($old_map)) {
                foreach ($old_map as $old_map_v) {
                    $old_map_v->delete();
                }
            }
            
//  Save permissions groups map
            if (!empty($groups)) {
                foreach ($groups as $group_id) {
                    $map_obj = new UsersGroupsMap();
                    $map_obj->user_id = $data['id'];
                    $map_obj->group_id = $group_id;
                    $map_obj->save(false);
                }
                
                $data['groups'] = $groups;
            }
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_EDIT, $data['id'], "User " . $data['name']);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $data['id'], $data);
        }
        
        return $result;
    }
    
    public function changePassword($password, $id)
    {
//  Get old data
        $data = self::findOne(['id' => $id]);
        if (empty($data)) {
            return false;
        }
            
//  Save password
        $data->password = self::encodePassword($password);
        $result = $data->save(false);
          
        if (!empty($result)) {
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_EDIT, $id, "User " . $data['name']);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $id, $data->toArray());
        }
        
        return $result;
    }
    
    /**
     * Gets user permissions according group(s)
     * @param \app\models\Users $data
     * @return array
     */
    public static function getPermissions(\app\models\Users $data)
    {
        $output = [];
        
        if ($data->basic == 1) {
//  For default user
            $output = self::getAllPermissions();
        } else {
//  For created users
            if (!empty($data->groups)) {
                foreach ($data->groups as $map_data) {
                    foreach ($map_data->group->getPermissions() as $controller_name => $actions) {
                        foreach ($actions as $action_name) {
                            $output[$controller_name][] = $action_name;
                        }
                    }
                }
            }
        }
        
        return $output;
    }
    
    public static function getAllPermissions()
    {
        $output = [];
        
        $menu_tree = include Yii::$app->basePath . '/backend/config/menu.php';
        foreach ($menu_tree as $item) {
            foreach ($item['actions'] as $action) {
                if ($action['check'] == true) {
                    $output[$item['url']][] = $action['url'];
                }
            }
        }
        
        return $output;
    }
    
    public static function checkAccessPermissions($controller, $action)
    {
//  Init
        $ignored_controllers = [
            'default',
            'site',
            'auth',
        ];
        
        $ignored_actions = [
            'validate',
            'validatefields',
        ];
        
//  Check ajax 
        if (Yii::$app->request->getIsAjax()) {
            return true;
        }
        
//  Check ignored controllers
        if (in_array($controller, $ignored_controllers)) {
            return true;
        }
        
//  Check ignored actions
        if (in_array($action, $ignored_actions)) {
            return true;
        }
        
//  Check permissions
        if (in_array($action, Yii::$app->session->get('auth')['permissions'][$controller])) {
            return true;
        }
        
        return false;
    }
    
    public static function encodePassword($password)
    {
        if (empty($password)) {
            return '';
        }
        
        return md5($password);
    }
    
    public static function generateCrossDomainHash($login, $password)
    {
        $hash = md5(
            '()-'
            . $password
            . '&6sd5f'
            . Yii::$app->params['crossd_domain_auth_secret']
            . '--><9'
            . $login
            . '111'
        );
        
        return $hash;
    }
}