<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property integer $id
 * @property string $iso_code
 * @property string $iso_code_2
 * @property string $phone_code
 * @property integer $active
 * @property integer $deleted
 */
class Countries extends \app\models\AbstractCommonDbLocal
{
    public $object_name = 'Country {iso_code}';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }
    
    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Название'),
            'iso_code' => Yii::t('app', 'ISO код (3 символа)'),
            'iso_code_2' => Yii::t('app', 'ISO код (2 символа)'),
            'phone_code' => Yii::t('app', 'Телефонный код'),
            'active' => Yii::t('app', 'Активность'),
            'region' => Yii::t('app', 'Регион'),
        ];
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    'name',
                    'iso_code',
                    'phone_code',
                ];
                $where = [
                    'conditions' => "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            }
        ];
    }

    public function validateRecord($data)
    {
        $errors = [];
        
        foreach ($data['l'] as $lang => $field) {
            $entry = trim($field['name']);
            if (empty($entry)) {
                $errors['name'] = Yii::t('app', "Заполните название");
                break;
            }
        }
        
        $data['iso_code'] = trim($data['iso_code']);
        if (empty($data['iso_code'])) {
            $errors['iso_code'] = Yii::t('app', 'Заполните ISO код');
        } else {
            if (strlen($data['iso_code']) != 3) {
                $errors['iso_code'] = Yii::t('app', 'ISO код должен состоять из 3 символов');
            } else {
                $check_query = self::find()
                    ->where([
                        "iso_code" => $data['iso_code'],
                        'deleted' => 0,
                    ])
                    ->limit(1);
                if (!empty($data['id'])) {
                    $check_query->andWhere("id != " . $data['id']);
                }
                $check = $check_query->one();
                if (!empty($check)) {
                    $errors['iso_code'] = Yii::t('app', 'Этот ISO код уже используется');
                }
            }
        }
        
        $data['iso_code_2'] = trim($data['iso_code_2']);
        if (empty($data['iso_code_2'])) {
            $errors['iso_code_2'] = Yii::t('app', 'Заполните ISO код');
        } else {
            if (strlen($data['iso_code_2']) != 2) {
                $errors['iso_code_2'] = Yii::t('app', 'ISO код должен состоять из 2 символов');
            } else {
                $check_query = self::find()
                    ->where([
                        "iso_code_2" => $data['iso_code_2'],
                        'deleted' => 0,
                    ])
                    ->limit(1);
                if (!empty($data['id'])) {
                    $check_query->andWhere("id != " . $data['id']);
                }
                $check = $check_query->one();
                if (!empty($check)) {
                    $errors['iso_code_2'] = Yii::t('app', 'Этот ISO код уже используется');
                }
            }
        }
        
        $data['phone_code'] = trim($data['phone_code']);
        if (!empty($data['phone_code'])) {
            if (!preg_match('/^([0-9]{1,3})$/', $data['phone_code'])) {
                $errors['phone_code'] = Yii::t('app', 'Телефонный код должен содержать не более 3 цыфр');
            } else {
                $check_query = self::find()
                    ->where([
                        "phone_code" => $data['phone_code'],
                        'deleted' => 0,
                    ])
                    ->limit(1);
                if (!empty($data['id'])) {
                    $check_query->andWhere("id != " . $data['id']);
                }
                $check = $check_query->one();
                if (!empty($check)) {
                    $errors['phone_code'] = Yii::t('app', 'Этот Телефонный код уже используется');
                }
            }
        }
        
        return $errors;
    }
    
    public function addRecord(array $data)
    {
        $data['iso_code'] = strtoupper($data['iso_code']);
        
        $id = parent::addRecord($data);
        
        return $id;
    }
    
    public function editRecord(array $data)
    {
        $data['iso_code'] = strtoupper($data['iso_code']);
        
        $result = parent::editRecord($data);
        
        return $result;
    }
    
    /**
     * Search for autocomplete
     * @param string $keyword
     * @return array
     */
    public static function autocompleteSearch($keyword)
    {
        if (empty($keyword)) {
            return [];
        }
        
        $list = [];
        $conditions = [];
        
//  Numeric keyword
        if (is_numeric($keyword)) {
            $conditions[] = self::tableName() . ".id = :keyword";
            $binds = [':keyword' => intval($keyword)];
        }
//  Other
        else {
            $fields = [
                'cl.name',
                'c.iso_code',
                'c.iso_code_2',
                'c.phone_code',
            ];
            
            $conditions[] = "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)";
            $binds = [':keyword' => '%' . $keyword . '%'];
        }
        
//  Make query
        $query = self::find()
            ->select([
                "c.id",
                "cl.name",
            ])
            ->from([
                self::tableName() . " AS c",
            ])
            ->join(
                "JOIN",
                CountriesLocal::tableName() . " AS cl",
                "cl.main_id = c.id AND cl.lang = '" . Yii::$app->language . "'" 
            )
            ->where([
                'c.active' => 1,
                'c.deleted' => 0,
            ])
            ->andWhere("(" . implode(" OR ", $conditions) . ")", $binds)
            ->orderBy("cl.name ASC")
            ->asArray();
        
        $result = $query->all();
        
//  Build output
        if (!empty($result)) {
            foreach ($result as $result_row) {
                $list[] = $result_row['id'] . ' | ' . $result_row['name'];
            }
        }
        
        return $list;
    }
    
    public static function getRegions()
    {
        $output = [];
        
        $result = self::find()
            ->select([
                "DISTINCT(region) AS region_name"
            ])
            ->andWhere("region != ''")
            ->orderBy("region_name ASC")
            ->asArray()
            ->all();
        
        if (!empty($result)) {
            foreach ($result as $result_v) {
                $output[] = $result_v['region_name'];
            }
        }
        
        return $output;
    }
}