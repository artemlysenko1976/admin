<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goods_descriptions".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $parameter_id
 * @property string $value
 */
class GoodsDescriptions extends \app\models\AbstractCommonDb
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_descriptions';
    }
}