<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries_l".
 *
 * @property integer $main_id
 * @property string $lang
 * @property string $name
 */
class CountriesLocal extends \app\models\AbstractCommonDb
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries_l';
    }
}