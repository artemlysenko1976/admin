<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payments_methods_l".
 *
 * @property integer $main_id
 * @property string $lang
 * @property string $name
 */
class PaymentsMethodsLocal extends \app\models\AbstractCommonDb
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_methods_l';
    }
}