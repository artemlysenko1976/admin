<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email_history".
 *
 * @property integer $id
 * @property string $subject
 * @property string $content
 * @property integer $user_id
 * @property string $user_name
 * @property string $recipients
 * @property string $create_time
 * @property integer $result
 * @property string $error
 */
class EmailHistory extends \app\models\AbstractModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_history';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject',
            'content' => 'Content',
            'user_id' => 'User ID',
            'recipients' => 'Recipients',
            'create_time' => 'Create Time',
            'result' => 'Result',
            'error' => 'Error',
        ];
    }
    
    public function log($subject, $content, $recipients, $result, $error, $user_id=0, $user_name='')
    {
        if (empty($subject)) {
            throw new \Exception('Trying to log e-mail history without subject');
        }
        if (empty($content)) {
            throw new \Exception('Trying to log e-mail history without message');
        }
        if (empty($recipients)) {
            throw new \Exception('Trying to log e-mail history without any recipients');
        }
        
        if (!is_array($recipients)) {
            $recipients = [$recipients];
        }
        
        $this->subject = $subject;
        $this->content = $content;
        $this->user_id = $user_id;
        $this->user_name = $user_name;
        $this->recipients = json_encode($recipients);
        $this->result = intval($result);
        $this->error = $error;
        $this->create_time = date('Y-m-d H:i:s');
        
        $save_result = $this->save(false);
        
        return $save_result;
    }
}
