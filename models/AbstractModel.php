<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\data\Pagination;
use yii\db\Query;
use app\models\UsersActivity;
use app\models\RecordsHistory;
use app\library\HttpClient;
use yii\helpers\Url;

/**
 * Abstract Model
 *
 */
class AbstractModel extends ActiveRecord
{
    /**
     * For logging admin actions
     * @var type string
     */
    public $object_name;
    
    public $children;
    
    public $restore_data;
    
    public $export_columns = [
        'id'
    ];
    
    public static $db_field_name_escape = '`';
    
    public static $db_like = 'LIKE';
    
    public static $list_limits = [
        25,
        50,
        100,
        250,
        500,
    ];
    
    /**
     * For notification of other platform frontend (if presents)
     * @var type string
     */
    protected $section_name = '';
    
    public static function getJoins()
    {
        return [];
    }
    
    /**
     * Builds records list with pagination
     * @param array $where 'conditions' and 'binds' elements
     * @param array $order
     * @param int $page
     * @param int $limit
     * @param array $joins array of arrays (join type, table name, conditions) f.e.: ['LEFT JOIN', 'users', 'users.id = orders.id']
     * @param array $select array of selected columns
     * @param array $group array of GROUP BY clauses
     * @return array
     */
    public function getPagerList($where=null, array $order=[], $page=1, $limit=25, $joins=[], $select=[], $group=[])
    {
//  Init
        $output = [
            'records' => [],
            'pages' => null,
        ];
        
        $query = static::find();
        
//  SELECT
        if (!empty($select)) {
            $query->select($select);
        }
        
//  JOINS
        if (!empty($joins)) {
            foreach ($joins as $join) {
                if (count($join) != 3) {
                    continue;
                }
                $query->join($join[0], $join[1], $join[2]);
            }
        }
        
//  WHERE
        if (!empty($where)) {
            if (is_string($where)) {
                $query->where($where);
            } elseif (isset($where['conditions'])) {
                $where_conditions = implode(" AND ", $where['conditions']);
                $where_binds = (!empty($where['binds']) ? $where['binds'] : []);
                $query->where($where_conditions, $where_binds);
            }
        }
        
//  ORDER
        if (!empty($order)) {
            $order = implode(', ', $order);
            $query->orderBy($order);
        }
        
//  GROUP BY
        if (!empty($group)) {
            $group = implode(', ', $group);
            $query->groupBy($group);
        }
        
//  Page
        if (!is_numeric($page) || $page <= 0) {
            $page = 1;
        }
        
//  Limit
        if (empty($limit) || !is_numeric($limit) || $limit <= 0) {
            $limit = self::$list_limits[0];
        }
        
//  Pagination
        $pagination = new Pagination([
            'totalCount' => $query->count(),
            'pageSize' => $limit
        ]);
        $output['pages'] = $pagination;
        
        $query->offset($pagination->offset);
        $query->limit($pagination->limit);
        
//  Output format
        if (!empty($select)) {
            $query->asArray();
        }
        
//  Get records
        $output['records'] = $query->all();
        
        return $output;
    }
    
    /**
     * Inserts data
     * @param array $data
     * @param boolean $log if TRUE then logs will be wrote down
     * @return int|boolean
     */
    public function addRecord(array $data, $log=true)
    {
//  Check data
        if (empty($data)) {
            return false;
        }
        
//  Unset CSRF data
        unset($data['_csrf']);
        
//  Set data
        foreach ($data as $data_k => $data_v) {
            if (is_string($data_v)) {
                $data_v = trim($data_v);
            }
            $this->$data_k = $data_v;
        }
        
//  Insert
        $result = $this->save(false);
        
        if (!empty($result)) {
            if ($log === true) {
//  Log user activity
                $object_name = $this->object_name;
                if (!empty($object_name)) {
                    preg_match('/\{(.*)\}/', $object_name, $object_property);
                    if (!empty($object_property)) {
                        $object_name = str_replace($object_property[0], $this->{$object_property[1]}, $object_name);
                    }
                }

                $activity = new UsersActivity();
                $activity->log(UsersActivity::OPERATION_ADD, $this->id, $object_name);
                
//  Log history
                $history = new RecordsHistory();
                $history->log($this->tableName(), $this->id, $this->toArray());
            }
            
//  Return
            return $this->id;
        } else {
            return false;
        }
    }
    
    /**
     * Updates data
     * @param array $data
     * @param boolean $log if TRUE then logs will be wrote down
     * @return boolean
     */
    public function editRecord(array $data, $log=true)
    {
//  Check data
        if (empty($data) || empty($data['id'])) {
            return false;
        }
        
//  Unset CSRF data
        unset($data['_csrf']);
        
//  Set record ID
        $id = $data['id'];
        if (!is_numeric($id) || $id <= 0) {
            return false;
        }
        
//  Get old data
        $old_data = self::findOne($id);
        if (empty($old_data)) {
            return false;
        }
        
//  Set data
        foreach ($data as $data_k => $data_v) {
            if (is_string($data_v)) {
                $data_v = trim($data_v);
            }
            $old_data->$data_k = $data_v;
        }
        
//  Store update time
        if ($old_data->hasAttribute('update_time')) {
            $old_data->update_time = date(Yii::$app->params['date_time_format'], time());
        }
        
//  Update
        $result = $old_data->save(false);
        if (!empty($result)) {
            if ($log === true) {
//  Log user activity
                $object_name = $this->object_name;
                if (!empty($object_name)) {
                    preg_match('/\{(.*)\}/', $object_name, $object_property);
                    if (!empty($object_property)) {
                        $object_name = str_replace($object_property[0], $old_data->{$object_property[1]}, $object_name);
                    }
                }
                
                $activity = new UsersActivity();
                $activity->log(UsersActivity::OPERATION_EDIT, $old_data->id, $object_name);
                
//  Log history
                $history = new RecordsHistory();
                $history->log($this->tableName(), $old_data->id, $old_data->toArray());
            }
        }
        
//  Return
        return $result;
    }
    
    /**
     * Deletes record
     * @param int $id
     * @param boolean $soft if TRUE then record only marked as deleted
     * @param boolean $log if TRUE then logs will be wrote down
     * @return boolean
     */
    public function deleteRecord($id, $soft=true, $log=true)
    {
//  Check record ID
        if (!is_numeric($id) || $id <= 0) {
            return false;
        }
        
//  Get old data
        $old_data = self::findOne($id);
        if (empty($old_data)) {
            return false;
        }

        if ($soft === true) {
//  Soft deleting
            if (!$old_data->hasAttribute('deleted')) {
                throw new \Exception('Soft deleting failed - record has not Deleted property');
            }
            
            $old_data->deleted = 1;
            $result = $old_data->save(false);
        } else {
//  Real deleting
            $object_name = $this->object_name;
            if (!empty($object_name)) {
                preg_match('/\{(.*)\}/', $object_name, $object_property);
                if (!empty($object_property)) {
                    $object_name = str_replace($object_property[0], $old_data->{$object_property[1]}, $object_name);
                }
            }
            
            $result = $old_data->delete();
        }
        
        if (!empty($result)) {
            if ($log === true) {
//  Log user activity
                $activity = new UsersActivity();
                $activity->log(UsersActivity::OPERATION_DELETE, $id, $object_name);
                
//  Log history
                if ($soft === true) {
                    $history = new RecordsHistory();
                    $history->log($this->tableName(), $id, $old_data->toArray());
                }
            }
        }
        
//  Return
        return $result;
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        return $errors;
    }
    
    public function getNextOrdering($where=null)
    {
        $query = self::find()
            ->select([
                'MAX(ordering) AS result'
            ])
            ->from(self::tableName())
            ->limit(1)
            ->asArray();
        if (!empty($where)) {
            $query->where(['and', $where]);
        }
        $result = $query->one();
        
        if (is_numeric($result['result'])) {
            $result['result']++;
        } else {
            $result['result'] = 0;
        }
        if ($result['result'] < 0) {
            $result['result'] = 0;
        }
        
        return $result['result'];
    }
    
    /**
     * Builds tree based on parent_id property
     * @param array $where
     * @param array $order
     * @param int $parent_id
     * @return array
     */
    public function getTree(array $where=[], array $order=[], $parent_id=0)
    {
        $query = self::find();
        $query->where([
            'parent_id' => $parent_id
        ]);
        if (!empty($where)) {
            foreach ($where as $where_k => $where_v) {
                if (is_numeric($where_k)) {
                    $query->andWhere($where_v);
                } else {
                    $query->andWhere([
                        $where_k => $where_v
                    ]);
                }
            }
            
        }
        if (!empty($order)) {
            $query->orderBy(implode(', ', $order));
        }
        $records = $query->all();
        if (!empty($records)) {
            foreach ($records as $records_k => $records_v) {
                $records[$records_k]['children'] = $this->getTree($where, $order, $records_v->id);
            }
        }
        
        return $records;
    }
    
    /**
     * Method to change ordering for every records of group
     * @param array $ordering
     * @return boolean
     */
    public function changeOrdering(array $ordering)
    {
        $records = 0;
        
        foreach ($ordering as $order => $record_id) {
            $condition = [
                'id' => $record_id
            ];
            $record = self::findOne($condition);
            if (!empty($record)) {
                $record->ordering = $order;
                $result = $record->save(false);
                if (!empty($result)) {
                    $records++;
                }
            }
        }
        
        return $records;
    }
    
    public static function prepareFieldName($string)
    {
        return strtolower(str_replace([' ', '+', '"', "'", '?', '!', '=', '+', '@', '#', '%', '^', '&', '$', '*'], ['_', ''], $string));
    }
    
    /**
     * Gets changes history by particular record
     * @param string $table table name
     * @param integer $record_id ID of record
     * @return boolean|array
     */
    public function getHistory($table=null, $record_id=null)
    {
//  Auto setting params
        if (empty($table)) {
            $table = $this->tableName();
        }
        
        if (empty($record_id)) {
            $record_id = $this->id;
        }
        
//  Check params
        if (empty($table) || !is_string($table)) {
            return false;
        }
        
        if (empty($record_id) || !is_numeric($record_id) || $record_id <= 0) {
            return false;
        }
        
//  Get history
        $history = RecordsHistory::find()
            ->where([
                'table' => $table,
                'record_id' => $record_id
            ])
            ->orderBy("create_time DESC")
            ->asArray()
            ->all();
        
        if (empty($history)) {
            return $history;
        }
        
        $data_array = [];
        foreach ($history as $history_k => $history_v) {
//  Add ID to data array
            $history_v['data'] = unserialize($history_v['data']);
            $data = [
                'id' => $history_v['record_id']
            ];
            foreach ($history_v['data'] as $data_k => $data_v) {
                $data[$data_k] = $data_v;
            }
            $history_v['data'] = serialize($data);
            
//  Unset duplicated records
            if (in_array($history_v['data'], $data_array)) {
                unset($history[$history_k]);
                continue;
            } 
            
            $data_array[] = $history_v['data'];
                
//  Unserialize data
            $history_v['data'] = unserialize($history_v['data']);
            
//  Process data
            $callbacks = $this->getHistoryCallbacks();
            $data_tmp = [];
            foreach ($history_v['data'] as $data_k => $data_v) {
                if ($data_k == 'id') {
                    continue;
                }
                
                $key = (!empty($this->attributeLabels()[$data_k]) ? $this->attributeLabels()[$data_k] : ucfirst($data_k));
                if (isset($callbacks[$data_k])) {
                    $value = $callbacks[$data_k]($data_v);
                } else {
                    $value = $data_v;
                }
                
//  Return empty data
                if ($value == null) {
                    continue;
                }
                
                if (is_array($value)) {
                    if ($data_k != 'l') {    
//  Nested array
                        foreach ($value as $value_k => $value_v) {
                            $data_tmp[$value_k] = $value_v;
                        }
                        continue;
                    }
                }
                
                $data_tmp[$key] = $value;
            }
            
            $history_v['data'] = $data_tmp;
            unset($data_tmp);
            $history[$history_k] = $history_v;
        }
        unset($data_array);
        
//  Clean up if only 1 record
        if (count($history) == 1) {
            return [];
        }
        
        return $history;
    }
    
    /**
     * Creates callback function to correctly display history data
     * @return array
     */
    public function getHistoryCallbacks()
    {
        return [
            'active' => function($value) {
                return ($value == 1 ? 'Yes' : 'No');
            },
            'ordering' => function($value) {
                return null;
            },
            'deleted' => function($value) {
                return null;
            },
        ];
    }
    
    /**
     * Gets data from history to restore particular record
     * @param integer $restore_id
     * @return boolean|app\models\AbstractModel
     */
    public function getRestoreData($restore_id) 
    {
//  Get history record
        $history_record = RecordsHistory::findOne([
            'table' => $this->tableName(),
            'record_id' => $this->id,
            'id' => $restore_id
        ]);
        
//  Check history record
        if (empty($history_record) || empty($history_record->data)) {
            return false;
        }
        
        $this->restore_data = $history_record;
        
//  Unserialize data
        $data = unserialize($history_record->data);
        
//  Fill current object
        $callbacks = $this->getRestoreCallbacks();
        foreach ($data as $field_name => $field_value) {
            if (isset($callbacks[$field_name])) {
                $value = $callbacks[$field_name]($field_value);
            } else {
                $value = $field_value;
            }
            if ($value !== null) {
                if ($field_name !== 'l') {
                    $this->$field_name = $value;
                } else {
//  Set local data
                    $this->local_restore = $value;
                }
            }
        }
        
        return $this;
    }
    
    /**
     * Creates callback function to correctly restore record from history data
     * @return array
     */
    public function getRestoreCallbacks()
    {
        return [
            'id' => function($value) {
                return null;
            },
            'ordering' => function($value) {
                return null;
            },
            'deleted' => function($value) {
                return null;
            },
            'add_timestamp' => function($value) {
                return null;
            },
        ];
    }
    
    /**
     * Creates callback function to search request params
     * @return array
     */
    public static function getSearchCallbacks()
    {
        return [
            'date_range_from' => function($filter) {
                $where = [
                    'conditions' => "create_time >= :date_from",
                    'binds' => [':date_from' => $filter . ' 00:00:00']
                ];
                return $where;
            },
            'date_range_to' => function($filter) {
                $where = [
                    'conditions' => "create_time <= :date_to",
                    'binds' => [':date_to' => $filter . ' 23:59:59']
                ];
                return $where;
            },
        ];
    }
    
    public function getFilterCallbacks()
    {
        return [];
    }
    
    public static function makeDateTimeString(array $date_time)
    {
        if (empty($date_time['date']) || empty($date_time['hour']) || empty($date_time['minute'])) {
            return '';
        }
        
        $date_time_string = $date_time['date']
            . ' ' .$date_time['hour']
            . ':' .$date_time['minute'];
        
        return $date_time_string;
    }
    
//  Global search =======================================
    /**
     * Making global search by current model by text fields
     * @param string $keyword Keyword
     * @return array
     */
    public function makeGlobalLikeSearch($keyword, $module='backend')
    {
        $records = [];
        
//  Check keyword
        $keyword = trim($keyword);
        if ($keyword === '') {
            return $records;
        }
        
//  Check permissions
        if (empty(static::$global_search_permissions)) {
            return $records;
        }
        
        if ($module == 'backend') {
            if (Users::checkAccessPermissions(static::$global_search_permissions[0], static::$global_search_permissions[1]) === false) {
                return $records;
            }
        }
        
        if (!empty(static::$global_search_fields)) {
            $search_fields = static::$global_search_fields; 
            
//  Check date/time format
            $is_date_time_format = preg_match('/^[\d-: ]{4,19}$/', $keyword);
            if ($is_date_time_format == false) {
                foreach ($search_fields as $search_fields_k => $search_fields_v) {
                    if (in_array($this->getTableSchema()->getColumn($search_fields_v)->type, ['timestamp', 'date_time', 'datetime', 'date'])) {
                        unset($search_fields[$search_fields_k]);
                    }
                }
            }
            
            if (empty($search_fields)) {
                return $records;
            }
            
//  Set conditions
            $conditions_deleted = ($this->hasAttribute('deleted') ? ['deleted' => 0] : []);
            $conditions = "(" . static::$db_field_name_escape . implode(static::$db_field_name_escape . " " . static::$db_like . " :keyword OR " . static::$db_field_name_escape, $search_fields) . static::$db_field_name_escape . " " . static::$db_like . " :keyword)";
            $binds = [':keyword' => '%' . $keyword . '%'];
            
//  Perform search
            $like_records = static::find()
                ->where($conditions_deleted)
                ->andWhere($conditions, $binds)
                ->all();
            
            $module_url = ($module == 'frontend' ? '' : $module . '/');
            
            if (!empty($like_records)) {
                foreach ($like_records as $like_record) {
                    $object_name = $like_record->getObjectName();
                    $records[Url::to('/' . $module_url . static::$global_search_permissions[0] . '/' . static::$global_search_permissions[1] . '/' . $like_record->id)] = $object_name;
                }
            }
        }
        
        return $records;
    }
    
    /**
     * Making global search by current model by callback functions
     * @param string $keyword Keyword
     * @return array
     */
    public function makeGlobalCallbackSearch($keyword, $module='backend')
    {
        $records = [];
        
//  Check permissions
        if (empty(static::$global_search_permissions)) {
            return $records;
        }
        
        if ($module == 'backend') {
            if (Users::checkAccessPermissions(static::$global_search_permissions[0], static::$global_search_permissions[1]) === false) {
                return $records;
            }
        }
        
        $module_url = ($module == 'frontend' ? '' : $module . '/');
        
//  Callback search
        $callback_records = $this->globalSearchCallback($keyword);
        if (!empty($callback_records)) {
            foreach ($callback_records as $object_id => $object_name) {
                $records[Url::to('/' . $module_url . static::$global_search_permissions[0] . '/' . static::$global_search_permissions[1] . '/' . $object_id)] = $object_name;
            }
        }
        
        return $records;
    }
    
    public function globalSearchCallback($keyword)
    {
        return [];
    }
    
    public function getObjectName()
    {
        $object_name = $this->object_name;
        if (!empty($object_name)) {
            $object_property = [];
            preg_match('/\{(.*)\}/', $object_name, $object_property);
            if (!empty($object_property)) {
                $object_name = str_replace($object_property[0], $this->{$object_property[1]}, $object_name);
            }
        }
        
        return $object_name;
    }
    
    public function getObjectNameFrom(array $data)
    {
        $object_name = $this->object_name;
        if (!empty($object_name)) {
            $object_property = [];
            preg_match('/\{(.*)\}/', $object_name, $object_property);
            if (!empty($object_property)) {
                $object_name = str_replace($object_property[0], $data[$object_property[1]], $object_name);
            }
        }
        
        return $object_name;
    }
    
    public static function getIdFromAutocomplete($string)
    {
        if (empty($string)) {
            return null;
        }
        
        $id = reset(explode(' ', $string));
        $id = str_replace('#', '', $id);
        
        if (!is_numeric($id)) {
            return false;
        }
        
        return $id;
    }
    
    /**
     * Preserves text edited by WYSIWYG editor from damage PHP tags
     * @param string $text
     * @return string processed text
     */
    public static function preservePhpTags($text)
    {
        if (empty($text)) {
            return $text;
        }
        
        $replacements = [
            '<!--?=' => '<?=',
            '?-->' => '?>',
            '<!--?php' => '<?php',
            '{ ?-->' => '{ ?>',
            'Yii::$app--->' => 'Yii::$app->',
            '$this--->' => '$this->',
            '=&gt;' => '=>',
            '?&gt;' => '?>',
        ];
        
        $text = str_replace(array_keys($replacements), array_values($replacements), $text);
        
        return $text;
    }
    
    /**
     * Returns period duration as weeks, days, hours, minutes, seconds
     * @param integer $start_timestamp
     * @param integer $end_timestamp
     * @param boolean $as_string if true - returns string like "X w, X d, X h, X m, X s" else returns array with keys: w, d, h, m, s
     * @return mixed string|array
     */
    public static function getPeriodDuration($start_timestamp, $end_timestamp, $as_string=true)
    {
        $output_array = [
            'w' => 0,
            'd' => 0,
            'h' => 0,
            'm' => 0,
            's' => 0,
        ];
        
        if (empty($start_timestamp) || empty($end_timestamp)) {
            return ($as_string === true ? '-' : $output_array);
        }
        
        $period = $end_timestamp - $start_timestamp;
        
        if ($period <= 0) {
            return ($as_string === true ? '-' : $output_array);
        }
        
        foreach (array_keys($output_array) as $k) {
            switch ($k) {
                case 'w':
                    $seconds = 60 * 60 * 24 * 7;
                    break;
                
                case 'd':
                    $seconds = 60 * 60 * 24;
                    break;
                
                case 'h':
                    $seconds = 60 * 60;
                    break;
                
                case 'm':
                    $seconds = 60;
                    break;
                
                case 's':
                    $seconds = 1;
                    break;
            }
            
            $result = intval($period / $seconds);
            $rest = $period % $seconds;
            
            $output_array[$k] = $result;
            
            if ($rest > 0) {
                $period = $rest;
            }
        }
        
        if ($as_string === false) {
            return $output_array;
        }
        
        $output_string = '';
        
        foreach ($output_array as $name => $value) {
            if ($value == 0) {
                continue;
            }
            
            $output_string .= $value . ' ' . $name . ', ';
        }
        
        $output_string = substr($output_string, 0, -2);
        
        return $output_string;
    }
    
    public static function getUniqueColValues($col)
    {
        $list = self::find()
            ->select([
                "DISTINCT(" . $col . ") AS col",
            ])
            ->orderBy("col ASC")
            ->asArray()
            ->column();
        
        return $list;
    }
    
#   API cache methods ==========================================================
    /**
     * Write data to API cache file
     * @param mixed $data
     * @return boolean
     * @throws \Exception
     */
    public static function writeApiCacheFile($data)
    {
        if (empty(static::$api_cache_file_name)) {
            throw new \Exception('Cache file name is not set');
        }
        
        $content = json_encode($data);
        $file_path = Yii::$app->basePath . static::$api_cache_file_name;
        
        try {
            $file = fopen($file_path, 'w');
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
        
        if (empty($file)) {
            throw new \Exception('Unable to open cache file');
        }
        
        try {
            $result = fwrite($file, $content);
            fclose($file);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        if (empty($result)) {
            throw new \Exception('Unable to write into cache file');
        }
        
        return $result;
    }
    
    /**
     * Read data from API cache file
     * @return string cache data
     * @throws \Exception
     */
    public static function readApiCacheFile()
    {
        if (empty(static::$api_cache_file_name)) {
            throw new \Exception('Cache file name is not set');
        }
        
        $file_path = Yii::$app->basePath . static::$api_cache_file_name;
        if (!file_exists($file_path)) {
            throw new \Exception('Cache file not found: ' . $file_path);
        }
        
        $content = file_get_contents($file_path);
        if (empty($content)) {
            throw new \Exception('Unable to read cache file: ' . $file_path);
        }
        
        return $content;
    }
    
    /**
     * Get data for API from cache or database
     * @return mixed boolean|string
     */
    public static function getApiData()
    {
        if (empty(static::$api_cache_file_name)) {
            return false;
        }
        
        try {
//  Try to get data from cache
            $data = static::readApiCacheFile();
        } catch (\Exception $ex) {
//  Get data from 
            $data = static::getDataForApiCache();
            
            try {
                static::writeApiCacheFile($data);
            } catch (\Exception $ex) {
                
            }
            
            if (empty($data)) {
                $data = '';
            } else {
                $data = json_encode($data);
            }
        }
        
        return $data;
    }
    
    /**
     * Set API cache from backend
     * @return boolean
     * @throws \Exception
     */
    public static function setApiCache()
    {
        try {
            $data = static::getDataForApiCache();
            $result = static::writeApiCacheFile($data);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
        
        return $result;
    }
#   END API cache methods ======================================================
    
    /**
     * Notify other platform frontend about data changes
     */
    public function notifyFrontend()
    {
        if (empty(Yii::$app->params['urls']['other_platform_frontend']) || empty($this->section_name)) {
            return;
        }
        
        $http_client = new HttpClient();
        
        $url = Yii::$app->params['urls']['other_platform_frontend'] . 'api/reload/' . $this->section_name;
        
        try {
            $response = $http_client->jsonRequest(
                $url, 
                HttpClient::METHOD_GET, 
                [], 
                HttpClient::FORMAT_RAW_URLENCODED
            );
        } catch (\Exception $ex) {
            return false;
        }
        
        if ($response['statusCode'] != 200 || $response['data'] != 'ok') {
            return false;
        }
        
        return true;
    }
}