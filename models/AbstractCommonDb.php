<?php

namespace app\models;

use Yii;

/**
 * AbstractDb2
 *
 */
class AbstractCommonDb extends AbstractPostgreSQL
{
    protected static $db;
    
    public static function getDb() 
    {
        if (!empty(static::$db)) {
            return static::$db;
        }
        
        return Yii::$app->db_common;
    }
    
    public static function setDb($db) 
    {
        static::$db = $db;
    }
    
    public static function getSearchCallbacks()
    {
        $callbacks = parent::getSearchCallbacks();
        $callbacks['date_range_from'] = function($filter) {
            $where = [
                'conditions' => "add_timestamp >= :date_from",
                'binds' => [':date_from' => strtotime($filter . ' 00:00:00')]
            ];
            return $where;
        };
        $callbacks['date_range_to'] = function($filter) {
            $where = [
                'conditions' => "add_timestamp <= :date_to",
                'binds' => [':date_to' => strtotime($filter . ' 23:59:59')]
            ];
            return $where;
        };
        
        return $callbacks;
    }
}