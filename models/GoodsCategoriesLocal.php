<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goods_categories_l".
 *
 * @property integer $main_id
 * @property string $lang
 * @property string $name
 */
class GoodsCategoriesLocal extends \app\models\AbstractCommonDb
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_categories_l';
    }
}