<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "password_restores".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $code
 * @property string $create_time
 * @property string $valid_to
 * @property integer $used
 */
class PasswordRestores extends \app\models\AbstractModel
{
    protected static $code_life_time = 3600;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'password_restores';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'code' => 'Code',
            'create_time' => 'Create Time',
            'valid_to' => 'Valid To',
            'used' => 'Used',
        ];
    }
    
    public static function createRestoreCode(\app\interfaces\Authorizable $user_data)
    {
        $data = new self;
        $data->user_id = $user_data->id;
        $data->code = self::generateCode($user_data);
        $data->create_time = date('Y-m-d H:i:s', time());
        $data->valid_to = date('Y-m-d H:i:s', time() + self::$code_life_time);
        $result = $data->save(false);
        
        if (empty($result)) {
            return false;
        }
        
        return $data->code;
    }
    
    protected static function generateCode(\app\interfaces\Authorizable $user_data)
    {
        $code = md5(
            'marat'
            . $user_data->email
            . time()
            . $user_data->id
            . 'gleb'
        );
        
        return $code;
    }
}