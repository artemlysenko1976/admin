<?php

namespace app\models;

use yii;

/**
 * Abstract PostgreSQL Localized Model
 */
class AbstractPostgreSQLLocal extends \app\models\AbstractLocal
{
    public static $db_field_name_escape = '"';
    
    public static $db_like = 'ILIKE';
    
//    public static function getCurrentLanngCode()
//    {
//        if (empty(Yii::$app->params['default_system_lang'])) {
//            throw new \Exception('No current lang code set');
//        }
//        
//        return Yii::$app->params['default_system_lang']->abbreviation;
//    }
}