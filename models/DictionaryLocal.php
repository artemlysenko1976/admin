<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "dictionary_l".
 *
 * @property integer $main_id
 * @property string $lang
 * @property string $entry
 */
class DictionaryLocal extends \app\models\AbstractModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dictionary_l';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $data = parent::attributeLabels();
        $data['entry'] = 'Entry';
        
        return $data;
    }
}
