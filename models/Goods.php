<?php

namespace app\models;

use Yii;
use app\models\GoodsLocal;
use app\models\GoodsCategories;
use app\models\GoodsCategoriesLocal;
use app\models\GoodsParameters;
use app\models\GoodsParametersLocal;
use app\models\GoodsParametersMap;
use app\models\GoodsDescriptions;
use app\models\Clients;
use app\models\Brands;
use app\library\Utils;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\web\UploadedFile;

/**
 * This is the model class for table "goods".
 *
 * @property integer $id
 * @property float $price
 * @property float $old_price
 * @property float $buy_price
 * @property float $extra_charge_percent
 * @property float $discount_percent
 * @property float $inner_price
 * @property integer $brand_id
 * @property integer $category_id
 * @property integer $quantity
 * @property string $photo
 * @property string $video
 * @property integer $client_id
 * @property integer $active
 * @property integer $deleted
 */
class Goods extends \app\models\AbstractCommonDbLocal
{
    public $object_name = 'Product {id}';
    
    public static $photo_path = '/web/images/goods/';
    public static $photo_size = [
        1500,
        1500,
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods';
    }
    
    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Цена продажи',
            'old_price' => 'Старая цена',
            'buy_price' => 'Цена закупки',
            'extra_charge_percent' => 'Наценка',
            'discount_percent' => 'Скидка',
            'inner_price' => 'Цена TG',
            'brand_id' => 'Бренд',
            'category_id' => 'Категория',
            'quantity' => 'Количество',
            'photo' => 'Фото',
            'video' => Yii::t('app', 'Видео'),
            'client_id' => Yii::t('app', 'Поставщик'),
            'name' => Yii::t('app', 'Название'),
            'description' => Yii::t('app', 'Описание'),
            'active' => Yii::t('app', 'Активность'),
            'parameters' => Yii::t('app', 'Параметры'),
        ];
    }
    
    public function getClient()
    {
        if (empty($this->client_id)) {
            return null;
        }
        
        return $this->hasOne(
            Clients::className(), 
            ['id' => 'client_id']
        );
    }
    
    public function getCategory()
    {
        return $this->hasOne(
            GoodsCategories::className(), 
            ['id' => 'category_id']
        );
    }
    
    public function getBrand()
    {
        return $this->hasOne(
            Brands::className(), 
            ['id' => 'brand_id']
        );
    }
    
    public function getParameters()
    {
        if (empty($this->id)) {
            return;
        }
        
        $params = GoodsParameters::find()
            ->select([
                "gp.*",
                "gd.value",
            ])
            ->from(GoodsParameters::tableName() . " AS gp")
            ->join(
                "JOIN",
                GoodsDescriptions::tableName() . " AS gd",
                "gd.parameter_id = gp.id"
            )
            ->where([
                "gd.product_id" => $this->id,
            ])
            ->orderBy("gd.id ASC")
            ->asArray()
            ->all();
        
        return $params;
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    GoodsLocal::tableName() . '.name',
                    GoodsLocal::tableName() . '.description',
                ];
                $where = [
                    'conditions' => "(" . implode(" ILIKE :keyword OR ", $fields) . " ILIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            },
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        foreach ($data['l'] as $lang => $field) {
            $entry = trim($field['name']);
            if (empty($entry)) {
                $errors['name'] = Yii::t('app', "Заполните название");
                break;
            }
        }
        
        if (empty($data['category_id'])) {
            $errors['category_id'] = Yii::t('app', "Выберите категорию");
        }
        
        if (!preg_match('/^([0-9]{1,10})$/', $data['quantity'])) {
            $errors['quantity'] = Yii::t('app', "Количество должно быть 0 или целым положительным числом");
        }
        
        if (!preg_match('/^([0-9]{1,10}\.[0-9]{2})$/', $data['price'])) {
            $errors['price'] = Yii::t('app', "Цена должна быть 0 или положительным числом (десятичным)");
        }
        
        if (!preg_match('/^([0-9]{1,10}\.[0-9]{2})$/', $data['buy_price'])) {
            $errors['buy_price'] = Yii::t('app', "Цена должна быть 0 или положительным числом (десятичным)");
        }
        
        if (!preg_match('/^([0-9]{1,10}\.[0-9]{2})$/', $data['inner_price'])) {
            $errors['inner_price'] = Yii::t('app', "Цена должна быть 0 или положительным числом (десятичным)");
        }
        
        if ($data['old_price'] != '') {
            if (!preg_match('/^([0-9]{1,10}\.[0-9]{2})$/', $data['old_price'])) {
                $errors['old_price'] = Yii::t('app', "Цена должна быть 0 или положительным числом (десятичным)");
            }
        }
        
        if (empty($data['extra_charge_percent'])
            || !is_numeric($data['extra_charge_percent'])
            || $data['extra_charge_percent'] < 0
        ) {
            $errors['extra_charge_percent'] = Yii::t('app', "Неверное значение");
        }
        
        if (empty($data['discount_percent'])
            || !is_numeric($data['discount_percent'])
            || $data['discount_percent'] < 0
        ) {
            $errors['discount_percent'] = Yii::t('app', "Неверное значение");
        }
        
        if (empty(trim($data['brand_id']))) {
            $errors['brand_id'] = Yii::t('app', "Выберите Бренд");
        }
        
        if (!empty(trim($data['video']))) {
            if (!preg_match('/^([0-9a-zA-Z]{10,12})$/', $data['video'])) {
                $errors['video'] = Yii::t('app', "Неверный формат кода видеоролика");
            }
        }
        
        if ($data['client_id'] != '') {
            $data['client_id'] = Utils::getIdFromAutocomplete($data['client_id']);
            if (!empty($data['client_id'])) {
                $client = Clients::find()
                    ->where([
                        'id' => $data['client_id'],
                        'deleted' => 0,
                    ])
                    ->limit(1)
                    ->one();
                if (empty($client)) {
                    $errors['client_id'] = Yii::t('app', "Указанный поставщик не найден");
                }
            } else {
                $errors['client_id'] = Yii::t('app', "Выберите поставщика из выпадающего списка");
            }
        }
        
        return $errors;
    }
    
    public function addRecord(array $data)
    {
        if (!empty($data['client_id'])) {
            $data['client_id'] = Utils::getIdFromAutocomplete($data['client_id']);
        } else {
            $data['client_id'] = 0;
        }
        
        if (!empty($data['parameters'])) {
            $params = $data['parameters'];
            unset($data['parameters']);
        }
        
        $id = parent::addRecord($data, false);
        
        if (!empty($id)) {
            $this->createDir($id);
            
//  Photo
            $uploaded_file = UploadedFile::getInstanceByName('photo');
            if (!empty($uploaded_file)) {
                $file_name = 'photo.' . $uploaded_file->extension;
                $images_result = Image::getImagine()
                    ->open($uploaded_file->tempName)
                    ->thumbnail(new Box(self::$photo_size[0], self::$photo_size[1]))
                    ->save(Yii::$app->basePath . self::$photo_path . $id . '/' . $file_name, ['quality' => 100]);
                if (!empty($images_result)) {
                    $data['photo'] = $file_name;
                    $this->photo = $file_name;
                    $this->save(false);
                }
            }
            
//  Parameters
            if (!empty($params)) {
                $this->saveParams($id, $params);
                $data['parameters'] = $params;
            }
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_ADD, $id, "Product " . $id);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $id, $data);
        }
        
        return $id;
    }
    
    public function editRecord(array $data)
    {
        $old_data = self::findOne($data['id']);
        if (empty($old_data)) {
            return false;
        }
        
        if (!empty($data['client_id'])) {
            $data['client_id'] = Utils::getIdFromAutocomplete($data['client_id']);
        } else {
            $data['client_id'] = 0;
        }
        
        if (!empty($data['parameters'])) {
            $params = $data['parameters'];
            unset($data['parameters']);
        }
        
//  Delete photo
        if (!empty($data['delete_photo'])) {
            unlink(Yii::$app->basePath . self::$photo_path . $old_data->id . '/' . $old_data->photo);
            $data['photo'] = null;
            unset($data['delete_photo']);
        }
        
//  Photo
        $uploaded_file = UploadedFile::getInstanceByName('photo');
        if (!empty($uploaded_file)) {
            if (!empty($old_data->photo)) {
                unlink(Yii::$app->basePath . self::$photo_path . $old_data->id . '/' . $old_data->photo);
            }
            
            $file_name = 'photo.' . $uploaded_file->extension;
            $images_result = Image::getImagine()
                ->open($uploaded_file->tempName)
                ->thumbnail(new Box(self::$photo_size[0], self::$photo_size[1]))
                ->save(Yii::$app->basePath . self::$photo_path . $old_data->id . '/' . $file_name, ['quality' => 100]);
            if (!empty($images_result)) {
                $data['photo'] = $file_name;
            }
        }
        
        $result = parent::editRecord($data, false);
        
        if (!empty($result)) {
            $this->saveParams($old_data->id, $params);
            $data['parameters'] = $params;
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_EDIT, $data['id'], "Product " . $old_data->id);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $old_data->id, $data);
        }
        
        return $result;
    }
    
    protected function saveParams(int $id, array $params=null)
    {
        GoodsDescriptions::deleteAll([
            'product_id' => $id,
        ]);
        
        if (!empty($params)) {
            foreach ($params as $param_id => $param_value) {
                if ($param_value === '') {
                    continue;
                }
                
                $desc_record = new GoodsDescriptions([
                    'product_id' => $id,
                    'parameter_id' => $param_id,
                    'value' => $param_value,
                ]);
                $desc_record->save();
            }
        }
    }
    
    /**
     * Search for autocomplete
     * @param string $keyword
     * @return array
     */
    public static function autocompleteSearch($keyword)
    {
        if (empty($keyword)) {
            return [];
        }
        
        $list = [];
        $conditions = [];
        
//  Numeric keyword
        if (is_numeric($keyword)) {
            $conditions[] = self::tableName() . ".id = :keyword";
            $binds = [':keyword' => intval($keyword)];
        }
//  Other
        else {
            $fields = [
                'cl.name',
            ];
            
            $conditions[] = "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)";
            $binds = [':keyword' => '%' . $keyword . '%'];
        }
        
//  Make query
        $query = self::find()
            ->select([
                "c.id",
                "cl.name",
            ])
            ->from([
                self::tableName() . " AS c",
            ])
            ->join(
                "JOIN",
                GoodsCategoriesLocal::tableName() . " AS cl",
                "cl.main_id = c.id AND cl.lang = '" . Yii::$app->language . "'" 
            )
            ->where([
                'c.active' => 1,
                'c.deleted' => 0,
            ])
            ->andWhere("(" . implode(" OR ", $conditions) . ")", $binds)
            ->orderBy("cl.name ASC")
            ->asArray();
        
        $result = $query->all();
        
//  Build output
        if (!empty($result)) {
            foreach ($result as $result_row) {
                $list[] = $result_row['id'] . ' | ' . $result_row['name'];
            }
        }
        
        return $list;
    }
    
    public function getNestedList(array $condition=null)
    {
        $tree_list = $this->getTree($condition, ["ordering ASC"]);
        if (empty($tree_list)) {
            return [];
        }
        
        $output = $this->getNestedListRecursive($tree_list);
        
        unset($tree_list);
        
        return $output;
    }
    
    protected function getNestedListRecursive(array $tree_list, array $output=[], int $level=0)
    {
        $prefix = '';
        if ($level > 0) {
            for ($i = 0; $i < $level; $i++) {
                $prefix .= '&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $prefix .= '- ';
        }
        
        foreach ($tree_list as $record) {
            $output[$record->id] = $prefix . $record->local->name;
            
            if (!empty($record->children)) {
                $output = $this->getNestedListRecursive($record->children, $output, $level + 1);
            }
        }
        
        return $output;
    }
    
    public static function loadParams(int $category_id)
    {
        $params = [];
        
        while (!empty($category_id)) {
            $list = GoodsParameters::find()
                ->select([
                    "gp.id",
                    "gc.parent_id",
                ])
                ->from(GoodsParameters::tableName() . " AS gp")
                ->join(
                     "JOIN",
                    GoodsParametersMap::tableName() . " AS gpm",
                    "gpm.parameter_id = gp.id"
                )
                ->join(
                     "JOIN",
                    self::tableName() . " AS gc",
                    "gc.id = gpm.category_id"
                )
                ->where([
                    "gpm.category_id" => $category_id,
                    "gp.active" => 1,
                    "gp.deleted" => 0,
                ])
                ->asArray()
                ->all();
            
            if (empty($list)) {
                return $params;
            }
            
            $category_id = $list[0]['parent_id'];
            
            foreach ($list as $row) {
                $params[$row['id']] = $row['rellocal']['name'];
            }
        }
        
        return $params;
    }
    
    protected function createDir(int $id)
    {
        $dir_name = Yii::$app->basePath . self::$photo_path . $id . '/';
        if (!is_dir($dir_name)) {
            mkdir($dir_name);
            chmod($dir_name, 0777);
        }
    }
}