<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_groups".
 *
 * @property integer $id
 * @property string $name
 * @property string $key
 * @property string $permissions
 * @property integer $basic
 * @property integer $deleted
 */
class UsersGroups extends \app\models\AbstractModel
{
    public $object_name = 'Group {name}';
    
    public static $global_search_fields = [
        'name',
    ];
    public static $global_search_permissions = [
        'groups',
        'edit',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_groups';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'permissions' => 'Permissions',
            'basic' => 'Basic',
            'deleted' => 'Deleted',
        ];
    }
    
    public function getHistoryCallbacks()
    {
        $callbacks = parent::getHistoryCallbacks();
        $callbacks['permissions'] = function($value) {
            $output = '';
            if (!empty($value)) {
                $value = json_decode($value, true);
                if (is_array($value)) {
                    foreach ($value as $controller_name => $actions) {
                        $output .= strtoupper($controller_name) . "\n";
                        foreach ($actions as $action_name) {
                            $output .= ' - ' . $action_name . "\n";
                        }
                    }
                }
            }
            return $output;
        };
        
        return $callbacks;
    }
    
    public function getPermissions() 
    {
        if ($this->basic == 1) {
            return Users::getAllPermissions();
        } else {
            if (!empty($this->permissions)) {
                return json_decode($this->permissions);
            }
        }
        
        return [];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        $data['name'] = trim($data['name']);
        if (empty($data['name'])) {
            $errors['name'] = Yii::t('app', "Name is required");
        } else {
            $check_query = self::find()
                ->select(['id'])
                ->where([
                    'deleted' => 0,
                    "name" => $data['name'],
                ])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere("id != " . $data['id']);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['name'] = Yii::t('app', "This name already exists");
            }
        }
        
        return $errors;
    }
    
    public function addRecord(array $data, $log=false)
    {
        if (!empty($data['permissions'])) {
            $data['permissions'] = json_encode($data['permissions']);
        }
        
        $id = parent::addRecord($data);
        
        return $id;
    }
    
    public function editRecord(array $data, $log=false)
    {
        if (!empty($data['permissions'])) {
            $data['permissions'] = json_encode($data['permissions']);
        }
        
        $result = parent::editRecord($data);
        
        return $result;
    }
    
    public function deleteRecord($id, $soft=true, $log=true)
    {
//  Delete record
        $result = parent::deleteRecord($id, $soft, $log);
        
        if (!empty($result)) {
//  Delete depended map records
            $depended_users = UsersGroupsMap::findAll(['group_id' => $id]);
            if (!empty($depended_users)) {
                foreach ($depended_users as $item) {
                    $item->delete();
                }
            }
        }
        
        return $result;
    }
    
    public static function getAvailableSections()
    {
        $sections = include Yii::$app->basePath . '/backend/config/menu.php';
        foreach ($sections as $sections_k => $sections_v) {
            if ($sections_v['url'] == 'groups') {
                unset($sections[$sections_k]);
                break;
            }
        }
        
        return $sections;
    }
}