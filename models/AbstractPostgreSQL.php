<?php

namespace app\models;

use Yii;

/**
 * Class to provide PostgreSQL connection
 *
 */
class AbstractPostgreSQL extends AbstractModel
{
    public static $db_field_name_escape = '"';
    
    public static $db_like = 'ILIKE';
}