<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "languages".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $code_full
 * @property string $basic
 * @property integer $active
 * @property integer $deleted
 */
class Languages extends \app\models\AbstractModel
{
    public $object_name = 'Language {name}';
    
    public static $global_search_fields = [
        'name',
        'code',
        'code_full',
    ];
    public static $global_search_permissions = [
        'local',
        'langsedit',
    ];
    
    public $section_name = 'languages';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'ISO Code',
            'code_full' => 'Full ISO Code',
            'basic' => 'Default',
            'active' => 'Active',
            'deleted' => 'Deleted',
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        $data['code'] = trim($data['code']);
        if (empty($data['code'])) {
            $errors['code'] = Yii::t('app', "ISO Code is required");
        } else {
            $check_query = self::find()
                ->select(['id'])
                ->where([
                    'deleted' => 0,
                    "code" => $data['code']
                ])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere("id != " . $data['id']);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['code'] = Yii::t('app', "This ISO Code already in use");
            }
            
            if (strlen($data['code']) != 2) {
                $errors['code'] = Yii::t('app', "ISO Code shiuld consists of 2 chars");
            }
        }
        
        $data['code_full'] = trim($data['code_full']);
        if (empty($data['code_full'])) {
            $errors['code_full'] = Yii::t('app', "ISO Code 2 is required");
        } else {
            $condition = ["code_full" => $data['code_full']];
            $check_query = $rows = (new Query())
                ->select(['id'])
                ->from(self::tableName())
                ->where($condition)
                ->andWhere(['deleted' => 0])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere(['and', "id != " . $data['id']]);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['code_full'] = Yii::t('app', "This ISO Code 2 already in use");
            }
            
            if (strlen($data['code_full']) != 5) {
                $errors['code_full'] = Yii::t('app', "ISO Code 2 shiuld consists of 5 chars");
            } elseif (!preg_match('/^[a-z]{2}\-[A-Z]{2}$/', $data['code_full'])) {
                $errors['code_full'] = Yii::t('app', "ISO Code 2 has wrong format");
            }
        }
        
        $data['name'] = trim($data['name']);
        if (empty($data['name'])) {
            $errors['name'] = Yii::t('app', "Name is required");
        } else {
            $condition = ["name" => $data['name']];
            $check_query = (new Query())
                ->select(['id'])
                ->from(self::tableName())
                ->where($condition)
                ->andWhere(['deleted' => 0])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere(['and', "id != " . $data['id']]);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['name'] = Yii::t('app', "This Name already in use");
            }
        }
        
        return $errors;
    }
    
    public function addRecord($data, $log=true)
    {
        $data['code'] = strtolower($data['code']);
        
        $id = parent::addRecord($data, $log);
        if (!empty($id)) {
//  Make dictionary dir
            mkdir(Yii::$app->basePath . '/messages/' . $data['code']);
            chmod(Yii::$app->basePath . '/messages/' . $data['code'], 0777);
            
//  Make e-mail templates dir
            mkdir(Yii::$app->basePath . '/mail/generated/' . $data['code']);
            chmod(Yii::$app->basePath . '/mail/generated/' . $data['code'], 0777);
            
            $this->notifyFrontend();
        }
        
        return $id;
    }
    
    public function editRecord($data)
    {
        $data['code'] = strtolower($data['code']);
        
        $result = parent::editRecord($data);
        
        $this->notifyFrontend();
        
        return $result;
    }
    
    public static function getDefaultLang()
    {
        if (!empty(Yii::$app->params['langs'])) {
            foreach (Yii::$app->params['langs'] as $lang) {
                if ($lang->basic == 1) {
                    return $lang;
                }
            }
        }
        
        $lang = self::findOne(['basic' => 1, 'deleted' => 0]);
        
        return $lang;
    }
    
    public static function getCurrentLang()
    {
        $url_lang = Yii::$app->language;
        $lang = self::find()
            ->where([
                'active' => 1,
                'deleted' => 0,
            ])
            ->andWhere("(code = '" . $url_lang . "') OR code_full = '" . $url_lang . "'")
            ->one();
        
        return $lang;
    }
    
    public static function getAdminLang()
    {
        if (!empty(Yii::$app->params['adminLang'])) {
            $lang = self::find()
                ->where([
                    'code' => Yii::$app->params['adminLang'],
                    'active' => 1,
                    'deleted' => 0,
                ])
                ->one();
        }
        
        if (empty($lang)) {
            $lang = self::getDefaultLang();
        }
        
        return $lang;
    }
    
    public static function getLangFromUrl(string $url, array $langs)
    {
        $url = reset(explode('?', $url));
        
        if (substr($url, 0, 1) == '/') {
            $url = substr($url, 1);
        }
        
        $url = explode('/', $url);
        
        $output = '';
        
        if (!empty($url[0])) {
            foreach ($langs as $lang) {
                if ($url[0] == $lang->code || $url[0] == strtolower($lang->code_full)) {
                    $output = $url[0];
                }
            }
        }
        
        return $output;
    }
    
    public static function getHreflangArray($current_uri)
    {
        $host = Yii::$app->getUrlManager()->getHostInfo();
        $list = [];
        $prepared_url = (!empty($current_uri) ? '/' . $current_uri : '');
        
        foreach (Yii::$app->params['langs'] as $lang) {
            $hreflang_url = $host;
            if ($lang->code != Yii::$app->params['default_lang']->code) {
                $hreflang_url .= '/' . $lang->code;
            }
            
            $hreflang_url .= $prepared_url;
            
            if ($lang->code == Yii::$app->params['default_lang']->code) {
                $default_lang_url = $hreflang_url;
            }
            
            if ($lang->code != Yii::$app->params['lang']->code) {
                $list[] = [
                    $hreflang_url, 
                    $lang->code
                ];
            }
        }
        
        $list[] = [
            $default_lang_url, 
            'x-default'
        ];
        
        return $list;
    }
}