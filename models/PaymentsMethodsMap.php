<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payments_methods_map".
 *
 * @property integer $id
 * @property integer $method_id
 * @property integer $currency_id
 * @property string $value
 */
class PaymentsMethodsMap extends \app\models\AbstractCommonDb
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_methods_map';
    }
}