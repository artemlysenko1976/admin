<?php

namespace app\models;

use Yii;
use app\models\PaymentsMethodsLocal;
use app\models\PaymentsMethodsMap;

/**
 * This is the model class for table "currencies".
 *
 * @property integer $id
 * @property string $code
 * @property float $rate
 * @property integer $fiat
 * @property integer $basic
 * @property integer $active
 * @property integer $deleted
 */
class Currencies extends \app\models\AbstractCommonDbLocal
{
    public $object_name = 'Currency {id}';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currencies';
    }
    
    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'rate' => 'Курс',
            'fiat' => 'Фиатная',
            'basic' => 'Основная',
            'name' => Yii::t('app', 'Название'),
            'active' => Yii::t('app', 'Активность'),
            'methods' => Yii::t('app', 'Методы оплаты'),
        ];
    }
    
    public function getMethods()
    {
        if (empty($this->id)) {
            return;
        }
        
        $list = PaymentsMethods::find()
            ->from(PaymentsMethods::tableName() . " AS pm")
            ->join(
                "JOIN",
                PaymentsMethodsMap::tableName() . " AS pmm",
                "pmm.method_id = pm.id"
            )
            ->where([
                "pmm.currency_id" => $this->id,
            ])
            ->all();
        
        return $list;
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    PaymentsMethodsLocal::tableName() . '.name',
                    self::tableName() . '.code',
                ];
                $where = [
                    'conditions' => "(" . implode(" ILIKE :keyword OR ", $fields) . " ILIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            },
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        $editable = true;
        
        if (!empty($data['id'])) {
            $old_data = self::findOne($data['id']);
            if (!empty($old_data) && $old_data->basic == 1) {
                $editable = false;
            }
        }
        
        foreach ($data['l'] as $lang => $field) {
            $entry = trim($field['name']);
            if (empty($entry)) {
                $errors['name'] = Yii::t('app', "Заполните название");
                break;
            }
        }
        
        if ($editable === true) {
            $data['code'] = trim($data['code']);
            if (!preg_match('/^([A-Z]{3,10})$/', $data['code'])) {
                $errors['code'] = Yii::t('app', "Неверный формат кода");
            } else {
                $check_query = self::find()
                    ->select(['id'])
                    ->where([
                        'deleted' => 0,
                        'code' => $data['key'],
                    ])
                    ->limit(1);
                if (!empty($data['id'])) {
                    $check_query->andWhere("id != " . $data['id']);
                }
                $check = $check_query->one();
                if (!empty($check)) {
                    $errors['code'] = Yii::t('app', "Такой код уже используеся");
                }
            }

            if (!preg_match('/^([0-9]{1,10}\.[0-9]{1,6})$/', $data['rate']) || $data['rate'] == 0) {
                $errors['rate'] = Yii::t('app', "Курс должен быть положительным десятичным числом");
            }
        }
        
        return $errors;
    }
    
    public function addRecord(array $data)
    {
        if (!empty($data['methods'])) {
            $methods = $data['methods'];
            unset($data['methods']);
        }
        
        $id = parent::addRecord($data, false);
        
        if (!empty($id)) {
            $this->saveMethods($id, $methods);
            $data['methods'] = $methods;
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_ADD, $id, "Currency " . $id);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $id, $data);
        }
        
        return $id;
    }
    
    public function editRecord(array $data)
    {
        $old_data = self::findOne($data['id']);
        if (empty($old_data)) {
            return false;
        }
        
        if (!empty($data['methods'])) {
            $methods = $data['methods'];
            unset($data['methods']);
        }
        
        $result = parent::editRecord($data, false);
        
        if (!empty($result)) {
            $this->saveMethods($old_data->id, $methods);
            $data['methods'] = $methods;
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_EDIT, $old_data->id, "Currency " . $old_data->id);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $old_data->id, $data);
        }
        
        return $result;
    }
    
    protected function saveMethods(int $currency_id, array $methods=null)
    {
        PaymentsMethodsMap::deleteAll([
            'currency_id' => $currency_id,
        ]);
        
        if (!empty($methods)) {
            foreach ($methods as $method_id) {
                $record = new PaymentsMethodsMap([
                    'currency_id' => $currency_id,
                    'method_id' => $method_id,
                ]);
                $record->save();
            }
        }
    }
}