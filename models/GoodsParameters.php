<?php

namespace app\models;

use Yii;
use app\models\GoodsParametersLocal;
use app\models\GoodsParametersMap;
use app\models\UsersActivity;
use app\models\RecordsHistory;

/**
 * This is the model class for table "goods_parameters".
 *
 * @property integer $id
 * @property integer $active
 * @property integer $deleted
 */
class GoodsParameters extends \app\models\AbstractCommonDbLocal
{
    public $object_name = 'Goods parameter {id}';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_parameters';
    }
    
    public static function attributeStaticLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Название'),
            'categories' => Yii::t('app', 'Категории'),
            'active' => Yii::t('app', 'Активность'),
        ];
    }
    
    public function getCategories()
    {
        if (empty($this->id)) {
            return false;
        }
        
        $categories = GoodsCategories::find()
            ->join(
                "JOIN",
                GoodsParametersMap::tableName() . " AS gpm",
                "gpm.category_id = " . GoodsCategories::tableName() . ".id"
            )
            ->where([
                "gpm.parameter_id" => $this->id,
            ])
            ->all();
        
        return $categories;
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    'gpl.name',
                ];
                $where = [
                    'conditions' => "(" . implode(" ILIKE :keyword OR ", $fields) . " ILIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            },
        ];
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        foreach ($data['l'] as $lang => $field) {
            $entry = trim($field['name']);
            if (empty($entry)) {
                $errors['name'] = Yii::t('app', "Заполните название");
                break;
            }
        }
        
        if (empty($data['categories'])) {
            $errors['categories'] = Yii::t('app', "Выберите одну или несколько категорий");
        }
        
        return $errors;
    }
    
    public function addRecord(array $data)
    {
        if (!empty($data['categories'])) {
            $categories = $data['categories'];
            unset($data['categories']);
        }
        
        $id = parent::addRecord($data, false);
        
        if (!empty($id)) {
            if (!empty($categories)) {
                foreach ($categories as $cat_id) {
                    $map_record = new GoodsParametersMap([
                        'parameter_id' => $id,
                        'category_id' => $cat_id,
                    ]);
                    $map_record->save();
                }
                
                $data['categories'] = $categories;
            }
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_ADD, $id, "Goods parameter " . $id);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $id, $data);
        }
        
        return $id;
    }
    
    public function editRecord(array $data)
    {
        if (!empty($data['categories'])) {
            $categories = $data['categories'];
            unset($data['categories']);
        }
        
        $result = parent::editRecord($data, false);
        
        if (!empty($result)) {
            GoodsParametersMap::deleteAll([
                'parameter_id' => $data['id'],
            ]);
            
            if (!empty($categories)) {
                foreach ($categories as $cat_id) {
                    $map_record = new GoodsParametersMap([
                        'parameter_id' => $data['id'],
                        'category_id' => $cat_id,
                    ]);
                    $map_record->save();
                }
                
                $data['categories'] = $categories;
            }
            
//  Logs
            $activity = new UsersActivity();
            $activity->log(UsersActivity::OPERATION_EDIT, $data['id'], "Goods parameter " . $data['id']);

            $history = new RecordsHistory();
            $history->log($this->tableName(), $data['id'], $data);
        }
        
        return $result;
    }
    
    /**
     * Search for autocomplete
     * @param string $keyword
     * @return array
     */
    public static function autocompleteSearch($keyword)
    {
        if (empty($keyword)) {
            return [];
        }
        
        $list = [];
        $conditions = [];
        
//  Numeric keyword
        if (is_numeric($keyword)) {
            $conditions[] = self::tableName() . ".id = :keyword";
            $binds = [':keyword' => intval($keyword)];
        }
//  Other
        else {
            $fields = [
                'cl.name',
            ];
            
            $conditions[] = "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)";
            $binds = [':keyword' => '%' . $keyword . '%'];
        }
        
//  Make query
        $query = self::find()
            ->select([
                "c.id",
                "cl.name",
            ])
            ->from([
                self::tableName() . " AS c",
            ])
            ->join(
                "JOIN",
                GoodsParametersLocal::tableName() . " AS cl",
                "cl.main_id = c.id AND cl.lang = '" . Yii::$app->language . "'" 
            )
            ->where([
                'c.active' => 1,
                'c.deleted' => 0,
            ])
            ->andWhere("(" . implode(" OR ", $conditions) . ")", $binds)
            ->orderBy("cl.name ASC")
            ->asArray();
        
        $result = $query->all();
        
//  Build output
        if (!empty($result)) {
            foreach ($result as $result_row) {
                $list[] = $result_row['id'] . ' | ' . $result_row['name'];
            }
        }
        
        return $list;
    }
}