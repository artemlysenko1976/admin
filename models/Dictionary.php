<?php

namespace app\models;

use Yii;
use yii\db\Query;
use app\models\Languages;
use yii\web\UploadedFile;

/**
 * This is the model class for table "dictionary".
 *
 * @property integer $id
 * @property string $key
 * @property integer $deleted
 */
class Dictionary extends \app\models\AbstractLocal
{
    public $object_name = 'Dictionary Entry {key}';
    
    public static $global_search_fields = [
        'key',
    ];
    public static $global_search_l_fields = [
        'entry',
    ];
    public static $global_search_permissions = [
        'local',
        'dictedit',
    ];
    
    public $export_columns = [
        'key',
        'entry',
    ];
    
    public static $import_formats = [
        'CSV',
        'Excell',
    ];
    
    public $section_name = 'dictionary';
    
    protected static $collect_stat = [
        'dirs' => 0,
        'files' => 0,
        'found' => 0,
        'stored' => 0,
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dictionary';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'entry' => 'Entry',
            'deleted' => 'Deleted',
        ];
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    '`key`',
                    'dl.entry'
                ];
                $where = [
                    'conditions' => "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)",
                    'binds' => [':keyword' => '%' . $filter . '%']
                ];
                
                return $where;
            }
        ];
    }
    
    public function getEntry()
    {
        return $this->local->entry;
    }
    
    public function validateRecord($data)
    {
        $errors = [];
        
        $data['key'] = trim($data['key']);
        if (empty($data['key'])) {
            $errors['key'] = Yii::t('app', "Key is required");
        } else {
            $check_query = self::find()
                ->select(['id'])
                ->where([
                    'deleted' => 0,
                    "key" => $data['key']
                ])
                ->limit(1);
            if (!empty($data['id'])) {
                $check_query->andWhere("id != " . $data['id']);
            }
            $check = $check_query->one();
            if (!empty($check)) {
                $errors['key'] = Yii::t('app', "This Key already in use");
            }
        }
        
//        $default_lang = Languages::getDefaultLang();
//        $entry = trim($data['l'][$default_lang->code]['entry']);
//        if (empty($entry)) {
//            $errors['l_' . $default_lang->code . '_entry'] = "Default language entry is required";
//        }
        
        return $errors;
    }
    
    public function addRecord($data, $rebuild_files=true, $log=true)
    {
        $id = parent::addRecord($data, $log);
        if (!empty($id)) {
            if ($rebuild_files === true) {
                $this->buildDictFiles();
            }
            
            $this->notifyFrontend();
        }
        
        return $id;
    }
    
    public function editRecord($data, $log=true)
    {
        $result = parent::editRecord($data);
        if (!empty($result)) {
            $this->buildDictFiles();
            
            $this->notifyFrontend();
        }
        
        return $result;
    }
    
    public function deleteRecord($id, $soft=true, $log=true)
    {
        $result = parent::deleteRecord($id, $soft, $log);
        if (!empty($result)) {
            $this->buildDictFiles();
        }
        
        return $result;
    }
    
    public function buildDictFiles()
    {
//  Get keys
        $keys = [];
        $records = self::find()
            ->select(['id', 'key'])
            ->where(['deleted' => 0])
            ->asArray()
            ->all();
        foreach ($records as $row) {
            $keys[$row['id']] = $row['key'];
        }
        
        foreach (Yii::$app->params['langs'] as $lang) {
//  Get data by language
            $local_records = DictionaryLocal::find()
                ->select(['main_id', 'entry'])
                ->where(['lang' => $lang->code])
                ->asArray()
                ->all();
            if (empty($local_records)) {
                continue;
            }
            
//  Set file content
            $file_content = '<?php' . "\r\n";
            $file_content .= 'return [' . "\r\n";
            foreach ($local_records as $records_v) {
                if (empty($keys[$records_v['main_id']])) {
                    continue;
                }
                $file_content .= '"' . str_replace('"', '\"', $keys[$records_v['main_id']]) . '" => "' . str_replace('"', '\"', $records_v['entry']) . '",' . "\r\n";
            }
            $file_content .= '];' . "\r\n";
            
//  Open file
            $file = fopen(Yii::$app->basePath . '/messages/' . $lang->code . '/app.php', 'w');
            if (empty($file)) {
                continue;
            }
            
//  Write file content
            fwrite($file, $file_content);
            
//  Close file
            fclose($file);
            
//  Set permissions
            chmod(Yii::$app->basePath . '/messages/' . $lang->code . '/app.php', 0777);
        }
    }
    
    public function import($file_field_name, $lang)
    {
        $inserted = 0;
        $errors = [];
        
//  Get uploaded file
        $uploaded_file = UploadedFile::getInstanceByName($file_field_name);
        $extension = end(explode('.', $uploaded_file->name));
        if (empty($uploaded_file)) {
            return [
                'errors' => ['No uploded file found']
            ];
        }
        
//  Detect format
        switch ($extension) {
            case 'csv':
                $format = 'csv';
                break;
            case 'xls':
            case 'xlsx':
            case 'ods':
                $format = 'excell';
                break;
        }
        
        try {
//  CSV
            if (strtolower($format) == 'csv') {
//  Read file
                $file = fopen($uploaded_file->tempName, 'r');
                if (empty($uploaded_file)) {
                    fclose($file);
                    return [
                        'errors' => ['Unable to open uploded file']
                    ];
                }

//  Check duplication
                while (($row = fgetcsv($file, 10000, ",")) !== false) {
                    $key = $row[0];
                    $entry = $row[1];
                    if (empty($key) || empty($entry)) {
                        continue;
                    }
                    
                    if ($key == 'Key' && $entry == 'Entry') {
                        continue;
                    }
                    
//  Find main record by key
                    $record = Dictionary::findOne([
                        'key' => $key,
                        'deleted' => 0,
                    ]);
                    if (empty($record)) {
                        continue;
                    }
                    
//  Find local record
                    $local_record = DictionaryLocal::findOne([
                        'main_id' => $record->id,
                        'lang' => $lang,
                    ]);
                    if (empty($local_record)) {
                        $local_record = new DictionaryLocal();
                        $local_record->main_id = $record->id;
                        $local_record->lang = $lang;
                    }
                    
//  Update local record
                    $local_record->entry = $entry;
                    $result = $local_record->save(false);
                    
                    if (!empty($result)) {
                        $inserted++;
                    }
                }

//  Close file
                fclose($file);
            }
//  Excell
            elseif (strtolower($format) == 'excell') {
                $list = \moonland\phpexcel\Excel::import($uploaded_file->tempName, [
                    'setFirstRecordAsKeys' => false
                ]);
                if (empty($list)) {
                    return [
                        'errors' => 'Unable to parse uploaded file'
                    ];
                }
                
                foreach ($list as $row) {
                    $key = $row['A'];
                    $entry = $row['B'];
                    if (empty($key) || empty($entry)) {
                        continue;
                    }
                    
                    if ($key == 'Key' && $entry == 'Entry') {
                        continue;
                    }
                    
//  Find main record by key
                    $record = Dictionary::findOne([
                        'key' => $key,
                        'deleted' => 0,
                    ]);
                    if (empty($record)) {
                        continue;
                    }
                    
//  Find local record
                    $local_record = DictionaryLocal::findOne([
                        'main_id' => $record->id,
                        'lang' => $lang,
                    ]);
                    if (empty($local_record)) {
                        $local_record = new DictionaryLocal();
                        $local_record->main_id = $record->id;
                        $local_record->lang = $lang;
                    }
                    
//  Update local record
                    $local_record->entry = $entry;
                    $result = $local_record->save(false);
                    
                    if (!empty($result)) {
                        $inserted++;
                    }
                }
            }
        } catch (\Exception $ex) {
            return [
                'errors' => $ex->getMessage()
            ];
        }
        
        return [
            'inserted' => $inserted,
            'errors' => $errors,
        ];
    }
    
#   Collect entries methods ====================================================
    public static function collectUntranslatedEntries()
    {
        self::resetStat();
        
        $paths = [
            'frontend',
            'models',
        ];
        
        foreach ($paths as $path) {
            self::readDir(Yii::$app->basePath . '/' . $path);
        }
        
        if ($output['stored'] > 0) {
            $obj = new Dictionary();
            $obj->buildDictFiles();
        }
        
        return self::$collect_stat;
    }
    
    protected static function readDir(string $path)
    {
        if (!is_dir($path)) {
            return false;
        }
        
        $dir = opendir($path);
        if (empty($dir)) {
            return false;
        }
        
        self::$collect_stat['dirs']++;
        
        while (($element = readdir($dir)) !== false) {
            if (!in_array($element, ['.', '..'])) {
                if (is_dir($path . '/' . $element)) {
                    self::readDir($path . '/' . $element);
                } elseif (is_file($path . '/' . $element)) {
                    self::parseFile($path . '/' . $element);
                }
            }
            
            unset($element);
        }
        
        closedir($dir);
        unset($dir);
    }
    
    protected static function parseFile(string $path)
    {
//  Get extension
        $extension = end(explode('.', $path));
        if ($extension != 'php') {
            return false;
        }
        
//  Get content
        $content = file_get_contents($path);
        if (empty($content)) {
            return false;
        }
        
        self::$collect_stat['files']++;
        
//  Parse
        $translations = [];
        $pattern = "/Yii::t\('[a-z]{2,5}'\, ('|\")[\d\w\s\{\}\?\-:;\,\.]+('|\")/";
        
        preg_match_all($pattern, $content, $translations);
        unset($content);
        if (empty($translations[0])) {
            return false;
        }
        
        $translations = $translations[0];
        foreach ($translations as $translation_k => $translation_v) {
            $replace_pattern = "/Yii::t\('[a-z]{2,5}'\, ('|\")/";
            $translation_v = preg_replace($replace_pattern, '', $translation_v);
            $translation_v = substr($translation_v, 0, -1);
            $translations[$translation_k] = $translation_v;
        }
        
        self::$collect_stat['found'] += count($translations);
        
//  Store to DB
        self::storeToDb($translations);
        
        return count($translations);
    }
    
    protected static function storeToDb(array $translations)
    {
        if (empty($translations) || !is_array($translations)) {
            return false;
        }
        
        $stored = 0;
        
        foreach ($translations as $translations_v) {
            $insert_data = [
                'key' => $translations_v,
                'l' => []
            ];
            foreach (Yii::$app->params['langs'] as $lang) {
                $insert_data['l'][$lang->code] = [
                    'entry' => ($lang->basic == 1 ? $translations_v : '')
                ];
            }
            
            $obj = new self();
            $errors = $obj::validateRecord($insert_data);
            if (!empty($errors)) {
                continue;
            }
            
            $id = $obj->addRecord($insert_data, false, false);
            if (!empty($id)) {
                $stored++;
                self::$collect_stat['stored']++;
            }
        }
        
        return $stored;
    }
    
    protected static function resetStat()
    {
        self::$collect_stat = [
            'dirs' => 0,
            'files' => 0,
            'found' => 0,
            'stored' => 0,
        ];
    }
#   END Collect entries methods ================================================
}