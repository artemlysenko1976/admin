<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "email_templates_l".
 *
 * @property integer $main_id
 * @property string $lang
 * @property string $subject
 * @property string $content
 */
class EmailTemplatesLocal extends \app\models\AbstractModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_templates_l';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $data = parent::attributeLabels();
        $data['subject'] = 'Subject';
        $data['content'] = 'Content';
        
        return $data;
    }
}
