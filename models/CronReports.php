<?php

namespace app\models;

use Yii;
use app\library\Utils;

/**
 * This is the model class for table "cron_reports".
 *
 * @property integer $id
 * @property string $name
 * @property integer $start_timestamp
 * @property integer $end_timestamp
 * @property string $result
 * @property string $error
 */
class CronReports extends \app\models\AbstractModel
{
    public static $statuses = [
        'success' => 'ОК',
        'in_progress' => 'В процессе',
        'error' => 'Ошибка',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cron_reports';
    }
    
    public function initReport($name) 
    {
        if (!empty($this->id)) {
            return;
        }
        
        $data = [
            'name' => $name,
            'start_timestamp' => time(),
        ];
        parent::__construct($data);
        
        $this->save(false);
    }
    
    public function completeReport($result=null, $error=null) 
    {
        if (empty($this->id) || !empty($this->end_timestamp)) {
            return;
        }
        
        $this->result = (!empty($result) ? json_encode($result) : null);
        $this->error = (!empty($error) ? json_encode($error) : null);
        $this->end_timestamp = time();
        
        return $this->save(false);
    }
    
    public function getFilterCallbacks()
    {
        return [
            'keyword' => function($filter) {
                $fields = [
                    'name',
                    'result',
                    'error',
                ];
                $binds = [
                    ':keyword' => '%' . $filter . '%'
                ];
                $conditions = [
                    "(" . implode(" LIKE :keyword OR ", $fields) . " LIKE :keyword)"
                ];
                
                $where = [
                    'conditions' => "(" . implode(" OR ", $conditions) . ")",
                    'binds' => $binds
                ];
                
                return $where;
            },
            'status' => function($filter) {
                $where = [
                    'conditions' => "",
                ];
                
                switch ($filter) {
                    case 'success':
                        $where['conditions'] = "(end_timestamp IS NOT NULL AND error IS NULL)";
                        break;
                        
                    case 'in_progress':
                        $where['conditions'] = "end_timestamp IS NULL";
                        break;
                        
                    case 'error':
                        $where['conditions'] = "error IS NOT NULL";
                        break;
                }
                
                return $where;
            },
            'timestamp_range_from' => function($filter) {
                $where = [
                    'conditions' => self::tableName() . ".start_timestamp >= :date_from",
                    'binds' => [':date_from' => strtotime($filter . ' 00:00:00')]
                ];
                return $where;
            },
            'timestamp_range_to' => function($filter) {
                $where = [
                    'conditions' => self::tableName() . ".start_timestamp <= :date_to",
                    'binds' => [':date_to' => strtotime($filter . ' 23:59:59')]
                ];
                return $where;
            },
        ];
    }
    
    public function getStatus()
    {
        if (empty($this->id)) {
            return false;
        }
        
        if (!empty($this->end_timestamp) && empty($this->error)) {
            $status = 'success';
        } elseif (!empty($this->error)) {
            $status = 'error';
        } elseif (empty($this->end_timestamp)) {
            $status = 'in_progress';
        } else {
            $status = '';
        }
        
        return $status;
    }
    
    public function getDuration()
    {
        if (empty($this->id)) {
            return false;
        }
        
        if (empty($this->end_timestamp)) {
            return false;
        }
        
        return Utils::getPeriodDuration($this->start_timestamp, $this->end_timestamp);
    }
    
    public static function showJsonData($string)
    {
        if (empty($string)) {
            return '';
        }
        
        $decoded_string = json_decode($string, true);
        if (empty($decoded_string)) {
            return $string;
        }
        
        $output = '';
        
        if (is_string($decoded_string)) {
            $output = $decoded_string;
        } elseif (is_array($decoded_string)) {
            foreach ($decoded_string as $k => $v) {
                if (is_numeric($k)) {
                    $output .= $v . '<br>';
                } else {
                    $output .= $k . ': <b>' . $v . '</b><br>';
                }
            }
        }
        
        return $output;
    }
}