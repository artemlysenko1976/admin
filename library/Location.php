<?php

namespace app\library;

use Yii;

/**
 * Class for communication with core server
 */
class Location 
{
    const MODE_COUNTRY_CODE_2 = 'country_code_2_chars';
    const MODE_COUNTRY_CODE_3 = 'country_code_3_chars';
    const MODE_COUNTRY_NAME = 'country_name';


    /**
     * Detect country by IP using GeoIP
     * @param string $ip
     * @return string|boolean
     */
    public static function detectByIp($ip, $mode=self::MODE_COUNTRY_CODE_2)
    {
        if (empty($ip)) {
            return false;
        }
        
        if ($ip == '127.0.0.1') {
            return '';
        }
        
        switch ($mode) {
            case self::MODE_COUNTRY_CODE_2:
                $result = geoip_country_code_by_name($ip);
                break;
            
            case self::MODE_COUNTRY_CODE_3:
                $result = geoip_country_code3_by_name($ip);
                break;
            
            case self::MODE_COUNTRY_NAME:
                $result = geoip_country_name_by_name($ip);
                break;
            
            default:
                $result = '';
        }
        
        return $result;
    }
}