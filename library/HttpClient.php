<?php

namespace app\library;

use Yii;
use yii\httpclient\Client;

/**
 * Class for HTTP requests performing
 *
 */
class HttpClient extends Client
{
    const METHOD_GET = 'get';
    const METHOD_POST = 'post';
    const METHOD_PUT = 'put';
    const METHOD_PATCH = 'patch';
    const METHOD_DELETE = 'delete';
    const METHOD_HEAD = 'head';
    const METHOD_OPTIONS = 'options';

    /**
     * Sends HTTP request and returns response
     * @param string $url URL to send request
     * @param string $method HTTP method
     * @param array $data
     * @param boolean $send_body_content If TRUE then data sends in request body
     * @param array $headers
     * @param array $options
     * @return yii\httpclient\Response
     */
    public function request(string $url, string $method=self::METHOD_GET, array $data=[], string $format=parent::FORMAT_URLENCODED, array $headers=[], array $options=[], bool $send_in_body=false) 
    {
        $result = null;
        
        if ($send_in_body === false) {
//  Prepare query string
            $formats_to_prepare_data = [
                parent::FORMAT_JSON,
                parent::FORMAT_XML
            ];
            if (!empty($data) && in_array($format, $formats_to_prepare_data)) {
                $data = http_build_query($data);
            }
        }
        
//  Prepare request based on method
        switch ($method) {
            case 'head':
                $request = $this->{$method}(
                    $url, 
                    (is_array($headers) ? $headers : []), 
                    (is_array($options) ? $options : [])
                );
                break;
            case 'options':
                $request = $this->{$method}(
                    $url, 
                    (is_array($options) ? $options : [])
                );
                break;
            default:
                $request = $this->{$method}(
                    $url, 
                    (!empty($data) ? $data : null), 
                    (is_array($headers) ? $headers : []), 
                    (is_array($options) ? $options : [])
                );
        }
        
        if (!empty($request)) {
//  Set request format
            $request->setFormat($format);
            
//  Send request
            try {
                $result = $request->send();
            } catch (\Exception $ex) {
                return false;
            } catch (\TypeError $ex) {
                return false;
            }
        }
        
        return $result;
    }
    
    /**
     * Sends HTTP request and returns parsed JSON response as array|string
     * @param string $url
     * @param string $method
     * @param array $data
     * @param string $format
     * @param array $headers
     * @param array $options
     * @param bool $send_in_body
     * @return array|string
     * @throws \Exception
     */
    public function jsonRequest(string $url, string $method=self::METHOD_GET, array $data=[], string $format=parent::FORMAT_URLENCODED, array $headers=[], array $options=[], bool $send_in_body=false) 
    {
        $http_response = $this->request($url, $method, $data, $format, $headers, $options, $send_in_body);
        
        if (empty($http_response)) {
            throw new \Exception('Unable to get response from ' . $url);
        }
        
        if ($http_response->headers['http-code'] > 300) {
            throw new \Exception('Error during gettinng API response:  ' . $http_response->headers['http-code'] . ' - ' . $http_response->content);
        }
        
        $parsed_response = json_decode($http_response->content, true);
        if (empty($parsed_response)) {
            throw new \Exception('Unable to parse API response: ' . $http_response->content);
        }
        
        return $parsed_response;
    }
}