<?php

namespace app\library;

/**
 * Parent class for exporting data
 */
class Export 
{
    /**
     * Available export formats
     * @var array 
     */
    public static $available_formats = [
        'Excell'
    ];
    
    /**
     * Available records limit
     * @var integer
     */
    public static $records_limit = 10000;
    
    public static function factory($format)
    {
        if (!in_array($format, self::$available_formats)) {
            throw new Exception('Unsupported export format: "' . $format . '"');
        }
        
        $class_name = 'app\\library\\export\\' . $format;
        $obj = new $class_name;
        
        return $obj;
    }
    
    protected function getExportColumns(\yii\db\BaseActiveRecord $model=null)
    {
        return $model->export_columns;
    }
}