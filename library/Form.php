<?php

namespace app\library;

/**
 * Class to provide form methods
 */
class Form 
{
    /**
     * Prepare options array for select, chckbox and radio templates
     * @param array $options
     * @param boolean $empty_option
     * @param string $option_key
     * @return array
     */
    public static function prepareOptions(array $options, $option_key=null, $option_value=null, $empty_option=false)
    {
        $options_array = [];
        
        if ($empty_option === true) {
            $options_array[''] = '-';
        }
        
        if (!empty($options) && is_array($options)) {
            if (reset(array_keys($options)) === 0) {
                if (is_array(reset($options))) {
                    $option_type = 'array';
                } elseif (is_object(reset($options))) {
                    $option_type = 'object';
                } else {
                    $option_type = 'flat';
                }
                
                if (in_array($option_type, ['array', 'object'])) {
                    if (empty($option_key) || empty($option_value)) {
                        throw new \Exception('You must set $option_key and $option_value variables');
                    }
                    
                    $option_value_array = explode('.', $option_value);
                    if (count($option_value_array) > 2) {
                        throw new \Exception('$option_value nested level is over 2');
                    }
                }
                
//  Numeric array
                foreach ($options as $option) {
                    if ($option_type == 'array') {
                        if (count($option_value_array) == 1) {
                            $options_array[$option[$option_key]] = $option[$option_value_array[0]];
                        } elseif (count($option_value_array) == 2) {
                            $options_array[$option[$option_key]] = $option[$option_value_array[0]][$option_value_array[1]];
                        }
                    } elseif ($option_type == 'object') {
                        if (count($option_value_array) == 1) {
                            $options_array[$option->$option_key] = $option->{$option_value_array[0]};
                        } elseif (count($option_value_array) == 2) {
                            $options_array[$option->$option_key] = $option->{$option_value_array[0]}->{$option_value_array[1]};
                        }
                    } elseif ($option_type == 'flat') {
                        $options_array[$option] = $option;
                    }
                }
            } else {
//  Associated array
                foreach ($options as $option_k => $option_v) {
                    $options_array[$option_k] = $option_v;
                }
            }
        }
        
        return $options_array;
    }
    
    /**
     * Create ID for 
     * @param string $name
     * @param mixed $value
     * @return string
     */
    public static function createOptionId(string $name, $value)
    {
        $replacements = [
            ' ' => '_',
            '"' => '',
            '\'' => '',
        ];
        
        $id = $name . '_' . str_replace(array_keys($replacements), array_values($replacements), $value);
        
        return $id;
    }
    
    /**
     * Create string to fill "onclick", "onchange" etc. listeners
     * @param string $event_name
     * @param string|array $listeners
     * @return string
     */
    public static function createEventlistenersString(string $event_name, $listeners=null)
    {
        if ($listeners === null) {
            return '';
        }
        
        if (!is_string($listeners) && !is_array($listeners)) {
            throw new \Exception('$listeners must be a string or an array');
        }
        
        if (is_array($listeners)) {
            $listeners = implode('; ', $listeners);
        }
        $listeners = str_replace('"', "'", $listeners);
        $string = $event_name . '="' . $listeners . ';"';
        
        return $string;
    }
    
    /**
     * Create attribute string
     * @param string $attribute_name
     * @param string $attribute_content
     * @return string
     */
    public static function createAttributeString(string $attribute_name, string $attribute_content=null)
    {
        $string = $attribute_name . '="' . str_replace('"', "'", $attribute_content) . '"';
        
        return $string;
    }
}