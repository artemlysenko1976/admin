<?php

namespace app\library;

use Yii;

/**
 * Global backend search
 *
 */
class GlobalSearch
{
    protected $module;
    
    function __construct($module='backend') 
    {
        $this->module = $module;
    }
    
    /**
     * Making global search
     * @return array
     */
    public function search($keyword) 
    {
//  Get models array
        $models = include Yii::$app->basePath . '/' . $this->module . '/config/search.php';
        if (empty($models)) {
            return false;
        }
        
//  Init result
        $results = [];

        foreach ($models as $model) {
            $class_name = '\app\models\\' . $model;
            $obj = new $class_name;
            
//  Perform text search by models
            $text_results = $obj->makeGlobalLikeSearch($keyword, $this->module);
            if (!empty($text_results)) {
                $results = array_merge($results, $text_results);
            }
            
//  Perform callback search by models
            $callback_results = $obj->makeGlobalCallbackSearch($keyword, $this->module);
            if (!empty($callback_results)) {
                $results = array_merge($results, $callback_results);
            }
        }
        
        return $results;
    }
}