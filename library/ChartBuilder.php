<?php

namespace app\library;

use Yii;
use app\models\Cases;
use app\models\CasesPositions;
use app\models\Positions;
use app\models\Investors;

/**
 * Class to building charts
 *
 */
class ChartBuilder
{
    /**
     * Builds pie chart by positions balance
     * @return array
     */
    public static function positionsBalances()
    {
//  Get positions
        $positions = CasesPositions::find()
            ->select([
                "DISTINCT(cp.name)",
                "cp.ticker",
                "SUM(cp.balance) AS balance",
            ])
            ->from([
                CasesPositions::tableName() . " AS cp",
            ])
            ->join(
                "JOIN",
                Cases::tableName() . " AS c",
                "c.id = cp.case_id"
            )
            ->join(
                "JOIN",
                Investors::tableName() . " AS i",
                "i.id = c.investor_id"
            )
            ->where([
                "cp.currency" => 'USD',
                "i.deleted" => 0,
                "i.status" => "active",
            ])
            ->groupBy([
                "cp.name",
                "cp.ticker",
            ])
            ->asArray()
            ->all();
        
//  Generate colors
        $colors = self::generateColors(count($positions));
        if (!is_array($colors)) {
            $colors = [$colors];
        }
        $bg_colors = [];
        $border_colors = [];
        foreach ($colors as $v) {
            $bg_colors[] = 'rgba(' . $v . ', 0.5)';
            $border_colors[] = 'rgba(' . $v . ', 1)';
        }
        
//  Init output
        $output = [
            'data' => [],
            'bg_colors' => $bg_colors,
            'border_colors' => $border_colors,
            'total_count' => 0
        ];
        
        foreach ($positions as $position) {
            $item_name = (!empty($position['name']) ? $position['name'] . ' - ' : '');
            $item_name .= $position['ticker'];
            $output['data'][$item_name] = round($position['balance'], 2);
            $output['total_count'] += $output['data'][$item_name];
        }
        
        return $output;
    }
    
    /**
     * Builds pie chart by position types balance
     * @return array
     */
    public static function positionsTypesBalances()
    {
//  Get positions
        $positions = CasesPositions::find()
            ->select([
                "p.instr_type AS name",
                "SUM(cp.balance) AS balance",
            ])
            ->from([
                CasesPositions::tableName() . " AS cp",
            ])
            ->join(
                "JOIN",
                Positions::tableName() . " AS p",
                "p.ticker = cp.ticker"
            )
            ->join(
                "JOIN",
                Cases::tableName() . " AS c",
                "c.id = cp.case_id"
            )
            ->join(
                "JOIN",
                Investors::tableName() . " AS i",
                "i.id = c.investor_id"
            )
            ->where([
                "cp.currency" => 'USD',
                "i.deleted" => 0,
                "i.status" => "active",
            ])
            ->groupBy([
                "p.instr_type",
            ])
            ->asArray()
            ->all();
        
//  Generate colors
        $colors = self::generateColors(count($positions));
        if (!is_array($colors)) {
            $colors = [$colors];
        }
        $bg_colors = [];
        $border_colors = [];
        foreach ($colors as $v) {
            $bg_colors[] = 'rgba(' . $v . ', 0.5)';
            $border_colors[] = 'rgba(' . $v . ', 1)';
        }
        
//  Init output
        $output = [
            'data' => [],
            'bg_colors' => $bg_colors,
            'border_colors' => $border_colors,
            'total_count' => 0
        ];
        
        foreach ($positions as $position) {
            $item_name = $position['name'];
            $output['data'][$item_name] = round($position['balance'], 2);
            $output['total_count'] += $output['data'][$item_name];
        }
        
        return $output;
    }
    
#   Additional methods =========================================================
    /**
     * Prepare dates array for charts with date range
     * @param date $start_date (2018-12-23)
     * @param date $end_date (2018-12-24)
     * @return array key: date, 0: start timestamp, 1: end timestamp
     */
    protected static function prepareDatesArray($start_date, $end_date)
    {
        $dates = [];
        
        $start_date_array = explode('-', $start_date);
        $start_year = $start_date_array[0];
        $start_month = $start_date_array[1];
        $start_day = $start_date_array[2];
        
        $end_date_array = explode('-', $end_date);
        $end_year = $end_date_array[0];
        $end_month = $end_date_array[1];
        $end_day = $end_date_array[2];
        
        $year = $start_year;
        $month = $start_month;
        $day = $start_day;
        
        while (!($year == $end_year && $month == $end_month && $day == $end_day)) {
            $date = $year
                . '-' . str_pad($month, 2, '0', STR_PAD_LEFT)
                . '-' . str_pad($day, 2, '0', STR_PAD_LEFT);
            $timestamp = mktime(0, 0, 0, $month, $day, $year);
            
            $dates[$date] = [
                $timestamp,
                $timestamp + 3600 * 24 - 1,
            ];
            
            $timestamp += 3600 * 24;
            list($year, $month, $day) = explode('-', date('Y-m-d', $timestamp));
        }
        
        $end_date = $end_year
            . '-' . str_pad($end_month, 2, '0', STR_PAD_LEFT)
            . '-' . str_pad($end_day, 2, '0', STR_PAD_LEFT);
        $end_timestamp = mktime(0, 0, 0, $end_month, $end_day, $end_year);
        
        $dates[$end_date] = [
            $end_timestamp,
            $end_timestamp + 3600 * 24 - 1,
        ];
        
        return $dates;
    }
    
    /**
     * Generate random colors
     * @param integer $quantity default: 1
     * @return mixed array|string of colors in RGB format
     */
    protected static function generateColors($quantity=1) 
    {
        $colors = [];
        
        for ($i = 0; $i < $quantity; $i++) {
            $colors[] = rand(0, 255) . ',' . rand(0, 255) . ',' . rand(0, 255);
        }
        
        if ($quantity === 1) {
            return $colors[0];
        }
        
        return $colors;
    }
}