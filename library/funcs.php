<?php

/**
 * Calls var_dump() function with formatted output and stops the script
 * @param mixed $var any type variable
 */
function dump($var)
{
    echo '<pre>';
    foreach (func_get_args() as $arg) {
        var_dump($arg);
    }
    die();
}