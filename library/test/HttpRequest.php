<?php

namespace app\library\test;

use Yii;
use app\library\HttpClient;
use yii\httpclient\Client;

/**
 * Class for testing HTTP requests
 */
class HttpRequest extends HttpClient
{
    /**
     * Makes HTTP request and displays it result
     * @param string $url
     * @param array $data [key => value]
     * @param string $method
     * @param string $format
     * @param boolean $send_in_body
     * @param array $headers
     * @throws \Exception
     */
    public static function test($url, array $data=null, $method=self::METHOD_GET, $format=self::FORMAT_URLENCODED, $send_in_body=false, array $headers=[])
    {
//  Check parameters
        if (self::checkUrl($url) === false) {
            self::displayError('Wrong URL format: "' . $url . '"');
            return [
                'error' => 'Wrong URL format: "' . $url . '"'
            ];
        }
        
        if (self::checkMethod($method) === false) {
            self::displayError('Wrong method given: "' . $method . '"');
            return [
                'error' => 'Wrong method given: "' . $method . '"'
            ];
        }
        $method = strtolower($method);
        
        if (self::checkFormat($format) === false) {
            self::displayError('Wrong format given: "' . $format . '"');
            return [
                'error' => 'Wrong format given: "' . $format . '"'
            ];
        }
        $format = strtolower($format);
        
        if (!empty($headers)) {
            if (self::checkAssocArray($headers) === false) {
                self::displayError('Keys of headers array can not be numeric');
                return [
                    'error' => 'Keys of headers array can not be numeric'
                ];
            }
        }
        
//  Make request
        try {
            $start_time = microtime(true);
            
            $http_client = new HttpClient();
            $http_response = $http_client->request(
                $url, 
                $method, 
                $data, 
                $format,
                $headers,
                [],
                $send_in_body
            );
            
            $end_time = microtime(true);
            $request_time = $end_time - $start_time;
        } catch (\Exception $ex) {
            self::displayError('HTTP request error: ' . $ex->getMessage());
            return [
                'error' => 'HTTP request error: ' . $ex->getMessage()
            ];
        }
        
        if (empty($http_response)) {
            self::displayError('Unable to get response');
            return [
                'error' => 'Unable to get response'
            ];
        }
        
        $response_headers = [];
        foreach ($http_response->headers->toArray() as $k => $v) {
            $response_headers[$k] = $v[0];
        }
        
//  Output result
        self::displayOutput($http_response, $url, $request_time);
        
        $output = [
            'http_code' => $http_response->headers['http-code'],
            'raw_response' => $http_response->content,
            'parsed_response' => json_decode($http_response->content, true),
            'headers' => $response_headers,
            'url' => $url,
            'request_time' => $request_time,
            'request_params' => func_get_args(),
        ];
        if ($send_in_body == true) {
            switch ($format) {
                case self::FORMAT_JSON:
                    $output['request_body'] = json_encode($data);
                    break;
            }
        }
        
        return $output;
    }
    
#   Validation methods =========================================================
    /**
     * Validates correct URL format
     * @param string $url
     */
    protected static function checkUrl($url)
    {
        $pattern = '/^http(s)?:\/\/(.*)$/';
//        $pattern = '/^http(s)?:\/\/(([a-z0-9-_]+)\.([a-z]+)|([0-9\.\/:]+))([\/a-z0-9_\-=\?&#]+)?$/';
        
        if (preg_match($pattern, $url) === 0) {
            return false;
        }
        
        return true;
    }
    
    protected static function checkAssocArray(array $array)
    {
        foreach ($array as $k => $v) {
            if (is_numeric($k)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Validates request method
     * @param string $method
     */
    protected static function checkMethod($method)
    {
        $reflection = new \ReflectionClass(new HttpClient());
        
        $available_methods = [];
        foreach ($reflection->getConstants() as $name => $const) {
            if (strpos($name, 'METHOD_') === 0) {
                $available_methods[] = $const;
            }
        }
        
        if (!in_array(strtolower($method), $available_methods)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Validates format
     * @param string $format
     */
    protected static function checkFormat($format)
    {
        $reflection = new \ReflectionClass(new Client());
                
        $available_formats = [];
        foreach ($reflection->getConstants() as $name => $const) {
            if (strpos($name, 'FORMAT_') === 0) {
                $available_formats[] = $const;
            }
        }
        
        if (!in_array(strtolower($format), $available_formats)) {
            return false;
        }
        
        return true;
    }
    
#   Output methods =============================================================
    /**
     * Displays error message
     * @param string $error_message
     */
    protected static function displayError($error_message)
    {
        return;
        
        echo '<div style="color:red;">';
        echo '<h1>Error</h1>';
        echo '<div>' . $error_message . '</div>';
        echo '</div>';
        
        exit();
    }
    
    /**
     * Displays request results
     * @param \app\library\test\yii\httpclient\Response $response
     * @param string $url
     * @param integer $request_time
     */
    protected static function displayOutput(yii\httpclient\Response $response, $url, $request_time)
    {
        return;
        
        echo '<div>';
        echo '<h1>';
        echo '<span style="color:' . ($response->headers['http-code'] == 200 ? 'green' : 'red') . '">' . $response->headers['http-code'] . '</span> - ';
        echo '<small>' . $url . '</small></span>';
        echo '</h1>';
        echo '<div>' . round($request_time, 5) . ' ms</div>';
        echo '<hr>';
        
        $parsed_response = json_decode($response->content, true);
        if ($parsed_response !== null) {
            echo '<h2>Parsed response</h2>';
            if (is_array($parsed_response)) {
                self::displayArray($parsed_response);
            } else {
                echo '<div>' . $parsed_response . '</div>';
            }
            echo '<hr>';
        }
        
        echo '<h2>Raw response</h2>';
        echo '<div>' . $response->content . '</div>';
        echo '<hr>';
        
        $headers = [];
        foreach ($response->headers->toArray() as $k => $v) {
            $headers[$k] = $v[0];
        }
        
        echo '<h2>Headers</h2>';
        self::displayArray($headers);
        echo '<hr>';
        
        echo '</div>';
        
        exit();
    }
    
    protected static function displayArray($data, $level=0)
    {
        if (!is_array($data)) {
            return false;
        }
        
        $padding = '';
        for ($i = 0; $i < $level; $i++) {
            $padding .= '&nbsp;&nbsp;&nbsp;&nbsp;';
        }
            
        echo '<table>';
        foreach ($data as $k => $v) {
            echo '<tr>';
            echo '<td style="vertical-align:top;">';
            echo $padding;
            echo '<b>' . $k . '</b>: ';
            echo '</td>';
            echo '<td>';
            if (!is_array($v)) {
                echo $v;
            } else {
                echo '<br>';
                self::displayArray($v, $level+1);
            }
            echo '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
}