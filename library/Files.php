<?php

namespace app\library;

/**
 * Class to manage web accessed files
 */
class Files 
{
    public static $allowed_types = [
        'pdf',
        'doc',
        'docx',
        'xls',
        'xlsx',
        'txt',
        'zip',
        'rar',
        'csv',
    ];
    
    public static function getFiles()
    {
        $files = [];
        
        $dir = opendir(self::getDir());
        
        if (empty($dir)) {
            return false;
        }
        
        while (false !== ($file_name = readdir($dir))) {
            if (in_array($file_name, ['.', '..',])) {
                continue;
            }
            
            $file_record = [
                'name' => $file_name,
                'link' => \yii\helpers\Url::to('/files/' . $file_name, 'https'),
                'type' => end(explode('.', $file_name)),
                'size' => round(filesize(self::getDir() . $file_name) / 1000, 1),
                'mod_timestamp' => filemtime(self::getDir() . $file_name),
            ];
            
            $files[] = $file_record;
        }
        
        return $files;
    }
    
    public static function getFile($file_name)
    {
        $file_path = self::getDir() . $file_name;
        if (!file_exists($file_path)) {
            return false;
        }
        
        $file_record = [
            'name' => $file_name,
            'link' => \yii\helpers\Url::to('/files/' . $file_name, 'https'),
            'type' => end(explode('.', $file_name)),
            'size' => round(filesize($file_path) / 1000, 1),
            'mod_timestamp' => filemtime($file_path),
        ];
        
        return $file_record;
    }
    
    public static function validateFile($data, $ajax_validation=true)
    {
        $errors = [];
        
//  Name
        if (!empty($data['name'])) {
            $pattern = '/^([A-Za-z0-9_\- ]{2,50})$/';
            if (!preg_match($pattern, $data['name'])) {
                $errors['name'] = 'File name is wrong';
            }
        }
        
//  Extension
        $file_extension = '';
        
        if (!empty($data['file'])) {
            $file_extension = strtolower(end(explode('.', $data['file'])));
            
            if (!in_array($file_extension, self::$allowed_types)) {
                $errors['file'] = "File has disallowed extension '" . $file_extension . '"';
            }
            
            $new_file_name = (!empty($data['name']) ? $data['name'] . '.' . $file_extension : $data['file']);
        }
        
        if (empty($data['old_name'])) {
//  Upload new file ------------------------------------------------------------
            if (empty($data['file'])) {
                $errors['file'] = 'File is required';
            } else {
                if (file_exists(self::getDir() . $new_file_name)) {
                    $errors['file'] = "File with name '" . $new_file_name . '" already exists';
                }
            } 
        } else {
//  Edit existed file ----------------------------------------------------------
            $old_data = Files::getFile($data['old_name']);
            if (empty($old_data)) {
                $errors['file'] = 'Unable to find old file';
            }
            
            $new_file_name = $data['name'] . '.' . $old_data['type'];
            
            if ($new_file_name != $data['old_name']) {
                $files = Files::getFiles();
                foreach ($files as $file) {
                    if ($file['name'] == $new_file_name) {
                        $errors['name'] = "File with name '" . $new_file_name . '" already exists';
                    }
                }
            }
        }
        
        return $errors;
    }
    
    public static function uploadFile($data)
    {
        $uploaded_file = \yii\web\UploadedFile::getInstanceByName('file');
        
        if (empty($uploaded_file)) {
            return false;
        }
        
        $extension = strtolower(end(explode('.', $uploaded_file->name)));
        $file_name = (!empty($data['name']) ? $data['name'] . '.' . $extension : $uploaded_file->name);
        $file_path = self::getDir() . $file_name;
        
        $upload_result = $uploaded_file->saveAs($file_path);
        if ($upload_result == true) {
            chmod($file_path, 0777);
        }
        
        return $upload_result;
    }
    
    public static function editFile($data)
    {
        $old_data = Files::getFile($data['old_name']);
        
        if (empty($old_data)) {
            return false;
        }
        
        if (!empty($data['name'])) {
            $new_file_name = $data['name'] . '.' . $old_data['type'];
        } else {
            $new_file_name = $data['old_name'];
        }
        
        $uploaded_file = \yii\web\UploadedFile::getInstanceByName('file');
        
        if ($new_file_name == $data['old_name'] && empty($uploaded_file)) {
            return false;
        }
        
        if (empty($uploaded_file)) {
//  Rename file
            $result = rename(self::getDir() . $data['old_name'], self::getDir() . $new_file_name);
        } else {
//  Reload (and rename) file
            unlink(self::getDir() . $data['old_name']);
            
            $extension = strtolower(end(explode('.', $uploaded_file->name)));
            $file_name = (!empty($data['name']) ? $data['name'] . '.' . $extension : $uploaded_file->name);
            $file_path = self::getDir() . $file_name;
            
            $result = $uploaded_file->saveAs($file_path);
            if ($result == true) {
                chmod($file_path, 0777);
            }
        }
        
        return $result;
    }
    
    public static function deleteFile($name)
    {
        $data = Files::getFile($name);
        
        if (empty($data)) {
            return false;
        }
        
        $result = unlink(self::getDir() . $data['name']);
        
        return $result;
    }
    
    public static function getDir()
    {
        return \Yii::$app->basePath . '/web/files/';
    }
    
    public static function getFileNameWithoutExtension($name)
    {
        $name_array = explode('.', $name);
        unset($name_array[count($name_array) - 1]);
        
        return implode('.', $name_array);
    }
}