<?php

namespace app\library\export;

use yii;
use app\library\Export;
use app\library\export\ExportInterface;
use moonland\phpexcel\Excel as ExcelLib;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use app\library\Utils;

/**
 * Export to Excell format
 */
class Excell extends Export implements ExportInterface
{
    public function build($list, $file_name, $export_columns=null, $export_columns_callbacks=null)
    {
        if (reset($list) instanceof \yii\db\BaseActiveRecord) {
            ExcelLib::export([
                'models' => $list,
                'mode' => 'export',
                'fileName' => $file_name,
                'columns' => $this->getExportColumns($list[0]),
            ]);
        } elseif (is_array(reset($list))) {
            if (empty($export_columns)) {
                throw new \Exception('Export columns are empty');
            }
            
//  Init spreadsheet
            $spreadsheet = new Spreadsheet();
            
            $spreadsheet->getProperties()->setCreator(Yii::$app->params['projectName'] .  ' Autocreation')
                ->setTitle($file_name)
                ->setCategory(Yii::$app->controller->id)
            ;
            
            $sheet = $spreadsheet->setActiveSheetIndex(0);
            
            $letters = Utils::getAlphabet(true);
            
//  Set headers
            $letters_index = 0;
            $letters_index_2 = false;
            
            foreach ($export_columns as $label) {
                $letter = $letters[$letters_index];
                if ($letters_index_2 !== false) {
                    $letter = $letters[$letters_index_2] . $letter;
                }
                
                $sheet->setCellValue($letter . '1', $label);
                
                $letters_index++;
                
                if (empty($letters[$letters_index])) {
                    $letters_index = 0;
                    if ($letters_index_2 === false) {
                        $letters_index_2 = 0;
                    } else {
                        $letters_index_2++;
                    }
                }
            }
            
//  Set rows
            $rows_index = 2;
            
            foreach ($list as $row) {
                $letters_index = 0;
                $letters_index_2 = false;
                
                foreach ($export_columns as $export_columns_name => $export_columns_label) {
                    if (!empty($export_columns_callbacks) && !empty($export_columns_callbacks[$export_columns_name])) {
                        $value = $export_columns_callbacks[$export_columns_name]($row[$export_columns_name], $row);
                    } else {
                        $value = $row[$export_columns_name];
                    }
                    
                    $letter = $letters[$letters_index];
                    if ($letters_index_2 !== false) {
                        $letter = $letters[$letters_index_2] . $letter;
                    }
                    
                    $sheet->setCellValue($letter . $rows_index, $value);
                    
                    $letters_index++;
                    
                    if (empty($letters[$letters_index])) {
                        $letters_index = 0;
                        if ($letters_index_2 === false) {
                            $letters_index_2 = 0;
                        } else {
                            $letters_index_2++;
                        }
                    }
                }
                
                $rows_index++;
            }
            
//  Output
            self::setHeaders($file_name .'.xlsx');
            
            $oWriter = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $oWriter->save('php://output');
        }
    }
    
    protected static function setHeaders($file_name)
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $file_name .'"');
        header('Cache-Control: max-age=0');
    }
}