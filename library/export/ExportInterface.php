<?php

namespace app\library\export;

/**
 *
 * @author artem
 */
interface ExportInterface 
{
    public function build($list, $file_name);
}
