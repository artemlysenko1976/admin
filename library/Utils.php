<?php

namespace app\library;

use Yii;

/**
 * Utils
 */
class Utils 
{
    /**
     * Returns period duration as weeks, days, hours, minutes, seconds
     * @param integer $start_timestamp
     * @param integer $end_timestamp
     * @param boolean $as_string if true - returns string like "X w, X d, X h, X m, X s" else returns array with keys: w, d, h, m, s
     * @return mixed string|array
     */
    public static function getPeriodDuration($start_timestamp, $end_timestamp, $as_string=true)
    {
        $output_array = [
            'w' => 0,
            'd' => 0,
            'h' => 0,
            'm' => 0,
            's' => 0,
        ];
        
        if (empty($start_timestamp) || empty($end_timestamp)) {
            return ($as_string === true ? '-' : $output_array);
        }
        
        $period = $end_timestamp - $start_timestamp;
        
        if ($period <= 0) {
            return ($as_string === true ? '-' : $output_array);
        }
        
        foreach (array_keys($output_array) as $k) {
            switch ($k) {
                case 'w':
                    $seconds = 60 * 60 * 24 * 7;
                    break;
                
                case 'd':
                    $seconds = 60 * 60 * 24;
                    break;
                
                case 'h':
                    $seconds = 60 * 60;
                    break;
                
                case 'm':
                    $seconds = 60;
                    break;
                
                case 's':
                    $seconds = 1;
                    break;
            }
            
            $result = intval($period / $seconds);
            $rest = $period % $seconds;
            
            $output_array[$k] = $result;
            
            if ($rest > 0) {
                $period = $rest;
            }
        }
        
        if ($as_string === false) {
            return $output_array;
        }
        
        $output_string = '';
        
        foreach ($output_array as $name => $value) {
            if ($value == 0) {
                continue;
            }
            
            $output_string .= $value . ' ' . $name . ', ';
        }
        
        $output_string = substr($output_string, 0, -2);
        
        return $output_string;
    }
    
    /**
     * Preserves text edited by WYSIWYG editor from damage PHP tags
     * @param string $text
     * @return string processed text
     */
    public static function preservePhpTags($text)
    {
        if (empty($text)) {
            return $text;
        }
        
        $replacements = [
            '<!--?=' => '<?=',
            '?-->' => '?>',
            '<!--?php' => '<?php',
            '{ ?-->' => '{ ?>',
            'Yii::$app--->' => 'Yii::$app->',
            '$this--->' => '$this->',
            '=&gt;' => '=>',
            '?&gt;' => '?>',
        ];
        
        $text = str_replace(array_keys($replacements), array_values($replacements), $text);
        
        return $text;
    }
    
    public static function getIdFromAutocomplete($string)
    {
        if (empty($string)) {
            return null;
        }
        
        $id = reset(explode(' ', $string));
        $id = str_replace('#', '', $id);
        
        if (!is_numeric($id)) {
            return null;
        }
        
        return $id;
    }
    
    public static function makeDateTimeString(array $date_time)
    {
        if (empty($date_time['date']) || empty($date_time['hour']) || empty($date_time['minute'])) {
            return '';
        }
        
        $date_time_string = $date_time['date']
            . ' ' .$date_time['hour']
            . ':' .$date_time['minute'];
        
        return $date_time_string;
    }
    
    public static function generateRandomString($length=8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters_length = strlen($characters);
        
        $random_string = '';
        for ($i = 0; $i < $length; $i++) {
            $random_string .= $characters[rand(0, $characters_length - 1)];
        }
        
        return $random_string;
    }
    
    public static function getRatesFromCache()
    {
        $rates_cache_path = Yii::$app->basePath . '/cache/rates.json';
        if (!file_exists($rates_cache_path)) {
            return [];
        }
        
        $rates_cache = file_get_contents($rates_cache_path);
        if (empty($rates_cache)) {
            return [];
        }

        $rates = json_decode($rates_cache, true);
        if (empty($rates) || !is_array($rates)) {
            return [];
        }
        
        return $rates;
    }
    
    public static function getAlphabet($uppercase=false)
    {
        $start_position = (empty($uppercase) ? 'a' : 'A');
        $end_position = (empty($uppercase) ? 'z' : 'Z');
        
        $array = $alphas = range($start_position, $end_position);

        return $array;
    }
    
    public static function displayArray(array $data, int $level=0)
    {
        $tab = '&nbsp;&nbsp;&nbsp;&nbsp;';
        
        foreach ($data as $k => $v) {
            for ($i = 0; $i < $level; $i++) {
                echo $tab;
            }
            
            echo $k . ': ';
            if (!is_array($v)) {
                echo $v;
                echo '<br>';
            } else {
                echo '<br>';
                self::displayArray($v, $level + 1);
            }
        }
    }
    
    public static function createPortalApiToken($method, $url, $params=null)
    {
        $token = '';
        $token .= strtolower($method);
        $token .= $url;
        
        if (!empty($params)) {
            foreach ($params as $k => $v) {
                $token .= $k;
            }
        }
        
        $token .= Yii::$app->params['portal_api_secret_key'];
        
        return strtoupper(md5($token));
    }
    
    public static function formatRate($rate)
    {
        if (empty($rate)) {
            return 0;
        }
        
        $output = number_format($rate, 2, '.', ' ');
        
//        $cut_zeros = true;
//        
//        if (!is_string($rate)) {
//            $output = strval($rate);
//        } else {
//            $output = $rate;
//        }
//        
//        if ($output == '0') {
//            return 0;
//        }
//        
//        $float_parts = explode('.', $output);
//        if (isset($float_parts[1])) {
//            $cut_zeros = true;
//        }
//        
//        if ($cut_zeros === true) {
//            $float_length = strlen($float_parts[1]);
//
//            $float_parts[1] = str_split($float_parts[1]);
//
//            for ($i = 0; $i < $float_length; $i++) {
//                if ($float_parts[1][count($float_parts[1]) - 1] == '0') {
//                    unset($float_parts[1][count($float_parts[1]) - 1]);
//                }
//            }
//
//            $float_parts[1] = implode('', $float_parts[1]);
//            $output = implode('.', $float_parts);
//        } else {
//            $float_length = 0;
//        }
//        
//        if (strpos($output, 'E') != false) {
//            $output = number_format($output, $float_length, '.',  '');
//        }
//        
//        if (strpos($output, '.') == strlen($output) - 1) {
//            $output = substr($output, 0, strlen($output) - 1);
//        } 
        
        return $output;
    }
    
    public static function decryptBrockerApiCredentials(string $encrypted_string)
    {
        $key = Yii::$app->params['brocker_encryption']['key'];
        $init_vector = Yii::$app->params['brocker_encryption']['init_vector'];
        
//  OpenSSL method
        $output = openssl_decrypt(
            $encrypted_string,
            Yii::$app->params['brocker_encryption']['algorythm'],
            $key,
            OPENSSL_ZERO_PADDING,   //  OPENSSL_RAW_DATA, OPENSSL_ZERO_PADDING
            $init_vector
        );
        
//  Mcrypt method
//        $output= mcrypt_decrypt(
//            MCRYPT_RIJNDAEL_128, 
//            $key, 
//            base64_decode($encrypted_string), 
//            MCRYPT_MODE_CBC, 
//            $init_vector
//        );
        
//  Cleanup unreadable chars
        $output = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $output);
        
        return $output;
    }
    
    public static function getImageTypeFromBinaryData($binary_data)
    {
        $types = [
            'jpeg' => "\xFF\xD8\xFF", 
            'gif' => 'GIF', 
            'png' => "\x89\x50\x4e\x47\x0d\x0a", 
            'bmp' => 'BM', 
            'psd' => '8BPS', 
            'swf' => 'FWS',
            'pdf' => '%PDF',
        ];
        $found = 'other';
        
        foreach ($types as $type => $header) {
            if (strpos($binary_data, $header) === 0) {
                $found = $type;
                break;
            }
        }
        
        return $found;
    }
}