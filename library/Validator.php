<?php

namespace app\library;

class Validator 
{
    public static function validatePhoneNumber($phone_number)
    {
        $result = preg_match('/^\+([\d]{10,20})$/', $phone_number);
        
        return $result;
    }
    
    public static function validateCalendarDate($date)
    {
        $result = preg_match('/^([\d]{4}\-[\d]{2}\-[\d]{2})$/', $date);
        
        return $result;
    }
}