<?php

namespace app\library;

use Yii;
use yii\swiftmailer\Mailer as BaseMailer;
use app\models\EmailTemplates;
use app\models\EmailHistory;

/**
 * Description of Mailer
 *
 * @author artem
 */
class Mailer extends BaseMailer
{
    /**
     * Composes and sends message using yii\swiftmailer\Mailer functionality
     * @param string $subject message subject
     * @param string $content message content
     * @param string|array $to recipient(s) address(es). If array, message will be sent to multiple recipients
     * @param string|array $from sender address, if array then first element - address, second - name
     * @param string|arra $copy_to copy recipient(s) address(es). If array, message will be sent to multiple recipients
     * @param array $attachments attached files
     * @param string $type html | text
     * @param boolean $log if TRUE sent messages will be logged
     * @return boolean
     */
    public function message($subject, $content, $to, $from, $copy_to=null, array $attachments=[], $type='html', $log=true)
    {
        if (empty($subject)
            || empty($content)
            || empty($from)
            || empty($to)
        ) {
            return false;
        }
        
//  Transform To to array
        if (!is_array($to)) {
            $to = [$to];
        }
        $to = array_unique($to);
        
//  Compose
        if ($type == 'html') {
            $message = $this->compose('base', [
                'message' => $content
            ]);
        } else {
            $message = $this->compose();
            $message->setTextBody($content);
        }
        
//  Detect SNTP host
        if (method_exists($this->getTransport(), 'getHost')) {
            $smtp_host = explode('.', $this->getTransport()->getHost());
            unset($smtp_host[0]);
            $smtp_host = implode('.', $smtp_host);
        } else {
            $smtp_host = Yii::$app->params['host'];
        }
        
//  Set message ID according SMTP host
        $id_hash = md5(
            $subject
            . $to[0]
            . time()
        );
        if (!empty($smtp_host)) {
            $message->setHeader('Message-ID', $id_hash . '@' . $smtp_host);
        }
        
//  Subject
        $message->setSubject($subject);
        
//  From
        if (!empty($from)) {
            if (!is_array($from)) {
                $message->setFrom($from);
            } else {
                $message->setFrom([$from[0] => $from[1]]);
            }
        }
        
//  To
        $message->setTo($to);
        
//  Copy
        if (!empty($copy_to)) {
            $message->setBcc($copy_to);
        }
        
//  Attachments
        if (!empty($attachments)) {
            foreach ($attachments as $file) {
                $message->attach($file);
            }
        }
        
//  Send
        try {
            $result = $message->send();
            $error = null;
        } catch (\Exception $ex) {
            $error = $ex->getMessage();
        }
        
//  Log
        $recipients = $to;
        if (!empty($copy_to)) {
            if (is_array($copy_to)) {
                $recipients = array_merge($recipients, $copy_to);
            } else {
                $recipients[] = $copy_to;
            }
        }
        if ($log === true) {
            $user_id = 0;
            $user_name = '';
            if (!empty(Yii::$app->session) && !empty(Yii::$app->session->get('auth'))) {
                $user_id = Yii::$app->session->get('auth')['id'];
                $user_name = Yii::$app->session->get('auth')['login'];
            } else {
                $user_id = 0;
                $user_name = 'System';
            }
            
            $log_obj = new EmailHistory();
            $log_obj->log($subject, $content, $recipients, $result, $error, $user_id, $user_name);
        }
        
        return $result;
    }
    
    /**
     * Sends message using e-mail template
     * @param string $template_key unique template identifier
     * @param string|array $to recipient(s) address(es). If array, message will be sent to multiple recipients
     * @param string|array $from sender address, if array then first element - address, second - name
     * @param string|arra $copy_to copy recipient(s) address(es). If array, message will be sent to multiple recipients
     * @param array $attachments attached files
     * @param boolean $log if TRUE sent messages will be logged
     * @return boolean
     */
    public function messageFromTemplate($template_key, $params, $to=null, $from=null, $copy_to=null, array $attachments=[], $log=true)
    {
        if (empty($template_key)) {
            return false;
        }
        
//  Get template data
        $tpl_data = EmailTemplates::getTemplateData($template_key);
        if (empty($tpl_data)) {
            return false;
        }
        
//  Prepare subject 
        $params_keys = [];
        $params_values = [];
        if (!empty($params)) {
            foreach ($params as $k => $v) {
                $params_keys[] = '<?= $' . $k .' ?>';
                $params_values[] = $v;
            }
        }
        $subject = str_replace($params_keys, $params_values, $tpl_data->local->subject);
        
//  Prepare content
        $content = $this->getView()->renderFile(
            Yii::$app->basePath . '/mail/generated/' . Yii::$app->params['lang']->code . '/' . $tpl_data->key . '.php', 
            $params
        );
        
//  Set From
        if (!empty($from)) {
            $email_from = $from;
        } elseif (!empty($tpl_data->email_from) && !empty($tpl_data->name_from)) {
            $email_from = [$tpl_data->email_from, $tpl_data->name_from];
        } elseif (!empty($tpl_data->email_from) && empty($tpl_data->name_from)) {
            $email_from = $tpl_data->email_from;
        }
        
//  Set To
        if (!empty($to)) {
            $email_to = $to;
        } elseif (!empty($tpl_data->email_to)) {
            $email_to = explode("\r\n", $tpl_data->email_to);
        }
        
//  Set Copy
        if (!empty($copy_to)) {
            $email_copy = $copy_to;
        } elseif (!empty ($tpl_data->email_copy)) {
            $email_copy = explode("\r\n", $tpl_data->email_copy);
        } else {
            $email_copy = null;
        }
        
//  Send
        $result = $this->message(
            $subject, 
            $content, 
            $email_to, 
            $email_from, 
            $email_copy, 
            $attachments,
            $log
        );
        
        return $result;
    }
}